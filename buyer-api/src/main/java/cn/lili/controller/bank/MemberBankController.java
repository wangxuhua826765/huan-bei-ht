package cn.lili.controller.bank;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.bank.entity.dos.MemberBank;
import cn.lili.modules.bank.service.MemberBankService;
import cn.lili.modules.sms.SmsUtil;
import cn.lili.modules.verification.entity.enums.VerificationEnums;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@Api(tags = "银行卡")
@RequestMapping("/buyer/member/bank")
public class MemberBankController {

	@Autowired
	private MemberBankService memberBankService;
	@Autowired
	private SmsUtil smsUtil;

	@ApiOperation(value = "插入银行卡")
	@RequestMapping("/insertBank")
	public ResultMessage<Boolean> insert(@RequestBody MemberBank memberBank) {
		memberBank.setMemberId(UserContext.getCurrentUser().getId());
		log.info("插入银行卡，入参：{}", JSON.toJSONString(memberBank));
		if (smsUtil.verifyCode(memberBank.getMobile(), VerificationEnums.BANK_VERIFY, memberBank.getMemberId(),
				memberBank.getMsgCode())) {
			return ResultUtil.data(memberBankService.insert(memberBank));
		} else {
			throw new ServiceException(ResultCode.VERIFICATION_SMS_CHECKED_ERROR);
		}
	}

	@ApiOperation(value = "识别银行卡")
	@RequestMapping("/identify")
	public ResultMessage<MemberBank> identify(@RequestParam("url") String url) throws Exception {
		log.info("识别银行卡，入参：{}", url);
		return ResultUtil.data(memberBankService.identify(url));
	}

	@ApiOperation(value = "获取银行卡")
	@RequestMapping("/getBank")
	public ResultMessage<List<MemberBank>> getBank() {
		return ResultUtil.data(memberBankService.getBank(UserContext.getCurrentUser().getId()));
	}

	@ApiOperation(value = "删除银行卡")
	@RequestMapping("/delBank")
	public ResultMessage<Boolean> delBank(@RequestParam("bankId") String bankId) {
		log.info("删除银行卡，入参bankId：{}", bankId);
		return ResultUtil.data(memberBankService.delBank(bankId));
	}

}
