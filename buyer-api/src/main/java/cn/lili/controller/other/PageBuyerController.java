package cn.lili.controller.other;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.dto.GoodsAndStores;
import cn.lili.modules.goods.entity.dto.GoodsSearchParams;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.page.entity.dto.PageDataDTO;
import cn.lili.modules.page.entity.enums.PageEnum;
import cn.lili.modules.page.entity.vos.PageDataVO;
import cn.lili.modules.page.service.PageDataService;
import cn.lili.modules.store.entity.vos.StoreSearchParams;
import cn.lili.modules.store.entity.vos.StoreVO;
import cn.lili.modules.system.entity.dos.Region;
import cn.lili.modules.system.service.RegionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 买家端,页面接口
 *
 * @author Chopper
 * @since 2020/11/16 10:08 下午
 */
@RestController
@Api(tags = "买家端,页面接口")
@RequestMapping("/buyer/pageData")
public class PageBuyerController {

	/**
	 * 页面管理
	 */
	@Autowired
	private PageDataService pageService;
	// 地区
	@Autowired
	private RegionService regionService;

	@ApiOperation(value = "获取首页数据")
	@RequestMapping("/getIndex")
	public ResultMessage<PageDataVO> getIndex(@RequestParam String clientType) {
		PageDataDTO pageDataDTO = new PageDataDTO(PageEnum.INDEX.name());
		pageDataDTO.setPageClientType(clientType);
		return ResultUtil.data(pageService.getPageData(pageDataDTO));
	}

	@ApiOperation(value = "获取页面数据")
	@GetMapping
	public ResultMessage<PageDataVO> get(PageDataDTO pageDataDTO) {
		return ResultUtil.data(pageService.getPageData(pageDataDTO));
	}

	@ApiOperation(value = "根据地区，获取商品列表")
	@RequestMapping("/getRegionCo")
	public ResultMessage<IPage<GoodsVO>> page(Region region, PageVO page) {
		return ResultUtil.data(regionService.getRegionCo(page, region));
	}

	// /** 暂时先注释不用
	// * 爆品商品
	// * 本区域前三的商品
	// */
	// @ApiOperation(value = "根据地区，获取商品列表")
	// @RequestMapping("/getCommodity")
	// public ResultMessage<Map> getCommodity(Region region) {
	// return ResultUtil.data(regionService.getCommodity(region));
	// }

	// /** 暂时先注释不用
	// * 本区域前六的店铺
	// */
	// @ApiOperation(value = "根据地区，获取店铺列表")
	// @RequestMapping("/getCommodityShop")
	// public ResultMessage<Map> getCommodityShop(Region region) {
	// return ResultUtil.data(regionService.getCommodityShop(region));
	// }

	@ApiOperation(value = "获取店铺列表分页")
	@RequestMapping("/getCommodityShop")
	public ResultMessage<IPage<StoreVO>> getByPage(StoreSearchParams entity, PageVO page) {
		return ResultUtil.data(regionService.findByConditionPage(entity, page));
	}

	@ApiOperation(value = "获取商品分页列表")
	@RequestMapping("/getCommodity")
	public ResultMessage<IPage<GoodsVO>> getByPage(GoodsSearchParams goodsSearchParams) {
		return ResultUtil.data(regionService.queryByParams(goodsSearchParams));
	}

	@ApiOperation(value = "首页-本地好店")
	@RequestMapping("/getGoodShop")
	public ResultMessage<List<StoreVO>> getGoodShop(StoreSearchParams entity) {
		return ResultUtil.data(regionService.getGoodShop(entity));
	}

	@ApiOperation(value = "首页- 热销商品")
	@RequestMapping("/hotGoods")
	public ResultMessage<IPage<GoodsVO>> getByhotGoods(GoodsSearchParams goodsSearchParams, PageVO page) {
		return ResultUtil.data(regionService.getByhotGoods(goodsSearchParams, page));
	}

	@ApiOperation(value = "首页- 新品新店")
	@RequestMapping("/new")
	public ResultMessage<IPage<GoodsAndStores>> getByNewGoodsAndStore(GoodsAndStores goodsAndStores, PageVO page) {
		return ResultUtil.data(regionService.getByNewGoodsAndStore(goodsAndStores, page));
	}

	@ApiOperation(value = "首页-本地商圈")
	@RequestMapping("/getGoodShopPage")
	public ResultMessage<IPage<StoreVO>> getGoodShopPage(StoreSearchParams entity, PageVO page) {
		return ResultUtil.data(regionService.getGoodShopPage(entity, page));
	}

	@ApiOperation(value = "首页-实物商品")
	@RequestMapping("/getGoodPage")
	public ResultMessage<IPage<GoodsVO>> getGoodPage(GoodsSearchParams goodsSearchParams, PageVO page) {
		return ResultUtil.data(regionService.getGoodPage(goodsSearchParams, page));
	}

	@ApiOperation(value = "实物商品 - 新品")
	@RequestMapping("/newGoods")
	public ResultMessage<IPage<GoodsVO>> getByNewGoods(GoodsSearchParams goodsSearchParams, PageVO page) {
		return ResultUtil.data(regionService.getByNewGoods(goodsSearchParams, page));
	}

}
