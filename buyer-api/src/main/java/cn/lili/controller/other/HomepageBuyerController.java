package cn.lili.controller.other;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.page.service.HomepageManagerService;
import cn.lili.modules.system.entity.dos.Homepage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "买家端,首页模块管理")
@RequestMapping("/buyer/homepage")
public class HomepageBuyerController {

	@Autowired
	private HomepageManagerService homepageManagerService;

	@ApiOperation(value = "查询首页模块接口")
	@GetMapping
	public ResultMessage<List<Homepage>> list() {
		return ResultUtil.data(homepageManagerService.list());
	}

}
