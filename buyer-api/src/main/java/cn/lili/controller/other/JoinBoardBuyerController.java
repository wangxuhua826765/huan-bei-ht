package cn.lili.controller.other;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.page.entity.dos.JoinBoard;
import cn.lili.modules.page.service.JoinBoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * JoinBoardBuyerController. 买家端，加盟申请
 * 
 * @author Li Bing
 * @since 2022.03.27
 */
@RestController
@Api(tags = "买家端,加盟申请")
@RequestMapping("/buyer/joinboard")
public class JoinBoardBuyerController {

	@Autowired
	private JoinBoardService joinBoardService;

	@ApiOperation(value = "添加加盟申请")
	@PostMapping
	public ResultMessage<Object> save(@Valid @RequestBody JoinBoard joinBoard) {

		joinBoardService.save(joinBoard);
		return ResultUtil.success();
	}

}
