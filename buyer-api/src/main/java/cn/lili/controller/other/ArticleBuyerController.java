package cn.lili.controller.other;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.page.entity.dos.Article;
import cn.lili.modules.page.entity.dos.ArticleRecordSummary;
import cn.lili.modules.page.entity.dto.ArticleSearchParams;
import cn.lili.modules.page.entity.vos.ArticleCategoryVO;
import cn.lili.modules.page.entity.vos.ArticleRecordSummaryVO;
import cn.lili.modules.page.entity.vos.ArticleVO;
import cn.lili.modules.page.service.ArticleCategoryService;
import cn.lili.modules.page.service.ArticleRecordSummaryService;
import cn.lili.modules.page.service.ArticleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 买家端,文章接口
 *
 * @author Chopper
 * @since 2020/11/16 10:02 下午
 */
@RestController
@Api(tags = "买家端,文章接口")
@RequestMapping("/buyer/article")
public class ArticleBuyerController {

	/**
	 * 文章
	 */
	@Autowired
	private ArticleService articleService;

	@Autowired
	private ArticleRecordSummaryService articleRecordSummaryService;

	/**
	 * 文章分类
	 */
	@Autowired
	private ArticleCategoryService articleCategoryService;

	@ApiOperation(value = "获取文章分类列表")
	@RequestMapping(value = "/articleCategory/list")
	public ResultMessage<List<ArticleCategoryVO>> getArticleCategoryList() {
		return ResultUtil.data(articleCategoryService.allChildren());
	}

	@ApiOperation(value = "分页获取")
	@GetMapping
	public ResultMessage<IPage<ArticleVO>> getByPage(ArticleSearchParams articleSearchParams) {
		return ResultUtil.data(articleService.articlePage(articleSearchParams));
	}

	@ApiOperation(value = "通过id获取文章")
	@ApiImplicitParam(name = "id", value = "文章ID", required = true, paramType = "path")
	@RequestMapping(value = "/get/{id}")
	public ResultMessage<Article> get(@NotNull(message = "文章ID") @PathVariable("id") String id) {
		return ResultUtil.data(articleService.customGet(id));
	}

	@ApiOperation(value = "通过类型获取文章")
	@ApiImplicitParam(name = "type", value = "文章类型", required = true, paramType = "path")
	@RequestMapping(value = "/type/{type}")
	public ResultMessage<List<ArticleRecordSummaryVO>> getByType(
			@NotNull(message = "文章类型") @PathVariable("type") String type) {
		return ResultUtil.data(articleService.getArticleRecordSummary(type));
	}

	@ApiOperation(value = "通过查询用户是否已读文章")
	@RequestMapping(value = "/getRecordSummary")
	public ResultMessage<List<ArticleRecordSummaryVO>> getRecordSummary() {
		List<ArticleRecordSummaryVO> articleRecordSummaryVOS = articleService.QueryRecord();
		if (articleRecordSummaryVOS.size() == 0 || articleRecordSummaryVOS == null) {
			return ResultUtil.error(ResultCode.ORDER_ERROR_EMPTY);
		}
		return ResultUtil.success();
	}

	@ApiOperation(value = "插入到已读列表")
	@RequestMapping(value = "/AddArticleRecordSummary")
	public ResultMessage<Integer> AddArticleRecordSummary(@RequestBody ArticleRecordSummary articleRecordSummary) {

		return ResultUtil.data(articleRecordSummaryService.AddArticleRecordSummary(articleRecordSummary));
	}
}
