package cn.lili.controller.settings;

import cn.hutool.json.JSONUtil;
import cn.lili.common.aop.annotation.DemoSite;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.system.entity.dos.Setting;
import cn.lili.modules.system.entity.dto.*;
import cn.lili.modules.system.entity.enums.SettingEnum;
import cn.lili.modules.system.service.SettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(tags = "小程序端,系统设置接口")
@RequestMapping("/buyer/setting")
public class SettingBuyerController {
	@Autowired
	private SettingService settingService;

	@DemoSite
	@ApiOperation(value = "查看配置")
	@RequestMapping(value = "/get")
	public ResultMessage settingGet() {
		List list = new ArrayList<>();
		ZoneSetting zoneSetting = createSetting("DEFAULT_ZONE");
		list.add(zoneSetting.getName());
		zoneSetting.setConsigneeAddressPath(list);
		zoneSetting.setTrapeze(zoneSetting.getCenter());
		return ResultUtil.data(zoneSetting);
	}

	private ZoneSetting createSetting(String key) {
		SettingEnum settingEnum = SettingEnum.valueOf(key);
		Setting setting = settingService.getBuyer(key);
		switch (settingEnum) {
			case DEFAULT_ZONE :
				return setting == null
						? new ZoneSetting()
						: JSONUtil.toBean(setting.getSettingValue(), ZoneSetting.class);
			default :
				throw new ServiceException(ResultCode.SETTING_NOT_TO_SET);
		}
	}
}
