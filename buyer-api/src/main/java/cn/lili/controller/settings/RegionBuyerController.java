package cn.lili.controller.settings;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.system.entity.dos.Region;
import cn.lili.modules.system.service.RegionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 管理端,行政地区管理接口
 *
 * @author Chopper
 * @since 2020/12/2 10:40
 */
@RestController
@Api(tags = "小程序端,行政地区管理接口")
@RequestMapping("/buyer/region")
@Transactional(rollbackFor = Exception.class)
public class RegionBuyerController {
	@Autowired
	private RegionService regionService;

	@RequestMapping(value = "/adcode")
	@ApiOperation(value = "通过adcode获取下一级详情")
	public ResultMessage<List<Region>> get(String chooseCode) {
		List<Region> regionList = new ArrayList<>();
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("ad_code", chooseCode);
		queryWrapper.ne("level", "street");
		Region one = regionService.getOne(queryWrapper);
		// 判断传过来的地区级别
		if (StringUtils.isNotEmpty(one.getLevel()) && one.getLevel().equals("province")) {
			QueryWrapper queryWrapper1 = new QueryWrapper();
			queryWrapper1.like("path", one.getId());
			queryWrapper1.eq("level", "city");
			regionList = regionService.list(queryWrapper1);
		} else if (StringUtils.isNotEmpty(one.getLevel()) && one.getLevel().equals("city")) {
			QueryWrapper queryWrapper1 = new QueryWrapper();
			queryWrapper1.like("path", one.getId());
			queryWrapper1.eq("level", "district");
			regionList = regionService.list(queryWrapper1);
		} else {
			QueryWrapper queryWrapper1 = new QueryWrapper();
			queryWrapper1.like("path", one.getParentId());
			queryWrapper1.eq("level", "district");
			regionList = regionService.list(queryWrapper1);
		}
		return ResultUtil.data(regionList);
	}

}
