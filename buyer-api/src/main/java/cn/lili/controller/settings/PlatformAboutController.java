package cn.lili.controller.settings;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.page.entity.dos.Article;
import cn.lili.modules.page.entity.dto.ArticleCategoryDto;
import cn.lili.modules.page.entity.dto.ArticleSearchParams;
import cn.lili.modules.page.service.ArticleService;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.vos.FreightTemplateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "关于平台")
@RequestMapping("/buyer/platformAbout")
public class PlatformAboutController {

	@Autowired
	private ArticleService articleService;

	@ApiOperation(value = "列表")
	@RequestMapping("/getPlatformAbout")
	public ResultMessage<List<ArticleCategoryDto>> getPlatformAbout(
			@RequestBody ArticleSearchParams articleSearchParams) {
		articleSearchParams.setOpenStatus(true);
		return ResultUtil.data(articleService.getPlatformAbout(articleSearchParams));
	}

	@ApiOperation(value = "详情")
	@RequestMapping("/getDetail")
	public ResultMessage<Article> getDetail(@RequestParam("id") String id) {
		return ResultUtil.data(articleService.getDetail(id));
	}
}
