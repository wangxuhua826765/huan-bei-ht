package cn.lili.controller.goods;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.dto.EsGoodsIndexDTO;
import cn.lili.modules.goods.entity.dto.EsStoreIndexDTO;
import cn.lili.modules.goods.entity.dto.GoodsOperationDTO;
import cn.lili.modules.goods.entity.dto.GoodsSearchParams;
import cn.lili.modules.goods.entity.enums.GoodsAuthEnum;
import cn.lili.modules.goods.entity.enums.GoodsStatusEnum;
import cn.lili.modules.goods.entity.vos.EsStoreIndexVO;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.goods.service.GoodsService;
import cn.lili.modules.goods.service.GoodsSkuService;
import cn.lili.modules.search.entity.dos.EsGoodsIndex;
import cn.lili.modules.goods.entity.vos.EsGoodsIndexVO;
import cn.lili.modules.search.entity.dos.EsGoodsRelatedInfo;
import cn.lili.modules.search.entity.dto.EsGoodsSearchDTO;
import cn.lili.modules.search.service.EsGoodsSearchService;
import cn.lili.modules.statistics.aop.PageViewPoint;
import cn.lili.modules.statistics.aop.enums.PageViewEnum;
import cn.lili.modules.statistics.entity.dto.GoodsStatisticsQueryParam;
import cn.lili.modules.statistics.entity.dto.StatisticsQueryParam;
import cn.lili.modules.statistics.entity.enums.SearchTypeEnum;
import cn.lili.modules.statistics.entity.enums.StatisticsQuery;
import cn.lili.modules.statistics.entity.vo.GoodsStatisticsDataVO;
import cn.lili.modules.statistics.entity.vo.StoreStatisticsDataVO;
import cn.lili.modules.statistics.service.IndexStatisticsService;
import cn.lili.modules.store.entity.vos.StoreSearchParams;
import cn.lili.modules.store.entity.vos.StoreVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.SearchPage;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 买家端,商品接口
 *
 * @author Chopper
 * @since 2020/11/16 10:06 下午
 */
@Slf4j
@Api(tags = "买家端,商品接口")
@RestController
@RequestMapping("/buyer/goods")
public class GoodsBuyerController {

	/**
	 * 商品
	 */
	@Autowired
	private GoodsService goodsService;
	/**
	 * 商品SKU
	 */
	@Autowired
	private GoodsSkuService goodsSkuService;
	/**
	 * ES商品搜索
	 */
	@Autowired
	private EsGoodsSearchService goodsSearchService;

	@Autowired
	private IndexStatisticsService indexStatisticsService;

	@ApiOperation(value = "通过id获取商品信息")
	@ApiImplicitParam(name = "goodsId", value = "商品ID", required = true, paramType = "path", dataType = "Long")
	@RequestMapping(value = "/get/{goodsId}")
	public ResultMessage<GoodsVO> get(@NotNull(message = "商品ID不能为空") @PathVariable("goodsId") String id) {
		return ResultUtil.data(goodsService.getGoodsVO(id));
	}

	@ApiOperation(value = "通过id获取商品信息")
	@ApiImplicitParams({@ApiImplicitParam(name = "goodsId", value = "商品ID", required = true, paramType = "path"),
			@ApiImplicitParam(name = "skuId", value = "skuId", required = true, paramType = "path")})
	@RequestMapping(value = "/sku/{goodsId}/{skuId}")
	@PageViewPoint(type = PageViewEnum.SKU, id = "#id")
	public ResultMessage<Map<String, Object>> getSku(
			@NotNull(message = "商品ID不能为空") @PathVariable("goodsId") String goodsId,
			@NotNull(message = "SKU ID不能为空") @PathVariable("skuId") String skuId) {
		try {
			// 读取选中的列表
			Map<String, Object> map = goodsSkuService.getGoodsSkuDetail(goodsId, skuId);
			return ResultUtil.data(map);
		} catch (ServiceException se) {
			log.info(se.getMsg(), se);
			throw se;
		} catch (Exception e) {
			log.error(ResultCode.GOODS_ERROR.message(), e);
			return ResultUtil.error(ResultCode.GOODS_ERROR);
		}

	}

	@ApiOperation(value = "获取小程序店铺分页列表")
	@RequestMapping("/geStoreInfoByData")
	public ResultMessage<IPage<EsStoreIndexVO>> geStoreInfoByData(EsStoreIndexDTO esStoreIndexDTO) {
		esStoreIndexDTO.setStoreDisable("OPEN");
		return ResultUtil.data(goodsService.geStoreInfoByData(esStoreIndexDTO));
	}

	@ApiOperation(value = "获取小程序搜索商品分页列表")
	@RequestMapping("/getGoodsByStore")
	public ResultMessage<IPage<EsGoodsIndexVO>> getGoodsByStore(EsGoodsIndexDTO esGoodsIndexDTO) {
		// IPage<EsGoodsIndexParams> esGoodsIndexParamsIPage =
		// goodsService.getGoodsByStore(esGoodsIndexParams.getGoodsName(),page,limit);
		esGoodsIndexDTO.setMarketEnable(GoodsStatusEnum.UPPER.name());
		esGoodsIndexDTO.setStoreDisable("OPEN");
		esGoodsIndexDTO.setAuthFlag(GoodsAuthEnum.PASS.name());
		return ResultUtil.data(goodsService.getGoodsByStore(esGoodsIndexDTO));
	}

	@ApiOperation(value = "从ES中获取商品信息")
	@RequestMapping("/es")
	public ResultMessage<SearchPage<EsGoodsIndex>> getGoodsByPageFromEs(EsGoodsSearchDTO goodsSearchParams,
			PageVO pageVO) {
		pageVO.setNotConvert(true);
		pageVO.setSort("sort");
		pageVO.setOrder("desc");
		SearchPage<EsGoodsIndex> esGoodsIndices = goodsSearchService.searchGoods(goodsSearchParams, pageVO);
		return ResultUtil.data(esGoodsIndices);
	}

	@ApiOperation(value = "从ES中获取相关商品品牌名称，分类名称及属性")
	@RequestMapping("/es/related")
	public ResultMessage<EsGoodsRelatedInfo> getGoodsRelatedByPageFromEs(EsGoodsSearchDTO goodsSearchParams,
			PageVO pageVO) {
		pageVO.setNotConvert(true);
		EsGoodsRelatedInfo selector = goodsSearchService.getSelector(goodsSearchParams, pageVO);
		return ResultUtil.data(selector);
	}

	@ApiOperation(value = "获取搜索热词")
	@RequestMapping("/hot-words")
	public ResultMessage<List<String>> getGoodsHotWords(Integer count) {
		List<String> hotWords = goodsSearchService.getHotWords(count);
		return ResultUtil.data(hotWords);
	}

	@ApiOperation(value = "今日爆品TOP10")
	@RequestMapping("/goodsStatistics")
	public ResultMessage<List<GoodsStatisticsDataVO>> goodsStatistics(
			GoodsStatisticsQueryParam goodsStatisticsQueryParam) {

		// 按照金额查询
		goodsStatisticsQueryParam.setType(StatisticsQuery.NUM.name());
		goodsStatisticsQueryParam.setSearchType(SearchTypeEnum.TODAY.name());
		return ResultUtil.data(indexStatisticsService.goodsStatistics(goodsStatisticsQueryParam));
	}

	@ApiOperation(value = "好店推荐TOP10")
	@RequestMapping("/storeStatistics")
	public ResultMessage<List<StoreStatisticsDataVO>> storeStatistics(StatisticsQueryParam statisticsQueryParam) {
		statisticsQueryParam.setSearchType(SearchTypeEnum.TODAY.name());
		return ResultUtil.data(indexStatisticsService.storeStatistics(statisticsQueryParam));
	}

	/**
	 * 小程序首页查询
	 */
	@ApiOperation(value = "获取商品分页列表")
	@GetMapping
	public ResultMessage<IPage<GoodsVO>> getGoodsByStore(GoodsSearchParams goodsSearchParams) {
		return ResultUtil.data(goodsService.queryByParams(goodsSearchParams));
	}

	@ApiOperation(value = "新增商品")
	@RequestMapping(value = "/create", consumes = "application/json", produces = "application/json")
	public ResultMessage<GoodsOperationDTO> save(@Valid @RequestBody GoodsOperationDTO goodsOperationDTO) {
		goodsService.addGoods(goodsOperationDTO);
		return ResultUtil.success();
	}

	@ApiOperation(value = "根据商品id查询同分类的商品")
	@RequestMapping("/getGoodsListByPage")
	public ResultMessage<IPage<GoodsVO>> getGoodsListByPage(String goodsId, PageVO page) {
		return ResultUtil.data(goodsService.getGoodsListByPage(goodsId, page));
	}
}
