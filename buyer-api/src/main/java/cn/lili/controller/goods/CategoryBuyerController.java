package cn.lili.controller.goods;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.dos.Category;
import cn.lili.modules.goods.entity.vos.CategoryBrandVO;
import cn.lili.modules.goods.entity.vos.CategoryVO;
import cn.lili.modules.goods.entity.vos.StoreGoodsLabelVO;
import cn.lili.modules.goods.service.CategoryBrandService;
import cn.lili.modules.goods.service.CategoryService;
import cn.lili.modules.goods.service.StoreGoodsLabelService;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.service.StoreService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * 买家端,商品分类接口
 *
 * @author Chopper
 * @since 2020/11/16 10:05 下午
 */
@RestController
@Api(tags = "买家端,商品分类接口")
@RequestMapping("/buyer/category")
public class CategoryBuyerController {
	/**
	 * 商品分类
	 */
	@Autowired
	private CategoryService categoryService;
	/**
	 * 分类品牌
	 */
	@Autowired
	private CategoryBrandService categoryBrandService;
	@Autowired
	private StoreService storeService;

	@Autowired
	private StoreGoodsLabelService storeGoodsLabelService;

	@ApiOperation(value = "获取商品分类列表")
	@ApiImplicitParam(name = "parentId", value = "上级分类ID，全部分类为：0", required = true, dataType = "Long", paramType = "path")
	@RequestMapping(value = "/get/{parentId}")
	public ResultMessage<List<CategoryVO>> list(@NotNull(message = "分类ID不能为空") @PathVariable String parentId) {

		return ResultUtil.data(categoryService.listAllChildren(parentId));
	}

	@ApiOperation(value = "获取所选分类关联的品牌信息")
	@RequestMapping(value = "/{categoryId}/brands")
	@ApiImplicitParams({@ApiImplicitParam(name = "categoryId", value = "分类id", required = true, paramType = "path"),})
	public List<CategoryBrandVO> queryBrands(@PathVariable String categoryId) {
		return this.categoryBrandService.getCategoryBrandList(categoryId);
	}

	@ApiOperation(value = "首页的商品分类(本地商圈)")
	@RequestMapping(value = "/index")
	public List<CategoryVO> getIndexCategory() {
		return this.categoryService.listAllChildren("1507750547560792066");
	}

	@ApiOperation(value = "获取当前店铺商品分类列表")
	@RequestMapping("/label")
	public ResultMessage<List<StoreGoodsLabelVO>> list() {
		String userId = Objects.requireNonNull(UserContext.getCurrentUser()).getId();
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("member_id", userId);
		Store store = storeService.getOne(queryWrapper);
		return ResultUtil.data(storeGoodsLabelService.listByStoreId(store.getId()));
	}

}
