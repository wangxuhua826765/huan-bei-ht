package cn.lili.controller.sysdict;

import cn.lili.cache.Cache;
import cn.lili.cache.CachePrefix;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.UuidUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.SysDictVO;
import cn.lili.modules.dict.service.SysDictItemService;
import cn.lili.modules.dict.service.SysDictService;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.Schedules;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 数据字典
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "管理端,数据字典接口")
@RequestMapping("/buyer/sysDict")
public class SysDictController {

	/**
	 * 文章分类
	 */
	@Autowired
	private SysDictService sysDictService;

	@Autowired
	private SysDictItemService sysDictItemService;

	@ApiOperation(value = "查询代理商的端口费")
	@RequestMapping(value = "/partnerPrice")
	public ResultMessage<SysDictItem> getPartnerPrice(String partner) {
		try {
			return ResultUtil.data(this.sysDictService.getPartnerPrice(partner));
		} catch (Exception e) {
			log.error("查询数据字典列表错误", e);
		}
		return ResultUtil.error(ResultCode.ERROR);
	}
}
