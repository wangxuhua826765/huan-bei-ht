package cn.lili.controller.activity;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import cn.lili.modules.activity.service.ActivityService;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.system.aspect.annotation.SystemLogPoint;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "小程序端-活动管理")
@RequestMapping("/buyer/activity")
public class ActivityBuyerController {

	@Autowired
	private ActivityService activityService;

	/**
	 * 自动分页查询
	 */
	@RequestMapping("/list")
	public ResultMessage<IPage<ActivityVO>> selectAppIPage(ActivityVO activityVO, PageVO page) {
		IPage<ActivityVO> activityVOIPage = activityService.selectAppIPage(activityVO, page);
		return ResultUtil.data(activityVOIPage);
	}

	/**
	 * 查询详情(宣传)
	 */
	@RequestMapping("/{id}")
	public ResultMessage<ActivityVO> selectAppActDetails(@PathVariable String id) {
		ActivityVO activityVO = activityService.selectAppActDetails(id);
		return ResultUtil.data(activityVO);
	}

	/**
	 * 查询详情(商品)
	 */
	@RequestMapping("/act/{id}")
	public ResultMessage<IPage<GoodsVO>> selectAppGoodsDetails(@PathVariable String id, PageVO page,
			String categoryId) {
		IPage<GoodsVO> goodsVOIPage = activityService.selectAppGoodsDetails(id, page, categoryId);
		return ResultUtil.data(goodsVOIPage);
	}

	/**
	 * 查询到期的活动 给商品下架
	 */
	@Scheduled(cron = "0 0 0 1/1 * ?")
	@PutMapping("/status")
	public ResultMessage updateGoodsStatus() {
		Boolean b = activityService.updateGoodsStatus();
		return ResultUtil.data(ResultCode.SUCCESS);
	}

}
