package cn.lili.controller.order;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.enums.PartnerStatusEnum;
import cn.lili.modules.member.entity.vo.MemberPartnerVO;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.service.RechargeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import java.util.List;

import static cn.lili.common.enums.ResultCode.USER_PARTNER_YES;

/**
 * 买家端,预存款充值记录接口
 *
 * @author paulG
 * @since 2020/11/16 10:07 下午
 */
@RestController
@Api(tags = "买家端,预存款充值记录接口")
@RequestMapping("/buyer/trade/recharge")
@Transactional(rollbackFor = Exception.class)
public class RechargeTradeBuyerController {

	@Autowired
	private RechargeService rechargeService;
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private MemberService memberService;

	@PostMapping
	@ApiOperation(value = "创建余额充值订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "price", value = "充值金额", required = true, dataType = "double", paramType = "query"),
			@ApiImplicitParam(name = "hbPrice", value = "充值焕贝金额", required = true, dataType = "double", paramType = "query"),
			@ApiImplicitParam(name = "vipPrice", value = "充值vip需要的焕贝金额", required = true, dataType = "double", paramType = "query"),
			@ApiImplicitParam(name = "rechargeType", value = "充值类型", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "orderSn", value = "订单号", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "tjrPhone", value = "推荐人手机号")})
	public ResultMessage<Recharge> create(
			@Max(value = 10000, message = "充值金额单次最多允许充值10000元") @Min(value = 1, message = "充值金额单次最少充值金额为1元") Double price,
			Double vipPrice, Double hbPrice, String rechargeType, String orderSn, String name, String linkMobile,
			String adPath, String storeAddressPath_path, String remark, String idCard, String partnerType,String tjrPhone) {
		Recharge recharge = this.rechargeService.recharge(price, vipPrice, hbPrice, WalletOwnerEnum.RECHARGE.name(),
				rechargeType, orderSn);
		if ("45".equals(rechargeType)) {
			// 创建合伙人信息
			QueryWrapper<Partner> queryWrapper = new QueryWrapper<Partner>()
					.eq("member_id", UserContext.getCurrentUser().getId()).notIn("partner_type", 3)
					.eq("partner_state", 0)
					.notIn("audit_state", PartnerStatusEnum.REFUSED, PartnerStatusEnum.WAIT_SUBMIT)
					.eq("delete_flag", false);
			List<Partner> partnerList = partnerService.list(queryWrapper);
			if (!partnerList.isEmpty()) {
				return ResultUtil.error(USER_PARTNER_YES);
			}
			MemberPartnerVO memberPartner = new MemberPartnerVO();
			memberPartner.setRealName(name);
			memberPartner.setIdCardNumber(idCard);
			memberPartner.setLinkMobile(linkMobile);
			memberPartner.setAdPath(adPath);
			memberPartner.setStoreAddressPath_path(storeAddressPath_path);
			memberPartner.setRemark(remark);
			memberPartner.setPartnerPrice(vipPrice);
			memberPartner.setSn(recharge.getRechargeSn());
			memberPartner.setPartnerType(Integer.valueOf(partnerType));
			memberPartner.setRecommendPhone(tjrPhone);
			memberService.addPartner(memberPartner);
		}
		return ResultUtil.data(recharge);
	}

	@RequestMapping(value = "/createPartner")
	@ApiOperation(value = "创建合伙人/会员充值订单")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "price", value = "充值金额", required = true, dataType = "double", paramType = "query"),
			@ApiImplicitParam(name = "type", value = "充值类型", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "owner", value = "订单来源", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "memberPartner", value = "订单来源", required = true, dataType = "String", paramType = "query")})
	public ResultMessage<Recharge> createPartner(
			@Max(value = 10000, message = "充值金额单次最多允许充值10000元") @Min(value = 1, message = "充值金额单次最少充值金额为1元") Double price,
			String type, String owner, MemberPartnerVO memberPartner) {
		Recharge recharge = this.rechargeService.rechargePartner(null, price, type, owner, null, null);
		if ("45".equals(type)) {
			// 创建合伙人信息
			QueryWrapper<Partner> queryWrapper = new QueryWrapper<Partner>()
					.eq("member_id", UserContext.getCurrentUser().getId()).notIn("partner_type", 3)
					.eq("partner_state", 0).notIn("audit_state", PartnerStatusEnum.REFUSED).eq("delete_flag", false);
			List<Partner> partnerList = partnerService.list(queryWrapper);
			if (!partnerList.isEmpty()) {
				return ResultUtil.error(USER_PARTNER_YES);
			}
			memberService.addPartner(memberPartner);
		}
		return ResultUtil.data(recharge);
	}

}
