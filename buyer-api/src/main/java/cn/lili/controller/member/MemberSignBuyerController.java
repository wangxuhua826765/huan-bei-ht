package cn.lili.controller.member;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.BeanUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.connect.service.ConnectService;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.MemberSign;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.member.service.MemberSignService;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.vos.StoreVO;
import cn.lili.modules.store.service.StoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会员签到控制器
 *
 * @author pikachu
 * @since 2020/11/16 10:07 下午
 */
@RestController
@Api(tags = "买家端，会员签到API")
@RequestMapping("/buyer/members/sign")
public class MemberSignBuyerController {
	@Autowired
	private MemberSignService memberSignService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private ConnectService connectService;

	@PostMapping
	@ApiOperation(value = "会员签到")
	public ResultMessage<Boolean> memberSign() {
		return ResultUtil.data(memberSignService.memberSign());
	}

	@GetMapping
	@ApiOperation(value = "根据时间查询会员签到表，类型是YYYYmm")
	public ResultMessage<List<MemberSign>> memberSign(String time) {
		return ResultUtil.data(memberSignService.getMonthSignDay(time));
	}

	/**
	 * 根据商家id获取商家的基本信息
	 *
	 * @return
	 */
	@ApiOperation(value = "根据商家id获取商家的基本信息")
	@ApiImplicitParam(name = "id", value = "商家ID", required = true, paramType = "path")
	@RequestMapping(value = "/getInfo/{id}")
	public ResultMessage<Store> getUserInfoById(@NotNull(message = "ID不能为空") @PathVariable("id") String id) {
		Map<String, Object> map = new HashMap<>();
		// Member byId = memberService.getById(id);
		Store store = storeService.getById(id);
		if (store == null) {
			return ResultUtil.data(null);
		}
		return ResultUtil.data(store);
	}
	/**
	 * 根据商家id获取商家的基本信息
	 *
	 * @return
	 */
	@ApiOperation(value = "根据商家id获取商家的基本信息")
	@ApiImplicitParam(name = "id", value = "商家ID", required = true, paramType = "path")
	@RequestMapping(value = "/getOpenStoreById/{id}")
	public ResultMessage<StoreVO> getOpenStoreById(@NotNull(message = "ID不能为空") @PathVariable("id") String id) {
		Map<String, Object> map = new HashMap<>();
		// Member byId = memberService.getById(id);
		Store store = storeService.getById(id);
		if (store == null || !"OPEN".equals(store.getStoreDisable())) {
			return ResultUtil.data(null);
		}
		StoreVO storeVO = new StoreVO();
		BeanUtil.copyProperties(store, storeVO);
		Member member = new Member();
		member.setId(store.getMemberId());
		storeVO.setOpenIds(connectService.findByMember(member));
		return ResultUtil.data(storeVO);
	}
}
