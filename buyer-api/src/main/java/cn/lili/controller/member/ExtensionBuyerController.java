package cn.lili.controller.member;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.message.entity.dos.ShortLink;
import cn.lili.modules.message.service.ShortLinkService;
import cn.lili.modules.wechat.util.WechatMpCodeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * create by yudan on 2022/2/28
 */
@RestController
@Api(tags = "买家端,推广")
@RequestMapping("/buyer/extension")
public class ExtensionBuyerController {

	@Autowired
	public WechatMpCodeUtil wechatMpCodeUtil;
	@Autowired
	private MemberService memberService;
	@Autowired
	private ShortLinkService shortLinkService;

	@RequestMapping("/mp/unlimited")
	@ApiOperation(value = "小程序太阳码生成：不限制数量，但是限制长度，只能存放32为长度")
	public ResultMessage<Map<String, Object>> unlimited(String page, String scene) {
		Map<String, Object> map = new HashMap<>();
		Member member = memberService.getUserInfo();
		if (StringUtils.isEmpty(scene)) {
			scene = member.getPromotionCode();
		}
		String code = wechatMpCodeUtil.createCode(page, scene, ClientTypeEnum.WECHAT_MP);
		System.out.println(" ====================================================================== code：" + code);
		map.put("sunCode", code);
		map.put("img",
				"https://thirdwx.qlogo.cn/mmopen/vi_32/W6a947hJCsyTia5wtickwgqeQUpgsGdmEHeNgz8HEF6pQSSqT3iciaBKE1BBm6TIW3pEebaGibicvvmmTQjpBkJIPc2A/132");
		return ResultUtil.data(map);
	}

	@RequestMapping("/getShortLink")
	@ApiOperation(value = "根据短链接id查询完整链接")
	public ResultMessage<String> getShortLink(String id) {
		ShortLink byId = shortLinkService.getById(id);
		return ResultUtil.data(byId.getOriginalParams());
	}

}
