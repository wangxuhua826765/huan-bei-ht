package cn.lili.controller.member;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.GradeLevel;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.enums.PartnerStatusEnum;
import cn.lili.modules.member.entity.vo.GradeVO;
import cn.lili.modules.member.entity.vo.MemberPartnerVO;
import cn.lili.modules.member.entity.vo.MemberVO;
import cn.lili.modules.member.mapper.PartnerMapper;
import cn.lili.modules.member.service.EextensionService;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.mapper.RoleMapper;
import cn.lili.modules.system.aspect.annotation.SystemLogPoint;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static cn.lili.common.enums.ResultCode.USER_PARTNER_YES;

/**
 * create by musen on 2022/3/31
 */
@RestController
@Api(tags = "加入合伙人")
@RequestMapping("/buyer/partner")
public class JoinPartnerController {

	@Autowired
	private MemberService memberService;
	@Autowired
	private EextensionService eextensionService;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private PartnerMapper partnerMapper;

	@RequestMapping(value = "/joinPartner")
	@ApiOperation(value = "获取登录用户id并绑定合伙人身份")
	@SystemLogPoint(description = "绑定合伙人身份", customerLog = "绑定合伙人身份请求", type = "2")
	public ResultMessage<Object> partnerIn() {
		// 根据用户id绑定合伙人
		MemberVO member = memberService.getUserInfos("BUYER", null);
		Partner partner = new Partner();
		if (member.getPartnerType() != null) {
			QueryWrapper<Partner> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("member_id", UserContext.getCurrentUser().getId());
			queryWrapper.eq("partner_type", member.getPartnerType());
			queryWrapper.eq("partner_state", 0);
			queryWrapper.orderByDesc("end_time");
			queryWrapper.last("limit 1");
			partner = partnerMapper.selectOne(queryWrapper);
			// 充值前先把原有的合伙人信息禁用
			partnerMapper.updateByMemberId(UserContext.getCurrentUser().getId());
			partner.setId(UUID.randomUUID().toString().replace("-", "").toLowerCase());
			partner.setPartnerState(0);
			partner.setCreateBy(UserContext.getCurrentUser().getId());
			partner.setCreateTime(new Date());
			partner.setUpdateBy(null);
			partner.setUpdateTime(null);
		} else {
			// 获取时间加一年或加一月或加一天
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);// 设置起时间
			cal.add(Calendar.YEAR, 1);// 增加一年
			partner.setBeginTime(date);
			partner.setEndTime(cal.getTime());
			partner.setMemberId(UserContext.getCurrentUser().getId());
			partner.setPartnerState(0);
		}
		QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
		partner.setPartnerType(3);
		queryWrapper.eq("name", "推广员");
		queryWrapper.eq("parent_id", 0);
		Role role = roleMapper.selectOne(queryWrapper);
		partner.setRoleId(role.getId());
		partnerMapper.insert(partner);
		// 添加推广码
		Member member1 = new Member();
		member1.setId(member.getId());
		if (StringUtils.isEmpty(member.getPromotionCode())) {
			member1.setPromotionCode(eextensionService.getExtension());
		}
		memberService.updateById(member1);
		return ResultUtil.success();
	}

	/**
	 * 获取合伙人信息
	 */
	@RequestMapping("/getPartner")
	public ResultMessage<MemberPartnerVO> getPartner() {
		MemberPartnerVO partner = memberService.getPartner();
		return ResultUtil.data(partner);
	}

	/**
	 * 提交合伙人信息
	 */
	@RequestMapping("/addPartner")
	@SystemLogPoint(description = "提交合伙人信息", customerLog = "提交合伙人信息请求", type = "2")
	public ResultMessage<Object> addPartner(MemberPartnerVO memberPartner) {
		QueryWrapper<Partner> queryWrapper = new QueryWrapper<Partner>()
				.eq("member_id", UserContext.getCurrentUser().getId()).notIn("partner_type", 3).eq("partner_state", 0)
				.notIn("audit_state", PartnerStatusEnum.REFUSED).eq("delete_flag", false);
		List<Partner> partnerList = partnerMapper.selectList(queryWrapper);
		if (!partnerList.isEmpty()) {
			return ResultUtil.error(USER_PARTNER_YES);
		}
		memberService.addPartner(memberPartner);
		return ResultUtil.success();
	}

}
