package cn.lili.controller.member;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.FootStore;
import cn.lili.modules.member.service.FootStoreService;
import cn.lili.modules.store.entity.vos.StoreVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * create by yudan on 2022/6/22
 */
@RestController
@Api(tags = "买家端,店铺浏览历史接口")
@RequestMapping("/buyer/footStore")
public class FootStoreController {

	/**
	 * 会员足迹
	 */
	@Autowired
	private FootStoreService footStoreService;

	@ApiOperation(value = "分页获取")
	@GetMapping
	public ResultMessage<List<StoreVO>> getByPage(PageVO page) {
		return ResultUtil.data(footStoreService.footStorePage(page));
	}

	@ApiOperation(value = "根据id删除")
	@ApiImplicitParam(name = "ids", value = "商品ID", required = true, allowMultiple = true, dataType = "String", paramType = "path")
	@DeleteMapping(value = "/delByIds/{ids}")
	public ResultMessage<Object> delAllByIds(@NotNull(message = "商品ID不能为空") @PathVariable("ids") List ids) {
		footStoreService.deleteByIds(ids);
		return ResultUtil.success();

	}

	@ApiOperation(value = "清空足迹")
	@DeleteMapping
	public ResultMessage<Object> deleteAll() {
		footStoreService.clean();
		return ResultUtil.success();
	}

	@ApiOperation(value = "获取当前会员足迹数量")
	@RequestMapping(value = "/getFootStoreNum")
	public ResultMessage<Object> getFootStoreNum() {
		return ResultUtil.data(footStoreService.getFootStoreNum());
	}

	@ApiOperation(value = "新增商品足迹")
	@RequestMapping(value = "/add")
	public ResultMessage<FootStore> save(FootStore footStore) {
		// 获取当前登录的会员
		AuthUser authUser = UserContext.getCurrentUser();
		if (authUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		footStore.setMemberId(authUser.getId());
		footStoreService.saveFootStore(footStore);
		return ResultUtil.success();
	}

}
