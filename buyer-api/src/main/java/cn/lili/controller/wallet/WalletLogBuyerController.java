package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.BeanUtil;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.connect.util.ComparatorTime;
import cn.lili.modules.transfer.service.TransferService;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import cn.lili.modules.wallet.service.RechargeService;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.modules.wallet.service.WalletLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 买家端,预存款变动日志记录接口
 *
 * @author pikachu
 * @since 2020/11/16 10:07 下午
 */
@RestController
@Api(tags = "买家端,预存款变动日志记录接口")
@RequestMapping("/buyer/wallet/log")
public class WalletLogBuyerController {

	@Autowired
	private WalletLogService walletLogService;
	@Autowired
	private WalletDetailService walletDetailService;
	@Autowired
	private RechargeService rechargeService;

	@Autowired
	private TransferService transferService;

	@ApiOperation(value = "分页获取预存款变动日志")
	@GetMapping
	public ResultMessage<IPage> getByPage(PageVO page, String walletFlag, String type) {
		List list = new ArrayList<>();
		// 获取当前登录用户
		AuthUser authUser = UserContext.getCurrentUser();
		IPage<WalletDetailVO> depositPage = null;
		if (StringUtils.isNotBlank(walletFlag) && (walletFlag.equals(WalletOwnerEnum.PROMOTE_HY.name())
				|| walletFlag.equals(WalletOwnerEnum.PROMOTE.name())
				|| walletFlag.equals(WalletOwnerEnum.PROMOTE_FW.name()))) {
			QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
			queryWrapper.gt("d.money", 0);
			// queryWrapper.eq("payer_owner",walletFlag);
			if (type.equals("all")) {
				queryWrapper.and(
						wrapper -> wrapper.and(w -> w.eq("d.payer", authUser.getId()).eq("d.payer_owner", walletFlag))
								.or(w -> w.eq("d.payee", authUser.getId()).eq("d.payee_owner", walletFlag)));
			} else if (type.equals("in")) {
				queryWrapper.eq("d.payee_owner", walletFlag);
				queryWrapper.eq("d.payee", authUser.getId());
			} else if (type.equals("out")) {
				queryWrapper.eq("d.payer_owner", walletFlag);
				queryWrapper.eq("d.payer", authUser.getId());
			}
			queryWrapper.groupBy("d.sn");
			queryWrapper.orderByDesc("d.create_time");
			depositPage = walletDetailService.getByPage(page, queryWrapper, authUser.getId(), type, walletFlag);
			for (WalletDetailVO walletDetailVO : depositPage.getRecords()) {
				QueryWrapper<WalletDetailVO> wrapper = new QueryWrapper<>();
				wrapper.gt("d.money", 0);
				if (type.equals("all")) {
					wrapper.and(wr -> wr.and(
							w -> w.eq("d.payer", authUser.getId()).eq("d.payer_owner", walletFlag).eq("d.STATUS", "fu"))
							.or(w -> w.eq("d.payee", authUser.getId()).eq("d.payee_owner", walletFlag).eq("d.STATUS",
									"shou")));
				} else if (type.equals("in")) {
					wrapper.eq("d.payee_owner", walletFlag);
					wrapper.eq("d.payee", authUser.getId());
				} else if (type.equals("out")) {
					wrapper.eq("d.payer_owner", walletFlag);
					wrapper.eq("d.payer", authUser.getId());
				}
				wrapper.eq("d.sn", walletDetailVO.getSn());

				List<WalletDetailVO> walletDetailVOList = walletDetailService.getListBySn(wrapper, authUser.getId(),
						type, walletFlag);
				if (walletDetailVOList.size() != 1) {
					Double sum = 0D;
					walletDetailVO.setWalletDetailList(walletDetailVOList);
					// for (WalletDetailVO walletDetailVO1 : walletDetailVOList)
					// sum = CurrencyUtil.add(sum, walletDetailVO1.getSumMoney());
					// walletDetailVO.setSumMoney(sum);
				}
				list.add(walletDetailVO);
			}
			// 查询推广钱包的转出
			/*
			 * if(type.equals("out") || type.equals("all")){ List<Transfer> transferList =
			 * transferService.list(new QueryWrapper<Transfer>()
			 * .eq("payer",authUser.getId()) .eq("state","success") .eq("delete_flag",0)
			 * .eq("payment_wallet",walletFlag)); for (Transfer transfer : transferList) {
			 * WalletDetailVO walletDetailVO = new WalletDetailVO();
			 * walletDetailVO.setSn(transfer.getSn());
			 * walletDetailVO.setCreateTime(transfer.getCreateTime());
			 * walletDetailVO.setSumMoney(Math.abs(Double.valueOf(transfer.getTransferMoney(
			 * ))+transfer.getCommission())*-1); walletDetailVO.setStatus("fu");
			 * walletDetailVO.setType("Z"); walletDetailVO.setTransactionType("transfer");
			 * list.add(walletDetailVO); } }
			 */
		} else if (StringUtils.isNotBlank(walletFlag) && walletFlag.equals(WalletOwnerEnum.SALE.name())) {
			QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
			queryWrapper.gt("d.money", 0);
			if (type.equals("all")) {
				queryWrapper.and(
						wrapper -> wrapper.and(w -> w.eq("d.payer", authUser.getId()).eq("d.payer_owner", walletFlag))
								.or(w -> w.eq("d.payee", authUser.getId()).eq("d.payee_owner", walletFlag)));
			} else if (type.equals("in")) {
				queryWrapper.eq("d.payee", authUser.getId());
				queryWrapper.eq("d.payee_owner", walletFlag);
			} else if (type.equals("out")) {
				queryWrapper.eq("d.payer", authUser.getId());
				queryWrapper.eq("d.payer_owner", walletFlag);
			}
			queryWrapper.groupBy("d.sn");
			queryWrapper.orderByDesc("d.create_time");
			depositPage = walletDetailService.getByPage(page, queryWrapper, authUser.getId(), type, walletFlag);
			for (WalletDetailVO walletDetailVO : depositPage.getRecords()) {
				QueryWrapper<WalletDetailVO> wrapper = new QueryWrapper<>();
				wrapper.gt("d.money", 0);
				if (type.equals("all")) {
					wrapper.and(wr -> wr.and(
							w -> w.eq("d.payer", authUser.getId()).eq("d.payer_owner", walletFlag).eq("d.STATUS", "fu"))
							.or(w -> w.eq("d.payee", authUser.getId()).eq("d.payee_owner", walletFlag).eq("d.STATUS",
									"shou")));
				} else if (type.equals("in")) {
					wrapper.eq("d.payee_owner", walletFlag);
					wrapper.eq("d.payee", authUser.getId());
				} else if (type.equals("out")) {
					wrapper.eq("d.payer_owner", walletFlag);
					wrapper.eq("d.payer", authUser.getId());
				}
				wrapper.eq("d.sn", walletDetailVO.getSn());

				List<WalletDetailVO> walletDetailVOList = walletDetailService.getListBySn(wrapper, authUser.getId(),
						type, walletFlag);
				if (walletDetailVOList.size() != 1) {
					Double sum = 0D;
					walletDetailVO.setWalletDetailList(walletDetailVOList);
					for (WalletDetailVO walletDetailVO1 : walletDetailVOList)
						sum = CurrencyUtil.add(sum, walletDetailVO1.getSumMoney());
					walletDetailVO.setSumMoney(sum);
				}
				list.add(walletDetailVO);
			}
			// 查询销售钱包的转出
			/*
			 * if(type.equals("out") || type.equals("all")){ List<Transfer> transferList =
			 * transferService.list(new QueryWrapper<Transfer>()
			 * .eq("payer",authUser.getId()) .eq("state","success") .eq("delete_flag",0)
			 * .eq("payment_wallet",walletFlag)); for (Transfer transfer : transferList) {
			 * WalletDetailVO walletDetailVO = new WalletDetailVO();
			 * walletDetailVO.setSn(transfer.getSn());
			 * walletDetailVO.setCreateTime(transfer.getCreateTime());
			 * walletDetailVO.setSumMoney(Math.abs(Double.valueOf(transfer.getTransferMoney(
			 * ))+transfer.getCommission())*-1); walletDetailVO.setStatus("fu");
			 * walletDetailVO.setType("Z"); walletDetailVO.setTransactionType("transfer");
			 * list.add(walletDetailVO); } }
			 */
		} else if (StringUtils.isNotBlank(walletFlag) && walletFlag.equals(WalletOwnerEnum.RECHARGE.name())) {
			QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper();
			queryWrapper.gt("d.money", 0);
			if (type.equals("all")) {
				queryWrapper.and(
						wrapper -> wrapper.and(w -> w.eq("d.payer", authUser.getId()).eq("d.payer_owner", walletFlag))
								.or(w -> w.eq("d.payee", authUser.getId()).eq("d.payee_owner", walletFlag)));
				queryWrapper.and(wr -> wr.and(w -> w.ne("r.recharge_type", "5")).or(w -> w.isNull("r.recharge_type")));
			} else if (type.equals("in")) {
				queryWrapper.eq("d.payee", authUser.getId());
				queryWrapper.eq("d.payee_owner", walletFlag);
			} else if (type.equals("out")) {
				queryWrapper.eq("d.payer", authUser.getId());
				queryWrapper.eq("d.payer_owner", walletFlag);
				queryWrapper.and(wr -> wr.and(w -> w.ne("r.recharge_type", "5")).or(w -> w.isNull("r.recharge_type")));
			}
			queryWrapper.groupBy("d.sn");
			queryWrapper.orderByDesc("d.create_time");
			PageVO pageVO = new PageVO();
			BeanUtil.copyProperties(page, pageVO);
			pageVO.setPageSize(999999999);
			pageVO.setPageNumber(1);
			depositPage = walletDetailService.getByPage(pageVO, queryWrapper, authUser.getId(), type, walletFlag);
			for (WalletDetailVO walletDetailVO : depositPage.getRecords()) {
				QueryWrapper<WalletDetailVO> wrapper = new QueryWrapper<>();
				wrapper.gt("d.money", 0);
				if (type.equals("all")) {
					wrapper.and(wr -> wr.and(
							w -> w.eq("d.payer", authUser.getId()).eq("d.payer_owner", walletFlag).eq("d.STATUS", "fu"))
							.or(w -> w.eq("d.payee", authUser.getId()).eq("d.payee_owner", walletFlag).eq("d.STATUS",
									"shou")));
				} else if (type.equals("in")) {
					wrapper.eq("d.payee_owner", walletFlag);
					wrapper.eq("d.payee", authUser.getId());
				} else if (type.equals("out")) {
					wrapper.eq("d.payer_owner", walletFlag);
					wrapper.eq("d.payer", authUser.getId());
				}
				wrapper.eq("d.sn", walletDetailVO.getSn());

				List<WalletDetailVO> walletDetailVOList = walletDetailService.getListBySn(wrapper, authUser.getId(),
						type, walletFlag);
				if (walletDetailVOList.size() != 1) {
					Double sum = 0D;
					walletDetailVO.setWalletDetailList(walletDetailVOList);
					for (WalletDetailVO walletDetailVO1 : walletDetailVOList)
						sum = CurrencyUtil.add(sum, walletDetailVO1.getSumMoney());
					walletDetailVO.setSumMoney(sum);
				}
				list.add(walletDetailVO);
			}
			// 查询充值记录
			if (type.equals("in") || type.equals("all")) {
				List<Recharge> rechargeList = rechargeService
						.list(new QueryWrapper<Recharge>().eq("member_id", authUser.getId()).eq("pay_status", "PAID")
								.in("recharge_type", "4", "42", "43", "44", "46"));
				for (Recharge recharge : rechargeList) {
					WalletDetailVO walletDetailVO = new WalletDetailVO();
					walletDetailVO.setSn(recharge.getRechargeSn());
					walletDetailVO.setCreateTime(recharge.getCreateTime());
					walletDetailVO.setSumMoney(recharge.getYiBei());
					walletDetailVO.setStatus("shou");
					walletDetailVO.setType("Y");
					walletDetailVO.setTransactionType("recharge");
					walletDetailVO.setRealMoney(recharge.getRechargeMoney());
					list.add(walletDetailVO);
				}
			}
			// 查询充值钱包的转入
			/*
			 * if(type.equals("in") || type.equals("all")){ List<TransferVO> transferList =
			 * transferService.transferList(new QueryWrapper<Transfer>()
			 * .eq("payee",authUser.getId()) .eq("state","success") .eq("delete_flag",0)
			 * .eq("payment_wallet",walletFlag)); for (TransferVO transfer : transferList) {
			 * WalletDetailVO walletDetailVO = new WalletDetailVO();
			 * walletDetailVO.setSn(transfer.getSn());
			 * walletDetailVO.setCreateTime(transfer.getCreateTime());
			 * walletDetailVO.setSumMoney(Double.valueOf(transfer.getTransferMoney()));
			 * walletDetailVO.setStatus("shou"); walletDetailVO.setType("Z");
			 * walletDetailVO.setTransactionType("transfer");
			 * walletDetailVO.setNickName(transfer.getPayeeName());
			 * walletDetailVO.setMobile(transfer.getPayeePhone());
			 * walletDetailVO.setCardName(transfer.getCardName());
			 * walletDetailVO.setFace(transfer.getFace()); list.add(walletDetailVO); } }
			 */
			// 排序
			ComparatorTime comparator = new ComparatorTime();
			Collections.sort(list, comparator);
			// 排序后逆序
			Collections.reverse(list);
			//总数
			int total = list.size();
			//总页数
			int pageSum = total % page.getPageSize() == 0 ? total / page.getPageSize() : total / page.getPageSize() + 1;
			//分页
			Object subList = list.stream().skip((page.getPageNumber() - 1) * page.getPageSize()).limit(page.getPageSize()).
				collect(Collectors.toList());
			list = (List)subList;
			depositPage.setSize(page.getPageSize());
			depositPage.setTotal(total);
		}
		return ResultUtil.data(depositPage.setRecords(list));
	}

	@ApiOperation(value = "分页获取预存款变动日志")
	@RequestMapping("/sum")
	public ResultMessage<Map> getByPageSum(String walletFlag) {
		List list = new ArrayList<>();
		// 获取当前登录用户
		AuthUser authUser = UserContext.getCurrentUser();
		Map<String, Object> map = new HashMap<>();
		Double zhichu = null;
		Double shouru = null;
		if (StringUtils.isNotBlank(walletFlag) && (walletFlag.equals(WalletOwnerEnum.PROMOTE_HY.name())
				|| walletFlag.equals(WalletOwnerEnum.PROMOTE.name())
				|| walletFlag.equals(WalletOwnerEnum.PROMOTE_FW.name()))) {
			QueryWrapper<WalletDetail> queryWrapper = new QueryWrapper<>();
			queryWrapper.gt("d.money", 0);
			queryWrapper.eq("d.payee_owner", walletFlag);
			queryWrapper.eq("d.payee", authUser.getId());
			queryWrapper.orderByDesc("d.create_time");
			shouru = walletDetailService.getByPageSum(queryWrapper);
			QueryWrapper<WalletDetail> wrapper = new QueryWrapper<>();
			wrapper.gt("d.money", 0);
			wrapper.eq("d.payer_owner", walletFlag);
			wrapper.eq("d.payer", authUser.getId());
			wrapper.orderByDesc("d.create_time");
			zhichu = walletDetailService.getByPageSum(wrapper);
			map.put("zhichu", zhichu);
			map.put("shouru", shouru);
		} else if (StringUtils.isNotBlank(walletFlag) && walletFlag.equals(WalletOwnerEnum.SALE.name())) {
			QueryWrapper<WalletDetail> queryWrapper = new QueryWrapper<>();
			queryWrapper.gt("d.money", 0);
			queryWrapper.eq("d.payee_owner", walletFlag);
			queryWrapper.eq("d.payee", authUser.getId());
			queryWrapper.orderByDesc("d.create_time");
			shouru = walletDetailService.getByPageSum(queryWrapper);
			QueryWrapper<WalletDetail> wrapper = new QueryWrapper<>();
			wrapper.gt("d.money", 0);
			wrapper.eq("d.payer_owner", walletFlag);
			wrapper.eq("d.payer", authUser.getId());
			wrapper.orderByDesc("d.create_time");
			zhichu = walletDetailService.getByPageSum(wrapper);
			map.put("zhichu", zhichu);
			map.put("shouru", shouru);
		} else if (StringUtils.isNotBlank(walletFlag) && walletFlag.equals(WalletOwnerEnum.RECHARGE.name())) {
			QueryWrapper<WalletDetail> queryWrapper = new QueryWrapper();
			queryWrapper.gt("d.money", 0);
			queryWrapper.eq("d.payee_owner", walletFlag);
			queryWrapper.eq("d.payee", authUser.getId());
			queryWrapper.orderByDesc("d.create_time");
			shouru = walletDetailService.getByPageSum(queryWrapper);
			QueryWrapper<WalletDetail> wrapper = new QueryWrapper<>();
			wrapper.gt("d.money", 0);
			wrapper.eq("d.payer_owner", walletFlag);
			wrapper.eq("d.payer", authUser.getId());
			wrapper.and(wr -> wr.and(w -> w.ne("r.recharge_type", "5")).or(w -> w.isNull("r.recharge_type")));
			wrapper.orderByDesc("d.create_time");
			zhichu = walletDetailService.getByPageSum(wrapper);
			Double shouru1 = rechargeService.getByPageSum(new QueryWrapper<Recharge>().eq("member_id", authUser.getId())
					.eq("pay_status", "PAID").in("recharge_type", "4", "42", "43", "44", "46"));

			shouru = shouru + shouru1;
			map.put("zhichu", zhichu);
			map.put("shouru", shouru);
			// 查询充值记录
			/*
			 * List<Recharge> rechargeList = rechargeService.list(new
			 * QueryWrapper<Recharge>() .eq("member_id",authUser.getId())
			 * .eq("pay_status","PAID") .isNotNull("yi_bei"));
			 */
		}
		return ResultUtil.data(map);
	}

}
