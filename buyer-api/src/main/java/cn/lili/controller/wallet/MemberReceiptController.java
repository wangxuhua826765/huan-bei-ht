package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.vo.MemberReceiptAddVO;
import cn.lili.modules.member.entity.vo.MemberReceiptVO;
import cn.lili.modules.member.service.MemberReceiptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 买家端,会员发票接口
 *
 * @author paulG
 * @since 2021-03-29 14:10:16
 */
@RestController
@Api(tags = "买家端,会员发票接口")
@RequestMapping("/buyer/member/receipt")
public class MemberReceiptController {

	@Autowired
	private MemberReceiptService memberReceiptService;

	@ApiOperation(value = "查询会员发票列表")
	@RequestMapping("/page")
	public ResultMessage<Object> page(MemberReceiptVO memberReceiptVO) {
		memberReceiptVO.setMemberId(UserContext.getCurrentUser().getId());
		return ResultUtil.data(memberReceiptService.getPage(memberReceiptVO));
	}

	@ApiOperation(value = "新增会员发票")
	@PostMapping
	public ResultMessage<Object> add(@RequestBody MemberReceiptAddVO memberReceiptAddVO) {
		return ResultUtil
				.data(memberReceiptService.addMemberReceipt(memberReceiptAddVO, UserContext.getCurrentUser().getId()));
	}

	@ApiOperation(value = "修改会员发票")
	@ApiImplicitParam(name = "id", value = "会员发票id", required = true, paramType = "path")
	@PutMapping
	public ResultMessage<Object> update(@RequestBody MemberReceiptAddVO memberReceiptAddVO) {
		return ResultUtil
				.data(memberReceiptService.editMemberReceipt(memberReceiptAddVO, UserContext.getCurrentUser().getId()));
	}

	@ApiOperation(value = "会员发票删除")
	@ApiImplicitParam(name = "id", value = "会员发票id", required = true, paramType = "path")
	@GetMapping
	public ResultMessage<Boolean> deleteMessage(@RequestParam("id") String id) {
		return ResultUtil.data(memberReceiptService.deleteMemberReceipt(id));
	}

}
