package cn.lili.controller.message;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.message.entity.dos.RollMessage;
import cn.lili.modules.message.service.RollMessageService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/buyer/message")
public class RollMessageBuyerController {

	@Autowired
	private RollMessageService rollMessageService;

	/**
	 * 首页滚动消息查询（150条）
	 */
	@RequestMapping("/list")
	public ResultMessage<List<RollMessage>> selectList() {
		QueryWrapper<RollMessage> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("del_flag", 0);
		queryWrapper.eq("type", 0);
		queryWrapper.eq("status", 1);
		queryWrapper.orderByDesc("create_time");
		queryWrapper.last("limit 150");
		List<RollMessage> list = rollMessageService.list(queryWrapper);
		if (list.size() == 150) {
			ResultUtil.data(list);
		}
		int a = 150 - list.size();
		QueryWrapper<RollMessage> wrapper = new QueryWrapper<>();
		wrapper.eq("del_flag", 0);
		wrapper.notIn("type", 0);
		wrapper.orderByDesc("create_time");
		wrapper.last("limit " + a);
		List<RollMessage> list1 = rollMessageService.list(wrapper);
		list.addAll(list1);
		return ResultUtil.data(list);
	}

}
