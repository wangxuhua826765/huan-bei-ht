package cn.lili.buyer.test.cart;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ByteUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.KeyUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.AsymmetricCrypto;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.signers.JWTSignerUtil;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;

public class TestMethod {

	@Test
	public void testStr() {
		String code = StrUtil.subPre("131313", 2);
		code = StrUtil.concat(true, code, "0000");
		System.out.println(code);
	}

	@Test
	public void jwtTest() {
		KeyPair kp = KeyUtil.generateKeyPair("RSA");
		System.out.println(Base64.encode(kp.getPublic().getEncoded()));

		System.out.println(Base64.encode(kp.getPrivate().getEncoded()));
	}

	@Test
	public void jwtjm() {
		String p = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDqpgjEvDTPjeUZQ5oKjXTKCJSZhl4ZHmqNtqzKZjrwNyeByrynlBxiF+jhyaOHwIiOsRBFTtqP0LfV+/GhkJkNazv/UHLWPtESnrGWL1Vr2ntWymehoJy+Mc2lmGYK7Iew7RFTjWM20rdDCiSBiwNEIdKuS+9UzG+d6AGpJgA9OwIDAQAB";
		String pass = "1LNdiJ3G0kBBJWN5oB3pa/ljWu3uI7PkBbkVy59smErsY/UXFHw2DqPm8EPfdeYVBB6Q5L6b0ixre7t77gdjbRfhskT67wIKR6cpXAui+1WDi9XfQFczc7ag6Ccecga1zS3GiHN92VatuB4at/NI4Jec7zIGntBvmz3rg1bJoKE=";
		String e6 = SecureUtil.rsa(null, p).decryptStr(pass, KeyType.PublicKey);
		System.out.println(e6);
	}

	@Test
	public void jwtj() {
		String p = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOqmCMS8NM+N5RlDmgqNdMoIlJmGXhkeao22rMpmOvA3J4HKvKeUHGIX6OHJo4fAiI6xEEVO2o/Qt9X78aGQmQ1rO/9QctY+0RKesZYvVWvae1bKZ6GgnL4xzaWYZgrsh7DtEVONYzbSt0MKJIGLA0Qh0q5L71TMb53oAakmAD07AgMBAAECgYBv50/eNx1+cF62TjmL18s/QJlRoDWHCCzruZctDVUFU2BZcyzGRInGtD1hVI7TAlx9pG6shv8PwAu0Fqu1D+cVB+aCkUGfaDRsJPEzYbYrWAgO3r8+P6cpNXZnXp9tJdOlxdqMTEvq7JQm/KZBO6JAY9vXzQ3Eft4G6HQRWCylpQJBAPj+73nb9NYp40s+G39AjbMDRNbkmUR8skMeEtyBgBRM3F9sDbrFSMa8Rw9tO9nw8KHHiJG9eJBi4eXjTilnmfcCQQDxP8gPVXWGWrh/rstlk9ARg6tMGUbjsj/yeSFS2eqpsw3o1eFC3cJWJc0gGdZiKU4ptuhWhSXzat6HVZNTRYXdAkEA2HVfoDM8RFYD12x1oQlwZdgIpzvxld1FZx2kzfbybjNnPLkFsoFBRCt3Jw0SA+P2fN2mdvQc9IBR23TghRQgfwJAFl3Pcnx91OL8T/ooqVY26CjI7Nk7FUklXJmWWVQSyYDsPTX3GHfu2IXQqrU6hwdFU+C9UaguiI7T6sAoVIQRkQJAWapdlT/8RQaO1vbw2X/3bM8mYCKLeNlul8Ce5EaXWmpxE8KM+TDyxZqy1g3V2Yn+GZx1We2AM+ju+ILMaPCljA==";
		String pass = "JQoiIiuyaoW0X3faVPnONe93yX2xwD3igsy/w5JpIXlh/qJxcugScRYWniPuyAoRfbSLIiiXLvsldkTEvvJIDgWTIjo4gMpqiPnSjfbK/7OH9HrGuL0DuSzPLC07SU6rPtHQKVhk4T6gen+7vMVOxEsKi1RuO7HrfO2MQu+vPpY=";
		String e6 = SecureUtil.rsa(p, null).encryptBase64("123", KeyType.PrivateKey);
		System.out.println(e6);

		String e7 = SecureUtil.rsa(p, null).decryptStr(e6, KeyType.PrivateKey);
		System.out.println(e7);

	}
}
