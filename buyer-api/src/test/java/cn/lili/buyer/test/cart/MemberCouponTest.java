package cn.lili.buyer.test.cart;

import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import cn.lili.modules.bank.entity.dos.MemberBank;
import cn.lili.modules.bank.service.MemberBankService;
import cn.lili.modules.bank.service.impl.MemberBankServiceImpl;
import cn.lili.modules.member.entity.vo.MemberReceiptAddVO;
import cn.lili.modules.member.service.MemberReceiptService;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.order.order.entity.dto.OrderSearchParams;
import cn.lili.modules.order.order.entity.vo.OrderSimpleVO;
import cn.lili.modules.order.order.service.OrderService;
import cn.lili.modules.page.entity.dto.ArticleCategoryDto;
import cn.lili.modules.page.entity.dto.ArticleSearchParams;
import cn.lili.modules.page.entity.enums.ArticleCategoryEnum;
import cn.lili.modules.page.service.ArticleService;
import cn.lili.modules.promotion.service.MemberCouponService;
import cn.lili.modules.wallet.entity.dos.MemberWallet;
import cn.lili.modules.wallet.service.MemberWalletService;
import cn.lili.modules.wallet.serviceimpl.MemberWalletServiceImpl;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author paulG
 * @since 2020/11/27
 **/
@ExtendWith(SpringExtension.class)
@SpringBootTest
class MemberCouponTest {

	@Autowired
	private MemberCouponService memberCouponService;

	@Autowired
	private MemberWalletServiceImpl memberWalletService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private ArticleService articleService;

	@Test
	public void getPlatformAbout() {
		ArticleSearchParams articleSearchParams = new ArticleSearchParams();
		articleSearchParams.setType(ArticleCategoryEnum.HELP_CENTER.value());
		List<ArticleCategoryDto> list = articleService.getPlatformAbout(articleSearchParams);
		System.out.println(list);
	}

	@Autowired
	private MemberBankService memberBankService;
	@Autowired
	private MemberService memberService;

	@Test
	public void bank() throws Exception {
		// memberService.revokePartnerPage("1513020093406318593");
		// memberService.applyRevokePartner("cea ","1513020093406318593");
		// memberService.applyRevokePage("1513020093406318593");
		// memberService.applyRevoke("1545003806684291074");
		MemberBank memberBank = memberBankService
				.identify("https://rsyl.oss-cn-beijing.aliyuncs.com/9e596c8adf3b483bbbc8d6f503f14d33.png");
		System.out.println(JSON.toJSONString(memberBank));
	}

	@Test
	void receiveCoupon() {
		memberCouponService.receiveCoupon("1333318596239843328", "1326834797335306240", "1");
		assertTrue(true);
	}

	@Autowired
	private MemberReceiptService memberReceiptService;

	@Test
	void memberReceiptAddVO() {
		MemberReceiptAddVO memberReceiptAddVO = new MemberReceiptAddVO();
		memberReceiptService.addMemberReceipt(memberReceiptAddVO, "memberId");
	}

	/**
	 * //钱包类型 RECHARGE PROMOTE SALE
	 * 
	 * @param sn
	 *            交易流水
	 * @param memberId
	 *            用户ID
	 * @param money
	 *            交易金额
	 * @param type
	 *            交易类型 hy 会员,xf 消费
	 */
	@Test
	void testWallet() {
		String sn = IdUtil.fastSimpleUUID();
		System.out.println(sn);
		MemberWallet from = memberWalletService.getMemberWalletInfo("1508029014072274946", "SALE");

		MemberWallet to = memberWalletService.getMemberWalletInfo("admin", "SALE", "平台");

		memberWalletService.payOrder(sn, from, to, 100D, 10D, 6D);
	}

	@Test
	void payOrder() {
		String sn = IdUtil.fastSimpleUUID();
		System.out.println(sn);

		MemberWallet from = memberWalletService.getMemberWalletInfo("1508029014072274946", "SALE");

		MemberWallet to = memberWalletService.getMemberWalletInfo("1508058378788843522", "SALE");

		memberWalletService.payOrder(sn, from, to, 500D, 10D, 30D);
	}

	@Test
	void compliteOrder() {
		MemberWallet to = memberWalletService.getMemberWalletInfo("1508058378788843522", "SALE");
		memberWalletService.complateOrder("bfd28ebcd62d47d0a404839c786845de", to, 500D, 20D);
	}

	@Test
	void tk() {
		// memberWalletService.cancelOrder("bfd28ebcd62d47d0a404839c786845de",false);
	}

	@Test
	void hy() {
		String sn = IdUtil.fastSimpleUUID();
		System.out.println(sn);
		MemberWallet from = memberWalletService.getMemberWalletInfo("1508029014072274946", "SALE");
		memberWalletService.upgradHy(sn, from, 258D);
	}

	@Test
	void scan() {
		String sn = IdUtil.fastSimpleUUID();
		System.out.println(sn);
		MemberWallet from = memberWalletService.getMemberWalletInfo("1508029014072274946", "SALE");

		MemberWallet to = memberWalletService.getMemberWalletInfo("1508058378788843522", "SALE");

		memberWalletService.scanPay(sn, from, to, 400D, 24D);
	}

	@Test
	void testOrder() {
		OrderSearchParams params = new OrderSearchParams();
		params.setPageSize(500);
		params.setPageNumber(0);
		IPage<OrderSimpleVO> data = orderService.queryByParams(params);
		System.out.println(JSONUtil.toJsonStr(data));
	}

}
