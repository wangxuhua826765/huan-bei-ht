package cn.lili.controller.activity;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.activity.entity.vo.ActivityExamineVO;
import cn.lili.modules.activity.service.ActivityExamineService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(tags = "商家端-活动审核管理")
@RequestMapping("/store/activity/examine")
public class ActivityExamineStoreController {

	@Autowired
	private ActivityExamineService activityExamineService;

	/**
	 * 根据活动id查询审核详情
	 */
	@RequestMapping("/activityId")
	public ResultMessage getGoodsByActivityId(String activityId) {
		List<ActivityExamineVO> activityExamines = activityExamineService.getByActivityId(activityId);
		return ResultUtil.data(activityExamines);
	}

}
