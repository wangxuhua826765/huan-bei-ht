package cn.lili.controller.activity;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.goods.service.GoodsService;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.vos.StoreVO;
import cn.lili.modules.store.service.StoreService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(tags = "商家端-活动商品管理")
@RequestMapping("/store/activity/goods")
public class ActivityGoodsStoreController {

	@Autowired
	private GoodsService goodsService;

	@Autowired
	private StoreService storeService;

	/**
	 * 根据活动id查询商品详情
	 */
	@GetMapping
	public ResultMessage getGoodsByActivityId(String activityId) {
		StoreVO storeVO = storeService.getStoreDetail();
		List<GoodsVO> goodsVOList = goodsService.getGoodsByActivityId(activityId, storeVO.getId());
		storeVO.setGoodsVOList(goodsVOList);
		return ResultUtil.data(storeVO);
	}

}
