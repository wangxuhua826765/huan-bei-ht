package cn.lili.controller.member;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.vo.ExtensionVO;
import cn.lili.modules.member.entity.vo.MemberSearchVO;
import cn.lili.modules.member.entity.vo.MemberVO;
import cn.lili.modules.member.service.EextensionService;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.member.serviceimpl.ExtensionServiceImpl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * create by yudan on 2022/3/13
 */

@RestController
@Api(tags = "商家端,推广管理")
@RequestMapping("/store/Extension")
public class SellerExtensionController {

	@Autowired
	private EextensionService eextensionService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private ExtensionServiceImpl extensionService;

	@ApiOperation(value = "推广管理列表")
	@GetMapping
	public ResultMessage<IPage<ExtensionVO>> getByPage(ExtensionVO extension, PageVO page) {
		return ResultUtil.data(eextensionService.getExtensionPage(extension, page));
	}

	@ApiOperation(value = "获取下级会员列表(天使/事业/焕商天使)")
	@RequestMapping("/getDisableMemberPageAll")
	public ResultMessage<IPage<MemberVO>> getDisableMemberPageAll(MemberSearchVO memberSearchVO, PageVO page) {
		IPage<MemberVO> p = memberService.getDisableMemberPageAll(memberSearchVO, page);
		if (p != null) {
			return ResultUtil.data(p);
		} else {
			return ResultUtil.data(new Page<>());
		}
	}

	@ApiOperation(value = "解绑会员")
	@RequestMapping(value = "/unBindingMemberList")
	public ResultMessage<Object> unBindingMemberList(
			@RequestParam(value = "choseBindMemberIds") List<String> choseBindMemberIds) {
		// 解绑会员
		Integer count = extensionService.updateBymemberId(choseBindMemberIds);
		if (count > 0) {
			return ResultUtil.success(ResultCode.SUCCESS);
		} else {
			return ResultUtil.success(ResultCode.USER_NOT_BINDING);
		}
	}

}
