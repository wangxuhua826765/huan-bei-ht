package cn.lili.controller.whitebar;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.DateUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.wallet.entity.dos.MemberWallet;
import cn.lili.modules.wallet.entity.dos.WalletLogDetail;
import cn.lili.modules.wallet.entity.dto.MemberWalletUpdateDTO;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.service.MemberWalletService;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.service.CreditManagementService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 授信管理
 *
 * @author 贾送兵
 * @since 2022-02-17 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "授信管理")
@RequestMapping("/store/whitebar/credit")
public class CreditStoreController {

	/**
	 * 授信管理
	 */
	@Autowired
	private CreditManagementService creditManagementService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberWalletService memberWalletService;

	@ApiOperation(value = "授信管理")
	@ApiImplicitParams({@ApiImplicitParam(name = "bName", value = "商家名称", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<CreditManagementVO>> getByPage(
			CreditManagementSearchParams creditManagementSearchParams) {
		AuthUser currentUser = UserContext.getCurrentUser();
		creditManagementSearchParams.setMemberId(currentUser.getId());
		return ResultUtil.data(creditManagementService.creditManagementPage(creditManagementSearchParams));
	}

	@RequestMapping(value = "/add")
	@ApiOperation(value = "新增")
	public ResultMessage<CreditManagement> save(CreditManagement creditManagement) {
		AuthUser currentUser = UserContext.getCurrentUser();
		creditManagement.setMemberId(currentUser.getId());
		creditManagement.setAuditStatus("0");
		creditManagementService.save(creditManagement);
		return ResultUtil.data(creditManagement);
	}

	@PutMapping("/update/{id}")
	@ApiOperation(value = "更新")
	public ResultMessage<CreditManagement> update(@PathVariable String id, CreditManagement department) {
		creditManagementService.updateById(department);
		return ResultUtil.data(department);
	}

	@RequestMapping("/repay/{id}")
	@ApiOperation(value = "还款")
	public ResultMessage<CreditManagement> repay(@PathVariable String id, CreditManagement department, String owner) {
		Member member = memberService.getById(department.getMemberId());

		MemberWallet from = memberWalletService.getMemberWalletInfo(member.getId(), owner);
		memberWalletService.repayOrder(id, from, null, department.getCreditLine());
		// memberWalletService.reduce(new MemberWalletUpdateDTO(
		// department.getCreditLine(),
		// member.getId(),
		// "授信订单【" + id + "】还款金额[" + department.getCreditLine() + "]",
		// DepositServiceTypeEnum.WALLET_PAY.name(), owner),new WalletLogDetail(
		// null,
		// null,
		// "10",
		// null,
		// member.getId(),
		// member.getNickName(),
		// null,
		// null,
		// null,
		// null,
		// null,
		// department.getCreditLine()
		// ));
		department.setRepaymentStatus("1");
		// department.setRepaymentDate(String.valueOf(new Date()));
		creditManagementService.updateById(department);
		return ResultUtil.success();
	}

	@DeleteMapping(value = "/delete/{ids}")
	@ApiOperation(value = "删除")
	public ResultMessage<Object> delAllByIds(@PathVariable List<String> ids) {
		creditManagementService.deleteByIds(ids);
		return ResultUtil.success();
	}

}
