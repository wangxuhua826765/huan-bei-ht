package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.order.order.entity.vo.OrderItemVO;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.transfer.entity.vo.TransferVO;
import cn.lili.modules.transfer.service.TransferService;
import cn.lili.modules.wallet.service.MemberWithdrawItemService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * create by yudan on 2022/8/2
 */
@RestController
@Api(tags = "商家端,会员转账接口")
@RequestMapping("/store/transfer")
public class TransferSellerController {

	@Autowired
	private TransferService transferService;
	@Autowired
	private RegionService regionService;

	@Autowired
	private MemberWithdrawItemService memberWithdrawItemService;

	@ApiOperation(value = "查询转账列表")
	@GetMapping
	public ResultMessage<IPage<TransferVO>> queryMineOrder(PageVO pageVO, TransferVO transferVO) {
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		QueryWrapper<TransferVO> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq(StringUtils.isNotEmpty(transferVO.getPaymentWallet()), "t.payment_wallet",
				transferVO.getPaymentWallet());
		queryWrapper.eq("t.state", "success");
		// 付款人
		// queryWrapper.eq("t.payer",currentUser.getId());
		if ("in".equals(transferVO.getType())) {
			// 收款记录
			queryWrapper.eq("t.payee", currentUser.getMemberId());
		} else if ("out".equals(transferVO.getType())) {
			// 付款人
			queryWrapper.eq("t.payer", currentUser.getMemberId());
		} else {
			queryWrapper.and(
					qw -> qw.eq("t.payee", currentUser.getMemberId()).or().eq("t.payer", currentUser.getMemberId()));
		}
		// 时间查询
		if (StringUtils.isNotEmpty(transferVO.getStartTime()) && StringUtils.isNotEmpty(transferVO.getEndTime())) {
			queryWrapper.le("DATE_FORMAT(t.payment_time,'%Y-%m-%d')", transferVO.getEndTime());
			queryWrapper.ge("DATE_FORMAT(t.payment_time,'%Y-%m-%d')", transferVO.getStartTime());
		}
		// 收款人手机号
		queryWrapper.eq(StringUtils.isNotEmpty(transferVO.getPayeePhone()), "t.payee_phone",
				transferVO.getPayeePhone());
		// 收款人级别
		queryWrapper.eq(StringUtils.isNotEmpty(transferVO.getPayeeGradeName()), "mc.name",
				transferVO.getPayeeGradeName());
		// 区域
		if (StringUtils.isNotEmpty(transferVO.getRegionId())) {
			List list = regionService.getItemAdCod(transferVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "m.location", list);
		}
		queryWrapper.orderByDesc("payment_time");
		IPage<TransferVO> transferIPage = transferService.queryList(PageUtil.initPage(pageVO), queryWrapper);
		if (transferIPage != null && transferIPage.getRecords().size() > 0) {
			for (TransferVO transfer : transferIPage.getRecords()) {
				Double commission = 0D;
				if (transfer.getCommission() != null) {
					commission = transfer.getCommission();
				}
				if (currentUser.getMemberId().equals(transfer.getPayee())) {
					transfer.setType("转入");
					transfer.setTransferMoneyStr(
							"+" + CurrencyUtil.add(transfer.getTransferMoney(), commission).toString());
				} else if (currentUser.getMemberId().equals(transfer.getPayer())) {
					transfer.setType("转出");
					transfer.setTransferMoneyStr(
							"-" + CurrencyUtil.add(transfer.getTransferMoney(), commission).toString());
				}
			}
		}
		return ResultUtil.data(transferIPage);
	}

	@ApiOperation(value = "扣率信息详情")
	@RequestMapping("/getFeeList")
	public ResultMessage<IPage<OrderItemVO>> getFeeList(PageVO page, String sn) {
		// 构建查询 返回数据
		IPage<OrderItemVO> orderItemVOIPage = memberWithdrawItemService.findItemByTransferSn(page, sn);
		return ResultUtil.data(orderItemVOIPage);
	}

}
