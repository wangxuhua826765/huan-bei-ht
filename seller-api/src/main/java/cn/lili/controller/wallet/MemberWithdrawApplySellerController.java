package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.order.order.entity.vo.OrderItemVO;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawApply;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyQueryVO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyVO;
import cn.lili.modules.wallet.service.MemberWithdrawApplyService;
import cn.lili.modules.wallet.service.MemberWithdrawItemService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/8/2
 */
@RestController
@Api(tags = "商家端,余额提现记录接口")
@RequestMapping("/store/withdraw-apply")
@Transactional(rollbackFor = Exception.class)
public class MemberWithdrawApplySellerController {

	@Autowired
	private MemberWithdrawApplyService memberWithdrawApplyService;

	@Autowired
	private RegionService regionService;

	@Autowired
	private MemberWithdrawItemService memberWithdrawItemService;

	@ApiOperation(value = "提现明细-销售钱包")
	@RequestMapping("/getSaleList")
	public ResultMessage<Map<String, Object>> getSaleList(PageVO page,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO) {
		Map<String, Object> map = new HashMap<>();
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		Double all = 0D;
		// 构建查询 返回数据
		QueryWrapper<MemberWithdrawApply> queryWrapper = new QueryWrapper<>();
		// 用户id
		queryWrapper.eq(!StringUtils.isEmpty(currentUser.getStoreId()), "member.id", currentUser.getMemberId());
		// 手机号
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getMobile()), "member.mobile",
				memberWithdrawApplyQueryVO.getMobile());
		// 提现状态
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getApplyStatus()), "apply.apply_status",
				memberWithdrawApplyQueryVO.getApplyStatus());
		// 创建时间
		if (!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getCreateDate())) {
			queryWrapper.eq("DATE_FORMAT(apply.create_time,'%Y-%m')", memberWithdrawApplyQueryVO.getCreateDate());
		}
		// 区域
		if (StringUtils.isNotEmpty(memberWithdrawApplyQueryVO.getRegionId())) {
			List list = regionService.getItemAdCod(memberWithdrawApplyQueryVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "member.location", list);
		}
		// 订单来源
		queryWrapper.likeLeft("apply.owner", WalletOwnerEnum.SALE.name());
		queryWrapper.eq("apply.delete_flag", 0);
		queryWrapper.orderByDesc("apply.create_time");

		IPage<MemberWithdrawApplyVO> memberWithdrawApplyPage = memberWithdrawApplyService.getListPage(page,
				queryWrapper);
		List<MemberWithdrawApplyVO> memberWithdrawApplyList = memberWithdrawApplyService.getListAll(queryWrapper);
		if (CollectionUtils.isNotEmpty(memberWithdrawApplyList)) {
			for (MemberWithdrawApplyVO memberWithdrawApplyVO : memberWithdrawApplyList) {
				if (memberWithdrawApplyVO.getApplyStatus().equals("FINISH"))
					all = CurrencyUtil.add(all, memberWithdrawApplyVO.getApplyMoney());
			}
		}
		map.put("all", BigDecimal.valueOf(all).setScale(2, RoundingMode.DOWN).doubleValue());
		map.put("data", memberWithdrawApplyPage);
		return ResultUtil.data(map);
	}

	@ApiOperation(value = "提现明细-销售钱包")
	@RequestMapping("/getPromoteList")
	public ResultMessage<Map<String, Object>> getPromoteList(PageVO page,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO) {
		Map<String, Object> map = new HashMap<>();
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		Double all = 0D;
		// 构建查询 返回数据
		QueryWrapper<MemberWithdrawApply> queryWrapper = new QueryWrapper<>();
		// 用户id
		queryWrapper.eq(!StringUtils.isEmpty(currentUser.getStoreId()), "member.id", currentUser.getMemberId());
		// 手机号
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getMobile()), "member.mobile",
				memberWithdrawApplyQueryVO.getMobile());
		// 提现状态
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getApplyStatus()), "apply.apply_status",
				memberWithdrawApplyQueryVO.getApplyStatus());
		// 创建时间
		if (!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getCreateDate())) {
			queryWrapper.eq("DATE_FORMAT(apply.create_time,'%Y-%m')", memberWithdrawApplyQueryVO.getCreateDate());
		}
		// 有无店铺
		if ("0".equals(memberWithdrawApplyQueryVO.getHaveStore())) {
			// 没有
			queryWrapper.eq("member.have_store", false);
		} else if ("1".equals(memberWithdrawApplyQueryVO.getHaveStore())) {
			// 有
			queryWrapper.eq("member.have_store", true);
		}

		// 身份类型
		queryWrapper.likeLeft(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getPartnerName()), "role.role_name",
				memberWithdrawApplyQueryVO.getPartnerName());

		// 区域
		if (StringUtils.isNotEmpty(memberWithdrawApplyQueryVO.getRegionId())) {
			List list = regionService.getItemAdCod(memberWithdrawApplyQueryVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "member.location", list);
		}
		// 订单来源
		queryWrapper.likeLeft("apply.owner", WalletOwnerEnum.PROMOTE.name());
		queryWrapper.eq("apply.delete_flag", 0);
		queryWrapper.orderByDesc("apply.create_time");

		IPage<MemberWithdrawApplyVO> memberWithdrawApplyPage = memberWithdrawApplyService.getListPage(page,
				queryWrapper);
		List<MemberWithdrawApplyVO> memberWithdrawApplyList = memberWithdrawApplyService.getListAll(queryWrapper);
		if (CollectionUtils.isNotEmpty(memberWithdrawApplyList)) {
			for (MemberWithdrawApplyVO memberWithdrawApplyVO : memberWithdrawApplyList) {
				if (memberWithdrawApplyVO.getApplyStatus().equals("FINISH"))
					all = CurrencyUtil.add(all, memberWithdrawApplyVO.getRealMoney());
			}
		}
		map.put("all", BigDecimal.valueOf(all).setScale(2, RoundingMode.DOWN).doubleValue());
		map.put("data", memberWithdrawApplyPage);
		return ResultUtil.data(map);
	}

	@ApiOperation(value = "扣率信息详情")
	@RequestMapping("/getFeeList")
	public ResultMessage<IPage<OrderItemVO>> getFeeList(PageVO page, String sn) {
		// 构建查询 返回数据
		IPage<OrderItemVO> orderItemVOIPage = memberWithdrawItemService.findItemBySn(page, sn);
		return ResultUtil.data(orderItemVOIPage);
	}

}
