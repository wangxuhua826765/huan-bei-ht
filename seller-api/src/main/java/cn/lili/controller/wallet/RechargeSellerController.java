package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.order.order.entity.enums.PayStatusEnum;
import cn.lili.modules.order.trade.entity.vo.RechargeQueryVO;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.vo.RechargeVO;
import cn.lili.modules.wallet.service.RechargeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商家端,预存款充值记录接口
 *
 * @author pikachu
 * @since 2020/11/16 10:07 下午
 */
@RestController
@Api(tags = "商家端,预存款充值记录接口")
@RequestMapping("/store/recharge")
@Transactional(rollbackFor = Exception.class)
public class RechargeSellerController {
	@Autowired
	private RechargeService rechargeService;

	@ApiOperation(value = "分页获取预存款充值记录")
	@GetMapping
	public ResultMessage<IPage<RechargeVO>> getByPage(PageVO page, RechargeQueryVO rechargeQueryVO) {
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		rechargeQueryVO.setMemberId(currentUser.getMemberId());
		// 构建查询 返回数据
		IPage<RechargeVO> rechargePage = rechargeService.rechargePage(page, rechargeQueryVO);
		if (CollectionUtils.isNotEmpty(rechargePage.getRecords())) {
			for (RechargeVO rechargeVO : rechargePage.getRecords()) {
				rechargeVO.setRechargeMoney(
						BigDecimal.valueOf(rechargeVO.getRechargeMoney()).setScale(2, RoundingMode.DOWN).doubleValue());
			}
		}
		return ResultUtil.data(rechargePage);
	}

	@ApiOperation(value = "充值总额")
	@RequestMapping("/rechargeMoney")
	public ResultMessage<Map> rechargeMoneyAdmin() {
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		Map<String, Object> map = new HashMap<>();
		QueryWrapper<Recharge> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("member_id", currentUser.getMemberId());
		queryWrapper.eq("pay_status", PayStatusEnum.PAID.name());
		Double rechargeMoney = 0D;
		List<Recharge> list = rechargeService.list(queryWrapper);
		if (CollectionUtils.isNotEmpty(list)) {
			for (Recharge recharge : list) {
				rechargeMoney = CurrencyUtil.add(rechargeMoney,
						recharge.getRechargeMoney() != null ? recharge.getRechargeMoney() : 0D);
			}
		}
		map.put("rechargeMoney", BigDecimal.valueOf(rechargeMoney).setScale(2, RoundingMode.DOWN).doubleValue());
		return ResultUtil.data(map);
	}

}
