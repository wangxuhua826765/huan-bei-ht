package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.vo.FinanceAdminVO;
import cn.lili.modules.member.entity.vo.MemberVO;
import cn.lili.modules.member.mapper.PartnerMapper;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.order.order.entity.vo.OrderVO;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.service.StoreService;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawApply;
import cn.lili.modules.wallet.entity.dto.CommissionDistributionSale;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.entity.vo.MemberWalletVO;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import cn.lili.modules.wallet.service.MemberWalletService;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.modules.wallet.service.WalletLogDetailService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/3/27
 */
@RestController
@Api(tags = "商家端,预存款接口")
@RequestMapping("/store/members/wallet")
public class MemberWalletSellerController {
	@Autowired
	private MemberWalletService memberWalletService;

	@Autowired
	private WalletLogDetailService walletLogDetailService;

	@Autowired
	private WalletDetailService walletDetailService;

	@Autowired
	private RegionService regionService;

	@Autowired
	private StoreService storeService;

	// @RequestMapping(value = "/allStore")
	// @ApiOperation(value = "商家钱包收入汇总")
	// public ResultMessage<Map<String ,Object>> allStore(WalletLogDetailVO
	// walletLogDetailVO) {
	// Map<String ,Object> map = new HashMap<>();
	// PageVO pageVO = new PageVO();
	// pageVO.setPageSize(999999);
	// //支出
	//// IPage<MemberWalletDTO> memberWallet =
	// memberWalletService.findCommissionVOStore(memberWalletSearchParams);
	//// Double outMoney = 0D;
	//// if(CollectionUtils.isNotEmpty(memberWallet.getRecords())){
	//// for (MemberWalletDTO c : memberWallet.getRecords()) {
	//// outMoney = CurrencyUtil.add(c.getMoney() != null ? c.getMoney() : 0D
	// ,outMoney);
	//// }
	//// }
	// QueryWrapper<WalletLogDetailVO> queryWrapper1 =new QueryWrapper<>();
	// queryWrapper1.like("d.payer",walletLogDetailVO.getMemberId());
	// queryWrapper1.in("d.transaction_type","1","4","6","7");
	// IPage<WalletLogDetailVO> list1 = walletLogDetailService.getList(pageVO,
	// queryWrapper1);
	// Double outMoney = 0D;
	// if(CollectionUtils.isNotEmpty(list1.getRecords())){
	// for (WalletLogDetailVO walletLogDetailVO1 : list1.getRecords()) {
	// outMoney = CurrencyUtil.add(walletLogDetailVO1.getMoney() != null ?
	// walletLogDetailVO1.getMoney() : 0D
	// ,outMoney);
	// }
	// }
	// map.put("outMoney",outMoney);
	// QueryWrapper<WalletLogDetailVO> queryWrapper2 =new QueryWrapper<>();
	// queryWrapper2.like("d.payee",walletLogDetailVO.getMemberId());
	// queryWrapper2.in("d.transaction_type","0","2","3","7","8");
	// IPage<WalletLogDetailVO> list2 = walletLogDetailService.getList(pageVO,
	// queryWrapper2);
	// Double inMoney = 0D;
	// if(CollectionUtils.isNotEmpty(list2.getRecords())){
	// for (WalletLogDetailVO walletLogDetailVO2 : list2.getRecords()) {
	// inMoney = CurrencyUtil.add(walletLogDetailVO2.getMoney() != null ?
	// walletLogDetailVO2.getMoney() : 0D ,inMoney);
	// }
	// }
	// map.put("inMoney",inMoney);
	// map.put("allMoney",CurrencyUtil.sub(inMoney,outMoney));
	// return ResultUtil.data(map);
	// }

	@RequestMapping(value = "/outlayStore")
	@ApiOperation(value = "商家钱包支出明细")
	public ResultMessage<IPage<WalletDetailVO>> outlayStore(PageVO pageVO, WalletDetailVO walletDetailVO) {
		QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
		queryWrapper.like("d.payer", walletDetailVO.getMemberId());
		if (StringUtils.isNotEmpty(walletDetailVO.getSn())) {
			queryWrapper.eq("d.sn", walletDetailVO.getSn());
		}
		if (StringUtils.isNotEmpty(walletDetailVO.getTransactionType())) {
			queryWrapper.eq("d.transaction_type", walletDetailVO.getTransactionType());
		}
		if (StringUtils.isNotEmpty(walletDetailVO.getStartTime())) {
			queryWrapper.ge("d.create_time", walletDetailVO.getStartTime());
			queryWrapper.le("d.create_time", walletDetailVO.getEndTime());
		} else {
			// 获取当前月份
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			String m = year + "-" + (month < 10 ? "0" + month : month);
			queryWrapper.eq("DATE_FORMAT(d.create_time,'%Y-%m')", m);
		}
		IPage<WalletDetailVO> list = walletDetailService.getOutlayList(pageVO, queryWrapper);
		list.getRecords().forEach(item -> {
			if (item.getMoney() != null)
				item.setMoney(BigDecimal.valueOf(item.getMoney()).setScale(2, RoundingMode.DOWN).doubleValue());
		});
		return ResultUtil.data(list);
	}

	@RequestMapping(value = "/incomeStore")
	@ApiOperation(value = "商家钱包收入明细")
	public ResultMessage<IPage<WalletDetailVO>> incomeStore(PageVO pageVO, WalletDetailVO walletDetailVO) {
		QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
		queryWrapper.like("d.payee", walletDetailVO.getMemberId());
		if (StringUtils.isNotEmpty(walletDetailVO.getSn())) {
			queryWrapper.eq("d.sn", walletDetailVO.getSn());
		}
		if (StringUtils.isNotEmpty(walletDetailVO.getTransactionType())) {
			queryWrapper.eq("d.transaction_type", walletDetailVO.getTransactionType());
		}
		if (StringUtils.isNotEmpty(walletDetailVO.getStartTime())) {
			queryWrapper.ge("d.create_time", walletDetailVO.getStartTime());
			queryWrapper.le("d.create_time", walletDetailVO.getEndTime());
		} else {
			// 获取当前月份
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			String m = year + "-" + (month < 10 ? "0" + month : month);
			queryWrapper.eq("DATE_FORMAT(d.create_time,'%Y-%m')", m);
		}
		IPage<WalletDetailVO> list = walletDetailService.getIncomeList(pageVO, queryWrapper);
		list.getRecords().forEach(item -> {
			if (item.getMoney() != null)
				item.setMoney(BigDecimal.valueOf(item.getMoney()).setScale(2, RoundingMode.DOWN).doubleValue());
		});
		return ResultUtil.data(list);
	}

	@RequestMapping("/price")
	@ApiOperation(value = "钱包余额")
	public ResultMessage<MemberVO> price() {
		MemberVO memberVO = new MemberVO();

		MemberWalletVO recharge = memberWalletService.getMemberWallet(UserContext.getCurrentUser().getId(),
				WalletOwnerEnum.RECHARGE.name());
		// MemberWalletVO promoteHy = new MemberWalletVO();
		// MemberWalletVO promoteFw = new MemberWalletVO();
		MemberWalletVO promote = null;
		MemberWalletVO sale = memberWalletService.getMemberWallet(UserContext.getCurrentUser().getId(),
				WalletOwnerEnum.SALE.name());
		// //根据合伙人身份获取钱包余额
		// QueryWrapper<Partner> queryWrapper = new QueryWrapper<>();
		// queryWrapper.eq("member_id", UserContext.getCurrentUser().getId());
		// queryWrapper.eq("partner_state",0);
		// queryWrapper.orderByDesc("end_time");
		// queryWrapper.last("limit 1");
		// Partner partner = partnerMapper.selectOne(queryWrapper);
		// if(partner != null && 4 == partner.getPartnerType()){
		// promoteHy =
		// memberWalletService.getMemberWallet(UserContext.getCurrentUser().getId(),
		// WalletOwnerEnum.PROMOTE_HY.name());
		// promoteFw =
		// memberWalletService.getMemberWallet(UserContext.getCurrentUser().getId(),
		// WalletOwnerEnum.PROMOTE_FW.name());
		// }else{
		promote = memberWalletService.getMemberWallet(UserContext.getCurrentUser().getId(),
				WalletOwnerEnum.PROMOTE.name());
		// }
		Double rechargeMoney = recharge != null ? recharge.getMemberWallet() : 0D;
		Double promoteMoney = promote != null ? promote.getMemberWallet() : 0D;
		// Double promoteHyMoney = promoteHy != null ? promoteHy.getMemberWallet() : 0D;
		// Double promoteFwMoney = promoteFw != null ? promoteFw.getMemberWallet() : 0D;
		Double saleMoney = sale != null ? sale.getMemberWallet() : 0D;

		memberVO.setRechargeMoney(rechargeMoney);
		memberVO.setSaleMoney(saleMoney);
		memberVO.setPromoteMoney(promoteMoney);
		// memberVO.setPromoteHyMoney(String.valueOf(promoteHyMoney));
		// memberVO.setPromoteFwMoney(String.valueOf(promoteFwMoney));
		// memberVO.setAllMoney(String.valueOf(CurrencyUtil.add(saleMoney,rechargeMoney,promoteMoney,promoteHyMoney,promoteFwMoney)));
		memberVO.setAllMoney(CurrencyUtil.add(saleMoney, rechargeMoney, promoteMoney));
		return ResultUtil.data(memberVO);
	}

	@ApiOperation(value = "分佣流向明细")
	@RequestMapping(value = "/GetCommissionDistribution")
	public ResultMessage<IPage<CommissionDistributionSale>> GetCommissionDistribution(FinanceAdminVO financeAdminVO,
			PageVO page) {
		QueryWrapper queryWrapper = new QueryWrapper();

		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}

		queryWrapper.eq(cn.lili.common.utils.StringUtils.isNotEmpty(financeAdminVO.getMobile()), "a.mobile",
				financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (cn.lili.common.utils.StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapper.in(list.size() > 0, "location", list);
		}
		if (financeAdminVO.getSearchTime() != null) {
			financeAdminVO.setStartTime(financeAdminVO.getSearchTime());
			// 获取下个月一号
			Calendar c = Calendar.getInstance();
			// 过去一月
			c.setTime(financeAdminVO.getSearchTime());
			c.add(Calendar.MONTH, 1);
			financeAdminVO.setEndTime(c.getTime());
			queryWrapper.between("a.createTime", financeAdminVO.getStartTime(), financeAdminVO.getEndTime());
		}
		if (StringUtils.isNotEmpty(currentUser.getStoreId())) {
			queryWrapper.eq("a.tsStoreId", currentUser.getStoreId());
		} else {
			Store one = storeService
					.getOne(new QueryWrapper<Store>().eq("member_id", currentUser.getId()).eq("delete_flag", 0));
			queryWrapper.eq("a.tsStoreId", one.getId());
		}
		queryWrapper.isNotNull("a.tsRealName");
		queryWrapper.orderByDesc("a.createTime");
		queryWrapper.groupBy("a.sn");
		IPage<CommissionDistributionSale> data = regionService.GetCommissionDistribution(queryWrapper, page);
		if (CollectionUtils.isNotEmpty(data.getRecords())) {
			for (CommissionDistributionSale commissionDistributionSale : data.getRecords()) {
				if (StringUtils.isNotEmpty(commissionDistributionSale.getLocation())) {
					commissionDistributionSale.setLocationName(
							regionService.getItemAdCodOrName(commissionDistributionSale.getLocation()));
				}
			}
		}
		return ResultUtil.data(data);
	}

	@ApiOperation(value = "分佣流向明细")
	@RequestMapping(value = "/GetCommissionDistributionAll")
	public ResultMessage<CommissionDistributionSale> GetCommissionDistributionAll(FinanceAdminVO financeAdminVO,
			PageVO page) {
		QueryWrapper queryWrapper = new QueryWrapper();
		page.setPageSize(999999999);
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		queryWrapper.eq(cn.lili.common.utils.StringUtils.isNotEmpty(financeAdminVO.getMobile()), "a.mobile",
				financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (cn.lili.common.utils.StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapper.in(list.size() > 0, "location", list);
		}
		if (financeAdminVO.getSearchTime() != null) {
			financeAdminVO.setStartTime(financeAdminVO.getSearchTime());
			// 获取下个月一号
			Calendar c = Calendar.getInstance();
			// 过去一月
			c.setTime(financeAdminVO.getSearchTime());
			c.add(Calendar.MONTH, 1);
			financeAdminVO.setEndTime(c.getTime());
			queryWrapper.between("a.createTime", financeAdminVO.getStartTime(), financeAdminVO.getEndTime());
		}
		if (StringUtils.isNotEmpty(currentUser.getStoreId())) {
			queryWrapper.eq("a.tsStoreId", currentUser.getStoreId());
		} else {
			Store one = storeService
					.getOne(new QueryWrapper<Store>().eq("member_id", currentUser.getId()).eq("delete_flag", 0));
			queryWrapper.eq("a.tsStoreId", one.getId());
		}
		queryWrapper.isNotNull("a.tsRealName");
		queryWrapper.orderByDesc("a.createTime");
		queryWrapper.groupBy("a.sn");
		IPage<CommissionDistributionSale> data = regionService.GetCommissionDistribution(queryWrapper, page);
		List<CommissionDistributionSale> list = data.getRecords();
		CommissionDistributionSale sale = new CommissionDistributionSale();
		sale.setCommission(0);
		sale.setFlowPrice(0);
		sale.setTsMoney(0);
		sale.setQyMoney(0);
		sale.setSyMoney(0);
		sale.setAdminMoney(0);
		for (CommissionDistributionSale s : list) {
			sale.setTsMoney(CurrencyUtil.add(s.getTsMoney(), sale.getTsMoney()));
			sale.setQyMoney(CurrencyUtil.add(s.getQyMoney(), sale.getQyMoney()));
			sale.setSyMoney(CurrencyUtil.add(s.getSyMoney(), sale.getSyMoney()));
			sale.setAdminMoney(CurrencyUtil.add(s.getAdminMoney(), sale.getAdminMoney()));
			sale.setCommission(CurrencyUtil.add(s.getCommission(), sale.getCommission()));
			sale.setFlowPrice(CurrencyUtil.add(s.getFlowPrice(), sale.getFlowPrice()));
		}
		return ResultUtil.data(sale);
	}

	@RequestMapping("/pricePc")
	@ApiOperation(value = "查询Pc钱包余额")
	public ResultMessage<MemberVO> pricePc() {
		MemberVO memberVO = new MemberVO();

		MemberWalletVO recharge = memberWalletService.getMemberWallet(UserContext.getCurrentUser().getMemberId(),
				WalletOwnerEnum.RECHARGE.name());
		// MemberWalletVO promoteHy = new MemberWalletVO();
		// MemberWalletVO promoteFw = new MemberWalletVO();
		MemberWalletVO promote = null;
		MemberWalletVO sale = memberWalletService.getMemberWallet(UserContext.getCurrentUser().getMemberId(),
				WalletOwnerEnum.SALE.name());
		// //根据合伙人身份获取钱包余额
		// QueryWrapper<Partner> queryWrapper = new QueryWrapper<>();
		// queryWrapper.eq("member_id", UserContext.getCurrentUser().getId());
		// queryWrapper.eq("partner_state",0);
		// queryWrapper.orderByDesc("end_time");
		// queryWrapper.last("limit 1");
		// Partner partner = partnerMapper.selectOne(queryWrapper);
		// if(partner != null && 4 == partner.getPartnerType()){
		// promoteHy =
		// memberWalletService.getMemberWallet(UserContext.getCurrentUser().getId(),
		// WalletOwnerEnum.PROMOTE_HY.name());
		// promoteFw =
		// memberWalletService.getMemberWallet(UserContext.getCurrentUser().getId(),
		// WalletOwnerEnum.PROMOTE_FW.name());
		// }else{
		promote = memberWalletService.getMemberWallet(UserContext.getCurrentUser().getMemberId(),
				WalletOwnerEnum.PROMOTE.name());
		// }
		Double rechargeMoney = recharge != null ? recharge.getMemberWallet() : 0D;
		Double promoteMoney = promote != null ? promote.getMemberWallet() : 0D;
		// Double promoteHyMoney = promoteHy != null ? promoteHy.getMemberWallet() : 0D;
		// Double promoteFwMoney = promoteFw != null ? promoteFw.getMemberWallet() : 0D;
		Double saleMoney = sale != null ? sale.getMemberWallet() : 0D;

		memberVO.setRechargeMoney(BigDecimal.valueOf(rechargeMoney).setScale(2, RoundingMode.DOWN).doubleValue());
		memberVO.setSaleMoney(BigDecimal.valueOf(saleMoney).setScale(2, RoundingMode.DOWN).doubleValue());
		memberVO.setPromoteMoney(BigDecimal.valueOf(promoteMoney).setScale(2, RoundingMode.DOWN).doubleValue());
		// memberVO.setPromoteHyMoney(String.valueOf(promoteHyMoney));
		// memberVO.setPromoteFwMoney(String.valueOf(promoteFwMoney));
		// memberVO.setAllMoney(String.valueOf(CurrencyUtil.add(saleMoney,rechargeMoney,promoteMoney,promoteHyMoney,promoteFwMoney)));
		memberVO.setAllMoney(BigDecimal.valueOf(CurrencyUtil.add(saleMoney, rechargeMoney, promoteMoney))
				.setScale(2, RoundingMode.DOWN).doubleValue());
		return ResultUtil.data(memberVO);
	}

	@RequestMapping("/getSaleMoney")
	@ApiOperation(value = "查询Pc钱包余额")
	public ResultMessage<IPage<OrderVO>> getSaleMoney(PageVO page, OrderVO orderVO) {
		QueryWrapper<OrderVO> queryWrapper = new QueryWrapper();
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		// 区域
		if (cn.lili.common.utils.StringUtils.isNotEmpty(orderVO.getRegionId())) {
			List list = regionService.getItemAdCod(orderVO.getRegionId());
			queryWrapper.in(list.size() > 0, "a.regionId", list);
		}
		// 时间
		if (StringUtils.isNotEmpty(orderVO.getStartDate()) && StringUtils.isNotEmpty(orderVO.getEndDate())) {
			queryWrapper.between("DATE_FORMAT(a.create_time ,'%Y-%m-%d')", orderVO.getStartDate(),
					orderVO.getEndDate());
		}
		if ("in".equals(orderVO.getType())) {
			// 收款记录
			queryWrapper.eq("a.payee", currentUser.getMemberId());
			queryWrapper.eq("a.payee_owner", WalletOwnerEnum.SALE.name());
		} else if ("out".equals(orderVO.getType())) {
			// 付款人
			queryWrapper.eq("a.payer", currentUser.getMemberId());
			queryWrapper.eq("a.payer_owner", WalletOwnerEnum.SALE.name());
		} else {
			queryWrapper.and(wrapper -> wrapper.and(
					w -> w.eq("a.payer", currentUser.getMemberId()).eq("a.payer_owner", WalletOwnerEnum.SALE.name()))
					.or(w -> w.eq("a.payee", currentUser.getMemberId()).eq("a.payee_owner",
							WalletOwnerEnum.SALE.name())));
		}
		// 会员级别
		queryWrapper.eq(StringUtils.isNotEmpty(orderVO.getGradeName()), "a.gradeName", orderVO.getGradeName());
		// 手机号
		queryWrapper.eq(StringUtils.isNotEmpty(orderVO.getMobile()), "a.mobile", orderVO.getMobile());
		queryWrapper.isNotNull("a.sn");
		queryWrapper.groupBy("a.sn");
		IPage<OrderVO> saleMoney = walletDetailService.getSaleMoney(page, queryWrapper);
		if (CollectionUtils.isNotEmpty(saleMoney.getRecords())) {
			for (OrderVO order : saleMoney.getRecords()) {
				if (StringUtils.isNotEmpty(order.getRegionId())) {
					order.setRegion(regionService.getItemAdCodOrName(order.getRegionId()));
				}
				order.setDingdan(BigDecimal.valueOf(order.getDingdan()).setScale(2, RoundingMode.DOWN).doubleValue());
				order.setYunfei(BigDecimal.valueOf(order.getYunfei()).setScale(2, RoundingMode.DOWN).doubleValue());
				order.setFuwufei(BigDecimal.valueOf(order.getFuwufei()).setScale(2, RoundingMode.DOWN).doubleValue());
				order.setAllMoney(BigDecimal.valueOf(order.getAllMoney()).setScale(2, RoundingMode.DOWN).doubleValue());
			}
		}
		return ResultUtil.data(saleMoney);
	}

	@RequestMapping("/getSaleMoneyAll")
	@ApiOperation(value = "查询Pc钱包余额总数")
	public ResultMessage<Map<String, Object>> getSaleMoneyAll(PageVO page) {
		page.setPageSize(999999999);
		AuthUser currentUser = UserContext.getCurrentUser();
		if (currentUser == null) {
			throw new ServiceException(ResultCode.USER_AUTH_EXPIRED);
		}
		Map<String, Object> map = new HashMap<>();
		Double out = 0D;
		Double in = 0D;
		QueryWrapper<OrderVO> queryWrapper1 = new QueryWrapper();
		// 付款人
		queryWrapper1.eq("a.payer", currentUser.getMemberId());
		queryWrapper1.isNotNull("a.sn");
		queryWrapper1.groupBy("a.sn");
		IPage<OrderVO> saleMoney1 = walletDetailService.getSaleMoney(page, queryWrapper1);
		if (CollectionUtils.isNotEmpty(saleMoney1.getRecords())) {
			for (OrderVO order : saleMoney1.getRecords()) {
				out = CurrencyUtil.add(out, order.getAllMoney());
			}
		}
		map.put("out", BigDecimal.valueOf(out).setScale(2, RoundingMode.DOWN).doubleValue());
		QueryWrapper<OrderVO> queryWrapper2 = new QueryWrapper();
		// v收款ren
		queryWrapper2.eq("a.payee", currentUser.getMemberId());
		queryWrapper2.isNotNull("a.sn");
		queryWrapper2.groupBy("a.sn");
		IPage<OrderVO> saleMoney2 = walletDetailService.getSaleMoney(page, queryWrapper2);
		if (CollectionUtils.isNotEmpty(saleMoney2.getRecords())) {
			for (OrderVO order : saleMoney2.getRecords()) {
				in = CurrencyUtil.add(in, order.getAllMoney());
			}
		}
		map.put("in", BigDecimal.valueOf(in).setScale(2, RoundingMode.DOWN).doubleValue());
		return ResultUtil.data(map);
	}
}
