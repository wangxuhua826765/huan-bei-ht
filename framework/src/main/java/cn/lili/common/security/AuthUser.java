package cn.lili.common.security;

import cn.lili.common.security.enums.UserEnums;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Chopper
 */
@Data
@AllArgsConstructor
public class AuthUser implements Serializable {

	private static final long serialVersionUID = 582441893336003319L;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 昵称
	 */
	private String nickName;

	/**
	 * 头像
	 */
	private String face;

	/**
	 * id
	 */
	private String id;

	/**
	 * 长期有效（用于手机app登录场景或者信任场景等）
	 */
	private Boolean longTerm = true;

	/**
	 * @see UserEnums 角色
	 */
	private UserEnums role;

	/**
	 * 如果角色是商家，则存在此店铺id字段 storeId
	 */
	private String storeId;

	/**
	 * 如果角色是商家，则存在此店铺名称字段 storeName
	 */
	private String storeName;

	/**
	 * 如果角色是商家，则存在此店铺名称字段 storeName
	 */
	private String roleIds;

	/**
	 * 是否是超级管理员
	 */
	private Boolean isSuper = false;

	// 手机号
	private String purePhoneNumber;
	// member表id
	private String memberId;

	public AuthUser(String username, String id, String nickName, String face, UserEnums role, String purePhoneNumber,
			String storeId, String storeName, String memberId) {
		this.username = username;
		this.face = face;
		this.id = id;
		this.role = role;
		this.nickName = nickName;
		this.purePhoneNumber = purePhoneNumber;
		this.storeId = storeId;
		this.storeName = storeName;
		this.memberId = memberId;
	}

	public AuthUser(String username, String id, String face, UserEnums manager, String nickName, Boolean isSuper,
			String purePhoneNumber, String storeId, String storeName, String memberId) {
		this.username = username;
		this.id = id;
		this.face = face;
		this.role = manager;
		this.isSuper = isSuper;
		this.nickName = nickName;
		this.purePhoneNumber = purePhoneNumber;
		this.storeId = storeId;
		this.storeName = storeName;
		this.memberId = memberId;
	}

}
