package cn.lili.common.security.enums;

/**
 * token角色类型
 *
 * @author Chopper
 * @version v1.0
 * @since 2020/8/18 15:23
 */
public enum UserEnums {
	/**
	 * 角色
	 */
	MEMBER("会员"), PARTNER("合伙人"), MANAGER("管理员"), SYSTEM("系统"), PROMOTER("推广员"), STORE("商家"), AGENT(
			"代理商"), ANGEL_PARTNER("天使合伙人"), BUSINESS_PARTNER("事业合伙人"), ORDINARY_USERS("普通用户");

	private final String role;

	UserEnums(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}
}
