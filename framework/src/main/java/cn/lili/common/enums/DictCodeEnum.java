package cn.lili.common.enums;

import io.swagger.annotations.ApiModelProperty;

/**
 * 字典KEY枚举
 *
 * @author Chopper
 * @since 2020/4/8 1:36 下午
 */
public enum DictCodeEnum {

	MSG_CATEGORY("msg_category", "通告类型"), POSITION_RANK("position_rank", "职务职级"), ORG_CATEGORY("org_category",
			"机构类型"), FORM_PERMS_TYPE("form_perms_type", "表单权限策略"), URGENT_LEVEL("urgent_level", "紧急程度"), EOA_PLAN_TYPE(
					"eoa_plan_type",
					"日程计划类型"), EOA_CMS_MENU_TYPE("eoa_cms_menu_type", "分类栏目类型"), EOA_PLAN_STATUS("eoa_plan_status",
							"日程计划状态"), DATABASE_TYPE("database_type", "数据库类型"), OL_FORM_BIZ_TYPE("ol_form_biz_type",
									"Online表单业务分类"), QUARTZ_STATUS("quartz_status", "定时任务状态"), TENANT_STATUS(
											"tenant_status",
											"租户状态"), IS_OPEN("is_open", "开关"), ACTIVITI_SYNC("activiti_sync",
													"同步工作流引擎"), PERMS_TYPE("perms_type", "权限策略"), MSG_TYPE("msg_type",
															"推送类别"), DEL_FLAG("del_flag", "删除状态"), SEX("sex",
																	"性别"), GLOBAL_PERMS_TYPE("global_perms_type",
																			"全局权限策略"), ONLINE_GRAPH_DISPLAY_TEMPLATE(
																					"online_graph_display_template",
																					"Online图表展示模板"), DICT_ITEM_STATUS(
																							"dict_item_status",
																							"字典状态"), PRIORITY(
																									"priority",
																									"优先级"), RULE_CONDITIONS(
																											"rule_conditions",
																											"条件规则"), MSGTYPE(
																													"msgType",
																													"发送消息类型"), VALID_STATUS(
																															"valid_status",
																															"有效无效状态"), USER_TYPE(
																																	"user_type",
																																	"用户类型"), CGFORM_TABLE_TYPE(
																																			"cgform_table_type",
																																			"Online表单类型"), BPM_STATUS(
																																					"bpm_status",
																																					"流程状态"), LOG_TYPE(
																																							"log_type",
																																							"日志类型"), STATUS(
																																									"status",
																																									"状态"), OPERATE_TYPE(
																																											"operate_type",
																																											"操作类型"), SEND_STATUS(
																																													"send_status",
																																													"发布状态"), YN(
																																															"yn",
																																															"1是0否"), MSGSENDSTATUS(
																																																	"msgSendStatus",
																																																	"消息发送状态"), MENU_TYPE(
																																																			"menu_type",
																																																			"菜单类型"), CESHI_ONLINE(
																																																					"ceshi_online",
																																																					"onlineT类型"), ONLINE_GRAPH_DATA_TYPE(
																																																							"online_graph_data_type",
																																																							"Online图表数据类型"), DEPART_STATUS(
																																																									"depart_status",
																																																									"部门状态"), ONLINE_GRAPH_TYPE(
																																																											"online_graph_type",
																																																											"Online图表类型"), BPM_PROCESS_TYPE(
																																																													"bpm_process_type",
																																																													"流程类型"), USER_STATUS(
																																																															"user_status",
																																																															"用户状态"), GOODS_SHOP_LIMIT(
																																																																	"goods_shop_limit",
																																																																	"好店推荐数量"),
	// 商品费率
	GOODS_RULE("goods_rule", "商品默认费率"),

	AUTO_COMMISSION("auto_commission", "自动分佣   天"), AUTO_RECEIVE("auto_receive", "自动收货 天"),

	// 费率配置
	// RECHARGE_RULE("recharge_rule", "充值费率"),
	PROMOTE_WITHDRAWAL_RATE("promote_withdrawal_rate", "推广钱包提现费率"), SALE_WITHDRAWAL_RATE("sale_withdrawal_rate",
			"销售钱包提现费率"), MEMBERSHIP_RATE("membership_rate", "会员费率"),
	// MEMBERSHIP_PRICE("membership_price", "会员会费"),
	FLOW_RATE("flow_rate", "流水费率"), PARTNER_RATE("partner_rate", "合伙人费率"), PARTNER_PRICE("partner_price",
			"合伙人会费"), ORDER_FEE("order_fee", "订单手续费"), APPORTION_FEE("apportion_fee", "分摊费率"),

	JIACHASHOUYI_QY("jiachashouyi_qy", "分摊费率"), JIACHASHOUYI_PT("jiachashouyi_pt", "分摊费率");
	// 用到value的角色配置
	// BUSINESS ("business","代理商"), //代理商
	// AGENT ("agent","商家"), //商家
	// PROMOTER ("promoter","推广员"), //推广员
	// ANGEL_PARTNER ("angel_partner","天使合伙人"), //天使合伙人
	// BUSINESS_PARTNER ("business_partner","事业合伙人"), //事业合伙人
	// ORDINARY_USERS ("ordinary_users","普通用户"); //普通用户

	private final String dictCode;
	private final String dictName;

	DictCodeEnum(String dictCode, String dictName) {
		this.dictCode = dictCode;
		this.dictName = dictName;
	}

	public String dictCode() {
		return this.dictCode;
	}

	public String dictName() {
		return this.dictName;
	}
}
