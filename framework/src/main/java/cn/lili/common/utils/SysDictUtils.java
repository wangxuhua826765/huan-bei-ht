package cn.lili.common.utils;

import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.SysDictItemVO;
import cn.lili.modules.dict.entity.vos.SysDictVO;
import cn.lili.modules.dict.service.SysDictItemService;
import cn.lili.modules.dict.service.SysDictService;
import cn.lili.modules.dict.serviceimpl.SysDictItemServiceImpl;
import cn.lili.modules.permission.entity.vo.SystemLogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @since 1.9.3
 */
@Component
public class SysDictUtils {

	@Autowired
	private SysDictItemService sysDictItemService;
	@Autowired
	private SysDictService sysDictService;

	private static SysDictUtils sysDictUtils;

	@PostConstruct
	// 通过该注解，将注入bean值set到工具类的变量中
	public void init() {
		sysDictUtils = this;
		sysDictUtils.sysDictItemService = this.sysDictItemService;
		sysDictUtils.sysDictService = this.sysDictService;
	}

	/**
	 * @param
	 * @return 返回一个封装数据
	 */
	public static SysDictVO getSysDictVO(String dictCode) {
		SysDict sysDict = sysDictUtils.sysDictService.getSysDict(dictCode);
		if (sysDict == null) {
			return new SysDictVO();
		}
		SysDictVO sysDictVO = new SysDictVO();
		BeanUtil.copyProperties(sysDict, sysDictVO);
		sysDictVO.setChildren(sysDictUtils.sysDictItemService.findValueSByDictCode(dictCode));
		return sysDictVO;
	}

	/**
	 * @param
	 * @return 返回一个封装数据
	 */
	public static SysDictVO getSysDictVO(String dictCode, String itemText) {
		SysDict sysDict = sysDictUtils.sysDictService.getSysDict(dictCode);
		if (sysDict == null) {
			return new SysDictVO();
		}
		SysDictVO sysDictVO = new SysDictVO();
		BeanUtil.copyProperties(sysDict, sysDictVO);
		sysDictVO.setChildren(sysDictUtils.sysDictItemService.findValueSByDictCodeAndDictText(dictCode, itemText));
		return sysDictVO;
	}

	/**
	 * @param
	 * @return 返回所有的字典值
	 */
	public static List<SysDictItemVO> getAllValue(String dictCode, String itemText) {
		return sysDictUtils.sysDictItemService.findValueSByDictCodeAndDictText(dictCode, itemText);
	}

	/**
	 * 取第一个值
	 * 
	 * @param
	 * @return 返回排序最小的一个字典值的value
	 */
	public static String getValueString(String dictCode, String itemText) {
		List<SysDictItemVO> sysDictItems = new ArrayList();
		sysDictItems = sysDictUtils.sysDictItemService.findValueSByDictCodeAndDictText(dictCode, itemText);
		SysDictItem item = new SysDictItem();
		for (SysDictItem i : sysDictItems) {
			if (i != null && (StringUtils.isEmpty(item.getSort())
					|| Integer.parseInt(i.getSort()) < Integer.parseInt(item.getSort()))) {
				item = i;
			}
		}
		return item.getItemValue().toString();
	}

	/**
	 * @param
	 * @return 返回所有的字典值
	 */
	public static List<SysDictItemVO> getAllValue(String dictCode) {
		List<SysDictItemVO> sysDictItems = new ArrayList();
		sysDictItems = sysDictUtils.sysDictItemService.findValueSByDictCode(dictCode);
		return sysDictItems;
	}

	/**
	 * 取第一个值
	 * 
	 * @param
	 * @return 返回排序最小的一个字典值
	 */
	public static SysDictItem getValue(String dictCode) {
		List<SysDictItemVO> sysDictItems = new ArrayList();
		sysDictItems = sysDictUtils.sysDictItemService.findValueSByDictCode(dictCode);
		SysDictItem item = new SysDictItem();
		for (SysDictItem i : sysDictItems) {
			if (i != null && (StringUtils.isEmpty(item.getSort())
					|| Integer.parseInt(i.getSort()) < Integer.parseInt(item.getSort()))) {
				item = i;
			}
		}
		return item;
	}

	/**
	 * 取第一个值
	 * 
	 * @param
	 * @return 返回排序最小的一个字典值的value
	 */
	public static String getValueString(String dictCode) {
		List<SysDictItemVO> sysDictItems = new ArrayList();
		sysDictItems = sysDictUtils.sysDictItemService.findValueSByDictCode(dictCode);
		SysDictItem item = new SysDictItem();
		for (SysDictItem i : sysDictItems) {
			if (i != null && (StringUtils.isEmpty(item.getSort())
					|| Integer.parseInt(i.getSort()) < Integer.parseInt(item.getSort()))) {
				item = i;
			}
		}
		return item.getItemValue().toString();
	}

	/**
	 * 取第一个值
	 * 
	 * @param
	 * @return 返回字典的数据列表Map结构
	 */
	public static List<Map> getValueMap(String dictCode) {
		List<SysDictItemVO> sysDictItems = new ArrayList();
		sysDictItems = sysDictUtils.sysDictItemService.findValueSByDictCode(dictCode);
		List<Map> list = new ArrayList();
		for (SysDictItem i : sysDictItems) {
			Map m = new HashMap();
			m.put("text", i.getItemText());
			m.put("value", i.getItemValue());
			m.put("status", i.getStatus());
			list.add(m);
		}
		return list;
	}
}
