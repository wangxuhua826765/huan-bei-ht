package cn.lili.common.utils;

import com.alibaba.fastjson.JSON;
import com.aliyun.tea.*;
import com.aliyun.ocr20191230.models.*;
import com.aliyun.teaopenapi.models.*;
import com.aliyun.teautil.models.*;

/**
 * create by yudan on 2022/7/24 营业执照识别
 */
public class OcrBusinessLicense {

	public static com.aliyun.ocr20191230.Client createClient(String accessKeyId, String accessKeySecret)
			throws Exception {
		Config config = new Config()
				// 您的 AccessKey ID
				.setAccessKeyId(accessKeyId)
				// 您的 AccessKey Secret
				.setAccessKeySecret(accessKeySecret);
		// 访问的域名
		config.regionId = "cn-shanghai";
		config.endpointType = "internal";
		config.endpoint = "ocr.cn-shanghai.aliyuncs.com";
		return new com.aliyun.ocr20191230.Client(config);
	}

	public static void main(String[] args_) throws Exception {
		java.util.List<String> args = java.util.Arrays.asList(args_);
		com.aliyun.ocr20191230.Client client = OcrBusinessLicense.createClient("LTAI5tSbyR61qsScm433YW5g",
				"lwXmSW6XqwHhOiZ18rQaMPPoD1civM");
		RecognizeBusinessLicenseRequest recognizeBusinessLicenseRequest = new RecognizeBusinessLicenseRequest()
				.setImageURL(
						"http://viapi-test.oss-cn-shanghai.aliyuncs.com/viapi-3.0domepic/ocr/RecognizeBusinessLicense/RecognizeBusinessLicense1.jpg");
		RuntimeOptions runtime = new RuntimeOptions();
		try {
			// 复制代码运行请自行打印 API 的返回值
			RecognizeBusinessLicenseResponse response = client
					.recognizeBusinessLicenseWithOptions(recognizeBusinessLicenseRequest, runtime);
			System.out.println("营业执照识别成功：");
			System.out.println("response：" + JSON.toJSONString(response));
			System.out.println("response.body.getData()：" + JSON.toJSONString(response.getBody().getData()));
		} catch (TeaException error) {
			// 如有需要，请打印 error
			com.aliyun.teautil.Common.assertAsString(error.message);
			System.out.println("营业执照识别失败：" + error.message);
		} catch (Exception _error) {
			TeaException error = new TeaException(_error.getMessage(), _error);
			// 如有需要，请打印 error
			com.aliyun.teautil.Common.assertAsString(error.message);
			System.out.println("营业执照识别失败：" + error.message);
		}
	}

}
