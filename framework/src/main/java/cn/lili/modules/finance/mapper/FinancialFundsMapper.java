package cn.lili.modules.finance.mapper;

import cn.lili.modules.finance.entity.dos.FinancialFunds;
import cn.lili.modules.finance.entity.vo.FinancialFundsVO;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 授信管理
 * 
 * @author 贾送兵
 * @since 2022-02-11 09:25
 */
public interface FinancialFundsMapper extends BaseMapper<FinancialFunds> {
	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select * from   li_financial_funds  ${ew.customSqlSegment}")
	IPage<FinancialFundsVO> getFinancialFundsList(IPage<FinancialFundsVO> page,
			@Param(Constants.WRAPPER) Wrapper<FinancialFundsVO> queryWrapper);

}