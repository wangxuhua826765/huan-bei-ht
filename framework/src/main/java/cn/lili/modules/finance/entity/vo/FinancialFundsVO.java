package cn.lili.modules.finance.entity.vo;

import cn.lili.modules.finance.entity.dos.FinancialFunds;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import lombok.Data;

/**
 * VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class FinancialFundsVO extends FinancialFunds {

}
