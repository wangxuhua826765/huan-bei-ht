package cn.lili.modules.finance.serviceimpl;

import cn.lili.modules.finance.entity.dos.FinancialFunds;
import cn.lili.modules.finance.entity.vo.FinancialFundsSearchParams;
import cn.lili.modules.finance.entity.vo.FinancialFundsVO;
import cn.lili.modules.finance.mapper.FinancialFundsMapper;
import cn.lili.modules.finance.service.FinancialFundsService;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.mapper.CreditManagementMapper;
import cn.lili.modules.whitebar.service.CreditManagementService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class FinancialFundsServiceImpl extends ServiceImpl<FinancialFundsMapper, FinancialFunds>
		implements
			FinancialFundsService {

	@Override
	public IPage<FinancialFundsVO> creditManagementPage(FinancialFundsSearchParams financialFundsSearchParams) {

		return this.baseMapper.getFinancialFundsList(PageUtil.initPage(financialFundsSearchParams),
				financialFundsSearchParams.queryWrapper());
	}

	@Override
	public void deleteByIds(List<String> ids) {
		this.removeByIds(ids);
	}

}