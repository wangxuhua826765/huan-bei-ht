package cn.lili.modules.finance.service;

import cn.lili.modules.finance.entity.dos.FinancialFunds;
import cn.lili.modules.finance.entity.vo.FinancialFundsSearchParams;
import cn.lili.modules.finance.entity.vo.FinancialFundsVO;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 退款日志 业务层
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface FinancialFundsService extends IService<FinancialFunds> {

	public IPage<FinancialFundsVO> creditManagementPage(FinancialFundsSearchParams financialFundsSearchParams);

	/**
	 * 删除
	 *
	 * @param ids
	 */
	void deleteByIds(List<String> ids);

}
