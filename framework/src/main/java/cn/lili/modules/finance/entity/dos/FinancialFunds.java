package cn.lili.modules.finance.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 财务-资金管理
 *
 * @author 贾送兵
 * @since
 */
@Data
@TableName("li_financial_funds")
@ApiModel(value = "资金管理")
public class FinancialFunds extends BaseEntity {

	private static final long serialVersionUID = -5339221840646353417L;

	@ApiModelProperty(value = "商户名称")
	private String name;

	@ApiModelProperty(value = "用户类型")
	private String nameType;
	@ApiModelProperty(value = "用户类型名称")
	private String nameTypeText;
	@ApiModelProperty(value = "业务类型")
	private String busType;
	@ApiModelProperty(value = "业务类型名称")
	private String busTypeText;

	@ApiModelProperty(value = "变动明细")
	private String changeDetails;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	@ApiModelProperty(value = "变动日期")
	private Date creditPeriod;

	@ApiModelProperty(value = "变动金额")
	private String changeAmount;

}