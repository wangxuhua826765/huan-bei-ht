package cn.lili.modules.dict.serviceimpl;

import cn.lili.modules.dict.entity.dos.SysDictPicture;
import cn.lili.modules.dict.mapper.SysDictPictureMapper;
import cn.lili.modules.dict.service.SysDictPictureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class SysDictPictureServiceImpl extends ServiceImpl<SysDictPictureMapper, SysDictPicture> implements
    SysDictPictureService {

    @Override public List<SysDictPicture> listByCode(String code) {
        return baseMapper.listByCode(code);
    }
}
