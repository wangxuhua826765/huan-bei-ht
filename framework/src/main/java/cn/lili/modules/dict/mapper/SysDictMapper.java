package cn.lili.modules.dict.mapper;

import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.dict.entity.vos.SysDictVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 字典Key数据处理层
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {
	@Select("SELECT * FROM sys_dict")
	List<SysDict> allKey();

	@Select("SELECT sd.* FROM sys_dict sd  WHERE sd.dict_code  = #{dictCode} AND sd.del_flag = 0")
	SysDict getSysDict(String dictCode);

	@Select("SELECT sd.* FROM sys_dict sd where sd.del_flag = 0 order by sort desc")
	List<SysDictVO> selectAllList();
}