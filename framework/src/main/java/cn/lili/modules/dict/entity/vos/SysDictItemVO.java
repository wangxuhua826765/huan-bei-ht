package cn.lili.modules.dict.entity.vos;

import cn.lili.modules.dict.entity.dos.SysDictItem;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class SysDictItemVO extends SysDictItem {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "字典名称")
	@NotEmpty(message = "字典名称不能为空")
	private String dictName;

	@ApiModelProperty(value = "字典编码")
	@NotEmpty(message = "字典编码不能为空")
	private String dictCode;

	@ApiModelProperty(value = "描述")
	private String description;

	@TableId
	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@ApiModelProperty(value = "字典key_ID")
	private String parentId;

}
