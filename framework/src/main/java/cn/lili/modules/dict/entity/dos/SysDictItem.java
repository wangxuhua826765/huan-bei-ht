package cn.lili.modules.dict.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * 数据字典
 *
 * @author pikachu
 * @author Bulbasaur
 * @since 2020/12/10 17:42
 */
@Data
@TableName("sys_dict_item")
@ApiModel(value = "数据字典")
@NoArgsConstructor
@AllArgsConstructor
public class SysDictItem {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "字典key_ID")
	@NotEmpty(message = "字典key_ID不能为空")
	private String dictId;

	@ApiModelProperty(value = "字典项文本")
	@NotEmpty(message = "字典Text不能为空")
	private String itemText;

	@ApiModelProperty(value = "字典项值")
	@NotEmpty(message = "字典Value不能为空")
	private String itemValue;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "排序")
	private String sort;

	@ApiModelProperty(value = "状态（1启用 0不启用）")
	private String status;

	@TableId
	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@CreatedBy
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建者", hidden = true)
	private String createBy;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date createTime;

	@LastModifiedBy
	@TableField(fill = FieldFill.UPDATE)
	@ApiModelProperty(value = "更新者", hidden = true)
	private String updateBy;

	@LastModifiedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.UPDATE)
	@ApiModelProperty(value = "更新时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date updateTime;

	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "删除标志 true/false 删除/未删除", hidden = true)
	private Boolean delFlag;
}