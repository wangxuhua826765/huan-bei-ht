package cn.lili.modules.dict.service;

import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.SysDictItemVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 字典value业务层
 *
 * @author Bulbasaur
 * @since 2020/11/24 17:07
 */
public interface SysDictItemService extends IService<SysDictItem> {

	List<SysDictItem> findValueSById(String id);

	List<SysDictItemVO> findValueSByDictCode(String dictCode);

	List<SysDictItemVO> findValueSByDictCodeAndDictText(String dictCode, String dictText);

}