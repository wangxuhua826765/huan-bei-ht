package cn.lili.modules.dict.service;

import cn.lili.modules.dict.entity.dos.SysDictPicture;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface SysDictPictureService extends IService<SysDictPicture> {

    List<SysDictPicture> listByCode(String code);
}
