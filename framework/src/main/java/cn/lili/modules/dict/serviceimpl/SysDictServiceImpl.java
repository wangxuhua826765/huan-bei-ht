package cn.lili.modules.dict.serviceimpl;

import cn.lili.cache.Cache;
import cn.lili.cache.CachePrefix;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.exception.ServiceException;
import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.dict.entity.vos.SysDictItemVO;
import cn.lili.modules.dict.entity.vos.SysDictVO;
import cn.lili.modules.dict.mapper.DictionaryMapper;
import cn.lili.modules.dict.mapper.SysDictItemMapper;
import cn.lili.modules.dict.mapper.SysDictMapper;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.dict.service.SysDictService;
import cn.lili.modules.goods.entity.dos.CategorySpecification;
import cn.lili.modules.page.entity.enums.ArticleCategoryEnum;
import cn.lili.modules.page.entity.enums.ArticleEnum;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 文章分类业务层实现
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

	/**
	 * 缓存
	 */
	@Autowired
	private Cache cache;
	@Autowired
	private SysDictMapper sysDictMapper;

	@Autowired
	private SysDictItemMapper sysDictItemMapper;

	@Override
	public List<SysDict> allKey() {
		return sysDictMapper.allKey();
	}

	@Override
	public SysDict getSysDict(String dictCode) {
		return sysDictMapper.getSysDict(dictCode);
	}

	@Override
	public List<SysDictVO> allChildren() {
		// 从缓存取所有的分类
		Object all = cache.get(CachePrefix.SYS_DICT.getPrefix());
		List<SysDictVO> articleCategories;
		if (all == null) {
			// 调用初始化分类缓存方法
			articleCategories = initCategory();
		} else {
			articleCategories = (List<SysDictVO>) all;
		}
		return articleCategories;
	}

	private List<SysDictVO> initCategory() {
		List<SysDictVO> articleCategories = sysDictMapper.selectAllList();
		articleCategories.forEach(item -> {
			List<SysDictItemVO> sysDictItemList = sysDictItemMapper.selectAllList(item.getId());
			item.setChildren(sysDictItemList);
		});
		cache.put(CachePrefix.SYS_DICT.getPrefix(), articleCategories);
		return articleCategories;
	}

	public SysDictItem getPartnerPrice(String partner) {
		List<SysDictItem> sysDictItemList = new ArrayList<>();
		SysDictItem sysDictItem = new SysDictItem();
		String dictCode = "partner_price";
		if (partner.equals("sy")) {
			sysDictItemList = sysDictItemMapper.getPartnerPrice(dictCode, "事业");
		} else if (partner.equals("cs")) {
			sysDictItemList = sysDictItemMapper.getPartnerPrice(dictCode, "代理商");
		} else if (partner.equals("ts")) {
			sysDictItemList = sysDictItemMapper.getPartnerPrice(dictCode, "天使");
		}
		sysDictItem = sysDictItemList.get(0);
		return sysDictItem;
	}
}