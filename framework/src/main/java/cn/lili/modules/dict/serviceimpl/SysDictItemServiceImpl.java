package cn.lili.modules.dict.serviceimpl;

import cn.lili.cache.Cache;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.SysDictItemVO;
import cn.lili.modules.dict.mapper.SysDictItemMapper;
import cn.lili.modules.dict.service.SysDictItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 字典value业务层实现
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements SysDictItemService {

	/**
	 * 缓存
	 */
	@Autowired
	private Cache cache;

	@Autowired
	private SysDictItemMapper sysDictItemMapper;

	@Override
	public List<SysDictItem> findValueSById(String id) {
		return sysDictItemMapper.findValueSById(id);
	}

	@Override
	public List<SysDictItemVO> findValueSByDictCode(String dictCode) {
		return sysDictItemMapper.findValueSByDictCode(dictCode);
	}

	@Override
	public List<SysDictItemVO> findValueSByDictCodeAndDictText(String dictCode, String itemText) {
		return sysDictItemMapper.findValueSByDictCodeAndDictText(dictCode, itemText);
	}
}