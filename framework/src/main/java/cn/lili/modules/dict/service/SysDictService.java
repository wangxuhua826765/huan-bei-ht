package cn.lili.modules.dict.service;

import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.dict.entity.vos.SysDictVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 字典KEY业务层
 *
 * @author Bulbasaur
 * @since 2020/11/24 17:07
 */
public interface SysDictService extends IService<SysDict> {

	List<SysDict> allKey();

	SysDict getSysDict(String dictCode);

	List<SysDictVO> allChildren();

	SysDictItem getPartnerPrice(String partner);
}