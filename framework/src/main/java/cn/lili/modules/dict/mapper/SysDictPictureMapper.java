package cn.lili.modules.dict.mapper;

import cn.lili.modules.dict.entity.dos.SysDictPicture;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysDictPictureMapper extends BaseMapper<SysDictPicture> {

    @Select("select * from sys_dict_picture where code = #{code}")
    List<SysDictPicture> listByCode(String code);

}
