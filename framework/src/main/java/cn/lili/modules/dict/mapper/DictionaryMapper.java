package cn.lili.modules.dict.mapper;

import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 文章分类数据处理层
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
public interface DictionaryMapper extends BaseMapper<DictionaryLi> {
	@Select("select * from li_dictionary where parent_id=(SELECT id FROM li_dictionary  WHERE dict_code= #{dictCode}) ")
	List<DictionaryVO> getDictionaryLiListVo(String dictCode);
}