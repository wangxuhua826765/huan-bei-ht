package cn.lili.modules.dict.service;

import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.page.entity.dos.ArticleCategory;
import cn.lili.modules.promotion.entity.vos.CouponActivityItemVO;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 文章分类业务层
 *
 * @author Bulbasaur
 * @since 2020/11/24 17:07
 */
public interface DictionaryService extends IService<DictionaryLi> {

	/**
	 * 添加
	 *
	 * @param dictionary
	 * @return
	 */
	DictionaryLi saveDictionaryLi(DictionaryLi dictionary);

	/**
	 * 修改
	 *
	 * @param
	 * @return
	 */
	DictionaryLi updateDictionaryLi(DictionaryLi dictionary);

	/**
	 * 查询所有的分类，父子关系
	 *
	 * @return 文章分类
	 */
	List<DictionaryVO> allChildren();

	/**
	 * 删除文章分类
	 *
	 * @param id
	 *            文章分类id
	 * @return 操作状态
	 */
	boolean deleteById(String id);

	/**
	 *
	 * @param dictCode
	 * @return
	 */
	List<DictionaryVO> getDictionaryLiListVo(String dictCode);

}