package cn.lili.modules.dict.entity.vos;

import cn.lili.common.utils.BeanUtil;
import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.page.entity.dos.ArticleCategory;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 数据字典VO
 *
 * @author
 * @since 2021-03-26 11:32
 */
@Data
public class DictionaryVO extends DictionaryLi {

	@ApiModelProperty(value = "子菜单")
	private List<DictionaryVO> children = new ArrayList<>();

	public DictionaryVO() {

	}

	public DictionaryVO(DictionaryLi dictionaryLi) {
		BeanUtil.copyProperties(dictionaryLi, this);
	}

	public List<DictionaryVO> getChildren() {
		if (children != null) {
			children.sort(new Comparator<DictionaryVO>() {
				@Override
				public int compare(DictionaryVO o1, DictionaryVO o2) {
					return o1.getSort().compareTo(o2.getSort());
				}
			});
			return children;
		}
		return null;
	}
}
