package cn.lili.modules.dict.mapper;

import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.SysDictItemVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 文章分类数据处理层
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

	@Select("SELECT sdi.* FROM sys_dict sd LEFT JOIN sys_dict_item sdi ON sd.id = sdi.dict_id WHERE sd.id = #{id} AND sd.del_flag = 0 and sdi.del_flag = 0")
	List<SysDictItem> findValueSById(String id);

	@Select("SELECT sdi.* FROM sys_dict sd LEFT JOIN sys_dict_item sdi ON sd.id = sdi.dict_id WHERE sd.dict_code  = #{dictCode} AND sd.del_flag = 0 ")
	List<SysDictItemVO> findValueSByDictCode(String dictCode);

	@Select("SELECT sdi.* FROM sys_dict sd LEFT JOIN sys_dict_item sdi ON sd.id = sdi.dict_id WHERE sd.dict_code  = #{dictCode} AND sdi.item_text = #{itemText}  AND sd.del_flag = 0")
	List<SysDictItemVO> findValueSByDictCodeAndDictText(String dictCode, String itemText);

	@Select("SELECT id,dict_id as parentId, item_text as dictName, item_value as dictCode, sort,description\n"
			+ "FROM sys_dict_item  where dict_id =#{dictId} and del_flag = 0 ORDER BY sort DESC")
	List<SysDictItemVO> selectAllList(String dictId);

	@Select("select item.* from  sys_dict dic LEFT JOIN sys_dict_item item on dic.id = item.dict_id where dic.dict_code = #{dictCode}and item.item_text LIKE concat('%',#{text},'%')")
	List<SysDictItem> getPartnerPrice(@Param("dictCode") String dictCode, @Param("text") String text);
}