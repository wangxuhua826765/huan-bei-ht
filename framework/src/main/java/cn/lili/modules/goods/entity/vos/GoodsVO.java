package cn.lili.modules.goods.entity.vos;

import cn.lili.modules.goods.entity.dos.Goods;
import cn.lili.modules.goods.entity.dto.GoodsParamsDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 商品VO
 *
 * @author pikachu
 * @since 2020-02-26 23:24:13
 */
@Data
public class GoodsVO extends Goods {

	private static final long serialVersionUID = 6377623919990713567L;

	@ApiModelProperty(value = "分类名称")
	private List<String> categoryName;

	@ApiModelProperty(value = "商品参数")
	private List<GoodsParamsDTO> goodsParamsDTOList;

	@ApiModelProperty(value = "商品图片")
	private List<String> goodsGalleryList;

	@ApiModelProperty(value = "sku列表")
	private List<GoodsSkuVO> skuList;

	@ApiModelProperty(value = "月销")
	private Integer monthSales;

	@ApiModelProperty(value = "评分")
	private String score;

	@ApiModelProperty(value = "地址名称id")
	private String regionId;

	@ApiModelProperty(value = "地址名称")
	private String region;

	@ApiModelProperty(value = "店铺联系方式")
	private String storePhone;

	/**
	 * 活动结束时间
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "活动结束时间", hidden = true)
	private Date activityEndTime;

	@ApiModelProperty(value = "限购数量")
	private Integer quantity;

	/**
	 * 是否限购（0不限购 1限购）
	 */
	private Boolean quantityFlag;
	/**
	 * 商品地址
	 */
	private String goodsUrl;

}
