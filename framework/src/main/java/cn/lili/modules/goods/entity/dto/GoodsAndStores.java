package cn.lili.modules.goods.entity.dto;

import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.goods.entity.vos.GoodsSkuVO;
import cn.lili.modules.store.entity.enums.StoreStatusEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class GoodsAndStores extends PageVO {

	private static final long serialVersionUID = 2544015852728566887L;

	@ApiModelProperty(value = "商品编号")
	private String id;

	@ApiModelProperty(value = "商品编号")
	private String goodsId;

	@ApiModelProperty(value = "商品详情列表")
	private List<GoodsSkuVO> skuList;

	@ApiModelProperty(value = "上下架状态")
	private String marketEnable;

	@ApiModelProperty(value = "审核状态")
	private String authFlag;

	@ApiModelProperty(value = "区域编码")
	private String adCode;

	@ApiModelProperty(value = "店铺状态")
	private String storeDisable;

	@ApiModelProperty(value = "地址id")
	private String regionId;

	@ApiModelProperty(value = "是否是自营店铺")
	private Integer selfOperated;

	@ApiModelProperty(value = "显示的数量")
	private Integer limit;

	@ApiModelProperty(value = "下一级id")
	private String nextId;

	@ApiModelProperty(value = "店铺商品名称")
	private String title;

	@ApiModelProperty(value = "店铺商品图片")
	private String image;

	@ApiModelProperty(value = "店铺地址")
	private String storeAddressPath;

	@ApiModelProperty(value = "店铺详细地址")
	private String storeAddressDetail;

	@ApiModelProperty(value = "经纬度")
	private String trapeze;

	@ApiModelProperty(value = "距离")
	private Integer distance;

	@Max(value = 99999999, message = "价格不能超过99999999")
	@ApiModelProperty(value = "商品价格")
	private Double price;

	@ApiModelProperty(value = "会员Id")
	private String memberId;

	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "店铺关闭时间")
	private Date storeEndTime;

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "经纬度")
	@NotEmpty
	private String storeCenter;

	@Size(min = 6, max = 200, message = "店铺简介需在6-200字符之间")
	@NotBlank(message = "店铺简介不能为空")
	@ApiModelProperty(value = "店铺简介")
	private String storeDesc;

	@ApiModelProperty(value = "高德坐标系经度")
	private String lon;

	@ApiModelProperty(value = "高德坐标系纬度")
	private String lat;

	@ApiModelProperty(value = "店铺折扣")
	private String discount;
	@ApiModelProperty(value = "描述评分")
	private Double descriptionScore;

	@ApiModelProperty(value = "服务评分")
	private Double serviceScore;

	@ApiModelProperty(value = "物流描述")
	private Double deliveryScore;

	@ApiModelProperty(value = "商品数量")
	private Integer goodsNum;

	@ApiModelProperty(value = "收藏数量")
	private Integer collectionNum;

	@ApiModelProperty(value = "腾讯云智服唯一标识")
	private String yzfSign;

	@ApiModelProperty(value = "腾讯云智服小程序唯一标识")
	private String yzfMpSign;

	@ApiModelProperty(value = "udesk IM标识")
	private String merchantEuid;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "年费开始时间")
	private Date memberCreateTime;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "年费结束时间")
	private Date memberEndTime;

	@ApiModelProperty(value = "上年度已缴纳的年费")
	private Double payMoney;

	@ApiModelProperty(value = "推荐码")
	private String promotionCode;

	// @ApiModelProperty(value = "充值折扣")
	// private Double rechargeRule;
	//
	// @ApiModelProperty(value = "提现折扣")
	// private Double withdrawalRate;
	//
	// @ApiModelProperty(value = "BtoB交易费率")
	// private Double jiaoyifei;

	@ApiModelProperty(value = "是否为推荐店铺", required = true)
	private Boolean recommend;

	@ApiModelProperty(value = "推荐排名", required = true)
	private int recommendRanking;

	@ApiModelProperty(value = "一级分类")
	private Long categoryFirst;

	@ApiModelProperty(value = "一级分类名称")
	private String categoryFirstName;

	@ApiModelProperty(value = "二级分类")
	private Long categorySecond;

	@ApiModelProperty(value = "二级分类名称")
	private String categorySecondName;

	@ApiModelProperty(value = "三级分类")
	private Long categoryThird;

	@ApiModelProperty(value = "三级分类名称")
	private String categoryThirdName;

	@ApiModelProperty(value = "拒绝原因")
	private String causer;

	@ApiModelProperty(value = "营业时间")
	private String sellTimeSta;

	@ApiModelProperty(value = "营业时间")
	private String sellTimeEnd;

	@ApiModelProperty(value = "店铺联系方式")
	private String storePhone;

	@ApiModelProperty(value = "店铺图片")
	private String storeImage;

	@ApiModelProperty(value = "月销")
	private String monthSales;

	@ApiModelProperty(value = "评分")
	private String score;

	public <T> QueryWrapper<T> storeWrapper() {
		QueryWrapper<T> storeWrapper = new QueryWrapper<>();
		/*
		 * if (selfOperated !=null) { storeWrapper.eq("self_operated", selfOperated); }
		 */
		if (StringUtils.isNotEmpty(regionId)) {
			storeWrapper.like("store_address_id_path", regionId);
		}
		if (nextId != null) {
			storeWrapper.notLike("store_address_id_path", nextId);
		}
		storeWrapper.orderByDesc("create_time");
		storeWrapper.eq("store_disable", "OPEN");

		if (limit != null) {
			storeWrapper.last("limit " + limit);
		}

		return storeWrapper;
	}

	public <T> QueryWrapper<T> goodsWrapper() {
		QueryWrapper<T> goodsWrapper = new QueryWrapper<>();
		/*
		 * if (selfOperated !=null) { goodsWrapper.eq("goods.self_operated",
		 * selfOperated); }
		 */
		if (StringUtils.isNotEmpty(regionId)) {
			goodsWrapper.like("store.store_address_id_path", regionId);
		}
		if (nextId != null) {
			goodsWrapper.notLike("store.store_address_id_path", nextId);
		}
		goodsWrapper.orderByDesc("goods.create_time");
		goodsWrapper.eq("goods.delete_flag", false);
		goodsWrapper.eq("goods.auth_flag", "PASS");
		goodsWrapper.eq("store.store_disable", StoreStatusEnum.OPEN.name());
		goodsWrapper.eq("goods.market_enable", "UPPER");

		if (limit != null) {
			goodsWrapper.last("limit " + limit);
		}
		return goodsWrapper;
	}

}
