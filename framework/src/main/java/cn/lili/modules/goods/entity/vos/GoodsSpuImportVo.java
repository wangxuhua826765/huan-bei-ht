package cn.lili.modules.goods.entity.vos;

import cn.lili.modules.goods.util.Excel;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.system.entity.dto.GoodsSetting;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author chenke
 * @since 2022-4-13 14:24:02
 */
@Data
public class GoodsSpuImportVo implements Serializable {

	@Excel(name = "产品编号")
	private String goodsNo;

	@Excel(name = "商品分类")
	private String category;

	@Excel(name = "商品类型", readConverterExp = "PHYSICAL_GOODS=实物商品,VIRTUAL_GOODS=虚拟商品,E_COUPON=电子卡券", combo = "实物商品,虚拟商品,电子卡券")
	private String goodsType;

	@Excel(name = "商品名称")
	private String goodsName;

	@Excel(name = "卖点")
	private String sellingPoint;

	@Excel(name = "计量单位")
	private String goodsUnit;

	@Excel(name = "销售区域")
	private String salesArea;

	@Excel(name = "商品图片")
	private String goodsGallery;

	@Excel(name = "商品图片后缀")
	private String goodsGallerySuffix;

	@Excel(name = "商品描述")
	private String goodsDescribe;

	@Excel(name = "商品详情图片")
	private String intro;

	@Excel(name = "商品详情图片后缀")
	private String introSuffix;

	@Excel(name = "是否立即发布")
	private String release;

	@Excel(name = "运费模板")
	private String templateName;

	@Excel(name = "保质期")
	private String shelfLife;

	@Excel(name = "生产日期")
	private String productionDate;

	/**
	 * 商家图片,用于校验数据
	 */
	private Map<String, String> goodsImgDictMap;

	/**
	 * 全部分类,用于校验数据
	 */
	private Map<String, String> categoryMap;

	/**
	 * 店铺信息
	 */
	private Store store;

	/**
	 * 图片设置
	 */
	private GoodsSetting goodsSetting;

	private List<String> goodsGalleryList;

}
