package cn.lili.modules.goods.entity.vos;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONUtil;
import cn.lili.common.enums.PromotionTypeEnum;
import cn.lili.common.vo.PageVO;
import cn.lili.elasticsearch.EsSuffix;
import cn.lili.modules.goods.entity.dos.GoodsSku;
import cn.lili.modules.goods.entity.dto.GoodsParamsDTO;
import cn.lili.modules.promotion.tools.PromotionTools;
import cn.lili.modules.search.entity.dos.EsGoodsAttribute;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.*;

/**
 *
 *
 * @author paulG
 **/
@Data
public class EsGoodsIndexVO {

	@ApiModelProperty("详细地址")
	private String storeAddressDetail;

	@ApiModelProperty("店铺简介")
	private String storeDesc;

	@ApiModelProperty("省市县地址")
	private String storeAddressPath;

	@ApiModelProperty("商品名称")
	private String goodsName;

	@ApiModelProperty("商品审批状态")
	private String marketEnable;

	/**
	 * 商品id
	 */
	@ApiModelProperty("商品Id")
	// @Field(type = FieldType.Text)
	private String id;

	/**
	 * 商品编号
	 */
	// @Field(type = FieldType.Keyword)
	@ApiModelProperty("商品编号")
	private String sn;

	/**
	 * 卖家id
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("卖家id")
	private String storeId;

	/**
	 * 卖家名称
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("卖家名称")
	private String storeName;

	@ApiModelProperty("店铺logo")
	private String storeLogo;

	/**
	 * 销量
	 */
	// @Field(type = FieldType.Integer)
	@ApiModelProperty("销量")
	private Integer buyCount;

	/**
	 * 小图
	 */
	@ApiModelProperty("小图")
	private String small;

	/**
	 * 缩略图
	 */
	@ApiModelProperty("缩略图")
	private String thumbnail;

	/**
	 * 品牌id
	 */
	// @Field(type = FieldType.Integer, fielddata = true)
	@ApiModelProperty("品牌id")
	private String brandId;

	/**
	 * 品牌名称
	 */
	// @Field(type = FieldType.Keyword, fielddata = true)
	@ApiModelProperty("品牌名称")
	private String brandName;

	/**
	 * 品牌图片地址
	 */
	// @Field(type = FieldType.Keyword, fielddata = true)
	@ApiModelProperty("品牌图片地址")
	private String brandUrl;

	/**
	 * 分类path
	 */
	// @Field(type = FieldType.Keyword)
	@ApiModelProperty("分类path")
	private String categoryPath;

	/**
	 * 分类名称path
	 */
	// @Field(type = FieldType.Keyword)
	@ApiModelProperty("分类名称path")
	private String categoryNamePath;

	/**
	 * 店铺分类id
	 */
	// @Field(type = FieldType.Keyword)
	@ApiModelProperty("店铺分类id")
	private String storeCategoryPath;

	/**
	 * 店铺分类名称
	 */
	// @Field(type = FieldType.Keyword)
	@ApiModelProperty("店铺分类名称")
	private String storeCategoryNamePath;

	/**
	 * 商品价格
	 */
	// @Field(type = FieldType.Double)
	@ApiModelProperty("商品价格")
	private Double price;

	/**
	 * 会员价
	 */
	@ApiModelProperty("会员价")
	private Double memberPrice;

	@ApiModelProperty(value = "B2B订单佣金")
	private Double commission;

	/**
	 * 促销价
	 */
	// @Field(type = FieldType.Double)
	@ApiModelProperty("促销价")
	private Double promotionPrice;

	/**
	 * 如果是积分商品需要使用的积分
	 */
	// @Field(type = FieldType.Integer)
	@ApiModelProperty("积分商品需要使用的积分")
	private Integer point;

	/**
	 * 评价数量
	 */
	// @Field(type = FieldType.Integer)
	@ApiModelProperty("评价数量")
	private Integer commentNum;

	/**
	 * 好评数量
	 */
	// @Field(type = FieldType.Integer)
	@ApiModelProperty("好评数量")
	private Integer highPraiseNum;

	/**
	 * 好评率
	 */
	// @Field(type = FieldType.Double)
	@ApiModelProperty("好评率")
	private Double grade;

	/**
	 * 详情
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("详情")
	private String intro;

	/**
	 * 商品移动端详情
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("商品移动端详情")
	private String mobileIntro;

	/**
	 * 是否自营
	 */
	// @Field(type = FieldType.Boolean)
	@ApiModelProperty("是否自营")
	private int selfOperated;

	/**
	 * 是否为推荐商品
	 */
	// @Field(type = FieldType.Boolean)
	@ApiModelProperty("是否为推荐商品")
	private Boolean recommend;

	/**
	 * 销售模式
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("销售模式")
	private String salesModel;

	/**
	 * 审核状态
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("审核状态")
	private String authFlag;

	/**
	 * 卖点
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("卖点")
	private String sellingPoint;

	/**
	 * 商品视频
	 */
	// @Field(type = FieldType.Text)
	@ApiModelProperty("商品视频")
	private String goodsVideo;

	@ApiModelProperty("商品发布时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	// @Field(type = FieldType.Date, format = DateFormat.basic_date_time)
	private Date releaseTime;

	/**
	 * @see cn.lili.modules.goods.entity.enums.GoodsTypeEnum
	 */
	@ApiModelProperty(value = "商品类型", required = true)
	private String goodsType;

	@ApiModelProperty(value = "商品sku基础分数", required = true)
	private Integer skuSource;

	/**
	 * 商品属性（参数和规格）
	 */
	@Field(type = FieldType.Nested)
	private List<EsGoodsAttribute> attrList;

	/**
	 * 商品促销活动集合 key 为 促销活动类型
	 *
	 * @see PromotionTypeEnum value 为 促销活动实体信息
	 */
	// @Field(type = FieldType.Nested)
	@ApiModelProperty("商品促销活动集合JSON，key 为 促销活动类型，value 为 促销活动实体信息 ")
	private String promotionMapJson;

	@ApiModelProperty(value = "排序 数字越大越前")
	private Long sort;

	@ApiModelProperty(value = "sku列表")
	private List<GoodsSkuVO> skuList;

	@ApiModelProperty(value = "月销")
	private String monthSales;

	@ApiModelProperty(value = "评分")
	private String score;
}
