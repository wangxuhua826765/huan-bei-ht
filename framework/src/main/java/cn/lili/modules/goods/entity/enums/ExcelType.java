package cn.lili.modules.goods.entity.enums;

/**
 * ExcelType.
 *
 * @author Li Bing
 * @since 2022.03.28
 */
public enum ExcelType {
	ALL(0), EXPORT(1), IMPORT(2);

	private final int value;

	ExcelType(int value) {
		this.value = value;
	}

	public int value() {
		return this.value;
	}
}
