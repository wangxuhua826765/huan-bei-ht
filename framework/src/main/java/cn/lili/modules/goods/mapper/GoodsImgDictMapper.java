package cn.lili.modules.goods.mapper;

import cn.lili.modules.goods.entity.dos.GoodsImgDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * GoodsImgDictMapper.
 *
 * @author Li Bing
 * @since 2022.03.28
 */
public interface GoodsImgDictMapper extends BaseMapper<GoodsImgDict> {

}
