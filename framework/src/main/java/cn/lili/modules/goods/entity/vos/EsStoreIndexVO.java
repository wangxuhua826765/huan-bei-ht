package cn.lili.modules.goods.entity.vos;

import cn.lili.common.enums.PromotionTypeEnum;
import cn.lili.modules.search.entity.dos.EsGoodsAttribute;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author paulG
 **/
@Data
public class EsStoreIndexVO {
	@ApiModelProperty("店铺名称")
	private String storeName;

	@ApiModelProperty("商品审批状态")
	private String storeDisable;

	@ApiModelProperty("店铺Id")
	private Long id;

	@ApiModelProperty("店铺logo")
	private String storeLogo;

	@ApiModelProperty("会员id")
	private String memberId;

	@ApiModelProperty("创建者")
	private String createBy;

	@ApiModelProperty("创建时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@ApiModelProperty("更新者")
	private String updateBy;

	@ApiModelProperty("更新时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	@ApiModelProperty("会员名称")
	private String memberName;

	@ApiModelProperty(value = "是否自营")
	private Boolean selfOperated;

	@ApiModelProperty("店铺关闭时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date storeEndTime;

	@ApiModelProperty("店铺详细地址")
	private String storeSddressDetail;

	@ApiModelProperty("地址id")
	private String storeAddressIdPath;

	@ApiModelProperty("地址名称")
	private String storeAddressPath;

	@ApiModelProperty("经纬度")
	private String storeCenter;

	@ApiModelProperty("店铺简介")
	private String storeDesc;

	@ApiModelProperty("物流评分")
	private Double deliveryScore;

	@ApiModelProperty("描述评分")
	private Double descriptionScore;

	@ApiModelProperty("服务评分")
	private Double serviceScore;

	@ApiModelProperty("商品数量")
	private int goodsNum;

	@ApiModelProperty("收藏数量")
	private int collectionNum;

	@ApiModelProperty("")
	private String yzfMpSign;

	@ApiModelProperty("")
	private String yzfSign;

	@ApiModelProperty("客服标识")
	private String merchantEuid;

	@ApiModelProperty("年费开始时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date memberCreateTime;

	@ApiModelProperty("年费结束时间")
	private Date memberEndTime;

	@ApiModelProperty("年费")
	private String payMoney;

	@ApiModelProperty("推荐吗")
	private String promotionCode;

	// @ApiModelProperty("充值折扣")
	// private Double rechargeRule;
	//
	// @ApiModelProperty("提现折扣")
	// private Double withdrawalRate;
	//
	// @ApiModelProperty("交易费")
	// private Double jiaoyifei;

	@ApiModelProperty("是否推荐（0否，1是）")
	private Boolean recommend;

	@ApiModelProperty("推荐排名")
	private int recommendRanking;

	@ApiModelProperty("店铺一级分类")
	private Long categoryFirst;

	@ApiModelProperty("店铺一级分类名称")
	private String categoryFirstName;

	@ApiModelProperty("店铺二级分类")
	private Long categorySecond;

	@ApiModelProperty("店铺二级分类名称")
	private String categorySecondName;

	@ApiModelProperty("店铺三级分类")
	private Long categoryThird;

	@ApiModelProperty("店铺三级分类名称")
	private String categoryThirdName;

	@ApiModelProperty("拒绝原因")
	private String causer;

	@ApiModelProperty(value = "排序 数字越大越前")
	private Long sort;

}
