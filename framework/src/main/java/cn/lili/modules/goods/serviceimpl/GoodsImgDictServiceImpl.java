package cn.lili.modules.goods.serviceimpl;

import cn.lili.modules.goods.entity.dos.GoodsImgDict;
import cn.lili.modules.goods.mapper.GoodsImgDictMapper;
import cn.lili.modules.goods.service.GoodsImgDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * GoodsImgDictServiceImpl.
 *
 * @author Li Bing
 * @since 2022.03.28
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GoodsImgDictServiceImpl extends ServiceImpl<GoodsImgDictMapper, GoodsImgDict>
		implements
			GoodsImgDictService {

}
