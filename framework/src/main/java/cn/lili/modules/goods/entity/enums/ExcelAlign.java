package cn.lili.modules.goods.entity.enums;

/**
 * ExcelAlign.
 *
 * @author Li Bing
 * @since 2022.03.28
 */
public enum ExcelAlign {
	AUTO(0), LEFT(1), CENTER(2), RIGHT(3);

	private final int value;

	ExcelAlign(int value) {
		this.value = value;
	}

	public int value() {
		return this.value;
	}
}
