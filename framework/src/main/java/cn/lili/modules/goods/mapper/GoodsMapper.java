package cn.lili.modules.goods.mapper;

import cn.lili.modules.goods.entity.dos.Goods;
import cn.lili.modules.goods.entity.dto.GoodsSearchParams;
import cn.lili.modules.goods.entity.vos.EsGoodsIndexVO;
import cn.lili.modules.goods.entity.vos.EsStoreIndexVO;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.search.entity.dos.EsGoodsIndex;
import cn.lili.modules.store.entity.vos.StoreVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 规格项数据处理层
 *
 * @author pikachu
 * @since 2020-02-18 15:18:56
 */
@Mapper
public interface GoodsMapper extends BaseMapper<Goods> {

	/**
	 * 根据店铺ID获取商品ID列表
	 *
	 * @param storeId
	 *            店铺ID
	 * @return 商品ID列表
	 */
	@Select("SELECT id FROM li_goods WHERE store_id = #{storeId}")
	List<String> getGoodsIdByStoreId(String storeId);

	/**
	 * 添加商品评价数量
	 *
	 * @param commentNum
	 *            评价数量
	 * @param goodsId
	 *            商品ID
	 */
	@Update("UPDATE li_goods SET comment_num = comment_num + #{commentNum} WHERE id = #{goodsId}")
	void addGoodsCommentNum(Integer commentNum, String goodsId);

	/**
	 * 查询商品VO分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 商品VO分页
	 */
	@Select("select s.store_phone,count(o.id) monthSales,"
			+ " FORMAT(IFNULL(SUM(e.grade)/COUNT(e.grade),5),1) as score," + "  g.* from li_goods g"
			+ " left join li_store s on s.id = g.store_id"
			+ " LEFT JOIN li_order_item o on o.goods_id = g.id and DATE_FORMAT(o.create_time,'%Y-%m') = DATE_FORMAT(NOW(),'%Y-%m')"
			+ " LEFT JOIN li_member_evaluation e on e.sku_id = o.sku_id" + " ${ew.customSqlSegment}")
	IPage<GoodsVO> queryByParams(IPage<GoodsSearchParams> page,
			@Param(Constants.WRAPPER) Wrapper<GoodsSearchParams> queryWrapper);

	// 商品推荐
	@Update("update li_goods set recommend = #{recommend},recommend_ranking = #{recommendRanking} where id = #{good}")
	Boolean updateRecommend(String good, Boolean recommend, int recommendRanking);

	// 商品排序
	@Update("update li_goods set sort = #{sort} where id = #{good}")
	Boolean updateSort(String good, Long sort);

	/**
	 * 小程序列表查询g INNER JOIN li_store l on g.store_id=l.id and g.market_enable='UPPER'
	 */
	@Select("SELECT g.*,l.store_logo,l.store_address_detail,l.store_address_path,l.store_desc,IFNULL((select SUM(num)\n"
			+ " FROM li_order_item WHERE goods_id = g.id and after_sale_status='NOT_APPLIED'),0) as monthSales,"
			+ " FORMAT(IFNULL((select SUM(grade)/COUNT(grade) FROM li_member_evaluation\n"
			+ " WHERE goods_id = g.id),5),1) as score  from li_goods g INNER JOIN li_store l on g.store_id=l.id  ${ew.customSqlSegment}")
	IPage<EsGoodsIndexVO> getGoodsByStore(IPage<EsGoodsIndexVO> page,
			@Param(Constants.WRAPPER) Wrapper<EsGoodsIndexVO> queryWrapper);
	// List<EsGoodsIndex> getGoodsByStore(String goodsName);

	@Select("SELECT * from li_store  ${ew.customSqlSegment}")
	IPage<EsStoreIndexVO> geStoreInfoByData(IPage<EsStoreIndexVO> page,
			@Param(Constants.WRAPPER) Wrapper<EsStoreIndexVO> queryWrapper);

	// 修改商品排序
	@Update("update li_goods set num = #{num} where id = #{goodsId}")
	Boolean updateSortGoods(String goodsId, int num);

	@Select("SELECT g.*" + "FROM li_goods g " + " LEFT JOIN li_activity_goods ag ON g.id = ag.goods_id "
			+ "where ag.activity_id = #{activityId} and ag.store_id = #{storeId}")
	List<GoodsVO> getGoodsByActivityId(String activityId, String storeId);

	@Select("SELECT  s.* FROM li_activity_goods ag  LEFT JOIN li_store s on s.id = ag.store_id "
			+ " where ag.activity_id = #{activityId} GROUP BY ag.store_id")
	List<StoreVO> getStoreByActivityId(String activityId);

	@Select("<script>" + "SELECT g.* FROM li_goods g "
			+ "WHERE g.store_id = #{storeId} AND g.auth_flag = 'PASS' AND g.market_enable = 'UPPER' AND g.delete_flag = '0'  "
			+ "and g.id not in (SELECT g.id FROM li_goods g "
			+ "  LEFT JOIN li_goods_gallery gg ON g.id = gg.goods_id  "
			+ " LEFT JOIN li_activity_goods ag on ag.goods_id = g.id  "
			+ " LEFT JOIN li_activity a on a.id = ag.activity_id " + "WHERE g.store_id = #{storeId} "
			+ " AND g.auth_flag = 'PASS' " + " AND g.market_enable = 'UPPER' "
			+ " AND g.delete_flag = '0' and a.delete_flag = '0' " + " AND NOW() &lt; a.activity_end_time "
			+ "<if test=\"activityId!= null and activityId != ''\">" + " AND a.id != #{activityId} " + "</if>) "
			+ "<if test=\"goodsName!=null and goodsName!=''\"> "
			+ "   and g.goods_name LIKE concat('%',#{goodsName},'%') " + "</if>"
			+ "<if test=\"shopCategoryPath!=null and shopCategoryPath!=''\"> "
			+ "   and g.store_category_path LIKE concat('%',#{shopCategoryPath},'%') " + "</if>" + "</script>")
	IPage<GoodsVO> getGoodsByStoreId(IPage<GoodsVO> page, @Param("storeId") String storeId,
			@Param("goodsName") String goodsName, @Param("activityId") String activityId,
			@Param("shopCategoryPath") String shopCategoryPath);

	@Select("SELECT count(o.id) monthSales,FORMAT(IFNULL(SUM(e.grade)/COUNT(e.grade),5),1) as score,gg.*"
			+ " FROM li_goods g " + " left join li_goods gg on g.category_path = gg.category_path"
			+ " left join li_store s on s.id = gg.store_id"
			+ " LEFT JOIN li_order_item o on o.goods_id = gg.id and DATE_FORMAT(o.create_time,'%Y-%m') = DATE_FORMAT(NOW(),'%Y-%m')"
			+ " LEFT JOIN li_member_evaluation e on e.sku_id = o.sku_id"
			+ " where g.id = #{goodsId} and gg.id != #{goodsId} and s.store_disable = 'OPEN'"
			+ " and gg.delete_flag = 0 and gg.auth_flag = 'PASS' and gg.market_enable = 'UPPER'" + " GROUP BY gg.id")
	IPage<GoodsVO> getGoodsListByPage(IPage<StoreVO> page, @Param("goodsId") String goodsId);

	@Update("<script>" + "UPDATE li_goods set market_enable = 'DOWN' where id in "
			+ "<foreach collection='activityList' item='item' open='(' separator=',' close=')'>" + "#{item}"
			+ "</foreach>" + "</script>")
	Boolean updateGoodsStatus(List<String> activityList);
}