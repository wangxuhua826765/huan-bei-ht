package cn.lili.modules.goods.entity.dos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * GoodsImgDict. 商品图片字典（用户图片名和图床关系）
 * 
 * @author Li Bing
 * @since 2022.03.28
 */
@Data
@TableName("li_goods_img_dict")
@ApiModel(value = "商品图片字典")
public class GoodsImgDict {

	@TableId
	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@ApiModelProperty(value = "图片名称")
	@NotEmpty(message = "商品名称不能为空")
	@Length(max = 100, message = "商品名称太长，不能超过100个字符")
	private String imgName;

	@ApiModelProperty(value = "图片路径")
	@NotEmpty(message = "商品名称不能为空")
	private String imgUrl;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建时间", hidden = true)
	private Date createTime;

	/**
	 *
	 * 店铺id
	 */
	@ApiModelProperty(value = "店铺id", hidden = true)
	private String storeId;

}
