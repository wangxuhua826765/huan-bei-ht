package cn.lili.modules.goods.entity.enums;

/**
 * ExcelColumnType.
 *
 * @author Li Bing
 * @since 2022.03.28
 */
public enum ExcelColumnType {
	NUMERIC(0), STRING(1);

	private final int value;

	ExcelColumnType(int value) {
		this.value = value;
	}

	public int value() {
		return this.value;
	}
}
