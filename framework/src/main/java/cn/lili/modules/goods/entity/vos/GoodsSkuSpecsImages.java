package cn.lili.modules.goods.entity.vos;

import lombok.Data;

@Data
public class GoodsSkuSpecsImages {

	private String url;

	private String name;

	private String status;
}
