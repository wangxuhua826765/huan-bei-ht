package cn.lili.modules.goods.entity.vos;

import cn.lili.modules.goods.util.Excel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * BatchGoodsImportVo.
 *
 * @author Li Bing
 * @since 2022.03.28
 */
@Data
public class BatchGoodsImportVo implements Serializable {

	// 对应categoryName 分类数组
	@Excel(name = "一级商品分类")
	private String firstCategory;
	@Excel(name = "二级商品分类")
	private String secondCategory;
	@Excel(name = "三级商品分类")
	private String thirdCategory;
	// @Excel(name = "商品分类(最后一级名称)")
	private String categoryPath;

	@Excel(name = "商品类型", readConverterExp = "PHYSICAL_GOODS=实物商品,VIRTUAL_GOODS=虚拟商品,E_COUPON=电子卡券", combo = "实物商品,虚拟商品,电子卡券")
	private String goodsType;

	@Excel(name = "商品名称")
	private String goodsName;

	@Excel(name = "商品价格")
	private String price;

	@Excel(name = "卖点")
	private String sellingPoint;

	// 没有0
	// @Excel(name = "品牌",defaultValue = "0")
	private String brandId = "0";

	@Excel(name = "计量单位")
	private String goodsUnit;

	// RETAIL
	// @Excel(name = "销售模式")
	private String salesModel = "RETAIL";

	@Excel(name = "商品图片")
	// private List<String> goodsGalleryList;
	private String goodsGallery;

	private List<String> goodsGalleryList;
	@Excel(name = "规格(规格名=规格值&规格名=规格值，重量，货号，库存，成本价，价格，图片（图片之间&号分割））规格之间英文分号分割")
	private String sku;

	// @Excel(name = "店内分类")
	// 1508371533020696578 id
	private String storeCategoryPath;

	@Excel(name = "商品描述(详情)")
	private String intro;

	@Excel(name = "商品移动端详情")
	private String mobileIntro;

	@Excel(name = "库存")
	private Integer quantity;

	@Excel(name = "是否立即发布", readConverterExp = "true=是,false=否", combo = "true,false")
	private Boolean release;

	@Excel(name = "是否是推荐商品", readConverterExp = "true=是,false=否", combo = "true,false")
	private Boolean recommend;

	@ApiModelProperty(value = "推荐排名", required = true)
	private int recommendRanking;

	// ??不知道干啥的
	// @ApiModelProperty(value = "商品参数")
	// private List<GoodsParamsDTO> goodsParamsDTOList;
	//

	@Excel(name = "运费模板")
	private String templateId;

	// @ApiModelProperty(value = "是否有规格", hidden = true)
	// private String haveSpec;

	// @ApiModelProperty(value = "商品描述")
	// private String info;

	@ApiModelProperty(value = "是否重新生成sku数据")
	private Boolean regeneratorSkuFlag = true;

	@ApiModelProperty(value = "销售区域")
	private String salesArea;

	@ApiModelProperty(value = "保质期")
	private String shelfLife;

	@ApiModelProperty(value = "生产日期")
	private String productionDate;

	@ApiModelProperty(value = "店铺内排序")
	private int num;

	@ApiModelProperty(value = "来源（pc   app）")
	private String origin = "pc";
}
