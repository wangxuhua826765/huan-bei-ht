package cn.lili.modules.goods.entity.dto;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.StrUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
public class EsGoodsIndexDTO extends PageVO {

	@Field(type = FieldType.Text, analyzer = "ik_max_word")
	@ApiModelProperty("商品名称")
	private String goodsName;

	@ApiModelProperty("商品状态")
	private String marketEnable;

	@ApiModelProperty("店铺id")
	private String storeId;

	@ApiModelProperty("店铺状态")
	private String storeDisable;

	@ApiModelProperty("分类路径id")
	private String categoryId;

	@ApiModelProperty("商品名称")
	private String keyword;

	@ApiModelProperty("商品名称")
	private String authFlag;

	@ApiModelProperty(value = "是否是自营店铺")
	private Integer selfOperated;

	@ApiModelProperty(value = "是否是自营店铺")
	private String storeCatId;

	@ApiModelProperty(value = "排序类型")
	private String type;

	@ApiModelProperty(value = "销量")
	private Integer monthSales;

	@ApiModelProperty(value = "评分")
	private Double score;

	@ApiModelProperty(value = "价格升序")
	private Double priceAsc;

	@ApiModelProperty(value = "价格降序")
	private Double priceDesc;
	@ApiModelProperty(value = "价格降序")
	private Double startPrice;
	@ApiModelProperty(value = "价格降序")
	private Double endPrice;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (StrUtil.isNotEmpty(goodsName)) {
			queryWrapper.like("goods_name", goodsName);
		}
		if (StrUtil.isNotEmpty(keyword)) {
			queryWrapper.like("goods_name", keyword);
		}
		if (StrUtil.isNotEmpty(storeId)) {
			queryWrapper.eq("store_id", storeId);
		}
		if (StrUtil.isNotEmpty(authFlag)) {
			queryWrapper.eq("auth_flag", authFlag);
		}
		if (StrUtil.isNotEmpty(categoryId)) {
			queryWrapper.like("category_path", categoryId);
		}
		if (StrUtil.isNotEmpty(marketEnable)) {
			queryWrapper.eq("market_enable", marketEnable);
		}
		if (StrUtil.isNotEmpty(storeDisable)) {
			queryWrapper.eq("store_disable", storeDisable);
		}
		if (selfOperated != null) {
			queryWrapper.eq("g.self_operated", selfOperated);
		}
		if (storeCatId != null) {
			queryWrapper.like("g.store_category_path", storeCatId);
		}
		if (null != score) {
			queryWrapper.orderByDesc("score");// 根据推荐评分排序
		}
		if (monthSales != null) {
			queryWrapper.orderByDesc("monthSales");
		}
		if (priceAsc != null) {
			queryWrapper.orderByAsc("price");
		}
		if (priceDesc != null) {
			queryWrapper.orderByDesc("price");
		}
		if (startPrice != null && endPrice != null) {
			queryWrapper.gt("price", startPrice);
			queryWrapper.lt("price", endPrice);
		}
		return queryWrapper;
	};
}
