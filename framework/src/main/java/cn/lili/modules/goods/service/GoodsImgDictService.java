package cn.lili.modules.goods.service;

import cn.lili.modules.goods.entity.dos.GoodsImgDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * GoodsImgDictService.
 *
 * @author Li Bing
 * @since 2022.03.28
 */
public interface GoodsImgDictService extends IService<GoodsImgDict> {

}
