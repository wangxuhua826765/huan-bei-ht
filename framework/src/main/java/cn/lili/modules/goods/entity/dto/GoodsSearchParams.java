package cn.lili.modules.goods.entity.dto;

import cn.hutool.core.text.CharSequenceUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.goods.entity.enums.GoodsAuthEnum;
import cn.lili.modules.goods.entity.enums.GoodsStatusEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;

/**
 * 商品查询条件
 *
 * @author pikachu
 * @since 2020-02-24 19:27:20
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GoodsSearchParams extends PageVO {

	private static final long serialVersionUID = 2544015852728566887L;

	@ApiModelProperty(value = "商品编号")
	private String goodsId;

	@ApiModelProperty(value = "商品名称")
	private String goodsName;

	@ApiModelProperty(value = "商品编号")
	private String id;

	@ApiModelProperty(value = "商家ID")
	private String storeId;

	@ApiModelProperty(value = "卖家名字")
	private String storeName;

	@ApiModelProperty(value = "价格,可以为范围，如10_1000")
	private String price;

	@ApiModelProperty(value = "分类path")
	private String categoryPath;

	@ApiModelProperty(value = "店铺分类id")
	private String storeCategoryPath;

	@ApiModelProperty(value = "地址id，'，'分割 ")
	private String storeAddressIdPath;

	@ApiModelProperty(value = "是否自营")
	private Boolean selfOperated;

	/**
	 * @see GoodsStatusEnum
	 */
	@ApiModelProperty(value = "上下架状态")
	private String marketEnable;

	/**
	 * @see GoodsAuthEnum
	 */
	@ApiModelProperty(value = "审核状态")
	private String authFlag;

	@ApiModelProperty(value = "库存数量")
	private Integer leQuantity;

	@ApiModelProperty(value = "库存数量")
	private Integer geQuantity;

	@ApiModelProperty(value = "是否为推荐商品")
	private Boolean recommend;

	@ApiModelProperty(value = "区域编码")
	private String adCode;
	private List<String> adCodeList;

	@ApiModelProperty(value = "区域编码")
	private String chooseCode;

	@ApiModelProperty(value = "1实物商品2爆品商品")
	private Integer state;

	/**
	 * @see cn.lili.modules.goods.entity.enums.GoodsTypeEnum
	 */
	@ApiModelProperty(value = "商品类型")
	private String goodsType;

	@ApiModelProperty(value = "显示的数量")
	private Integer limit;

	@ApiModelProperty(value = "经纬度")
	private String trapeze;

	@ApiModelProperty(value = "评价分类 1：销量 2：评价")
	private String type;

	@ApiModelProperty(value = "月销")
	private Integer monthSales;

	@ApiModelProperty(value = "评分")
	private Double score;

	@ApiModelProperty(value = "销量排序 1 正序  0  倒叙")
	private String sortSales;

	@ApiModelProperty(value = "是否根据id分组 true 是  false 否 ")
	private Boolean groupBySort;

	@ApiModelProperty(value = "会员Id")
	private List<String> memberIds;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (CharSequenceUtil.isNotEmpty(storeAddressIdPath)) {
			queryWrapper.like("g.store_address_id_path", storeAddressIdPath);
		}
		if (memberIds != null && memberIds.size() > 0) {
			queryWrapper.in("s.member_id", memberIds);
		}
		if (adCodeList != null && adCodeList.size() > 0) {
			queryWrapper.in("s.ad_code", adCodeList);
		}
		if (CharSequenceUtil.isNotEmpty(goodsId)) {
			queryWrapper.eq("g.goods_id", goodsId);
		}
		if (CharSequenceUtil.isNotEmpty(goodsName)) {
			queryWrapper.like("g.goods_name", goodsName);
		}
		if (CharSequenceUtil.isNotEmpty(id)) {
			queryWrapper.in("g.id", Arrays.asList(id.split(",")));
		}
		if (CharSequenceUtil.isNotEmpty(storeId)) {
			queryWrapper.eq("g.store_id", storeId);
		}
		if (CharSequenceUtil.isNotEmpty(storeName)) {
			queryWrapper.like("g.store_name", storeName);
		}
		if (CharSequenceUtil.isNotEmpty(categoryPath)) {
			queryWrapper.like("g.category_path", categoryPath);
		}
		if (CharSequenceUtil.isNotEmpty(storeCategoryPath)) {
			queryWrapper.like("g.store_category_path", storeCategoryPath);
		}
		if (selfOperated != null) {
			queryWrapper.eq("g.self_operated", selfOperated);
		}
		if (CharSequenceUtil.isNotEmpty(marketEnable)) {
			queryWrapper.eq("g.market_enable", marketEnable);
		}
		if (CharSequenceUtil.isNotEmpty(authFlag)) {
			queryWrapper.eq("g.auth_flag", authFlag);
		}
		if (leQuantity != null) {
			queryWrapper.le("g.quantity", leQuantity);
		}
		if (geQuantity != null) {
			queryWrapper.ge("g.quantity", geQuantity);
		}
		if (recommend != null && recommend) {
			queryWrapper.eq("g.recommend", recommend);
		}
		if (CharSequenceUtil.isNotEmpty(goodsType)) {
			queryWrapper.eq("g.goods_type", goodsType);
		}

		queryWrapper.eq("g.delete_flag", false);
		this.betweenWrapper(queryWrapper);
		if (null != recommend && recommend && state != null) {
			queryWrapper.orderByAsc("g.recommend_ranking");// 根据推荐排名排序
		}
		if (recommend != null && !recommend) {
			queryWrapper.orderByDesc("g.sort");
		}
		if (groupBySort != null && groupBySort) {
			queryWrapper.groupBy("g.id");
		}
		if (sortSales != null && "1".equals(sortSales)) {
			queryWrapper.orderByAsc("monthSales");
		}
		if (sortSales != null && "0".equals(sortSales)) {
			queryWrapper.orderByDesc("monthSales");
		}

		if (limit != null) {
			queryWrapper.last("limit " + limit);
		}
		return queryWrapper;
	}

	public <T> QueryWrapper<T> queryWrappers() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (CharSequenceUtil.isNotEmpty(storeAddressIdPath)) {
			queryWrapper.like("store_address_id_path", storeAddressIdPath);
		}
		if (CharSequenceUtil.isNotEmpty(goodsId)) {
			queryWrapper.eq("goods_id", goodsId);
		}
		if (CharSequenceUtil.isNotEmpty(goodsName)) {
			queryWrapper.like("goods_name", goodsName);
		}
		if (CharSequenceUtil.isNotEmpty(id)) {
			queryWrapper.in("id", Arrays.asList(id.split(",")));
		}
		if (CharSequenceUtil.isNotEmpty(storeId)) {
			queryWrapper.eq("store_id", storeId);
		}
		if (CharSequenceUtil.isNotEmpty(storeName)) {
			queryWrapper.like("store_name", storeName);
		}
		if (CharSequenceUtil.isNotEmpty(categoryPath)) {
			queryWrapper.like("category_path", categoryPath);
		}
		if (CharSequenceUtil.isNotEmpty(storeCategoryPath)) {
			queryWrapper.like("store_category_path", storeCategoryPath);
		}
		if (selfOperated != null) {
			queryWrapper.eq("self_operated", selfOperated);
		}
		if (CharSequenceUtil.isNotEmpty(marketEnable)) {
			queryWrapper.eq("market_enable", marketEnable);
		}
		if (CharSequenceUtil.isNotEmpty(authFlag)) {
			queryWrapper.eq("auth_flag", authFlag);
		}
		if (leQuantity != null) {
			queryWrapper.le("quantity", leQuantity);
		}
		if (geQuantity != null) {
			queryWrapper.ge("quantity", geQuantity);
		}
		if (recommend != null && recommend) {
			queryWrapper.eq("recommend", recommend);
		}
		if (CharSequenceUtil.isNotEmpty(goodsType)) {
			queryWrapper.eq("goods_type", goodsType);
		}

		queryWrapper.eq("delete_flag", false);
		this.betweenWrapper(queryWrapper);
		if (null != recommend && recommend && state != null) {
			queryWrapper.orderByAsc("recommend_ranking");// 根据推荐排名排序
		}
		if (recommend != null && !recommend) {
			queryWrapper.orderByDesc("sort");
		}
		if (groupBySort != null && groupBySort) {
			queryWrapper.groupBy("id");
		}
		if (sortSales != null && "1".equals(sortSales)) {
			queryWrapper.orderByAsc("monthSales");
		}
		if (sortSales != null && "0".equals(sortSales)) {
			queryWrapper.orderByDesc("monthSales");
		}

		if (limit != null) {
			queryWrapper.last("limit " + limit);
		}
		return queryWrapper;
	}

	private <T> void betweenWrapper(QueryWrapper<T> queryWrapper) {
		if (CharSequenceUtil.isNotEmpty(price)) {
			String[] s = price.split("_");
			if (s.length > 1) {
				queryWrapper.between("price", s[0], s[1]);
			} else {
				queryWrapper.ge("price", s[0]);
			}
		}
	}

}
