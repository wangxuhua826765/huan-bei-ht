package cn.lili.modules.goods.entity.vos;

import cn.lili.modules.goods.entity.dos.Goods;
import cn.lili.modules.goods.util.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author chenke
 * @since 2022-4-13 14:24:02
 */
@Data
public class GoodsSkuImportVo implements Serializable {

	@Excel(name = "产品编号")
	private String goodsNo;

	@Excel(name = "产品名称")
	private String goodsName;

	@Excel(name = "规格名")
	private String specs;

	@Excel(name = "重量")
	private String weight;

	@Excel(name = "货号")
	private String sn;

	@Excel(name = "库存")
	private String quantity;

	@Excel(name = "结算价（人民币）")
	private String cost;

	@Excel(name = "零售价（焕贝）")
	private String price;

	@Excel(name = "图片")
	private String specsImg;

	@Excel(name = "图片后缀")
	private String specsImgSuffix;

	/**
	 * 商家图片,用于校验数据
	 */
	private Map<String, String> goodsImgDictMap;

	/**
	 * SPU信息
	 */
	private Goods goods;
}
