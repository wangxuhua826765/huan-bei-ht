package cn.lili.modules.goods.entity.dto;

import cn.hutool.core.util.StrUtil;
import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
public class EsStoreIndexDTO extends PageVO {

	@Field(type = FieldType.Text, analyzer = "ik_max_word")
	@ApiModelProperty("店铺名称")
	private String storeName;

	@ApiModelProperty("店铺状态")
	private String storeDisable;

	@ApiModelProperty("店铺id")
	private String id;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (StrUtil.isNotEmpty(storeName)) {
			queryWrapper.like("store_name", storeName);
		}
		if (StrUtil.isNotEmpty(storeDisable)) {
			queryWrapper.eq("store_disable", storeDisable);
		}
		if (StrUtil.isNotEmpty(id)) {
			queryWrapper.eq("id", id);
		}
		return queryWrapper;
	};
}
