package cn.lili.modules.statistics.mapper;

import cn.lili.modules.order.order.entity.dos.StoreFlow;
import cn.lili.modules.statistics.entity.vo.CategoryStatisticsDataVO;
import cn.lili.modules.statistics.entity.vo.GoodsStatisticsDataVO;
import cn.lili.modules.statistics.entity.vo.StoreStatisticsDataVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 商品统计数据处理层
 *
 * @author Bulbasaur
 * @since 2020/11/17 7:34 下午
 */
public interface StoreFlowStatisticsMapper extends BaseMapper<StoreFlow> {

	/**
	 * 商品统计
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 商品统计列表
	 */
	@Select("SELECT oi.goods_id,oi.goods_name,SUM(oi.goods_price) AS price,SUM(oi.num) AS num,oi.image FROM li_order_item oi"
			+ " inner join li_order o on o.sn = oi.order_sn inner JOIN li_store s ON s.id = o.store_id  ${ew.customSqlSegment}")
	List<GoodsStatisticsDataVO> getGoodsStatisticsData(IPage<GoodsStatisticsDataVO> page,
			@Param(Constants.WRAPPER) Wrapper<GoodsStatisticsDataVO> queryWrapper);

	/**
	 * 分类统计
	 *
	 * @param queryWrapper
	 *            查询条件
	 * @return 分类统计列表
	 */
	@Select("SELECT category_id,category_name,SUM(price) AS price,SUM(num) AS num FROM li_store_flow ${ew.customSqlSegment}")
	List<CategoryStatisticsDataVO> getCateGoryStatisticsData(
			@Param(Constants.WRAPPER) Wrapper<CategoryStatisticsDataVO> queryWrapper);

	/**
	 * 店铺统计列表
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询参数
	 * @return 店铺统计列表
	 */
	@Select("SELECT s.id AS storeId, s.store_name AS storeName, SUM( wd.money ) AS price, "
		+ "count( wd.sn ) AS num,  s.store_logo AS storeLogo, "
		+ "s.store_address_path AS storeAddressPath,  s.store_address_detail AS storeAddressDetail  "
		+ "FROM  li_wallet_detail wd  "
		+ "LEFT JOIN li_store s ON s.member_id = wd.payee  ${ew.customSqlSegment}")
	List<StoreStatisticsDataVO> getStoreStatisticsData(IPage<GoodsStatisticsDataVO> page,
			@Param(Constants.WRAPPER) Wrapper<GoodsStatisticsDataVO> queryWrapper);

	@Select("SELECT SUM( flow_price - IFNULL(commission,0) ) AS price, COUNT( 0 ) AS num FROM li_order ${ew.customSqlSegment}")
	Map<String, Object>  getOrderStatisticsPrice(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);
}