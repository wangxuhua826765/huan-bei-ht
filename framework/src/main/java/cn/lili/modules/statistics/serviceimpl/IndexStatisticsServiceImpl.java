package cn.lili.modules.statistics.serviceimpl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.utils.BeanUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.goods.entity.enums.GoodsAuthEnum;
import cn.lili.modules.goods.entity.enums.GoodsStatusEnum;
import cn.lili.modules.member.entity.dos.GradeLevel;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.service.GradeLevelService;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.member.serviceimpl.ExtensionServiceImpl;
import cn.lili.modules.order.order.entity.enums.FlowTypeEnum;
import cn.lili.modules.order.order.entity.enums.OrderStatusEnum;
import cn.lili.modules.order.order.service.OrderService;
import cn.lili.modules.order.trade.entity.enums.AfterSaleTypeEnum;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.service.AdminUserService;
import cn.lili.modules.statistics.entity.dto.GoodsStatisticsQueryParam;
import cn.lili.modules.statistics.entity.dto.StatisticsQueryParam;
import cn.lili.modules.statistics.entity.enums.SearchTypeEnum;
import cn.lili.modules.statistics.entity.vo.*;
import cn.lili.modules.statistics.service.*;
import cn.lili.modules.statistics.util.StatisticsDateUtil;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.enums.BillStatusEnum;
import cn.lili.modules.store.entity.enums.StoreStatusEnum;
import cn.lili.modules.store.service.StoreService;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.service.MemberWithdrawApplyService;
import cn.lili.modules.wallet.service.RechargeService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 首页统计数据业务层实现
 *
 * @author Bulbasaur
 * @since 2020/12/15 17:57
 */
@Service
public class IndexStatisticsServiceImpl implements IndexStatisticsService {

	/**
	 * 订单统计
	 */
	@Autowired
	private OrderStatisticsService orderStatisticsService;

	/**
	 * 会员统计
	 */
	@Autowired
	private MemberStatisticsService memberStatisticsService;
	/**
	 * 商品统计
	 */
	@Autowired
	private GoodsStatisticsService goodsStatisticsService;
	/**
	 * 商品统计
	 */
	@Autowired
	private StoreFlowStatisticsService storeFlowStatisticsService;
	/**
	 * 店铺
	 */
	@Autowired
	private StoreStatisticsService storeStatisticsService;
	/**
	 * 店铺
	 */
	@Autowired
	private MemberEvaluationStatisticsService memberEvaluationStatisticsService;
	/**
	 * 售后
	 */
	@Autowired
	private AfterSaleStatisticsService afterSaleStatisticsService;
	/**
	 * 投诉
	 */
	@Autowired
	private OrderComplaintStatisticsService orderComplaintStatisticsService;
	/**
	 * 分销员提现
	 */
	@Autowired
	private DistributionCashStatisticsService distributionCashStatisticsService;
	/**
	 * 平台PV统计
	 */
	@Autowired
	private PlatformViewService platformViewService;
	/**
	 * 结算单
	 */
	@Autowired
	private BillStatisticsService billStatisticsService;
	/**
	 * 秒杀活动
	 */
	@Autowired
	private SeckillStatisticsService seckillStatisticsService;
	/**
	 * 合伙人
	 */
	@Autowired
	private PartnerService partnerService;

	/**
	 * 会员
	 */
	@Autowired
	private MemberService memberService;
	/**
	 * 会员等级
	 */
	@Autowired
	private GradeLevelService gradeLevelService;
	@Autowired
	private MemberWithdrawApplyService memberWithdrawApplyService;
	@Autowired
	private AdminUserService adminUserService;
	@Autowired
	private RegionService regionService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private RechargeService rechargeService;

	@Override
	public IndexNoticeVO indexNotice() {

		IndexNoticeVO indexNoticeVO = new IndexNoticeVO();
		// 商品审核
		indexNoticeVO.setGoods(goodsStatisticsService.goodsNum(null, GoodsAuthEnum.TOBEAUDITED, new ArrayList()));
		// 店铺入驻审核
		indexNoticeVO.setStore(storeStatisticsService.auditNum());
		// 售后申请
		indexNoticeVO.setRefund(afterSaleStatisticsService.applyNum(null));
		// 投诉审核
		indexNoticeVO.setComplain(orderComplaintStatisticsService.waitComplainNum());
		// 分销员提现审核
		indexNoticeVO.setDistributionCash(distributionCashStatisticsService.newDistributionCash());
		// 待处理商家结算
		indexNoticeVO.setWaitPayBill(billStatisticsService.billNum(BillStatusEnum.CHECK));
		// 待处理申请的合伙人 new
		indexNoticeVO.setPartner(partnerService.partnerNum());
		// 销售钱包提现
		indexNoticeVO.setSale(memberWithdrawApplyService.saleNum());
		// 推广钱包提现
		indexNoticeVO.setPromote(memberWithdrawApplyService.promoteNum());
		return indexNoticeVO;
	}

	@Override
	public IndexStatisticsVO indexStatistics() {
		List<String> memberIdListAdmin = new ArrayList(); // 超级管理员下所有的会员
		List<String> memberIdList = new ArrayList(); // 该区域下所的会员人员
		List storeIdList = new ArrayList(); // 该区域下的所有店铺
		List<String> storeIdListAdmin = new ArrayList(); // 超级管理员下所有的会员
		List itemAdCod = new ArrayList(); // 代理商所管辖的区域adcode
		// 首页统计内容130102
		IndexStatisticsVO indexStatisticsVO = new IndexStatisticsVO();
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		Boolean agent = currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT));
		if (agent) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// if (null != adminUser && StringUtils.isNotEmpty(adminUser.getRegionId())) {
			// itemAdCod = regionService.getItemAdCod(adminUser.getRegionId());
			// List<Member> memberList =
			// memberService.list(new QueryWrapper<Member>().in("location",
			// itemAdCod).eq("delete_flag", 0));
			// memberList.forEach(item -> {
			// memberIdList.add(item.getId());
			// });
			// }
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				itemAdCod = regionService.getPartnerAdCode(partner.getRegionId(), "4");
				List<Member> memberList = memberService
						.list(new QueryWrapper<Member>().in("location", itemAdCod).eq("delete_flag", 0));
				memberList.forEach(item -> {
					memberIdList.add(item.getId());
				});
			}
		}
		if (agent) {
			List<Store> list = storeService
					.list(new QueryWrapper<Store>().in("member_id", memberIdList).eq("delete_flag", 0)
						.in("store_disable",StoreStatusEnum.OPEN.name(), StoreStatusEnum.CLOSED.name()));
			list.forEach(item -> {
				storeIdList.add(item.getId());
			});
		}
		// 管理员登录进来所有的用户所有店铺
		if (!agent) {
			List<Member> memberList = memberService.list(new QueryWrapper<Member>().eq("delete_flag", 0)
			.eq("disabled",1));
			memberList.forEach(item -> {
				memberIdListAdmin.add(item.getId());
			});
			List<Store> list = storeService
					.list(new QueryWrapper<Store>().in("member_id", memberIdListAdmin).eq("delete_flag", 0)
						.in("store_disable",StoreStatusEnum.OPEN.name(), StoreStatusEnum.CLOSED.name()));
			list.forEach(items -> {
				storeIdListAdmin.add(items.getId());
			});
		}
		/**
		 * 今日概括
		 */
		// 下单统计
		Map<String, Object> map = storeFlowStatisticsService.getOrderStatisticsPrice(agent ? memberIdList : new ArrayList());
		// 今日下单数
		indexStatisticsVO.setTodayOrderNum(map.get("num") == null ? 0L : (Long) map.get("num"));
		// 今日下单金额
		indexStatisticsVO.setTodayOrderPrice(map.get("price") == null ? 0D : (Double) map.get("price"));
		// 今日新增店铺数量
		indexStatisticsVO.setTodayStoreNum(storeStatisticsService.todayStoreNum(agent ? memberIdList : memberIdListAdmin));
		// 今日新增商品数量
		indexStatisticsVO.setTodayGoodsNum(goodsStatisticsService.todayUpperNum(agent ? storeIdList : storeIdListAdmin));
		// 今日新增合伙人数量
		QueryWrapper<Partner> queryWrapper = new QueryWrapper<>();
		queryWrapper.ge("create_time", DateUtil.beginOfDay(new DateTime()));
		queryWrapper.eq("delete_flag",0);
		queryWrapper.eq("audit_state","PASS");
		if (itemAdCod.size() > 0) {
			queryWrapper.in("region_id", itemAdCod);
		}
		indexStatisticsVO.setTodayPartnerNum(partnerService.count(queryWrapper));
		/**
		 * 会员数量
		 */
		// c端会员数
		indexStatisticsVO.setMemberCNum(gradeLevelService.getSlMap(itemAdCod, ""));
		// 金卡数量
		indexStatisticsVO.setMemberJkNum(gradeLevelService.getSlMap(itemAdCod, "2"));
		// 银卡
		indexStatisticsVO.setMemberYkNum(gradeLevelService.getSlMap(itemAdCod, "1"));
		// 普通
		indexStatisticsVO.setMemberPtNum(gradeLevelService.getSlMap(itemAdCod, "0"));
		// 合伙人数
		QueryWrapper<Partner> pqueryWrapper = new QueryWrapper<>();
		pqueryWrapper.eq("p.delete_flag", false);
		pqueryWrapper.eq("m.delete_flag", false);
		if (agent && memberIdList.size() > 0) {
			pqueryWrapper.in("p.member_id", memberIdList);
		}
		if (agent && memberIdListAdmin.size() > 0) {
			pqueryWrapper.in("p.member_id", memberIdListAdmin);
		}
		pqueryWrapper.eq("p.audit_state","PASS");
		pqueryWrapper.in(" p.partner_type","1","2","4");
		pqueryWrapper.eq("m.disabled",1);
		pqueryWrapper.eq(" p.partner_state",0);
		pqueryWrapper.eq("p.pay_state",1).or().isNull("p.pay_state");
		indexStatisticsVO.setPartnerNum(partnerService.countPartner(pqueryWrapper));

		/**
		 * 合伙人
		 */
		// B端天使合伙人
		indexStatisticsVO.setMemberTianShiBNum(partnerService.getCountMember(itemAdCod, "2", true));
		// 天使合伙人
		indexStatisticsVO.setMemberTianShiNum(partnerService.getCountMember(itemAdCod, "2", false));
		// 事业合伙人
		indexStatisticsVO.setMemberShiYeNum(partnerService.getCountMember(itemAdCod, "1", false));
		/**
		 * 城市合伙人下属会员
		 */
		// C端
		indexStatisticsVO.setChengShicduan(partnerService.getChengshi(itemAdCod, ""));
		// B端
		indexStatisticsVO.setChengShiBDuan(partnerService.getChengshi(itemAdCod, "b"));
		// 天使
		indexStatisticsVO.setChengShiTShi(partnerService.getChengshi(itemAdCod, "2"));
		// 事业
		indexStatisticsVO.setChengShiSye(partnerService.getChengshi(itemAdCod, "1"));
		/**
		 * 关于店铺
		 */
		// 获取总店铺数量
		if (agent) {
			indexStatisticsVO.setStoreNum(storeIdList.size());
			// 获取总上架商品数量
			indexStatisticsVO.setGoodsNum(
					goodsStatisticsService.goodsNum(GoodsStatusEnum.UPPER, GoodsAuthEnum.PASS, storeIdList));
			// 获取总订单数量
			indexStatisticsVO.setOrderNum(orderStatisticsService.orderNum(null, memberIdList));

		} else {
			indexStatisticsVO.setStoreNum(storeIdListAdmin.size());
			// 获取总上架商品数量
			indexStatisticsVO.setGoodsNum(
					goodsStatisticsService.goodsNum(GoodsStatusEnum.UPPER, GoodsAuthEnum.PASS, storeIdListAdmin));
			// 获取总订单数量
			indexStatisticsVO.setOrderNum(orderStatisticsService.orderNum(null, new ArrayList()));

		}

		// //获取总会员数量
		// indexStatisticsVO.setMemberNum(memberStatisticsService.getMemberCount());
		// //今日新增会员数量
		// indexStatisticsVO.setTodayMemberNum(memberStatisticsService.todayMemberNum());
		// //今日新增评论数量
		// indexStatisticsVO.setTodayMemberEvaluation(memberEvaluationStatisticsService.todayMemberEvaluation());
		// 当前在线人数
		// indexStatisticsVO.setCurrentNumberPeopleOnline(platformViewService.online());
		// 流量统计
		// StatisticsQueryParam queryParam = new StatisticsQueryParam();
		// 今日uv
		// queryParam.setSearchType(SearchTypeEnum.TODAY.name());
		// indexStatisticsVO.setTodayUV(platformViewService.countUv(queryParam));
		// 昨日访问数UV
		// queryParam.setSearchType(SearchTypeEnum.YESTERDAY.name());
		// indexStatisticsVO.setYesterdayUV(platformViewService.countUv(queryParam));
		// 前七日访问数UV
		// queryParam.setSearchType(SearchTypeEnum.LAST_SEVEN.name());
		// indexStatisticsVO.setLastSevenUV(platformViewService.countUv(queryParam));
		// 三十日访问数UV
		// queryParam.setSearchType(SearchTypeEnum.LAST_THIRTY.name());
		// indexStatisticsVO.setLastThirtyUV(platformViewService.countUv(queryParam));

		return indexStatisticsVO;
	}

	@Override
	public StoreIndexStatisticsVO storeIndexStatistics() {

		String storeId = Objects.requireNonNull(UserContext.getCurrentUser()).getStoreId();
		StoreIndexStatisticsVO storeIndexStatisticsVO = new StoreIndexStatisticsVO();

		// 商品总数量
		storeIndexStatisticsVO
				.setGoodsNum(goodsStatisticsService.goodsNum(GoodsStatusEnum.UPPER, null, new ArrayList()));
		// 订单总数量、订单总金额
		Map<String, Object> map = orderStatisticsService.getOrderStatisticsPrice();
		storeIndexStatisticsVO.setOrderNum(Convert.toInt(map.get("num").toString()));
		storeIndexStatisticsVO
				.setOrderPrice(map.get("price") != null ? Double.parseDouble(map.get("price").toString()) : 0.0);
		// 线下订单总额,线下订单数量
		Map<String, Object> orderStatisticsPriceOffline = orderStatisticsService.getOrderStatisticsPriceOffline();
		storeIndexStatisticsVO.setOrdersOffline(orderStatisticsPriceOffline.get("price") != null
				? Double.parseDouble(orderStatisticsPriceOffline.get("price").toString())
				: 0.0);
		storeIndexStatisticsVO.setOrderNumOffline(Convert.toInt(orderStatisticsPriceOffline.get("num").toString()));
		// 线上订单总额,线上订单数量
		Map<String, Object> orderStatisticsPriceOnline = orderStatisticsService.getOrderStatisticsPriceOnline();
		storeIndexStatisticsVO.setOrdersOnline(orderStatisticsPriceOnline.get("price") != null
				? Double.parseDouble(orderStatisticsPriceOnline.get("price").toString())
				: 0.0);
		storeIndexStatisticsVO.setOrderNumOnline(Convert.toInt(orderStatisticsPriceOnline.get("num").toString()));
		// 虚拟订单总额,虚拟订单数量
		Map<String, Object> orderStatisticsFictitious = orderStatisticsService.getOrderStatisticsFictitious();
		storeIndexStatisticsVO.setOrdersFictitious(orderStatisticsFictitious.get("price") != null
				? Double.parseDouble(orderStatisticsFictitious.get("price").toString())
				: 0.0);
		storeIndexStatisticsVO.setOrderNumFictitious(Convert.toInt(orderStatisticsFictitious.get("num").toString()));
		// 访问量
		StatisticsQueryParam queryParam = new StatisticsQueryParam();
		queryParam.setSearchType(SearchTypeEnum.TODAY.name());
		queryParam.setStoreId(storeId);
		PlatformViewVO platformViewVO = platformViewService.list(queryParam).get(0);
		storeIndexStatisticsVO.setStoreUV(platformViewVO.getUvNum().intValue());

		// 待付款订单数量
		storeIndexStatisticsVO
				.setUnPaidOrder(orderStatisticsService.orderNum(OrderStatusEnum.UNPAID.name(), new ArrayList<>()));
		// 待发货订单数量
		storeIndexStatisticsVO.setUnDeliveredOrder(
				orderStatisticsService.orderNum(OrderStatusEnum.UNDELIVERED.name(), new ArrayList<>()));
		// 待收货订单数量
		storeIndexStatisticsVO.setDeliveredOrder(
				orderStatisticsService.orderNum(OrderStatusEnum.DELIVERED.name(), new ArrayList<>()));

		// 待处理退货数量
		storeIndexStatisticsVO
				.setReturnGoods(afterSaleStatisticsService.applyNum(AfterSaleTypeEnum.RETURN_GOODS.name()));
		// 待处理退款数量
		storeIndexStatisticsVO
				.setReturnMoney(afterSaleStatisticsService.applyNum(AfterSaleTypeEnum.RETURN_MONEY.name()));
		// 待回复评价数量
		storeIndexStatisticsVO.setMemberEvaluation(memberEvaluationStatisticsService.getWaitReplyNum());
		// 待处理投诉数量
		storeIndexStatisticsVO.setComplaint(orderComplaintStatisticsService.waitComplainNum());

		// 待上架商品数量
		storeIndexStatisticsVO
				.setWaitUpper(goodsStatisticsService.goodsNum(GoodsStatusEnum.DOWN, null, new ArrayList()));
		// 待审核商品数量
		storeIndexStatisticsVO
				.setWaitAuth(goodsStatisticsService.goodsNum(null, GoodsAuthEnum.TOBEAUDITED, new ArrayList()));

		// 可参与秒杀活动数量
		storeIndexStatisticsVO.setSeckillNum(seckillStatisticsService.getApplyNum());
		// 待处理商家结算
		storeIndexStatisticsVO.setWaitPayBill(billStatisticsService.billNum(BillStatusEnum.OUT));

		return storeIndexStatisticsVO;
	}

	@Override
	public List<GoodsStatisticsDataVO> goodsStatistics(GoodsStatisticsQueryParam statisticsQueryParam) {
		// 查询商品
		return storeFlowStatisticsService.getGoodsStatisticsData(statisticsQueryParam, 10);
	}

	@Override
	public List<StoreStatisticsDataVO> storeStatistics(StatisticsQueryParam statisticsQueryParam) {

		QueryWrapper queryWrapper = Wrappers.query();

		Date[] dates = StatisticsDateUtil.getDateArray(statisticsQueryParam);
		Date startTime = dates[0], endTime = dates[1];
//		queryWrapper.between("f.create_time", startTime, endTime);

		queryWrapper.orderByDesc("price");

		queryWrapper.groupBy("wd.payee");

		queryWrapper.in(CollectionUtils.isNotEmpty(statisticsQueryParam.getAdCodeList()), "s.ad_code",
				statisticsQueryParam.getAdCodeList());

		queryWrapper.eq("wd.payee_owner", WalletOwnerEnum.SALE.name());
		queryWrapper.likeRight("wd.sn","O");

		// 查询前十条记录
		Page page = new Page<StoreStatisticsDataVO>(1, 10);

		return storeFlowStatisticsService.getStoreStatisticsData(page, queryWrapper);
	}

	/**
	 * 获取当月查询参数
	 *
	 * @return 当月查询参数
	 */
	private StatisticsQueryParam getStatisticsQueryParam() {

		StatisticsQueryParam statisticsQueryParam = new StatisticsQueryParam();

		return statisticsQueryParam;
	}

	/**
	 * 获取当月订单查询条件
	 *
	 * @return 当月订单查询参数
	 */
	private GoodsStatisticsQueryParam getGoodsStatisticsQueryParam() {
		GoodsStatisticsQueryParam goodsStatisticsQueryParam = new GoodsStatisticsQueryParam();
		StatisticsQueryParam statisticsQueryParam = this.getStatisticsQueryParam();
		BeanUtil.copyProperties(goodsStatisticsQueryParam, statisticsQueryParam);

		// 如果登录是商家的账号，获取商家相关统计内容
		if (UserContext.getCurrentUser().getRole().equals(UserEnums.STORE)) {
			goodsStatisticsQueryParam.setStoreId(UserContext.getCurrentUser().getStoreId());
		}
		DateTime dateTime = new DateTime();
		goodsStatisticsQueryParam.setYear(dateTime.year());
		goodsStatisticsQueryParam.setMonth(dateTime.monthBaseOne());
		return goodsStatisticsQueryParam;
	}

	@Override
	public Map<String, List> chart(GoodsStatisticsQueryParam statisticsQueryParam) {
		List itemAdCod = new ArrayList(); // 代理商所管辖的区域adcode
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// if (null != adminUser && StringUtils.isNotEmpty(adminUser.getRegionId())) {
			// itemAdCod = regionService.getItemAdCod(adminUser.getRegionId());
			// }
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				itemAdCod = regionService.getPartnerAdCode(partner.getRegionId(), "4");
			}
		}
		Map map = new HashMap();
		List member = new ArrayList(); // c
		List partnerSy = new ArrayList(); // 事业
		List partnerTs = new ArrayList(); // 天使
		List<String> dateList = new ArrayList<>();
		if (StringUtils.isNotEmpty(statisticsQueryParam.getState()) && statisticsQueryParam.getState().equals("year")) {
			dateList = this.getMonthStr();
		} else {
			dateList = gatDateS(statisticsQueryParam.getStartTime(), statisticsQueryParam.getEndTime());
		}
		for (int a = 0; a < dateList.size(); a++) {
			// c段会员
			member.add(memberService.count(new QueryWrapper<Member>().like("create_time", dateList.get(a))
					.in(itemAdCod.size() > 0, "location", itemAdCod)));
			// 事业
			partnerSy.add(partnerService.count(new QueryWrapper<Partner>().like("create_time", dateList.get(a))
					.eq("partner_type", "1").in(itemAdCod.size() > 0, "region_id", itemAdCod)));
			// 天使
			partnerTs.add(partnerService.count(new QueryWrapper<Partner>().like("create_time", dateList.get(a))
					.eq("partner_type", "2").in(itemAdCod.size() > 0, "region_id", itemAdCod)));
		}
		map.put("date", dateList);
		map.put("members", member);
		map.put("partnerSy", partnerSy);
		map.put("partnerTs", partnerTs);
		return map;
	}

	@Override
	public Map<String, List> chartMoney(GoodsStatisticsQueryParam statisticsQueryParam) {
		List<String> memberIdList = new ArrayList(); // 该区域下所的会员人员
		List<String> partnerIdList = new ArrayList<>(); // 带合伙人身份的消费总额
		List itemAdCod = new ArrayList(); // 代理商所管辖的区域adcode
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// if (null != adminUser && StringUtils.isNotEmpty(adminUser.getRegionId())) {
			// itemAdCod = regionService.getItemAdCod(adminUser.getRegionId());
			// List<Member> memberList =
			// memberService.list(new QueryWrapper<Member>().in("location",
			// itemAdCod).eq("delete_flag", 0));
			// memberList.forEach(item -> {
			// memberIdList.add(item.getId());
			// });
			// }
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				itemAdCod = regionService.getPartnerAdCode(partner.getRegionId(), "4");
				List<Member> memberList = memberService
						.list(new QueryWrapper<Member>().in("location", itemAdCod).eq("delete_flag", 0));
				memberList.forEach(item -> {
					memberIdList.add(item.getId());
				});
			}
		}
		if (memberIdList.size() > 0) {
			List<Partner> memberIds = partnerService.list(new QueryWrapper<Partner>().in("member_id", memberIdList));
			memberIds.forEach(item -> {
				partnerIdList.add(item.getMemberId());
			});
		} else {
			List<Partner> memberIds = partnerService.list();
			memberIds.forEach(item -> {
				partnerIdList.add(item.getMemberId());
			});
		}
		Map map = new HashMap();
		List memberMoney = new ArrayList(); // 会员费总额
		List rechargeMoney = new ArrayList(); // 充值费总额
		List consumptionC = new ArrayList(); // 消费总额
		List partnerConsumption = new ArrayList(); // 合伙人身份消费
		List rechargeType = new ArrayList(); // 42 充值并交年费 43 充值并交银卡会员 44 充值并交金卡会员
		List<String> dateList = new ArrayList<>();
		rechargeType.add("42");
		rechargeType.add("43");
		rechargeType.add("44");
		if (StringUtils.isNotEmpty(statisticsQueryParam.getState()) && statisticsQueryParam.getState().equals("year")) {
			dateList = this.getMonthStr();
		} else {
			dateList = gatDateS(statisticsQueryParam.getStartTime(), statisticsQueryParam.getEndTime());
		}
		for (int a = 0; a < dateList.size(); a++) {
			// 会员费总额
			memberMoney
					.add(rechargeService.getByPageSum(new QueryWrapper<Recharge>().like("create_time", dateList.get(a))
							.in("recharge_type", rechargeType).in(memberIdList.size() > 0, "member_id", memberIdList)));
			// 充值费总额
			rechargeMoney
					.add(rechargeService.getByPageSum(new QueryWrapper<Recharge>().like("create_time", dateList.get(a))
							.eq("recharge_type", 4).in(memberIdList.size() > 0, "member_id", memberIdList)));
			// 消费总额
			consumptionC
					.add(rechargeService.getByPageSum(new QueryWrapper<Recharge>().like("create_time", dateList.get(a))
							.eq("recharge_type", 6).in(memberIdList.size() > 0, "member_id", memberIdList)));
			// 带合伙人身份的消费
			partnerConsumption
					.add(rechargeService.getByPageSum(new QueryWrapper<Recharge>().like("create_time", dateList.get(a))
							.eq("recharge_type", 6).in(memberIdList.size() > 0, "member_id", partnerIdList)));
		}
		map.put("date", dateList);
		map.put("memberMoney", memberMoney);
		map.put("rechargeMoney", rechargeMoney);
		map.put("consumptionC", consumptionC);
		map.put("partnerConsumption", partnerConsumption);
		return map;
	}

	private Logger log = LoggerFactory.getLogger(ExtensionServiceImpl.class);

	// 根据开始时间结束时间获取每一天
	private List<String> gatDateS(String start, String end) {
		List list = new ArrayList();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar st = Calendar.getInstance();
			Calendar ed = Calendar.getInstance();
			st.setTime(sdf.parse(start));
			ed.setTime(sdf.parse(end));
			while (!st.after(ed)) {
				list.add(sdf.format(st.getTime()));
				System.out.println(sdf.format(st.getTime()));
				st.add(Calendar.DAY_OF_YEAR, 1);
			}
		} catch (Exception e) {
			log.info(e + "");
		}
		return list;
	}

	/**
	 * 获得12个月时间戳
	 */
	public List<String> getMonthStr() {
		List<String> list = new ArrayList<String>();
		try {
			Long sj = System.currentTimeMillis();// 时间戳
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy");
			String dateStr = dateformat.format(sj);
			for (int i = 1; i <= 12; i++) {
				if (i < 10) {
					String mydate = dateStr + "-0" + i;
					list.add(mydate);
				} else {
					String mydate = dateStr + "-" + i;
					list.add(mydate);
				}
			}
		} catch (Exception e) {
			log.info("" + e);
		}
		return list;
	}
}
