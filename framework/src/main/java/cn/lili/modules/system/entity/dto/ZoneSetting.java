package cn.lili.modules.system.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 默认地区设置
 *
 * @author Bulbasaur
 * @since 2021/5/16 11:10 下午
 */
@Data
public class ZoneSetting implements Serializable {

	@ApiModelProperty(value = "adCode")
	private String adCode;

	@ApiModelProperty(value = "经纬度")
	private String center;

	@ApiModelProperty(value = "等级")
	private String level;

	@ApiModelProperty(value = "城市code")
	private String cityCode;

	@ApiModelProperty(value = "名称")
	private String name;

	@ApiModelProperty(value = "行政地区路径，类似：1，2，3 ")
	private String path;

	@ApiModelProperty(value = "排序")
	private Integer orderNum;

	@ApiModelProperty(value = "父id")
	private String parentId;

	@ApiModelProperty(value = "行政路径")
	private List consigneeAddressPath;

	@ApiModelProperty(value = "经纬度")
	private String trapeze;

}
