package cn.lili.modules.system.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.goods.entity.dto.GoodsAndStores;
import cn.lili.modules.goods.entity.dto.GoodsSearchParams;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.vos.StoreSearchParams;
import cn.lili.modules.store.entity.vos.StoreVO;
import cn.lili.modules.system.entity.dos.Region;
import cn.lili.modules.system.entity.vo.RegionVO;
import cn.lili.modules.wallet.entity.dto.CommissionDistributionSale;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Map;

/**
 * 行政地区业务层
 *
 * @author Chopper
 * @since 2020/12/2 14:14
 */
@CacheConfig(cacheNames = "{regions}")
public interface RegionService extends IService<Region> {

	/**
	 * 同步行政数据
	 *
	 * @param url
	 */
	@CacheEvict
	void synchronizationData(String url);

	/**
	 * 获取地区列表
	 *
	 * @param id
	 *            地区ID
	 * @return 地区列表
	 */
	@Cacheable(key = "#id")
	List<Region> getItem(String id);

	/**
	 * 获取地区列表
	 *
	 * @param id
	 *            地区ID
	 * @return 地区列表
	 */
	List getItemAdCod(String id);

	Region getItemAdCodAll(String adCode);

	/**
	 * 根据code查询合伙人管理的区域Code
	 *
	 * @param id
	 *            地区ID
	 * @param type
	 *            合伙人类型
	 * @return 地区列表
	 */
	List getPartnerAdCode(String id, String type);

	/**
	 * 根据ad_code去找上级省市区
	 */
	String getItemAdCodOrName(String adCode);

	/**
	 * 获取地址
	 *
	 * @param addressCode
	 *            城市编码
	 * @param townName
	 *            镇名称
	 * @return
	 */
	Map<String, Object> getRegion(String addressCode, String townName);

	/**
	 * 获取所有的城市
	 *
	 * @return
	 */
	@Cacheable(key = "'ALL_CITY'")
	List<RegionVO> getAllCity();

	/**
	 * 根据省市区搜素商品
	 *
	 * @param page
	 *            分页条件
	 * @param region
	 *            地区接口
	 * @return 会员地址分页列表
	 */
	IPage<GoodsVO> getRegionCo(PageVO page, Region region);

	/**
	 * 本区域销量前几名的商品
	 */
	Map getCommodity(Region region);

	/**
	 * 本区域前几名的店铺
	 */
	Map getCommodityShop(Region region);

	/**
	 * 分页条件查询 用于展示店铺列表
	 *
	 * @param entity
	 * @return
	 */
	IPage<StoreVO> findByConditionPage(StoreSearchParams entity, PageVO page);

	/**
	 * 商品查询
	 *
	 * @param goodsSearchParams
	 *            查询参数
	 * @return 商品分页
	 */
	IPage<GoodsVO> queryByParams(GoodsSearchParams goodsSearchParams);

	@Cacheable(key = "#id")
	List<Region> getItemBySetting(String id);

	List<StoreVO> getGoodShop(StoreSearchParams entity);

	IPage<GoodsVO> getByhotGoods(GoodsSearchParams goodsSearchParams, PageVO page);

	IPage<GoodsAndStores> getByNewGoodsAndStore(GoodsAndStores goodsAndStores, PageVO page);

	IPage<StoreVO> getGoodShopPage(StoreSearchParams entity, PageVO page);

	IPage<GoodsVO> getGoodPage(GoodsSearchParams goodsSearchParams, PageVO page);

	IPage<GoodsVO> getByNewGoods(GoodsSearchParams goodsSearchParams, PageVO page);

	Region getAdRegionId(String adCode);

	// 获取销售转账分佣
	IPage<CommissionDistributionSale> GetCommissionDistributionSale(QueryWrapper queryWrapper, PageVO page);

	// 商家订单分佣明细
	IPage<CommissionDistributionSale> GetCommissionDistribution(QueryWrapper queryWrapper, PageVO page);

	// 获取会员分佣
	IPage<CommissionDistributionSale> GetCommissionDistributionMember(QueryWrapper queryWrapper, PageVO page);

}