package cn.lili.modules.system.entity.vo;

import cn.lili.modules.system.entity.dos.Region;
import cn.lili.modules.system.entity.dos.RegionDetail;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/9/5
 */
@Data
@NoArgsConstructor
public class RegionDetailVO extends RegionDetail {
}
