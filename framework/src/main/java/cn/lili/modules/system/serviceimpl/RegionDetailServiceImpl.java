package cn.lili.modules.system.serviceimpl;

import cn.lili.modules.system.entity.dos.RegionDetail;
import cn.lili.modules.system.mapper.RegionDetailMapper;
import cn.lili.modules.system.service.RegionDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * create by yudan on 2022/9/5
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RegionDetailServiceImpl extends ServiceImpl<RegionDetailMapper, RegionDetail>
		implements
			RegionDetailService {
}
