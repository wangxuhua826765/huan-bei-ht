package cn.lili.modules.system.mapper;

import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.vo.MemberVO;
import cn.lili.modules.system.entity.dos.Region;
import cn.lili.modules.system.entity.dos.RegionDetail;
import cn.lili.modules.system.entity.vo.RegionDetailVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * create by yudan on 2022/9/5
 */
@Mapper
public interface RegionDetailMapper extends BaseMapper<RegionDetail> {

	@Select("select * from li_region_detail ${ew.customSqlSegment} ")
	IPage<RegionDetailVO> getRegionDetailList(IPage<RegionDetailVO> page,
			@Param(Constants.WRAPPER) Wrapper<RegionDetailVO> queryWrapper);

}
