package cn.lili.modules.system.aspect.annotation;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.lang.annotation.*;

/**
 * 系统日志AOP注解
 *
 * @author Chopper
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface SystemLogPoint {

	/**
	 * 日志名称
	 *
	 * @return
	 */
	String description() default "";

	/**
	 * 自定义日志内容
	 *
	 * @return
	 */
	String customerLog() default "";

	/**
	 * 日志类型
	 *
	 * @return
	 */
	String type() default "";

	/**
	 * 是否带有参数
	 *
	 * @return
	 */
	boolean parameter() default false;
}
