package cn.lili.modules.system.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by yudan on 2022/9/5
 */
@Data
@TableName("li_region_detail")
@ApiModel(value = "区域变更记录")
public class RegionDetail extends BaseEntity {

	@ApiModelProperty(value = "用户id")
	private String memberId;

	@ApiModelProperty(value = "修改前区域ad_code")
	private String beforeAdCode;

	@ApiModelProperty(value = "修改前区域")
	private String beforeName;

	@ApiModelProperty(value = "修改后区域ad_code")
	private String afterAdCode;

	@ApiModelProperty(value = "修改后区域")
	private String afterName;
}
