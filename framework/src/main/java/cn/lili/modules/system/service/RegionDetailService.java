package cn.lili.modules.system.service;

import cn.lili.modules.system.entity.dos.RegionDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * create by yudan on 2022/9/5
 */
public interface RegionDetailService extends IService<RegionDetail> {
}
