package cn.lili.modules.store.entity.vos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * StoreAddrDetailVO.
 *
 * @author Li Bing
 * @since 2022.03.27
 */
@Data
@ApiModel("店铺地址信息")
public class StoreAddrDetailVO implements Serializable {

	@ApiModelProperty(value = "经纬度")
	private String storeCenter;

	@ApiModelProperty(value = "详细地址")
	private String storeAddressDetail;

}
