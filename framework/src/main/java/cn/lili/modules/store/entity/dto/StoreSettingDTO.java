package cn.lili.modules.store.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 店铺设置
 *
 * @author Bulbasaur
 * @since 2020/12/16 15:15
 */
@Data
public class StoreSettingDTO {

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "店铺简介")
	private String storeDesc;

	@ApiModelProperty(value = "地址id，'，'分割 ")
	private String storeAddressIdPath;

	@ApiModelProperty(value = "地址名称， '，'分割")
	private String storeAddressPath;

	@ApiModelProperty(value = "详细地址")
	private String storeAddressDetail;

	@NotEmpty
	@ApiModelProperty(value = "经纬度")
	private String storeCenter;

	@ApiModelProperty(value = "adcode省市区编码")
	private String adCode;

	@ApiModelProperty(value = "高德坐标系经度")
	private String lon;

	@ApiModelProperty(value = "高德坐标系纬度")
	private String lat;

	@ApiModelProperty(value = "店铺联系方式")
	private String storePhone;

	@ApiModelProperty(value = "店铺图片")
	private String storeImage;

}
