package cn.lili.modules.store.serviceimpl;

import cn.lili.common.security.context.UserContext;
import cn.lili.common.sensitive.SensitiveWordsFilter;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.order.order.entity.dos.OrderItem;
import cn.lili.modules.store.entity.dos.StoreEvaluation;
import cn.lili.modules.store.entity.dto.StoreEvaluationDTO;
import cn.lili.modules.store.mapper.StoreEvaluationMapper;
import cn.lili.modules.store.service.StoreEvaluationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.catalina.manager.ManagerServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class StoreEvaluationServiceImpl extends ServiceImpl<StoreEvaluationMapper, StoreEvaluation>
		implements
			StoreEvaluationService {
	/**
	 * 会员
	 */
	@Autowired
	private MemberService memberService;

	@Override
	public StoreEvaluationDTO addStoreEvaluation(StoreEvaluationDTO storeEvaluationDTO) {
		// 获取用户信息
		Member member = memberService.getUserInfo();
		if (storeEvaluationDTO.getStoreScore() == 0) {
			storeEvaluationDTO.setStoreScore(5);
		}
		StoreEvaluation storeEvaluation = new StoreEvaluation(storeEvaluationDTO, member);
		// 过滤商品咨询敏感词
		storeEvaluation.setContent(SensitiveWordsFilter.filter(storeEvaluation.getContent()));
		this.save(storeEvaluation);
		return storeEvaluationDTO;
	}

	@Override
	public Integer storeScoreSum() {
		String storeId = UserContext.getCurrentUser().getStoreId();
		Integer integer = this.baseMapper.storeScoreSum(storeId);
		return integer;
	}

}
