package cn.lili.modules.store.entity.vos;

import cn.lili.modules.store.entity.enums.StoreStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 店铺基本信息DTO
 *
 * @author Bulbasaur
 * @since 2020/12/7 14:43
 */
@Data
public class StoreBasicInfoVO {

	@ApiModelProperty(value = "店铺ID")
	private String storeId;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "营业时间")
	private String sellTimeSta;

	@ApiModelProperty(value = "营业时间")
	private String sellTimeEnd;

	/**
	 * @see StoreStatusEnum
	 */
	@ApiModelProperty(value = "店铺状态")
	private String storeDisable;

	@ApiModelProperty(value = "高德地图经度")
	private String lon;

	@ApiModelProperty(value = "高德地图纬度")
	private String lat;

	@ApiModelProperty(value = "地址名称， '，'分割")
	private String companyAddressPath;

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "店铺简介")
	private String storeDesc;

	@ApiModelProperty(value = "PC端页面")
	private String pcPageData;

	@ApiModelProperty(value = "移动端页面")
	private String mobilePageData;

	@ApiModelProperty(value = "是否自营")
	private Integer selfOperated;

	@ApiModelProperty(value = "商品数量")
	private Integer goodsNum;

	@ApiModelProperty(value = "收藏数量")
	private Integer collectionNum;

	@ApiModelProperty(value = "腾讯云智服唯一标识")
	private String yzfSign;

	@ApiModelProperty(value = "腾讯云智服小程序唯一标识")
	private String yzfMpSign;

	@ApiModelProperty(value = "udesk标识")
	private String merchantEuid;

	@ApiModelProperty(value = "地址名称， '，'分割")
	private String storeAddressPath;

	@ApiModelProperty(value = "详细地址")
	private String storeAddressDetail;

	@ApiModelProperty(value = "联系电话")
	private String linkPhone;

	@ApiModelProperty(value = "店铺联系方式")
	private String storePhone;

	@ApiModelProperty(value = "店铺图片")
	private String storeImage;

	@ApiModelProperty(value = "距离")
	private Integer distance;

	@ApiModelProperty(value = "月销")
	private String monthSales;

	@ApiModelProperty(value = "评分")
	private String score;

	@ApiModelProperty(value = "店铺一级分类名称")
	private String categoryFirstName;

	@ApiModelProperty(value = "店铺二级分类名称")
	private String categorySecondName;

	@ApiModelProperty(value = "店铺三级分类名称")
	private String categoryThirdName;

	@ApiModelProperty(value = "店铺经营类目")
	private String goodsManagementCategory;

	@ApiModelProperty(value = "店铺经营类目名称")
	private String goodsManagementCategoryName;

	@ApiModelProperty(value = "法定经营范围")
	private String scope;

}
