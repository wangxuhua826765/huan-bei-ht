package cn.lili.modules.store.entity.dos;

import cn.lili.common.enums.SwitchEnum;
import cn.lili.modules.goods.entity.dos.GoodsSku;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dto.MemberEvaluationDTO;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.store.entity.dto.StoreEvaluationDTO;
import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import javax.validation.constraints.NotNull;

@Data
@TableName("li_store_evaluation")
@ApiModel(value = "店铺评价表")
public class StoreEvaluation extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "会员ID")
	private String memberId;

	@NotNull
	@ApiModelProperty(value = "店铺ID")
	private String storeId;

	@NotNull
	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@NotNull
	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@NotNull
	@ApiModelProperty(value = "会员头像")
	private String memberProfile;

	@NotNull
	@ApiModelProperty(value = "订单号")
	private String orderNo;

	@ApiModelProperty(value = "内容")
	private String content;

	@NotNull
	@ApiModelProperty(value = "状态  OPEN 正常 ,CLOSE 关闭 ")
	private String status;

	@ApiModelProperty(value = "店铺评分")
	private Integer storeScore;

	public StoreEvaluation(StoreEvaluationDTO storeEvaluationDTO, Member member) {
		// 复制评价信息
		BeanUtils.copyProperties(storeEvaluationDTO, this);
		// 设置会员
		this.memberId = member.getId();
		// 会员名称
		this.memberName = member.getNickName();
		// 设置会员头像
		this.memberProfile = member.getFace();
		// 设置会员
		this.content = storeEvaluationDTO.getContent();
		// 设置店铺ID
		this.storeId = storeEvaluationDTO.getStoreId();
		// 设置店铺名称
		this.storeName = storeEvaluationDTO.getStoreName();
		// 设置店铺评分
		this.storeScore = storeEvaluationDTO.getStoreScore();
		// 默认开启评价
		this.status = SwitchEnum.OPEN.name();
	}
}
