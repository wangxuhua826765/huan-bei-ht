package cn.lili.modules.store.entity.vos;

import cn.lili.modules.connect.entity.Connect;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.store.entity.dos.Store;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 店铺VO
 *
 * @author pikachu
 * @since 2020-03-07 17:02:05
 */
@Data
public class StoreVO extends Store {

	@ApiModelProperty(value = "库存预警数量")
	private Integer stockWarning;

	@ApiModelProperty(value = "登录用户的昵称")
	private String nickName;

	@ApiModelProperty(value = "用户联系方式")
	private String mobile;

	@ApiModelProperty(value = "浏览时间")
	private String footTime;

	@ApiModelProperty(value = "距离")
	private Integer distance;

	@ApiModelProperty(value = "月销")
	private Integer monthSales;

	@ApiModelProperty(value = "评分")
	private String score;

	@ApiModelProperty(value = "订单数")
	private String orderCount;

	@ApiModelProperty(value = "订单额")
	private String orderMoney;

	@ApiModelProperty(value = "商品集合")
	private List<GoodsVO> goodsVOList;

	@ApiModelProperty(value = "推广人姓名")
	private String nameExte;

	@ApiModelProperty(value = "推广人电话")
	private String mobileExte;

	@ApiModelProperty(value = "openId")
	private List<Connect> openIds;

}
