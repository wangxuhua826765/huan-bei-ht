package cn.lili.modules.store.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class StoreEvaluationDTO {

	@ApiModelProperty(value = "会员ID")
	private String memberId;

	// @NotNull
	@ApiModelProperty(value = "店铺ID")
	private String storeId;

	// @NotNull
	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	// @NotNull
	@ApiModelProperty(value = "会员名称")
	private String memberName;

	// @NotNull
	@ApiModelProperty(value = "会员头像")
	private String memberProfile;

	// @NotNull
	@ApiModelProperty(value = "订单号")
	private String orderNo;

	@ApiModelProperty(value = "内容")
	private String content;

	// @NotNull
	@ApiModelProperty(value = "状态  OPEN 正常 ,CLOSE 关闭 ")
	private String status;

	@ApiModelProperty(value = "店铺评分")
	private Integer storeScore;

}
