package cn.lili.modules.store.entity.dos;

import cn.lili.mybatis.BaseEntity;
import cn.lili.common.utils.BeanUtil;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.store.entity.dto.AdminStoreApplyDTO;
import cn.lili.modules.store.entity.enums.StoreStatusEnum;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 店铺
 *
 * @author pikachu
 * @since 2020-02-18 15:18:56
 */
@Data
@TableName("li_store")
@ApiModel(value = "店铺")
@NoArgsConstructor
public class Store extends BaseEntity {

	private static final long serialVersionUID = -5861767726387892272L;

	@ApiModelProperty(value = "会员Id")
	private String memberId;

	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "店铺关闭时间")
	private Date storeEndTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "店铺审核时间")
	private Date storeAuditTime;

	/**
	 * @see StoreStatusEnum
	 */
	@ApiModelProperty(value = "店铺状态")
	private String storeDisable;

	@ApiModelProperty(value = "是否自营")
	private Integer selfOperated;

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "经纬度")
	@NotEmpty
	private String storeCenter;

	@Size(min = 6, max = 200, message = "店铺简介需在6-200字符之间")
	@NotBlank(message = "店铺简介不能为空")
	@ApiModelProperty(value = "店铺简介")
	private String storeDesc;

	@ApiModelProperty(value = "地址名称， '，'分割")
	private String storeAddressPath;

	@ApiModelProperty(value = "地址id，'，'分割 ")
	private String storeAddressIdPath;

	@ApiModelProperty(value = "详细地址")
	private String storeAddressDetail;

	@ApiModelProperty(value = "adcode省市区编码")
	private String adCode;

	@ApiModelProperty(value = "高德坐标系经度")
	private String lon;

	@ApiModelProperty(value = "高德坐标系纬度")
	private String lat;

	@ApiModelProperty(value = "店铺折扣")
	private String discount;
	@ApiModelProperty(value = "描述评分")
	private Double descriptionScore;

	@ApiModelProperty(value = "服务评分")
	private Double serviceScore;

	@ApiModelProperty(value = "物流描述")
	private Double deliveryScore;

	@ApiModelProperty(value = "商品数量")
	private Integer goodsNum;

	@ApiModelProperty(value = "收藏数量")
	private Integer collectionNum;

	@ApiModelProperty(value = "腾讯云智服唯一标识")
	private String yzfSign;

	@ApiModelProperty(value = "腾讯云智服小程序唯一标识")
	private String yzfMpSign;

	@ApiModelProperty(value = "udesk IM标识")
	private String merchantEuid;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "年费开始时间")
	private Date memberCreateTime;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "年费结束时间")
	private Date memberEndTime;

	@ApiModelProperty(value = "上年度已缴纳的年费")
	private Double payMoney;

	@ApiModelProperty(value = "推荐码")
	private String promotionCode;
	//
	// @ApiModelProperty(value = "充值折扣")
	// private Double rechargeRule;
	//
	// @ApiModelProperty(value = "提现折扣")
	// private Double withdrawalRate;
	//
	// @ApiModelProperty(value = "BtoB交易费率")
	// private Double jiaoyifei;

	@ApiModelProperty(value = "是否为推荐店铺", required = true)
	private Boolean recommend;

	@ApiModelProperty(value = "推荐排名", required = true)
	private int recommendRanking;

	@ApiModelProperty(value = "一级分类")
	private Long categoryFirst;

	@ApiModelProperty(value = "一级分类名称")
	private String categoryFirstName;

	@ApiModelProperty(value = "二级分类")
	private Long categorySecond;

	@ApiModelProperty(value = "二级分类名称")
	private String categorySecondName;

	@ApiModelProperty(value = "三级分类")
	private Long categoryThird;

	@ApiModelProperty(value = "三级分类名称")
	private String categoryThirdName;

	@ApiModelProperty(value = "排序 数字越大越前")
	private Long sort;

	@ApiModelProperty(value = "拒绝原因")
	private String causer;

	@ApiModelProperty(value = "营业时间")
	private String sellTimeSta;

	@ApiModelProperty(value = "营业时间")
	private String sellTimeEnd;

	@ApiModelProperty(value = "店铺联系方式")
	private String storePhone;

	@ApiModelProperty(value = "店铺图片")
	private String storeImage;

	@ApiModelProperty(value = "关闭店铺退回金额")
	private Double closingReturnAmount;

	@ApiModelProperty(value = "回收焕贝总额")
	private Double totalRecovered;

	@ApiModelProperty(value = "回收推广")
	private Double totalRecoveredPromote;

	@ApiModelProperty(value = "回收销售")
	private Double totalRecoveredSale;

	@ApiModelProperty(value = "是否有合同")
	private String isHaveContract;

	public Store(Member member) {
		this.memberId = member.getId();
		this.memberName = member.getUsername();
		storeDisable = StoreStatusEnum.APPLY.value();
		selfOperated = 0;
		deliveryScore = 5.0;
		serviceScore = 5.0;
		descriptionScore = 5.0;
		goodsNum = 0;
		collectionNum = 0;
	}

	public Store(Member member, AdminStoreApplyDTO adminStoreApplyDTO) {
		BeanUtil.copyProperties(adminStoreApplyDTO, this);
		this.memberId = member.getId();
		this.memberName = member.getUsername();
		storeDisable = StoreStatusEnum.APPLYING.value();
		selfOperated = adminStoreApplyDTO.getSelfOperated() != null ? adminStoreApplyDTO.getSelfOperated() : 0;
		deliveryScore = 5.0;
		serviceScore = 5.0;
		descriptionScore = 5.0;
		goodsNum = 0;
		collectionNum = 0;
		sellTimeSta = adminStoreApplyDTO.getSellTimeSta();
		sellTimeEnd = adminStoreApplyDTO.getSellTimeEnd();
	}

}