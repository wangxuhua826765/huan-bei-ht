package cn.lili.modules.store.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 店铺-银行信息
 *
 * @author Bulbasaur
 * @since 2020/12/7 15:54
 */
@Data
public class StoreBankDTO {

	@ApiModelProperty(value = "结算银行开户行名称")
	private String settlementBankAccountName;

	@ApiModelProperty(value = "结算银行开户账号")
	private String settlementBankAccountNum;

	@ApiModelProperty(value = "结算银行开户支行名称")
	private String settlementBankBranchName;

	@ApiModelProperty(value = "结算银行支行联行号")
	private String settlementBankJointName;

	@ApiModelProperty(value = "开户银行许可证电子版")
	private String settlementBankLicencePhoto;

}
