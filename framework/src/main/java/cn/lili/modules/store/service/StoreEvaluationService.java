package cn.lili.modules.store.service;

import cn.lili.modules.store.entity.dto.StoreEvaluationDTO;

public interface StoreEvaluationService {

	StoreEvaluationDTO addStoreEvaluation(StoreEvaluationDTO storeEvaluationDTO);

	Integer storeScoreSum();

}
