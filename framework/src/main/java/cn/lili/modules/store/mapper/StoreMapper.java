package cn.lili.modules.store.mapper;

import cn.lili.modules.member.entity.vo.GradeLevelVO;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.vos.StoreAddrDetailVO;
import cn.lili.modules.store.entity.vos.StoreVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 店铺数据处理层
 *
 * @author pikachu @since2020-03-07 09:18:56
 */
public interface StoreMapper extends BaseMapper<Store> {

	/**
	 * 获取店铺详细
	 *
	 * @param id
	 *            店铺ID
	 * @return 店铺VO
	 */
	@Select("select s.*,d.* from li_store s inner join li_store_detail d on s.id=d.store_id where s.member_id=#{id} and s.delete_flag = '0' limit 1")
	StoreVO getStoreDetail(String id);

	/**
	 * 获取店铺分页列表
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 店铺VO分页列表
	 */
	@Select("<script>" + "SELECT count(o.id) as orderCount,sum(ifnull(o.flow_price,0)) as orderMoney,m1.real_name AS nameExte, m1.mobile AS mobileExte,"
			+ "FORMAT(IFNULL(SUM(se.store_score)/COUNT(se.store_score),5),1) as score,"
			+ "s.*,m.mobile,m.nick_name AS nickName FROM li_store s " + " LEFT JOIN li_member m ON m.id = s.member_id "
			+ " LEFT JOIN li_extension e1 ON e1.member_id = m.id AND e1.delete_flag = 0 "
			+ " LEFT JOIN li_member m1 ON e1.extension = m1.promotion_code"
			+ " LEFT JOIN li_order o on o.store_id = s.id and o.order_status in ('AFTERSALEING','COMPLETED','AFTERSALEFINISHED') "
			+ "<if test=\"orderStartDate!=null and orderStartDate!='' and orderEndDate!=null and orderEndDate!=''\"> "
			+ " <![CDATA[ AND o.create_time >= #{orderStartDate} AND o.create_time <= #{orderEndDate}]]>" + "</if> "
			+ " LEFT JOIN li_store_evaluation se on se.store_id = s.id" + " ${ew.customSqlSegment}" + "</script>")
	IPage<StoreVO> getStoreList(IPage<StoreVO> page, @Param(Constants.WRAPPER) Wrapper<StoreVO> queryWrapper,
			@Param("orderStartDate") String orderStartDate, @Param("orderEndDate") String orderEndDate);

	@Select("SELECT count(o.id) monthSales,FORMAT(IFNULL(SUM(e.store_score)/COUNT(e.store_score),5),1) as score,ss.*"
			+ " FROM li_store s " + " left join li_store ss on s.category_third = ss.category_third"
			+ " LEFT JOIN li_order o on ss.id = o.store_id and DATE_FORMAT(o.create_time,'%Y-%m') = DATE_FORMAT(NOW(),'%Y-%m')"
			+ " LEFT JOIN li_store_evaluation e on e.store_id = o.store_id" + " where s.id = #{id} and ss.id != #{id}"
			+ " and ss.store_disable = 'OPEN' GROUP BY ss.id")
	IPage<StoreVO> getStoreListByPage(IPage<StoreVO> page, @Param("id") String id);

	@Select("SELECT s.*,m.mobile FROM li_store s " + "LEFT JOIN li_member m ON m.id = s.member_id "
			+ "left join li_extension e on e.extension = m.promotion_code ${ew.customSqlSegment}")
	IPage<StoreVO> pageByStoreVO(IPage<StoreVO> page, @Param(Constants.WRAPPER) Wrapper<Store> queryWrapper);

	/**
	 * 修改店铺收藏数据
	 *
	 * @param storeId
	 *            店铺id
	 * @param num
	 *            收藏数量
	 */
	@Update("update li_store set collection_num = collection_num + #{num} where id = #{storeId}")
	void updateCollection(String storeId, Integer num);

	@Select("select store_address_detail storeAddressDetail,store_center storeCenter from li_store  where id=#{storeId} ")
	StoreAddrDetailVO getAddressDetail(@Param("storeId") String storeId);

	// 根据用户id获取商家年费信息
	@Select("SELECT l.* FROM li_grade_level l" + " left join li_store s on s.member_id = l.member_id"
			+ " where l.grade_state = 0 and l.delete_flag = 0 and l.owner = 'STORE'"
			+ " and l.end_time > now() and l.member_id = #{memberId}")
	GradeLevelVO findGradeLevel(@Param("memberId") String memberId);

	// 店铺推荐
	@Update("update li_store set recommend = #{recommend},recommend_ranking = #{recommendRanking} where id = #{storeId}")
	Boolean updateRecommend(String storeId, Boolean recommend, int recommendRanking);

	@Select("SELECT s.*,m.mobile FROM li_store s " + " LEFT JOIN li_member m ON m.id = s.member_id "
			+ " ${ew.customSqlSegment}")
	IPage<StoreVO> findByConditionPageShenhe(IPage<StoreVO> page,
			@Param(Constants.WRAPPER) Wrapper<StoreVO> queryWrapper);

	@Update("update li_store set delete_flag = 1 where member_id = #{memberId}")
	void deleteByMemberId(String memberId);
}