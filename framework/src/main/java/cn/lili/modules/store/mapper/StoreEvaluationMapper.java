package cn.lili.modules.store.mapper;

import cn.lili.modules.store.entity.dos.StoreEvaluation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

public interface StoreEvaluationMapper extends BaseMapper<StoreEvaluation> {

	@Select("select FORMAT(IFNULL(SUM(store_score)/COUNT(store_score),0),1) as storeScore FROM li_store_evaluation WHERE store_id =#{storeId}")
	Integer storeScoreSum(String storeId);
}
