package cn.lili.modules.store.entity.vos;

import cn.hutool.core.date.DateUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.store.entity.enums.StoreStatusEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * 店铺搜索参数VO
 *
 * @author pikachu
 * @since 2020-03-07 17:02:05
 */
@Data
public class StoreSearchParams extends PageVO implements Serializable {

	private static final long serialVersionUID = 6916054310764833369L;

	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@ApiModelProperty(value = "店铺id")
	private String storeId;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;
	/**
	 * @see StoreStatusEnum
	 */
	@ApiModelProperty(value = "店铺状态")
	private String storeDisable;

	@ApiModelProperty(value = "开始时间")
	private String startDate;

	@ApiModelProperty(value = "结束时间")
	private String endDate;

	@ApiModelProperty(value = "关店时间排序")
	private Boolean orderByStoreEndTime;

	@ApiModelProperty(value = "地址id")
	private String regionId;

	@ApiModelProperty(value = "是否为推荐商品")
	private Boolean recommend;

	@ApiModelProperty(value = "是否是自营店铺")
	private Integer selfOperated;

	@ApiModelProperty(value = "区域编码")
	private String adCode;

	@ApiModelProperty(value = "定位、选择、默认的区域编码")
	private String chooseCode;

	@ApiModelProperty(value = "1本地商圈2好店推荐")
	private Integer state;

	@ApiModelProperty(value = "一级分类", required = true)
	private Long categoryFirst;

	@ApiModelProperty(value = "二级分类", required = true)
	private Long categorySecond;

	@ApiModelProperty(value = "三级分类", required = true)
	private Long categoryThird;

	@ApiModelProperty(value = "是否根据sort排序 true  是 false 否", required = true)
	private Boolean orderBySort;

	@ApiModelProperty(value = "是否根据推荐排序 true  是 false 否", required = true)
	private Boolean orderByCommendRanking;

	@ApiModelProperty(value = "是否根据sort分组 true  是 false 否", required = false)
	private Boolean groupBySort;

	@ApiModelProperty(value = "用户联系方式")
	private String mobile;

	@ApiModelProperty(value = "显示的数量")
	private Integer limit;
	@ApiModelProperty(value = "省级id")
	private String provinceId;

	@ApiModelProperty(value = "高德地图经度")
	private String lon;

	@ApiModelProperty(value = "高德地图纬度")
	private String lat;

	@ApiModelProperty(value = "经纬度")
	private String trapeze;

	@ApiModelProperty(value = "距离")
	private Integer distance;
	@ApiModelProperty(value = "距离")
	private Integer distance1;

	@ApiModelProperty(value = "评价分类 1：销量 2：评价 3：距离")
	private String type;

	@ApiModelProperty(value = "月销")
	private Integer monthSales;

	@ApiModelProperty(value = "评分")
	private Double score;

	@ApiModelProperty(value = "订单开始时间")
	private String orderStartDate;

	@ApiModelProperty(value = "订单结束时间")
	private String orderEndDate;

	@ApiModelProperty(value = "评分")
	private String adcode1;

	@ApiModelProperty(value = "会员Id")
	private List<String> memberIds;

	@ApiModelProperty(value = "推广人姓名")
	private String nameExte;

	@ApiModelProperty(value = "推广人电话")
	private String mobileExte;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (StringUtils.isNotEmpty(storeName)) {
			queryWrapper.like("s.store_name", storeName);
		}
		// 推广人姓名
		queryWrapper.like(StringUtils.isNotEmpty(nameExte),"m1.real_name",nameExte);

		// 推广人电话
		queryWrapper.like(StringUtils.isNotEmpty(mobileExte),"m1.mobile",mobileExte);

		if (null != memberIds && memberIds.size() > 0) {
			queryWrapper.in("s.member_id", memberIds);
		}
		if (null != recommend && recommend) {
			queryWrapper.eq("recommend", recommend);
		}
		if (null != recommend && !recommend) {
			queryWrapper.and(wrapper -> wrapper.eq("recommend", recommend).or().isNull("recommend"));
		}

		if (selfOperated != null) {
			queryWrapper.eq("self_operated", selfOperated);
		}
		if (StringUtils.isNotEmpty(memberName)) {
			queryWrapper.like("s.member_name", memberName);
		}
		if (StringUtils.isNotEmpty(mobile)) {
			queryWrapper.like("m.mobile", mobile);
		}
		if (StringUtils.isNotEmpty(storeDisable)) {
			queryWrapper.eq("store_disable", storeDisable);
		} else {
			queryWrapper.and(Wrapper -> Wrapper.eq("store_disable", StoreStatusEnum.OPEN.name()).or()
					.eq("store_disable", StoreStatusEnum.CLOSED.name()));
		}
		// 按时间查询
		if (StringUtils.isNotEmpty(startDate)) {
			queryWrapper.ge("s.create_time", DateUtil.parse(startDate));
		}
		if (StringUtils.isNotEmpty(endDate)) {
			queryWrapper.le("s.create_time", DateUtil.parse(endDate));
		}

		if (StringUtils.isNotEmpty(regionId)) {
			queryWrapper.like("store_address_id_path", regionId);
		}
		if (null != distance && distance != 0) {
			queryWrapper.lt("distance", distance);// 小于多少千米的
		}
		if (null != adcode1 && StringUtils.isNotEmpty(adcode1)) {
			queryWrapper.eq("s.ad_code", adcode1);// 根据adcode查询
		}
		if (null != score) {
			queryWrapper.orderByDesc("score");// 根据推荐评分排序
		}

		if (null != distance1 && distance1 != 0) {
			queryWrapper.orderByAsc("distance");// 根据推荐距离排序
		}
		if (monthSales != null) {
			queryWrapper.orderByDesc("monthSales");
		}
		if (orderByStoreEndTime != null && orderByStoreEndTime) {
			queryWrapper.orderByDesc("store_end_time");// 关店时间排序
		}

		if (categoryFirst != null) {
			queryWrapper.like("category_first", categoryFirst);
		}

		if (categorySecond != null) {
			queryWrapper.like("category_second", categorySecond);
		}

		if (categoryThird != null) {
			queryWrapper.like("category_third", categoryThird);
		}
		if (provinceId != null) {
			queryWrapper.notLike("store_address_id_path", provinceId);
		}

		if (orderBySort != null && orderBySort) {
			queryWrapper.orderByDesc("sort").orderByDesc("collection_num");
		}

		if (orderByCommendRanking != null && orderByCommendRanking) {
			queryWrapper.orderByAsc("recommend_ranking");
		}

		if (groupBySort != null && groupBySort) {
			queryWrapper.groupBy("s.id");
		}

		if (limit != null) {
			queryWrapper.last("limit " + limit);
		}
		return queryWrapper;
	}
}
