package cn.lili.modules.permission.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by yudan on 2022/2/11
 */
@Data
@TableName("li_audit")
@ApiModel(value = "审核表")
public class Audit extends BaseEntity {

	@ApiModelProperty(value = "申请人")
	private String userId;

	@ApiModelProperty(value = "申请角色  0 商家 1 代理商   2 合伙人")
	private String type;

	@ApiModelProperty(value = "审核人")
	private String auditBy;

	@ApiModelProperty(value = "审核状态  0 待审核  1 审核通过  2 驳回")
	private String state;

	@ApiModelProperty(value = "驳回理由")
	private String reason;

}
