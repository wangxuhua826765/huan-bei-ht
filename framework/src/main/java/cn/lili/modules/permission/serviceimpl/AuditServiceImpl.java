package cn.lili.modules.permission.serviceimpl;

import cn.lili.common.security.enums.UserEnums;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.entity.dos.Audit;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.mapper.AuditMapper;
import cn.lili.modules.permission.service.AdminUserService;
import cn.lili.modules.permission.service.AuditService;
import cn.lili.modules.permission.service.RoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * create by yudan on 2022/2/11
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class AuditServiceImpl extends ServiceImpl<AuditMapper, Audit> implements AuditService {

	@Autowired
	private RoleService roleService;
	@Autowired
	private AdminUserService adminUserService;

	@Override
	public void audit(Audit audit) {
		baseMapper.audit(audit);
		// 如果审核通过，用户表需要添加代理商角色
		if ("1".equals(audit.getState())) {
			QueryWrapper<Role> wrapper = Wrappers.query();
			wrapper.eq("name", UserEnums.AGENT.getRole());
			Role role = roleService.getOne(wrapper);
			audit = baseMapper.selectById(audit.getId());
			AdminUser adminUser = adminUserService.getById(audit.getUserId());
			String s = adminUser.getRoleIds();
			if (StringUtils.isNotEmpty(s)) {
				List<String> list = Arrays.asList(s.split(","));
				adminUser.setRoleIds(s + "," + role.getId());
				adminUserService.updateRole(adminUser.getId(), list);
				adminUserService.updateById(adminUser);
			} else {
				List<String> list = new ArrayList<>();
				list.add(role.getId());
				adminUser.setRoleIds(role.getId());
				adminUserService.updateRole(adminUser.getId(), list);
				adminUserService.updateById(adminUser);
			}
		}
	}

	@Override
	public void deleteByUserId(String id, int type) {
		baseMapper.deleteByUserId(id, type);
		// 删除用户的对应角色
		QueryWrapper<Role> wrapper = Wrappers.query();
		// 0 商家 1 代理商 2 合伙人
		if (type == 0) {
			wrapper.eq("name", UserEnums.STORE.getRole());
		} else if (type == 1) {
			wrapper.eq("name", UserEnums.AGENT.getRole());
		} else if (type == 2) {
			wrapper.eq("name", UserEnums.PARTNER.getRole());
		}
		Role role = roleService.getOne(wrapper);
		AdminUser adminUser = adminUserService.getById(id);
		String s = adminUser.getRoleIds();
		if (StringUtils.isNotEmpty(s)) {
			List<String> list = Arrays.asList(s.split(","));
			List<String> newlist = new ArrayList<String>(list);
			Iterator iterator = newlist.iterator();
			while (iterator.hasNext()) {
				if (iterator.next().equals(role.getId())) {
					iterator.remove();
				}
			}
			if (CollectionUtils.isNotEmpty(newlist)) {
				adminUser.setRoleIds(newlist.stream().collect(Collectors.joining(",")));
				adminUserService.updateRole(adminUser.getId(), newlist);
			} else {
				adminUser.setRoleIds(null);
				adminUserService.updateRole(adminUser.getId(), null);
			}
			// adminUserService.updateById(adminUser);
			QueryWrapper<AdminUser> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("id", id);
			adminUserService.remove(queryWrapper);
		}
	}
}
