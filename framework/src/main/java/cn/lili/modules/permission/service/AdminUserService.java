package cn.lili.modules.permission.service;

import cn.lili.common.security.token.Token;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.entity.dto.AdminUserDTO;
import cn.lili.modules.permission.entity.vo.AdminUserVO;
import cn.lili.modules.permission.entity.vo.AgentVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheConfig;

import java.util.List;

/**
 * 用户业务层
 *
 * @author Chopper
 * @since 2020/11/17 3:42 下午
 */
@CacheConfig(cacheNames = "{adminuser}")
public interface AdminUserService extends IService<AdminUser> {

	/**
	 * 获取管理员分页
	 *
	 * @param initPage
	 * @param initWrapper
	 * @return
	 */
	IPage<AdminUserVO> adminUserPage(Page initPage, QueryWrapper<AdminUser> initWrapper);

	/**
	 * 通过用户名获取用户
	 *
	 * @param username
	 * @return
	 */
	AdminUser findByUsername(String username);

	List<Role> findRoleById(String id);

	/**
	 * 更新管理员
	 *
	 * @param adminUser
	 * @param roles
	 * @return
	 */
	boolean updateAdminUser(AdminUser adminUser, List<String> roles);

	// 修改代理商的代理区域
	boolean updateAdminUserRegion(AdminUser adminUser);

	/**
	 * 修改管理员密码
	 *
	 * @param password
	 * @param newPassword
	 */
	void editPassword(String password, String newPassword);

	/**
	 * 重置密码
	 *
	 * @param ids
	 *            id集合
	 */
	void resetPassword(List<String> ids);

	// 验证手机号
	AdminUser findByPhone(String mobilePhone);

	/**
	 * 新增管理员
	 *
	 * @param adminUser
	 * @param roles
	 */
	void saveAdminUser(AdminUserDTO adminUser, List<String> roles);

	/**
	 * 彻底删除
	 *
	 * @param ids
	 */
	void deleteCompletely(List<String> ids);

	/**
	 * 用户登录
	 *
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @return token
	 */
	Token login(String username, String password);

	/**
	 * 刷新token
	 *
	 * @param refreshToken
	 * @return token
	 */
	Token refreshToken(String refreshToken);

	/**
	 * 获取代理商分页
	 *
	 * @param agent
	 *            代理商
	 * @param page
	 *            分页
	 * @return 代理商分页
	 */
	IPage<AgentVO> getAgentPage(AgentVO agent, PageVO page);

	void updateRole(String id, List<String> list);

	/**
	 * 修改状态
	 *
	 * @param id
	 *            id
	 * @param status
	 *            状态
	 * @return 修改结果
	 */
	Boolean updateStatus(String id, Boolean status);
}
