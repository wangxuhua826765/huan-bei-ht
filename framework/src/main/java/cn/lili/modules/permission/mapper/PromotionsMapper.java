package cn.lili.modules.permission.mapper;

import cn.lili.modules.permission.entity.dos.Promotions;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * create by yudan on 2022/2/11
 */
public interface PromotionsMapper extends BaseMapper<Promotions> {

}
