package cn.lili.modules.permission.mapper;

import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.entity.dos.StoreUser;
import cn.lili.modules.permission.entity.vo.AgentVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商家端用户数据处理层
 *
 * @author Chopper
 * @since 2020-11-22 09:17
 */
@Mapper
public interface StoreUserMapper extends BaseMapper<StoreUser> {

}
