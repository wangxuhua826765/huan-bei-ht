package cn.lili.modules.permission.service;

import cn.lili.modules.permission.entity.dos.Promotions;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheConfig;

/**
 * create by yudan on 2022/2/11
 */
@CacheConfig(cacheNames = "{promotion}")
public interface PromotionsService extends IService<Promotions> {
}
