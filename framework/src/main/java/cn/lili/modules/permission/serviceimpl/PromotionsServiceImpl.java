package cn.lili.modules.permission.serviceimpl;

import cn.lili.modules.permission.entity.dos.Promotions;
import cn.lili.modules.permission.mapper.PromotionsMapper;
import cn.lili.modules.permission.service.PromotionsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * create by yudan on 2022/2/11
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class PromotionsServiceImpl extends ServiceImpl<PromotionsMapper, Promotions> implements PromotionsService {
}
