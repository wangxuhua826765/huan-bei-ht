package cn.lili.modules.permission.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/2/11
 */
@Data
@TableName("li_promotion")
@ApiModel(value = "推广下级关联表")
@NoArgsConstructor
public class Promotions extends BaseEntity {

	@ApiModelProperty(value = "推广码")
	private String promotionCode;

	@ApiModelProperty(value = "被推广人id/店铺id")
	private String userId;

	@ApiModelProperty(value = "被推广人类型  0 用户   1 商家")
	private Integer userRole;

}
