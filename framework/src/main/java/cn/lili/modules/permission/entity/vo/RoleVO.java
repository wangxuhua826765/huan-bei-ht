package cn.lili.modules.permission.entity.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.lili.modules.goods.entity.dos.Category;
import cn.lili.modules.goods.entity.vos.CategoryVO;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.entity.dos.RoleMenu;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.List;

/**
 * RoleVO
 *
 * @author Chopper
 * @since 2020-11-22 17:42
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleVO extends Role {

	private static final long serialVersionUID = 8625345346785692513L;

	@ApiModelProperty(value = "拥有权限")
	private List<RoleMenu> roleMenus;

	@ApiModelProperty(value = "父节点名称")
	private String parentTitle;

	@ApiModelProperty("子分类列表")
	private List<RoleVO> children;

	public RoleVO(Role role) {
		BeanUtil.copyProperties(role, this);
	}

	public List<RoleVO> getChildren() {

		if (children != null) {
			children.sort(new Comparator<RoleVO>() {
				@Override
				public int compare(RoleVO o1, RoleVO o2) {
					return o1.getSortOrder().compareTo(o2.getSortOrder());
				}
			});
			return children;
		}
		return null;
	}
}
