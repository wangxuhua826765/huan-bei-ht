package cn.lili.modules.permission.service;

import cn.lili.common.security.token.Token;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.entity.dos.StoreUser;
import cn.lili.modules.permission.entity.dto.AdminUserDTO;
import cn.lili.modules.permission.entity.vo.AdminUserVO;
import cn.lili.modules.permission.entity.vo.AgentVO;
import cn.lili.modules.permission.entity.vo.StoreUserVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheConfig;

import java.util.List;

/**
 * 商家端用户业务层
 *
 * @author Chopper
 * @since 2020/11/17 3:42 下午
 */
@CacheConfig(cacheNames = "{adminuser}")
public interface StoreUserService extends IService<StoreUser> {

	/**
	 * 获取管理员分页
	 *
	 * @param initPage
	 * @param initWrapper
	 * @return
	 */
	IPage<StoreUserVO> StoreUserPage(Page initPage, QueryWrapper<StoreUser> initWrapper);

	/**
	 * 彻底删除
	 *
	 * @param ids
	 */
	void deleteCompletely(List<String> ids);

	/**
	 * 重置密码
	 *
	 * @param ids
	 *            id集合
	 */
	void resetPassword(List<String> ids);

}
