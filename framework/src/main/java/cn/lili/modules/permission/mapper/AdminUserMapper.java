package cn.lili.modules.permission.mapper;

import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.entity.vo.AgentVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户数据处理层
 *
 * @author Chopper
 * @since 2020-11-22 09:17
 */
@Mapper
public interface AdminUserMapper extends BaseMapper<AdminUser> {

	/**
	 * 通过用户名获取用户
	 * 
	 * @param username
	 * @return
	 */
	AdminUser findByUsername(String username);

	/**
	 * 通过部门id获取
	 * 
	 * @param departmentId
	 * @return
	 */
	List<AdminUser> findByDepartmentId(String departmentId);

	/**
	 * 通过用户名模糊搜索
	 * 
	 * @param username
	 * @param status
	 * @return
	 */
	List<AdminUser> findByUsernameLikeAndStatus(String username, Integer status);

	@Select("select u.*,p.id as auditId,p.audit_state as state,p.region_id,p.region from li_admin_user u "
			+ "inner join li_member m on m.mobile = u.mobile "
			+ "inner join li_partner p on p.member_id = m.id ${ew.customSqlSegment}")
	IPage<AgentVO> pageByAgentVO(IPage<AgentVO> page, @Param(Constants.WRAPPER) Wrapper<AdminUser> queryWrapper);

	@Select("SELECT r.id,r.name from li_role r" + " left join li_user_role ur on ur.role_id = r.id"
			+ " left join li_admin_user u on u.id = ur.user_id" + " where u.id = #{id}")
	List<Role> findRoleById(String id);
}
