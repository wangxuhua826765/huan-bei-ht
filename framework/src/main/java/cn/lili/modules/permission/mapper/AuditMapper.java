package cn.lili.modules.permission.mapper;

import cn.lili.modules.permission.entity.dos.Audit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * create by yudan on 2022/2/11
 */
public interface AuditMapper extends BaseMapper<Audit> {

	@Update("update li_audit set state = #{state}, reason = #{reason} where id = #{id}")
	Boolean audit(Audit audit);

	@Update("update li_audit set delete_flag = true where user_id = #{id} and type = #{type}")
	Boolean deleteByUserId(String id, int type);
}
