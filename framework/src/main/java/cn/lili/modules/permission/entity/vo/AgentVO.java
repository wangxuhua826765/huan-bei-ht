package cn.lili.modules.permission.entity.vo;

import cn.lili.modules.permission.entity.dos.AdminUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * create by yudan on 2022/2/10
 */
@Data
public class AgentVO extends AdminUser {

	@ApiModelProperty(value = "发展商家数")
	private Integer agentNum;

	@ApiModelProperty(value = "代理佣金")
	private BigDecimal agentMoney;

	@ApiModelProperty(value = "审核状态  0 待审核  1 审核通过  2 驳回")
	private String state;

	@ApiModelProperty(value = "审核表id")
	private String auditId;
}
