package cn.lili.modules.permission.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 角色
 *
 * @author Chopper
 * @since 2020/11/19 11:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("li_role")
@ApiModel(value = "角色")
public class Role extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "权限名称")
	private String name;

	@ApiModelProperty(value = "是否为注册默认角色")
	private Boolean defaultRole;

	@ApiModelProperty(value = "备注")
	private String description;

	@ApiModelProperty(value = "父分类ID")
	private String parentId;

	@ApiModelProperty(value = "排序")
	@Min(value = 0, message = "排序值最小0，最大9999999999")
	@Max(value = 999999999, message = "排序值最小0，最大9999999999")
	@NotNull(message = "排序值不能为空")
	private Integer sortOrder;

	@ApiModelProperty(value = "祖级列表")
	private String ancestors;

	@ApiModelProperty(value = "角色名")
	private String roleName;

	@ApiModelProperty(value = "0平台 1商家")
	private String state;

}
