package cn.lili.modules.permission.mapper;

import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.permission.entity.dos.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 角色数据处理层
 *
 * @author Chopper
 * @since 2020-11-22 09:17
 */
public interface RoleMapper extends BaseMapper<Role> {

	@Select("select * from li_role where name = #{name} and parent_id = '0' ")
	Role seleGetById(String name);

	@Select("SELECT * FROM li_role WHERE state = #{state} and id = #{id} or find_in_set( #{id}  , ancestors )")
	List<Role> ListRole(String id, String state);

	@Select("select * from li_role where name = #{name}")
	List<String> getListRoleId(String name);
}
