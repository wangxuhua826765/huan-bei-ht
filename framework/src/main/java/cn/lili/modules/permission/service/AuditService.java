package cn.lili.modules.permission.service;

import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.entity.dos.Audit;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheConfig;

/**
 * create by yudan on 2022/2/11
 */
@CacheConfig(cacheNames = "{audit}")
public interface AuditService extends IService<Audit> {

	// 修改审核状态
	void audit(Audit audit);

	// 删除代理商申请
	void deleteByUserId(String id, int type);
}
