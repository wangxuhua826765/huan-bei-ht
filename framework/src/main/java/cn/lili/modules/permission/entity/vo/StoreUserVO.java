package cn.lili.modules.permission.entity.vo;

import cn.lili.common.utils.BeanUtil;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.entity.dos.Menu;
import cn.lili.modules.permission.entity.dos.Role;
import cn.lili.modules.permission.entity.dos.StoreUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 管理员VO
 *
 * @author Chopper
 * @since 2020-11-22 09:17
 */
@Data
public class StoreUserVO extends StoreUser {

	private static final long serialVersionUID = -2378384199695839312L;

	@ApiModelProperty(value = "用户拥有角色")
	private List<Role> roles;

	@ApiModelProperty(value = "用户拥有的权限")
	private List<Menu> menus;

	public StoreUserVO(StoreUser user) {
		BeanUtil.copyProperties(user, this);
	}

}
