package cn.lili.modules.order.order.mapper;

import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.order.order.entity.dto.OrderExportDTO;
import cn.lili.modules.order.order.entity.vo.OrderSimpleVO;
import cn.lili.modules.order.order.entity.vo.OrderVO;
import cn.lili.modules.order.order.entity.vo.PaymentLog;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.poi.ss.formula.functions.T;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 订单数据处理层
 *
 * @author Chopper
 * @since 2020/11/17 7:35 下午
 */
public interface OrderMapper extends BaseMapper<Order> {

	@Select("SELECT s.member_id s_member_id,o.* from li_order o left join li_store s on s.id = o.store_id ${ew.customSqlSegment}")
	OrderVO getOne(@Param(Constants.WRAPPER) Wrapper<Order> queryWrapper);

	/**
	 * 修改订单状态
	 *
	 * @param status
	 *            状态
	 * @param orderSn
	 *            订单编号
	 */
	@Update({
			"update li_order set deliver_status= #{status}, order_status = #{status},receiving_time=#{receivingTime} where sn = #{orderSn}"})
	void updateStatus(String status, String orderSn, Date receivingTime);

	@Update({"update li_order set frozend = #{frozend} where sn = #{orderSn}"})
	void updateFrozend(String orderSn, Boolean frozend);

	/**
	 * 查询导出订单DTO列表
	 *
	 * @param queryWrapper
	 *            查询条件
	 * @return 导出订单DTO列表
	 */
	@Select("SELECT o.sn,o.create_time,o.member_name,o.consignee_name,o.consignee_mobile,o.consignee_address_path,o.consignee_detail,"
			+ "o.payment_method, o.logistics_name,o.freight_price,o.goods_price,o.discount_price,o.flow_price,oi.goods_name,oi.num,"
			+ "o.remark,o.order_status,o.pay_status,o.deliver_status,o.need_receipt,o.store_name FROM li_order o LEFT JOIN li_order_item oi "
			+ "ON oi.order_sn=o.sn and oi.merchandise_status = 0 ${ew.customSqlSegment}")
	List<OrderExportDTO> queryExportOrder(@Param(Constants.WRAPPER) Wrapper<OrderSimpleVO> queryWrapper);

	/**
	 * 查询订单支付记录
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 订单支付记录分页
	 */
	@Select("select  DISTINCT o.*,m.mobile " + "from li_order o" + " left join li_member m on m.id = o.member_id"
			+ " left join li_extension e on e.member_id = o.member_id"
			+ " LEFT JOIN li_store s ON s.id = o.store_id  ${ew.customSqlSegment} ")
	IPage<PaymentLog> queryPaymentLogs(IPage<PaymentLog> page,
			@Param(Constants.WRAPPER) Wrapper<PaymentLog> queryWrapper);

	/**
	 * 查询订单简短信息分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 简短订单分页
	 */
	@Select("select s.store_address_path regionName,s.store_address_id_path regionId,e.extension,"
			+ "cast(o.flow_price - IFNULL(o.commission,0) AS DECIMAL(10,2)) AS storePrice,o.sn,o.verification_code,"
			+ "o.flow_price,o.create_time,o.order_status,o.pay_status,o.payment_method,o.payment_time,"
			+ "o.member_name,o.store_name as store_name,o.store_id as store_id,o.client_type,o.order_type,"
			+ "o.deliver_status,o.owner,o.commission,m.mobile" + ",GROUP_CONCAT(oi.goods_id) as group_goods_id,"
			+ " GROUP_CONCAT(oi.sku_id) as group_sku_id," + " GROUP_CONCAT(oi.num) as group_num"
			+ ",GROUP_CONCAT(oi.image) as group_images" + ",GROUP_CONCAT(oi.goods_name) as group_name "
			+ ",GROUP_CONCAT(oi.after_sale_status) as group_after_sale_status"
			+ ",GROUP_CONCAT(oi.complain_status) as group_complain_status"
			+ ",GROUP_CONCAT(oi.comment_status) as group_comment_status"
			+ ",GROUP_CONCAT(oi.sn) as group_order_items_sn " + ",GROUP_CONCAT(oi.goods_price) as group_goods_price "
			+ ",f.receipt_status as receiptStatus" + " FROM li_order o" + " left join li_member m on m.id = o.member_id"
			+ " LEFT JOIN li_order_item AS oi on o.sn = oi.order_sn and oi.merchandise_status = 0"
			+ " left join li_store s on s.id = o.store_id"
			+ " left join li_extension e on e.member_id = o.member_id and e.delete_flag = '0' "
			+ " LEFT JOIN li_receipt f  on f.order_sn = o.sn " + " ${ew.customSqlSegment} ")
	IPage<OrderSimpleVO> queryByParamsPage(IPage<OrderSimpleVO> page,
			@Param(Constants.WRAPPER) Wrapper<OrderSimpleVO> queryWrapper);

	@Select("select o.sn,o.flow_price,o.create_time,o.order_status,o.pay_status,o.payment_method,o.payment_time,o.member_name,o.store_name as store_name,o.store_id as store_id,o.client_type,o.order_type,o.deliver_status,o.owner "
			+ ",GROUP_CONCAT(oi.goods_id) as group_goods_id," + " GROUP_CONCAT(oi.sku_id) as group_sku_id,"
			+ " GROUP_CONCAT(oi.num) as group_num" + ",GROUP_CONCAT(oi.image) as group_images"
			+ ",GROUP_CONCAT(oi.goods_name) as group_name "
			+ ",GROUP_CONCAT(oi.after_sale_status) as group_after_sale_status"
			+ ",GROUP_CONCAT(oi.complain_status) as group_complain_status"
			+ ",GROUP_CONCAT(oi.comment_status) as group_comment_status"
			+ ",GROUP_CONCAT(oi.sn) as group_order_items_sn " + ",GROUP_CONCAT(oi.goods_price) as group_goods_price "
			+ " FROM li_order o LEFT JOIN li_order_item AS oi on o.sn = oi.order_sn and oi.merchandise_status = 0 ${ew.customSqlSegment} ")
	List<OrderSimpleVO> queryByParams(@Param(Constants.WRAPPER) Wrapper<OrderSimpleVO> queryWrapper);

	/**
	 * 查询订单信息
	 *
	 * @param queryWrapper
	 *            查询条件
	 * @return 简短订单分页
	 */
	@Select("select o.* "
			+ " FROM li_order o INNER JOIN li_order_item AS oi on o.sn = oi.order_sn and oi.merchandise_status = 0 ${ew.customSqlSegment} ")
	List<Order> queryListByParams(@Param(Constants.WRAPPER) Wrapper<Order> queryWrapper);

	@Select("select o.*,oi.goods_name  "
			+ " FROM li_order o INNER JOIN li_order_item AS oi on o.sn = oi.order_sn and oi.merchandise_status = 0 ${ew.customSqlSegment} ")
	List<OrderSimpleVO> findAll(@Param(Constants.WRAPPER) QueryWrapper<Order> queryWrapper);

	/**
	 * 修改解冻状态
	 * 
	 * @param id
	 *            订单的id
	 * @param status
	 *            当前状态
	 * @param upstatus
	 *            修改成的状态
	 * @return 执行结果
	 */
	@Update("UPDATE li_order SET frozend=#{upstatus} where id=#{id} AND frozend = #{status}")
	int updateFrozenStatusById(@Param("id") String id, @Param("status") Boolean status,
			@Param("upstatus") Boolean upstatus);

	@Select("SELECT m.* from li_member m where id = #{memberId}")
	Member getMemberInfo(@Param("memberId") String memberId);

	@Select("SELECT o.* from li_order o where trade_sn = #{tradeSn}")
	OrderVO getOrderInfoByTradeSn(@Param("tradeSn") String tradeSn);

	// 查询收货七天前的数据
	@Select("SELECT sn from li_order WHERE li_order.receiving_time <= DATE_SUB(now(), INTERVAL 6 DAY) and order_type!='OFFLINE' ")
	List<String> queryReceivingTime();

	// 查找冻结订单
	@Select("SELECT sum(commission) commission FROM li_order o "
		+ "WHERE o.order_status IN ( 'UNDELIVERED', 'DELIVERED' ) AND o.pay_status = 'PAID' and o.order_type = 'NORMAL'")
	Map<String,Object> findFrozenMap();

}