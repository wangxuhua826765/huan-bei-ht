package cn.lili.modules.order.order.entity.vo;

import cn.lili.modules.order.order.entity.dos.OrderItem;
import cn.lili.modules.order.order.entity.enums.CommentStatusEnum;
import cn.lili.modules.order.order.entity.enums.OrderComplaintStatusEnum;
import cn.lili.modules.order.order.entity.enums.OrderItemAfterSaleStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * 子订单VO
 *
 * @author Chopper
 * @since 2020-08-17 20:28
 */
@Data
@NoArgsConstructor
public class OrderItemVO extends OrderItem {

	@ApiModelProperty(value = "编号")
	private String sn;

	@ApiModelProperty(value = "用户名")
	private String nickName;

	@ApiModelProperty(value = "商品ID")
	private String goodsId;

	@ApiModelProperty(value = "货品ID")
	private String skuId;

	@ApiModelProperty(value = "销售量")
	private Integer num;

	@ApiModelProperty(value = "图片")
	private String image;

	@ApiModelProperty(value = "商品名称")
	private String name;

	@ApiModelProperty(value = "商品名称")
	private Double goodsPrice;

	/**
	 * @see OrderItemAfterSaleStatusEnum
	 */
	@ApiModelProperty(value = "售后状态", allowableValues = "NOT_APPLIED(未申请),ALREADY_APPLIED(已申请),EXPIRED(已失效不允许申请售后)")
	private String afterSaleStatus;

	/**
	 * @see cn.lili.modules.order.trade.entity.enums.AfterSaleTypeEnum
	 */
	@ApiModelProperty(value = "售后类型", allowableValues = "RETURN_GOODS,RETURN_MONEY")
	private String serviceType;

	/**
	 * @see cn.lili.modules.order.trade.entity.enums.AfterSaleStatusEnum
	 */
	@ApiModelProperty(value = "售后单状态", allowableValues = "APPLY,PASS,REFUSE,BUYER_RETURN,SELLER_RE_DELIVERY,BUYER_CONFIRM,SELLER_CONFIRM,COMPLETE")
	private String serviceStatus;

	/**
	 * 售后单号
	 */
	@ApiModelProperty(value = "售后单号")
	private String afterSn;

	/**
	 * @see OrderComplaintStatusEnum
	 */
	@ApiModelProperty(value = "投诉状态")
	private String complainStatus;

	/**
	 * @see CommentStatusEnum
	 */
	@ApiModelProperty(value = "评论状态:未评论(UNFINISHED),待追评(WAIT_CHASE),评论完成(FINISHED)，")
	private String commentStatus;

	@ApiModelProperty(value = "评论内容")
	private String content;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "店铺区域")
	private String region;

	@ApiModelProperty(value = "提现费率")
	private Double applyFee;

	@ApiModelProperty(value = "提现现金额")
	private Double realMoney;

	@ApiModelProperty(value = "提现焕呗额")
	private Double applyMoney;

	public OrderItemVO(String sn, String goodsId, String skuId, Integer num, String image, String name,
			String afterSaleStatus, String complainStatus, String commentStatus, Double goodsPrice, String content) {
		this.sn = sn;
		this.goodsId = goodsId;
		this.skuId = skuId;
		this.num = num;
		this.image = image;
		this.name = name;
		this.afterSaleStatus = afterSaleStatus;
		this.complainStatus = complainStatus;
		this.commentStatus = commentStatus;
		this.goodsPrice = goodsPrice;
		this.content = content;
	}

}
