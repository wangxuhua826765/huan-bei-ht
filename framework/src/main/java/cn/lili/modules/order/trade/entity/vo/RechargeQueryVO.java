package cn.lili.modules.order.trade.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 预存款充值记录查询条件
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
@Data
@ApiModel(value = "预存款充值记录查询条件")
@AllArgsConstructor
@NoArgsConstructor
public class RechargeQueryVO implements Serializable {

	private static final long serialVersionUID = 318396158590640917L;

	@ApiModelProperty(value = "id")
	private String id;

	@ApiModelProperty(value = "手机号")
	private String mobile;
	/**
	 * 充值订单编号
	 */
	@ApiModelProperty(value = "充值订单编号")
	private String rechargeSn;

	/**
	 * 会员ID
	 */
	@ApiModelProperty(value = "会员Id")
	private String memberId;
	/**
	 * 会员名称
	 */
	@ApiModelProperty(value = "会员名称")
	private String memberName;
	/**
	 * 充值时间
	 */
	@ApiModelProperty(value = "充值开始时间")
	private String startDate;

	/**
	 * 充值时间
	 */
	@ApiModelProperty(value = "充值结束时间")
	private String endDate;

	/**
	 * 区域id
	 */
	@ApiModelProperty(value = "区域id")
	private String location;

	/**
	 * 级别
	 */
	@ApiModelProperty(value = "级别 0:普 /1:银 /2:金")
	private String level;

	/**
	 * 身份名称
	 */
	@ApiModelProperty(value = "身份名称")
	private String roleName;

}