package cn.lili.modules.order.order.mapper;

import cn.lili.modules.order.order.entity.dos.OrderItem;
import cn.lili.modules.order.order.entity.vo.OrderItemVO;
import cn.lili.modules.order.order.entity.vo.OrderSimpleVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 子订单数据处理层
 *
 * @author Chopper
 * @since 2020/11/17 7:34 下午
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

	@Select("SELECT * FROM li_order_item AS oi " + "INNER JOIN li_order AS o ON oi.order_sn=o.sn "
			+ "left JOIN li_member_evaluation AS e ON e.order_no = o.sn and oi.sku_id = e.sku_id"
			+ " LEFT JOIN ( SELECT aa.sn as afterSn,aa.order_sn,aa.goods_id,aa.service_type,aa.service_status FROM ( SELECT * FROM li_after_sale ORDER BY create_time DESC ) aa GROUP BY order_sn, goods_id ) a ON "
			+ " a.order_sn = oi.order_sn and  a.goods_id = oi.goods_id" + " ${ew.customSqlSegment}")
	List<OrderItemVO> selectLists(@Param(Constants.WRAPPER) Wrapper<OrderSimpleVO> queryWrapper);

	/**
	 * 获取等待操作订单子项目
	 *
	 * @param queryWrapper
	 *            查询条件
	 * @return 订单子项列表
	 */
	@Select("SELECT * FROM li_order_item AS oi INNER JOIN li_order AS o ON oi.order_sn=o.sn ${ew.customSqlSegment}")
	List<OrderItem> waitOperationOrderItem(@Param(Constants.WRAPPER) Wrapper<OrderSimpleVO> queryWrapper);
}