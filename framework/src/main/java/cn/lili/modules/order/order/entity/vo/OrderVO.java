package cn.lili.modules.order.order.entity.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.order.order.entity.dos.OrderItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 订单vo
 *
 * @author Bulbasaur
 * @since 2020/11/28 11:38
 */
@Data
@NoArgsConstructor
public class OrderVO extends Order {

	private static final long serialVersionUID = 5820637554656388777L;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "商家id")
	private String sMemberId;

	@ApiModelProperty(value = "会员级别")
	private String gradeName;

	@ApiModelProperty(value = "会员昵称")
	private String nickName;

	@ApiModelProperty(value = "区域id")
	private String regionId;

	@ApiModelProperty(value = "区域")
	private String region;

	@ApiModelProperty(value = "付款人")
	private String payer;

	@ApiModelProperty(value = "收款人")
	private String payee;

	@ApiModelProperty(value = "订单额")
	private Double dingdan;

	@ApiModelProperty(value = "运费")
	private Double yunfei;

	@ApiModelProperty(value = "服务费")
	private Double fuwufei;

	@ApiModelProperty(value = "焕呗总额")
	private Double allMoney;

	@ApiModelProperty(value = "订单类型  out 收入  in 支出")
	private String type;

	@ApiModelProperty(value = "起始日期")
	private String startDate;

	@ApiModelProperty(value = "结束日期")
	private String endDate;

	@ApiModelProperty(value = "订单商品项目")
	private List<OrderItem> orderItems;

	public OrderVO(Order order, List<OrderItem> orderItems) {
		BeanUtil.copyProperties(order, this);
		this.setOrderItems(orderItems);
	}
}
