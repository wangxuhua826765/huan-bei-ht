package cn.lili.modules.order.order.entity.enums;

/**
 * FrozenEnum. 解冻枚举
 * 
 * @author Li Bing
 * @since 2022.03.27
 */
public enum FrozenEnum {

	DONG_JIE(true, "冻结"), JIE_DONG(false, "解冻");

	private final Boolean code;
	private final String value;

	public Boolean getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	FrozenEnum(Boolean code, String value) {
		this.code = code;
		this.value = value;
	}
}
