package cn.lili.modules.order.aftersale.mapper;

import cn.lili.modules.order.aftersale.entity.dos.AfterSaleReason;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 售后原因数据处理层
 *
 * @author Chopper
 * @since 2020/11/17 7:34 下午
 */
public interface AfterSaleReasonMapper extends BaseMapper<AfterSaleReason> {
}