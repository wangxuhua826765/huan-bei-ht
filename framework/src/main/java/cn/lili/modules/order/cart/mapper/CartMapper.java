package cn.lili.modules.order.cart.mapper;

import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.order.order.entity.dos.Trade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 交易数据处理层
 *
 * @author Chopper
 * @since 2020/11/17 7:35 下午
 */
public interface CartMapper extends BaseMapper<Order> {

	@Select("SELECT id from li_store where delete_flag = 0 and member_id = #{memberId}")
	String getStoreId(String memberId);

	@Select("SELECT\n" + "\tid,\n" + "\tsn,\n" + "\ttrade_sn,\n" + "\tstore_id,\n" + "\tstore_name,\n"
			+ "\tmember_id,\n" + "\tmember_name,\n" + "\torder_status,\n" + "\tpay_status,\n" + "\tdeliver_status,\n"
			+ "\treceivable_no,\n" + "\tpayment_method,\n" + "\tpayment_time,\n" + "\tconsignee_name,\n"
			+ "\tconsignee_mobile,\n" + "\tdelivery_method,\n" + "\tconsignee_address_path,\n"
			+ "\tconsignee_address_id_path,\n" + "\tconsignee_detail,\n" + "\tflow_price,\n" + "\tgoods_price,\n"
			+ "\tfreight_price,\n" + "\tdiscount_price,\n" + "\tupdate_price,\n" + "\tlogistics_no,\n"
			+ "\tlogistics_code,\n" + "\tlogistics_name,\n" + "\tweight,\n" + "\tgoods_num,\n" + "\tremark,\n"
			+ "\tcancel_reason,\n" + "\tcomplete_time,\n" + "\tlogistics_time,\n" + "\tpay_order_no,\n"
			+ "\tclient_type,\n" + "\tOWNER,\n" + "\tcommission,\n" + "\tneed_receipt,\n" + "\tparent_order_sn,\n"
			+ "\tpromotion_id,\n" + "\torder_type,\n" + "\torder_promotion_type,\n" + "\tprice_detail,\n"
			+ "\tcan_return,\n" + "\tverification_code,\n" + "\tdistribution_id,\n" + "\tuse_store_member_coupon_ids,\n"
			+ "\tuse_platform_member_coupon_id,\n" + "\tfrozend,\n" + "\tcreate_by,\n" + "\tcreate_time,\n"
			+ "\tupdate_by,\n" + "\tupdate_time,\n" + "\tdelete_flag \n" + "FROM\n" + "\tli_order \n" + "WHERE\n"
			+ "\t( ( order_status = 'TAKE' OR order_status = 'DELIVERED' ) AND store_id = #{storeId} AND verification_code = #{verificationCode} )")
	Order getOrderInfo(String storeId, String verificationCode);
}