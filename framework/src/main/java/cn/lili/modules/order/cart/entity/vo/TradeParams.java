package cn.lili.modules.order.cart.entity.vo;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.modules.order.cart.entity.dto.StoreRemarkDTO;
import cn.lili.modules.order.cart.entity.enums.DeliveryMethodEnum;
import cn.lili.modules.transfer.entity.Transfer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 交易参数
 *
 * @author paulG
 * @since 2021/2/23
 **/
@Data
public class TradeParams implements Serializable {

	private static final long serialVersionUID = -8383072817737513063L;

	@ApiModelProperty(value = "购物车购买：CART/立即购买：BUY_NOW/拼团购买：PINTUAN / 积分购买：POINT/ 线下消费：OFFLINE/ 端口费：PORT_FEE")
	private String way;

	/**
	 * @see ClientTypeEnum
	 */
	@ApiModelProperty(value = "客户端：H5/移动端 PC/PC端,WECHAT_MP/小程序端,WECHAT_MP2/小程序商家端b2b,APP/移动应用端")
	private String client;

	@ApiModelProperty(value = "店铺备注")
	private List<StoreRemarkDTO> remark;

	@ApiModelProperty(value = "是否为其他订单下的订单，如果是则为依赖订单的sn，否则为空")
	private String parentOrderSn;
	/**
	 * WalletOwnerEnum
	 */
	@ApiModelProperty(value = "订单来源  STORE 商家端   BUYER 用户端")
	private String owner;

	/**
	 * @see DeliveryMethodEnum
	 */
	@ApiModelProperty(value = "配送方式")
	private String deliveryMethod;

	@ApiModelProperty(value = "店铺id")
	private String storeId;

	@ApiModelProperty(value = "金额")
	private Double price;

	@ApiModelProperty(value = "转账收款人手机号")
	private String payeePhone;

	@ApiModelProperty(value = "转账手续费")
	private Double commission;

}
