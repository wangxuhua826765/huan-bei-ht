package cn.lili.modules.order.order.entity.vo;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.StrUtil;
import cn.lili.modules.order.aftersale.entity.enums.ComplaintStatusEnum;
import cn.lili.modules.order.order.entity.dos.OrderComplaint;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 订单投诉查询参数
 *
 * @author paulG
 * @since 2020/12/4
 **/
@Data
public class OrderComplaintSearchParams {

	/**
	 * @see ComplaintStatusEnum
	 */
	@ApiModelProperty(value = "交易投诉状态")
	private String status;

	@ApiModelProperty(value = "订单号")
	private String orderSn;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "商家id")
	private String storeId;

	@ApiModelProperty(value = "商家名称")
	private String storeName;

	@ApiModelProperty(value = "推广人")
	private String extension;

	@ApiModelProperty(value = "合伙人区域")
	private String adcode;

	public QueryWrapper<OrderComplaintVO> QueryWrapper() {
		QueryWrapper<OrderComplaintVO> queryWrapper = new QueryWrapper<>();
		if (StrUtil.isNotEmpty(status)) {
			queryWrapper.eq("o.complain_status", status);
		}
		if (StrUtil.isNotEmpty(orderSn)) {
			queryWrapper.eq("o.order_sn", orderSn);
		}
		if (StrUtil.isNotEmpty(storeName)) {
			queryWrapper.like("o.store_name", storeName);
		}
		if (StrUtil.isNotEmpty(storeId)) {
			queryWrapper.eq("o.store_id", storeId);
		}
		if (StrUtil.isNotEmpty(memberName)) {
			queryWrapper.like("o.member_name", memberName);
		}
		if (StrUtil.isNotEmpty(memberId)) {
			queryWrapper.eq("o.member_id", memberId);
		}
		if (StrUtil.isNotEmpty(mobile)) {
			queryWrapper.like("m.mobile", mobile);
		}

		// 按推广人
		queryWrapper.eq(CharSequenceUtil.isNotEmpty(extension), "e.extension", extension);

		queryWrapper.eq("o.delete_flag", false);
		queryWrapper.orderByDesc("o.create_time");
		return queryWrapper;
	}

}
