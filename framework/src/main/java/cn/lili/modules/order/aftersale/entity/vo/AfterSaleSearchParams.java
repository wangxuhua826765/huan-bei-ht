package cn.lili.modules.order.aftersale.entity.vo;

import cn.hutool.core.text.CharSequenceUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 售后搜索参数
 *
 * @author paulG
 * @since 2020/12/4
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class AfterSaleSearchParams extends PageVO {

	@ApiModelProperty(value = "售后服务单号")
	private String sn;

	@ApiModelProperty(value = "订单编号")
	private String orderSn;

	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "商家名称")
	private String storeName;

	@ApiModelProperty(value = "商家ID")
	private String storeId;

	@ApiModelProperty(value = "商品名称")
	private String goodsName;

	@ApiModelProperty(value = "申请退款金额,可以为范围，如10_1000")
	private String applyRefundPrice;

	@ApiModelProperty(value = "实际退款金额,可以为范围，如10_1000")
	private String actualRefundPrice;

	@ApiModelProperty(value = "总价格,可以为范围，如10_1000")
	private String flowPrice;

	@ApiModelProperty(value = "合伙人区域")
	private String adcode;
	/**
	 * @see cn.lili.modules.order.trade.entity.enums.AfterSaleTypeEnum
	 */
	@ApiModelProperty(value = "售后类型", allowableValues = "CANCEL,RETURN_GOODS,EXCHANGE_GOODS,REISSUE_GOODS")
	private String serviceType;

	/**
	 * @see cn.lili.modules.order.trade.entity.enums.AfterSaleStatusEnum
	 */
	@ApiModelProperty(value = "售后单状态", allowableValues = "APPLY,PASS,REFUSE,BUYER_RETURN,SELLER_RE_DELIVERY,BUYER_CONFIRM,SELLER_CONFIRM,COMPLETE")
	private String serviceStatus;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private Date startDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private Date endDate;

	@ApiModelProperty(value = "推广人")
	private String extension;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (CharSequenceUtil.isNotEmpty(sn)) {
			queryWrapper.like("sn", sn);
		}
		if (CharSequenceUtil.isNotEmpty(orderSn)) {
			queryWrapper.like("order_sn", orderSn);
		}
		// 按买家查询
		if (CharSequenceUtil.equals(UserContext.getCurrentUser().getRole().name(), UserEnums.MEMBER.name())) {
			queryWrapper.eq("a.member_id", UserContext.getCurrentUser().getId());
		}
		// 按卖家查询
		if (CharSequenceUtil.equals(UserContext.getCurrentUser().getRole().name(), UserEnums.STORE.name())) {
			queryWrapper.eq("a.store_id", UserContext.getCurrentUser().getStoreId());
		}

		if (CharSequenceUtil.equals(UserContext.getCurrentUser().getRole().name(), UserEnums.MANAGER.name())
				&& CharSequenceUtil.isNotEmpty(storeId)) {
			queryWrapper.eq("a.store_id", storeId);
		}
		if (CharSequenceUtil.isNotEmpty(memberName)) {
			queryWrapper.like("a.member_name", memberName);
		}
		if (CharSequenceUtil.isNotEmpty(mobile)) {
			queryWrapper.like("m.mobile", mobile);
		}
		if (CharSequenceUtil.isNotEmpty(storeName)) {
			queryWrapper.like("a.store_name", storeName);
		}
		if (CharSequenceUtil.isNotEmpty(goodsName)) {
			queryWrapper.like("a.goods_name", goodsName);
		}
		// 按时间查询
		if (startDate != null) {
			queryWrapper.ge("a.create_time", startDate);
		}
		if (endDate != null) {
			queryWrapper.le("a.create_time", endDate);
		}
		if (CharSequenceUtil.isNotEmpty(serviceStatus)) {
			queryWrapper.eq("a.service_status", serviceStatus);
		}
		if (CharSequenceUtil.isNotEmpty(serviceType)) {
			queryWrapper.eq("a.service_type", serviceType);
		}

		// 按推广人
		queryWrapper.eq(CharSequenceUtil.isNotEmpty(extension), "e.extension", extension);

		this.betweenWrapper(queryWrapper);
		queryWrapper.eq("a.delete_flag", false);
		return queryWrapper;
	}

	private <T> void betweenWrapper(QueryWrapper<T> queryWrapper) {
		if (CharSequenceUtil.isNotEmpty(applyRefundPrice)) {
			String[] s = applyRefundPrice.split("_");
			if (s.length > 1) {
				queryWrapper.between("a.apply_refund_price", s[0], s[1]);
			} else {
				queryWrapper.ge("a.apply_refund_price", s[0]);
			}
		}
		if (CharSequenceUtil.isNotEmpty(actualRefundPrice)) {
			String[] s = actualRefundPrice.split("_");
			if (s.length > 1) {
				queryWrapper.between("a.actual_refund_price", s[0], s[1]);
			} else {
				queryWrapper.ge("a.actual_refund_price", s[0]);
			}
		}
		if (CharSequenceUtil.isNotEmpty(flowPrice)) {
			String[] s = flowPrice.split("_");
			if (s.length > 1) {
				queryWrapper.between("a.flow_price", s[0], s[1]);
			} else {
				queryWrapper.ge("a.flow_price", s[0]);
			}
		}
	}

}
