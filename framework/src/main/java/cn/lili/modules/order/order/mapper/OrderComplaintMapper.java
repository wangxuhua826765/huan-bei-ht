package cn.lili.modules.order.order.mapper;

import cn.lili.modules.order.order.entity.dos.OrderComplaint;
import cn.lili.modules.order.order.entity.vo.OrderComplaintVO;
import cn.lili.modules.order.order.entity.vo.OrderSimpleVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 订单投诉数据处理层
 *
 * @author paulG
 * @since 2020/12/5
 **/
public interface OrderComplaintMapper extends BaseMapper<OrderComplaint> {

	@Select("SELECT o.*,m.mobile FROM li_order_complaint o" + " left join li_member m on m.id = o.member_id"
			+ " left join li_extension e on e.member_id = o.member_id"
			+ " LEFT JOIN li_store s ON s.id = o.store_id ${ew.customSqlSegment}")
	IPage<OrderComplaintVO> selectPage(IPage<OrderComplaint> page,
			@Param(Constants.WRAPPER) QueryWrapper<OrderComplaintVO> queryWrapper);

}
