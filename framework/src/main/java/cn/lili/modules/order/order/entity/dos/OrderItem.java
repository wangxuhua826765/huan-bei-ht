package cn.lili.modules.order.order.entity.dos;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import cn.lili.common.utils.BeanUtil;
import cn.lili.common.utils.SnowFlake;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.order.cart.entity.dto.TradeDTO;
import cn.lili.modules.order.cart.entity.vo.CartSkuVO;
import cn.lili.modules.order.cart.entity.vo.CartVO;
import cn.lili.modules.order.order.entity.dto.PriceDetailDTO;
import cn.lili.modules.order.order.entity.enums.CommentStatusEnum;
import cn.lili.modules.order.order.entity.enums.OrderComplaintStatusEnum;
import cn.lili.modules.order.order.entity.enums.OrderItemAfterSaleStatusEnum;
import cn.lili.modules.promotion.entity.vos.PromotionSkuVO;
import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.stream.Collectors;

/**
 * 子订单
 *
 * @author Chopper
 * @since 2020/11/17 7:30 下午
 */
// @Data
@TableName("li_order_item")
@ApiModel(value = "子订单")
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem extends BaseEntity {

	private static final long serialVersionUID = 2108971190191410182L;

	@ApiModelProperty(value = "订单编号")
	private String orderSn;

	@ApiModelProperty(value = "子订单编号")
	private String sn;

	@ApiModelProperty(value = "单价")
	private Double unitPrice;

	@ApiModelProperty(value = "小记")
	private Double subTotal;

	@ApiModelProperty(value = "商品ID")
	private String goodsId;

	@ApiModelProperty(value = "货品ID")
	private String skuId;

	@ApiModelProperty(value = "销售量")
	private Integer num;

	@ApiModelProperty(value = "交易编号")
	private String tradeSn;

	@ApiModelProperty(value = "图片")
	private String image;

	@ApiModelProperty(value = "商品名称")
	private String goodsName;

	@ApiModelProperty(value = "分类ID")
	private String categoryId;

	@ApiModelProperty(value = "快照id")
	private String snapshotId;

	@ApiModelProperty(value = "规格json")
	private String specs;

	@ApiModelProperty(value = "促销类型")
	private String promotionType;

	@ApiModelProperty(value = "促销id")
	private String promotionId;

	@ApiModelProperty(value = "销售金额")
	private Double goodsPrice;

	@ApiModelProperty(value = "实际金额")
	private Double flowPrice;

	/**
	 * @see CommentStatusEnum
	 */
	@ApiModelProperty(value = "评论状态:未评论(UNFINISHED),待追评(WAIT_CHASE),评论完成(FINISHED)，")
	private String commentStatus;

	/**
	 * @see OrderItemAfterSaleStatusEnum
	 */
	@ApiModelProperty(value = "售后状态")
	private String afterSaleStatus;

	@ApiModelProperty(value = "价格详情")
	private String priceDetail;

	/**
	 * @see OrderComplaintStatusEnum
	 */
	@ApiModelProperty(value = "投诉状态")
	private String complainStatus;

	@ApiModelProperty(value = "交易投诉id")
	private String complainId;

	@ApiModelProperty(value = "退货商品数量")
	private Integer returnGoodsNumber;

	@ApiModelProperty(value = "可提现金额")
	private Double deposit;

	@ApiModelProperty(value = "提现折扣汇率")
	private Double fee;

	@ApiModelProperty(value = "订单提现金额是否提现完-1 金额提现完，0 金额未提现完")
	private Integer realization;

	@ApiModelProperty(value = "剩余可提现金额")
	private Double surplus;

	@ApiModelProperty(value = "剩余可提现焕呗金额")
	private Double surplusPrice;

	@ApiModelProperty(value = "是否为商品状态0,1为邮费")
	private Integer MerchandiseStatus;

	public OrderItem(CartSkuVO cartSkuVO, CartVO cartVO, TradeDTO tradeDTO) {
		String oldId = this.getId();
		BeanUtil.copyProperties(cartSkuVO.getGoodsSku(), this);
		BeanUtil.copyProperties(cartSkuVO.getPriceDetailDTO(), this);
		BeanUtil.copyProperties(cartSkuVO, this);
		this.setId(oldId);
		if (cartSkuVO.getPriceDetailDTO().getJoinPromotion() != null
				&& !cartSkuVO.getPriceDetailDTO().getJoinPromotion().isEmpty()) {
			this.setPromotionType(CollUtil.join(cartSkuVO.getPriceDetailDTO().getJoinPromotion().stream()
					.map(PromotionSkuVO::getPromotionType).collect(Collectors.toList()), ","));
			this.setPromotionId(CollUtil.join(cartSkuVO.getPriceDetailDTO().getJoinPromotion().stream()
					.map(PromotionSkuVO::getActivityId).collect(Collectors.toList()), ","));
		}
		this.setAfterSaleStatus(OrderItemAfterSaleStatusEnum.NEW.name());
		this.setCommentStatus(CommentStatusEnum.NEW.name());
		this.setComplainStatus(OrderComplaintStatusEnum.NEW.name());
		this.setPriceDetailDTO(cartSkuVO.getPriceDetailDTO());
		this.setOrderSn(cartVO.getSn());
		this.setTradeSn(tradeDTO.getSn());
		this.setImage(cartSkuVO.getGoodsSku().getThumbnail());
		this.setGoodsName(cartSkuVO.getGoodsSku().getGoodsName());
		this.setSkuId(cartSkuVO.getGoodsSku().getId());
		this.setCategoryId(cartSkuVO.getGoodsSku().getCategoryPath()
				.substring(cartSkuVO.getGoodsSku().getCategoryPath().lastIndexOf(",") + 1));
		this.setGoodsPrice(cartSkuVO.getGoodsSku().getPrice());
		this.setUnitPrice(cartSkuVO.getPurchasePrice());
		this.setSubTotal(cartSkuVO.getSubTotal());
		this.setSn(SnowFlake.createStr("OI"));

	}

	public PriceDetailDTO getPriceDetailDTO() {
		return JSONUtil.toBean(priceDetail, PriceDetailDTO.class);
	}

	public void setPriceDetailDTO(PriceDetailDTO priceDetail) {
		this.priceDetail = JSONUtil.toJsonStr(priceDetail);
	}

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getTradeSn() {
		return tradeSn;
	}

	public void setTradeSn(String tradeSn) {
		this.tradeSn = tradeSn;
	}

	public String getImage() {
		if (StringUtils.isNotBlank(image) && image.lastIndexOf("?") > 0) {
			image = image.substring(0, image.lastIndexOf("?"));
		}
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public String getSpecs() {
		return specs;
	}

	public void setSpecs(String specs) {
		this.specs = specs;
	}

	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}

	public Double getGoodsPrice() {
		return goodsPrice;
	}

	public void setGoodsPrice(Double goodsPrice) {
		this.goodsPrice = goodsPrice;
	}

	public Double getFlowPrice() {
		return flowPrice;
	}

	public void setFlowPrice(Double flowPrice) {
		this.flowPrice = flowPrice;
	}

	public String getCommentStatus() {
		return commentStatus;
	}

	public void setCommentStatus(String commentStatus) {
		this.commentStatus = commentStatus;
	}

	public String getAfterSaleStatus() {
		return afterSaleStatus;
	}

	public void setAfterSaleStatus(String afterSaleStatus) {
		this.afterSaleStatus = afterSaleStatus;
	}

	public String getPriceDetail() {
		return priceDetail;
	}

	public void setPriceDetail(String priceDetail) {
		this.priceDetail = priceDetail;
	}

	public String getComplainStatus() {
		return complainStatus;
	}

	public void setComplainStatus(String complainStatus) {
		this.complainStatus = complainStatus;
	}

	public String getComplainId() {
		return complainId;
	}

	public void setComplainId(String complainId) {
		this.complainId = complainId;
	}

	public Integer getReturnGoodsNumber() {
		return returnGoodsNumber;
	}

	public void setReturnGoodsNumber(Integer returnGoodsNumber) {
		this.returnGoodsNumber = returnGoodsNumber;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Double getDeposit() {
		return deposit;
	}

	public Double getFee() {
		return fee;
	}

	public Integer getRealization() {
		return realization;
	}

	public Double getSurplus() {
		return surplus;
	}

	public Integer getMerchandiseStatus() {
		return MerchandiseStatus;
	}

	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public void setRealization(Integer realization) {
		this.realization = realization;
	}

	public void setSurplus(Double surplus) {
		this.surplus = surplus;
	}

	public Double getSurplusPrice() {
		return surplusPrice;
	}

	public void setSurplusPrice(Double surplusPrice) {
		this.surplusPrice = surplusPrice;
	}

	public void setMerchandiseStatus(Integer merchandiseStatus) {
		MerchandiseStatus = merchandiseStatus;
	}
}