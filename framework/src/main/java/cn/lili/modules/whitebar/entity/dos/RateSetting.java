package cn.lili.modules.whitebar.entity.dos;

import com.baomidou.mybatisplus.annotation.TableName;
import cn.lili.mybatis.BaseEntity;
import com.alibaba.druid.sql.visitor.functions.Char;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 费率管理
 *
 * @author 贾送兵
 * @since
 */
@Data
@TableName("li_rate_setting")
@ApiModel(value = "费率管理")
public class RateSetting extends BaseEntity {

	private static final long serialVersionUID = -5339221840646353417L;

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "角色id")
	private String roleId;
	// @ApiModelProperty(value = "充值费率")
	// private Double rechargeRule;
	//
	// @ApiModelProperty(value = "提现费率")
	// private Double withdrawalRate;
	@ApiModelProperty(value = "会员费率")
	private Double membershipRate;

	@ApiModelProperty(value = "合伙人费率")
	private Double partnerRate;

	@ApiModelProperty(value = "合伙人会费")
	private Double partnerPrice;

	@ApiModelProperty(value = "流水费率")
	private Double flowRate;

	@ApiModelProperty(value = "状态")
	private Integer enableState;

}