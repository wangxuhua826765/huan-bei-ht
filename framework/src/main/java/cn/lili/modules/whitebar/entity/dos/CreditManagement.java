package cn.lili.modules.whitebar.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 授信管理
 *
 * @author 贾送兵
 * @since
 */
@Data
@TableName("li_credit_management")
@ApiModel(value = "授信管理")
public class CreditManagement extends BaseEntity {

	private static final long serialVersionUID = -5339221840646353417L;

	@ApiModelProperty(value = "商户名称")
	private String name;

	@ApiModelProperty(value = "授信额度")
	private Double creditLine;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	@ApiModelProperty(value = "授信日期")
	private Date creditDate;
	@ApiModelProperty(value = "操作人id")
	private String operatorId;
	@ApiModelProperty(value = "操作人姓名")
	private String operatorName;

	@ApiModelProperty(value = "到期日期")
	private String expiryDate;

	@ApiModelProperty(value = "会员Id")
	private String memberId;

	@ApiModelProperty(value = "审核状态  0 待审核  1 审核通过  2 驳回")
	private String auditStatus;

	@ApiModelProperty(value = "还款时间")
	private String repaymentDate;

	@ApiModelProperty(value = "还款状态  0未还  1  已还")
	private String repaymentStatus;

}