package cn.lili.modules.whitebar.mapper;

import cn.lili.modules.payment.entity.dos.RecyclingManagement;
import cn.lili.modules.payment.entity.vo.RecyclingManagementVO;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 授信管理
 *
 * @author 贾送兵
 * @since 2022-02-11 09:25
 */
public interface CreditManagementMapper extends BaseMapper<CreditManagement> {
	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select a.*,b.store_name,b.store_address_detail,h.mobile from   li_credit_management a left join li_store b on a.member_id = b.id  left join li_member h on a.member_id = h.id  ${ew.customSqlSegment}")
	IPage<CreditManagementVO> getCreditManagementList(IPage<CreditManagementVO> page,
			@Param(Constants.WRAPPER) Wrapper<CreditManagementVO> queryWrapper);

}