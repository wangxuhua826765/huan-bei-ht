package cn.lili.modules.whitebar.service;

import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 退款日志 业务层
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface CreditManagementService extends IService<CreditManagement> {

	public IPage<CreditManagementVO> creditManagementPage(CreditManagementSearchParams creditManagementSearchParams);

	/**
	 * 删除
	 *
	 * @param ids
	 */
	void deleteByIds(List<String> ids);

}
