package cn.lili.modules.whitebar.entity.vo;

import cn.hutool.core.text.CharSequenceUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Calendar;

/**
 * 查询条件 佣金管理
 *
 * @author jiasongbing
 * @since 2022-02-12 19:27:20
 */
@Data
public class CommissionSearchParams extends PageVO {

	@ApiModelProperty(value = "用户名称")
	private String userName;
	@ApiModelProperty(value = "商家ID")
	private String storeId;

	@ApiModelProperty(value = "流水号")
	private String serialNumber;

	@ApiModelProperty(value = "订单来源")
	private String owner;

	@ApiModelProperty(value = "金额")
	private String flowPrice;

	@ApiModelProperty(value = "时间")
	private String endTime;
	private String startTime;

	public <T> QueryWrapper<T> queryWrapper() {
		// 查询店铺id
		AuthUser currentUser = UserContext.getCurrentUser();
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq(StringUtils.isNotBlank(userName), "c.user_name", userName);
		queryWrapper.eq(StringUtils.isNotBlank(serialNumber), "c.serial_number", serialNumber);
		queryWrapper.isNotNull("c.transaction_flow");
		if (currentUser != null) {
			// 店铺查询
			queryWrapper.eq(CharSequenceUtil.equals(currentUser.getRole().name(), UserEnums.STORE.name()), "store_id",
					currentUser.getStoreId());

		}
		if (StringUtils.isNotBlank(startTime)) {
			// queryWrapper.between("c.create_time", startTime,endTime);
			queryWrapper.ge("c.transaction_time", startTime);
			queryWrapper.le("c.transaction_time", endTime);
		} else {
			// 获取当前月份
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			String m = year + "-" + (month < 10 ? "0" + month : month);
			queryWrapper.eq("DATE_FORMAT(c.transaction_time,'%Y-%m')", m);
		}
		// 1，用户流水佣金
		// if("1".equals(owner))
		// queryWrapper.eq("o.owner", WalletOwnerEnum.BUYER_RECHARGE.name());
		// 2，商家流水佣金
		// if("2".equals(owner))
		// queryWrapper.eq("o.owner",WalletOwnerEnum.STORE_RECHARGE.name());
		queryWrapper.orderByDesc("c.transaction_time");
		return queryWrapper;
	}

}
