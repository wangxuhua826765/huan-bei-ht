package cn.lili.modules.whitebar.service;

import cn.lili.modules.whitebar.entity.dos.Commission;
import cn.lili.modules.whitebar.entity.vo.CommissionSearchParams;
import cn.lili.modules.whitebar.entity.vo.CommissionVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 佣金
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface CommissionService extends IService<Commission> {

	public IPage<CommissionVO> commissionPage(CommissionSearchParams commissionSearchParams);

	/**
	 * 删除
	 *
	 * @param ids
	 */
	void deleteByIds(List<String> ids);

	// 查询列表
	List<CommissionVO> findList(String memberId);

	List<CommissionVO> findList(QueryWrapper<CommissionVO> queryWrapper);

	IPage<CommissionVO> findCommissionVO(CommissionSearchParams commissionSearchParams);

}
