package cn.lili.modules.whitebar.serviceimpl;

import cn.lili.modules.whitebar.entity.dos.Commission;
import cn.lili.modules.whitebar.entity.vo.CommissionSearchParams;
import cn.lili.modules.whitebar.entity.vo.CommissionVO;
import cn.lili.modules.whitebar.mapper.CommissionMapper;
import cn.lili.modules.whitebar.service.CommissionService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 佣金
 *
 * @author 贾送兵
 * @since 2022-2-17 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class CommissionServiceImpl extends ServiceImpl<CommissionMapper, Commission> implements CommissionService {

	@Override
	public IPage<CommissionVO> commissionPage(CommissionSearchParams commissionSearchParams) {

		return this.baseMapper.getCommissionList(PageUtil.initPage(commissionSearchParams),
				commissionSearchParams.queryWrapper());
	}

	@Override
	public IPage<CommissionVO> findCommissionVO(CommissionSearchParams commissionSearchParams) {

		return this.baseMapper.findCommissionVO(PageUtil.initPage(commissionSearchParams),
				commissionSearchParams.queryWrapper());
	}

	@Override
	public void deleteByIds(List<String> ids) {
		this.removeByIds(ids);
	}

	@Override
	public List<CommissionVO> findList(String memberId) {
		QueryWrapper<CommissionVO> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", memberId);
		return this.baseMapper.findList(queryWrapper);
	}

	@Override
	public List<CommissionVO> findList(QueryWrapper<CommissionVO> queryWrapper) {
		return this.baseMapper.findList(queryWrapper);
	}

}