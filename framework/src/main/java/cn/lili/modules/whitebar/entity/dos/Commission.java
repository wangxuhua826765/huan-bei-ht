package cn.lili.modules.whitebar.entity.dos;

import cn.hutool.core.text.CharSequenceUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 佣金管理
 *
 * @author 贾送兵
 * @since
 */
@Data
@TableName("li_commission")
@ApiModel(value = "佣金管理")
public class Commission extends BaseEntity {

	private static final long serialVersionUID = -5339221840646353417L;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "用户id")
	private String userId;
	@ApiModelProperty(value = "充值佣金")
	private Double recharge;

	@ApiModelProperty(value = "会员佣金")
	private Double member;
	@ApiModelProperty(value = "交易流水佣金")
	private Double transactionFlow;
	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	@ApiModelProperty(value = "交易时间")
	private Date transactionTime;

	@ApiModelProperty(value = "流水号")
	private String serialNumber;

	@ApiModelProperty(value = "交易类型")
	private String transactionType;

	@ApiModelProperty(value = "交易类型名称")
	private String transactionTypeText;

	@ApiModelProperty(value = "商家ID")
	private String storeId;

	public <T> QueryWrapper<T> queryWrapper() {
		AuthUser currentUser = UserContext.getCurrentUser();
		QueryWrapper<T> wrapper = new QueryWrapper<>();
		if (currentUser != null) {
			// 店铺查询
			wrapper.eq(CharSequenceUtil.equals(currentUser.getRole().name(), UserEnums.MANAGER.name())
					&& CharSequenceUtil.isNotEmpty(storeId), "o.store_id", storeId);

		}
		return wrapper;
	}

}