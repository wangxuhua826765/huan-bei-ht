package cn.lili.modules.whitebar.service;

import cn.lili.modules.whitebar.entity.dos.RateSetting;
import cn.lili.modules.whitebar.entity.vo.RateSettingSearchParams;
import cn.lili.modules.whitebar.entity.vo.RateSettingVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 退款日志 业务层
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface RateSettingService extends IService<RateSetting> {

	public IPage<RateSettingVO> rateSettingPage(RateSettingSearchParams rateSettingSearchParams);

	/**
	 * 删除
	 *
	 * @param ids
	 */
	void deleteByIds(List<String> ids);

	// 根据用户身份查询费率
	RateSettingVO findByPartner(String memberId);

}
