package cn.lili.modules.whitebar.mapper;

import cn.lili.modules.whitebar.entity.dos.Commission;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CommissionVO;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 佣金管理
 * 
 * @author 贾送兵
 * @since 2022-02-11 09:25
 */
public interface CommissionMapper extends BaseMapper<Commission> {
	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select * from li_commission c ${ew.customSqlSegment}")
	IPage<CommissionVO> getCommissionList(IPage<CommissionVO> page,
			@Param(Constants.WRAPPER) Wrapper<CommissionVO> queryWrapper);

	@Select(" SELECT c.serial_number,c.transaction_flow as flowPrice,c.transaction_time create_time "
			+ " FROM li_commission c " + " left join li_order o on o.sn = c.serial_number "
			+ " ${ew.customSqlSegment} ")
	IPage<CommissionVO> findCommissionVO(IPage<CommissionVO> page,
			@Param(Constants.WRAPPER) Wrapper<CommissionVO> queryWrapper);

	// 查询列表
	@Select("select c.*,o.flow_price,o.owner from li_commission c "
			+ "left join li_order o on o.sn = c.serial_number ${ew.customSqlSegment}")
	List<CommissionVO> findList(@Param(Constants.WRAPPER) Wrapper<CommissionVO> queryWrapper);

}