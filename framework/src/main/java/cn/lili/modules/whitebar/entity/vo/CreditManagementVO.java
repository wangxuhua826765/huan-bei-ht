package cn.lili.modules.whitebar.entity.vo;

import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import cn.lili.modules.payment.entity.dos.PaymentsCode;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class CreditManagementVO extends CreditManagement {

	@ApiModelProperty(value = "详细地址")
	private String storeAddressDetail;

	@ApiModelProperty(value = "手机号码", required = true)
	@Sensitive(strategy = SensitiveStrategy.PHONE)
	private String mobile;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

}
