package cn.lili.modules.whitebar.entity.vo;

import cn.lili.modules.whitebar.entity.dos.Commission;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * VO 佣金管理
 * 
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class CommissionVO extends Commission {

	@ApiModelProperty(value = "订单金额")
	private Double flowPrice;

	/**
	 * WalletOwnerEnum
	 */
	@ApiModelProperty(value = "订单来源")
	private String owner;
}
