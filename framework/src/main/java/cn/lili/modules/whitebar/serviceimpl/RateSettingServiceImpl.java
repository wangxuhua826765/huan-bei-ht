package cn.lili.modules.whitebar.serviceimpl;

import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.dos.RateSetting;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.entity.vo.RateSettingSearchParams;
import cn.lili.modules.whitebar.entity.vo.RateSettingVO;
import cn.lili.modules.whitebar.mapper.CreditManagementMapper;
import cn.lili.modules.whitebar.mapper.RateSettingMapper;
import cn.lili.modules.whitebar.service.CreditManagementService;
import cn.lili.modules.whitebar.service.RateSettingService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class RateSettingServiceImpl extends ServiceImpl<RateSettingMapper, RateSetting> implements RateSettingService {

	@Override
	public IPage<RateSettingVO> rateSettingPage(RateSettingSearchParams rateSettingSearchParams) {

		return this.baseMapper.getRateSettingList(PageUtil.initPage(rateSettingSearchParams),
				rateSettingSearchParams.queryWrapper());
	}

	@Override
	public void deleteByIds(List<String> ids) {
		this.removeByIds(ids);
	}

	@Override
	public RateSettingVO findByPartner(String memberId) {
		return this.baseMapper.findByPartner(memberId);
	}

}