package cn.lili.modules.whitebar.serviceimpl;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.exception.ServiceException;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.mapper.CreditManagementMapper;
import cn.lili.modules.whitebar.service.CreditManagementService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class CreditManagementServiceImpl extends ServiceImpl<CreditManagementMapper, CreditManagement>
		implements
			CreditManagementService {

	@Override
	public IPage<CreditManagementVO> creditManagementPage(CreditManagementSearchParams creditManagementSearchParams) {

		return this.baseMapper.getCreditManagementList(PageUtil.initPage(creditManagementSearchParams),
				creditManagementSearchParams.queryWrapper());
	}

	@Override
	public void deleteByIds(List<String> ids) {
		this.removeByIds(ids);
	}

}