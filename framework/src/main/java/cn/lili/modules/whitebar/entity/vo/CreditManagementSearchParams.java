package cn.lili.modules.whitebar.entity.vo;

import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询条件
 *
 * @author jiasongbing
 * @since 2022-02-12 19:27:20
 */
@Data
public class CreditManagementSearchParams extends PageVO {

	@ApiModelProperty(value = "商家名称")
	private String name;

	@ApiModelProperty(value = "会员Id")
	private String memberId;

	@ApiModelProperty(value = "审核状态  0 待审核  1 审核通过  2 驳回")
	private String auditStatus;

	@ApiModelProperty(value = "还款状态  0未还  1  已还")
	private String repaymentStatus;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.like(StringUtils.isNotBlank(name), "name", name);
		queryWrapper.eq(StringUtils.isNotBlank(memberId), "a.member_id", memberId);
		queryWrapper.eq(StringUtils.isNotBlank(auditStatus), "a.audit_status", auditStatus);
		queryWrapper.eq(StringUtils.isNotBlank(repaymentStatus), "a.repayment_status", repaymentStatus);
		return queryWrapper;
	}
}
