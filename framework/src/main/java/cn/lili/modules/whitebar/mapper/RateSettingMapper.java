package cn.lili.modules.whitebar.mapper;

import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.dos.RateSetting;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.entity.vo.RateSettingVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 费率管理
 * 
 * @author 贾送兵
 * @since 2022-02-11 09:25
 */
public interface RateSettingMapper extends BaseMapper<RateSetting> {
	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select * from li_rate_setting  ${ew.customSqlSegment}")
	IPage<RateSettingVO> getRateSettingList(IPage<RateSettingVO> page,
			@Param(Constants.WRAPPER) Wrapper<RateSettingVO> queryWrapper);

	// 根据用户身份查询费率
	@Select("select r.* from li_rate_setting r" + " left join li_partner p on p.role_id = r.role_id"
			+ " where p.member_id = #{memberId} "
			+ " and p.partner_state = 0 and p.delete_flag = 0 AND NOW( ) < DATE_FORMAT( end_time, '%Y-%m-%d' )")
	RateSettingVO findByPartner(String memberId);
}