package cn.lili.modules.whitebar.entity.vo;

import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询条件
 *
 * @author jiasongbing
 * @since 2022-02-12 19:27:20
 */
@Data
public class RateSettingSearchParams extends PageVO {

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.like(StringUtils.isNotBlank(roleName), "role_name", roleName);
		return queryWrapper;
	}
}
