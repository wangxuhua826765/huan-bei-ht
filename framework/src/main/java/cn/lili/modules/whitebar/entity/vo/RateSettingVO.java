package cn.lili.modules.whitebar.entity.vo;

import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.dos.RateSetting;
import lombok.Data;

/**
 * VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class RateSettingVO extends RateSetting {

}
