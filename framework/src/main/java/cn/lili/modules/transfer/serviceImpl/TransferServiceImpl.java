package cn.lili.modules.transfer.serviceImpl;

import cn.lili.common.utils.StringUtils;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.order.order.entity.vo.OrderSimpleVO;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.transfer.entity.Transfer;
import cn.lili.modules.transfer.entity.vo.TransferQueryVO;
import cn.lili.modules.transfer.entity.vo.TransferVO;
import cn.lili.modules.transfer.mapper.TransferMapper;
import cn.lili.modules.transfer.service.TransferService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/6/30
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TransferServiceImpl extends ServiceImpl<TransferMapper, Transfer> implements TransferService {

	@Autowired
	private TransferMapper transferMapper;
	@Autowired
	private RegionService regionService;

	@Override
	public Transfer getTransferBySn(String sn) {
		QueryWrapper<Transfer> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("sn", sn);
		queryWrapper.last("limit 1");
		return getOne(queryWrapper);
	}

	@Override
	public Transfer getTransferByTradeSn(String sn) {
		QueryWrapper<Transfer> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("trade_sn", sn);
		queryWrapper.last("limit 1");
		return getOne(queryWrapper);
	}

	@Override
	public IPage<TransferVO> queryByParams(IPage<TransferVO> page, Wrapper<TransferVO> queryWrapper) {
		return transferMapper.queryByParams(page, queryWrapper);
	}

	@Override
	public IPage<TransferVO> queryList(IPage<TransferVO> page, Wrapper<TransferVO> queryWrapper) {
		IPage<TransferVO> transferVOIPage = transferMapper.queryList(page, queryWrapper);
		if (CollectionUtils.isNotEmpty(transferVOIPage.getRecords())) {
			for (TransferVO transferVO : transferVOIPage.getRecords()) {
				if (StringUtils.isNotEmpty(transferVO.getRegionId())) {
					transferVO.setPayeeRegion(
							regionService.getItemAdCodOrName(StringUtils.toString(transferVO.getRegionId())));
					transferVO.setTransferMoney(BigDecimal.valueOf(transferVO.getTransferMoney())
							.setScale(2, RoundingMode.DOWN).doubleValue());

				}
			}
		}
		return transferVOIPage;
	}

	@Override
	public List<TransferVO> queryByParamsList(Wrapper<TransferVO> queryWrapper) {
		return transferMapper.queryByParamsList(queryWrapper);
	}

	@Override
	public IPage<Map> queryByParamsAdmin(Page<Object> initPage, QueryWrapper<TransferQueryVO> queryWrapper) {
		return transferMapper.queryByParamsAdmin(initPage, queryWrapper);
	}

	@Override
	public List<TransferVO> transferList(QueryWrapper<Transfer> eq) {
		return transferMapper.transferList(eq);
	}
}
