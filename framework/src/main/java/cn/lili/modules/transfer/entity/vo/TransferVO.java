package cn.lili.modules.transfer.entity.vo;

import cn.lili.modules.transfer.entity.Transfer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * create by yudan on 2022/7/1
 */
@Data
public class TransferVO extends Transfer {

	@ApiModelProperty(value = "类型")
	private String type;

	@ApiModelProperty(value = "类型")
	private String transferMoneyStr;

	@ApiModelProperty(value = "金卡/银卡")
	private String cardName;

	@ApiModelProperty(value = "头像")
	private String face;

	@ApiModelProperty(value = "收款人区域")
	private String payeeRegion;
	private String regionId;

	@ApiModelProperty(value = "收款人级别")
	private String payeeGradeName;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String endTime;

	@ApiModelProperty(value = "用户id")
	private String memberId;
}
