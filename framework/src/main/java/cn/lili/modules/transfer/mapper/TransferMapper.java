package cn.lili.modules.transfer.mapper;

import cn.lili.modules.transfer.entity.Transfer;
import cn.lili.modules.transfer.entity.vo.TransferQueryVO;
import cn.lili.modules.transfer.entity.vo.TransferVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/6/30
 */
public interface TransferMapper extends BaseMapper<Transfer> {

	// 查询列表
	@Select("SELECT * FROM li_transfer ${ew.customSqlSegment}")
	IPage<TransferVO> queryByParams(IPage<TransferVO> page, @Param(Constants.WRAPPER) Wrapper<TransferVO> queryWrapper);

	@Select("SELECT m.location as regionId,mc.name as payeeGradeName,t.* " + " FROM li_transfer t"
			+ " left join li_member m on m.id = t.payee"
			+ " left join li_grade_level g on g.member_id = t.payee and g.delete_flag = 0"
			+ " left join li_membership_card mc on mc.id = g.grade_id" + " ${ew.customSqlSegment}")
	IPage<TransferVO> queryList(IPage<TransferVO> page, @Param(Constants.WRAPPER) Wrapper<TransferVO> queryWrapper);

	@Select("SELECT * FROM li_transfer ${ew.customSqlSegment}")
	List<TransferVO> queryByParamsList(@Param(Constants.WRAPPER) Wrapper<TransferVO> queryWrapper);

	@Select("  SELECT IFNULL ( lm1.nick_name, '' ) AS zNickName, IFNULL( lm1.real_name, '' ) AS zRealName, IFNULL( lm1.id_card_number, '' ) AS zIdCard, IFNULL( lm1.mobile, '' ) AS zMobile, IFNULL( lmc1.NAME, '' ) AS zLevelName, IFNULL( lro1.NAME, '' ) AS zRoleName, IFNULL( lt.sn, '' ) AS sn, RoundDown ( IFNULL( lt.transfer_money, 0 ), 2 ) AS transferMoney, IFNULL( DATE_FORMAT ( lt.payment_time, '%Y-%m-%d %H:%i:%S' ), '' ) AS paymentTime, RoundDown ( IFNULL( lt.commission, 0 ), 2 ) AS commission, IFNULL( lm2.nick_name, '' ) sNickName, IFNULL( lm2.mobile, '' ) sMobile, IFNULL( lmc2.NAME, '' ) AS sLevelName, IFNULL( lro2.NAME, '' ) AS sRoleName, IFNULL( lm1.location, '' ) AS zLocation, IFNULL( lm2.location, '' ) AS slocation, ( CASE  lt.payment_wallet  WHEN 'SALE' THEN '销售钱包' WHEN 'PROMOTE' THEN '推广钱包' WHEN 'RECHARGE' THEN '充值钱包' WHEN 'PROMOTE_FW' THEN '推广钱包-服务费' WHEN 'PROMOTE_HY' THEN '推广钱包-会员费' ELSE '' END ) AS walletName  FROM  li_transfer lt  LEFT JOIN li_member lm1 ON lm1.id = lt.payer  LEFT JOIN li_grade_level lgl1 ON lgl1.member_id = lm1.id  LEFT JOIN li_membership_card lmc1 ON lmc1.id = lgl1.grade_id  LEFT JOIN li_member lm2 ON lm2.id = lt.payee  LEFT JOIN li_grade_level lgl2 ON lgl2.member_id = lm2.id  LEFT JOIN li_membership_card lmc2 ON lmc2.id = lgl2.grade_id  LEFT JOIN li_partner lp1 ON lp1.member_id = lm1.id  LEFT JOIN li_role lro1 ON lro1.id = lp1.role_id  LEFT JOIN li_partner lp2 ON lp2.member_id = lm2.id  LEFT JOIN li_role lro2 ON lro2.id = lp2.role_id ${ew.customSqlSegment}")
	IPage<Map> queryByParamsAdmin(Page<Object> initPage,
			@Param(Constants.WRAPPER) QueryWrapper<TransferQueryVO> queryWrapper);

	@Select(" SELECT t.*,m.nick_name,m.mobile,mc.name as cardName,m.face \n" + " FROM li_transfer t\n"
			+ " LEFT JOIN li_member m ON m.id = t.payee\n"
			+ " LEFT JOIN li_grade_level l ON l.member_id = m.id and l.delete_flag = 0\n"
			+ " LEFT JOIN li_membership_card mc ON mc.id = l.grade_id and mc.`level` in (1, 2)")
	List<TransferVO> transferList(@Param(Constants.WRAPPER) QueryWrapper<Transfer> eq);
}
