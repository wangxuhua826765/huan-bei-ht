package cn.lili.modules.transfer.service;

import cn.lili.modules.order.order.entity.dto.OrderSearchParams;
import cn.lili.modules.transfer.entity.Transfer;
import cn.lili.modules.transfer.entity.vo.TransferQueryVO;
import cn.lili.modules.transfer.entity.vo.TransferVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/6/30
 */
public interface TransferService extends IService<Transfer> {

	Transfer getTransferBySn(String sn);

	Transfer getTransferByTradeSn(String sn);

	// 查询转账列表
	IPage<TransferVO> queryByParams(IPage<TransferVO> page, Wrapper<TransferVO> queryWrapper);

	IPage<TransferVO> queryList(IPage<TransferVO> page, Wrapper<TransferVO> queryWrapper);

	List<TransferVO> queryByParamsList(Wrapper<TransferVO> queryWrapper);

	// 平台 查看 转账列表
	IPage<Map> queryByParamsAdmin(Page<Object> initPage, QueryWrapper<TransferQueryVO> queryWrapper);

	List<TransferVO> transferList(QueryWrapper<Transfer> eq);
}
