package cn.lili.modules.transfer.entity.vo;

import cn.lili.modules.transfer.entity.Transfer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by yudan on 2022/7/1
 *
 * 转账记录查询条件
 */
@Data
public class TransferQueryVO extends Transfer {

	@ApiModelProperty(value = "类型")
	private String type;

	@ApiModelProperty(value = "类型")
	private String transferMoneyStr;
	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号")
	private String mobile;
	/**
	 * 充值时间
	 */
	@ApiModelProperty(value = "充值开始时间")
	private String startDate;

	/**
	 * 充值时间
	 */
	@ApiModelProperty(value = "充值结束时间")
	private String endDate;

	/**
	 * 区域id
	 */
	@ApiModelProperty(value = "区域id")
	private String location;

	/**
	 * 级别
	 */
	@ApiModelProperty(value = "级别 0:普 /1:银 /2:金")
	private String level;

	/**
	 * 身份名称
	 */
	@ApiModelProperty(value = "身份名称")
	private String roleName;

}
