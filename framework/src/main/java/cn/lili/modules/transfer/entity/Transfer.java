package cn.lili.modules.transfer.entity;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/6/30
 */
@Data
@TableName("li_transfer")
@ApiModel(value = "转账")
@NoArgsConstructor
public class Transfer extends BaseEntity {

	@ApiModelProperty(value = "收款人id")
	private String payee;

	@ApiModelProperty(value = "收款人手机号")
	private String payeePhone;

	@ApiModelProperty(value = "收款人姓名")
	private String payeeName;

	@ApiModelProperty(value = "收款人会员身份")
	private String payeeCard;

	@ApiModelProperty(value = "收款人头像")
	private String payeeImg;

	@ApiModelProperty(value = "转账金额")
	private Double transferMoney;

	@ApiModelProperty(value = "手续费")
	private Double commission;

	@ApiModelProperty(value = "付款钱包")
	private String paymentWallet;

	@ApiModelProperty(value = "付款人id")
	private String payer;

	@ApiModelProperty(value = "付款人手机号")
	private String payerPhone;

	@ApiModelProperty(value = "流水号")
	private String sn;

	@ApiModelProperty(value = "交易编号 关联Trade")
	private String tradeSn;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "转账时间")
	private Date paymentTime;

	@ApiModelProperty(value = "状态  success 转账成功  fail 转账失败")
	private String state;

	@ApiModelProperty(value = "类型 端口费：PORT_FEE")
	private String type;

}
