package cn.lili.modules.audit.entity.dto;

import cn.lili.common.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class AuditPartnerDto extends PageVO {

	@ApiModelProperty(value = "开始时间")
	private String startTime;
	@ApiModelProperty(value = "结束时间")
	private String endTime;
	@ApiModelProperty(value = "会员手机号")
	private String mobile;
	@ApiModelProperty(value = "省市区地址Id")
	private String address;
	@ApiModelProperty(value = "审核状态1：待审核2审核通过3审核驳回4已撤回")
	private String auditStatus;
}
