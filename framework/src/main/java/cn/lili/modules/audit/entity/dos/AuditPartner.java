package cn.lili.modules.audit.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("li_audit_partner")
@ApiModel(value = "撤销合伙人审核表")
@NoArgsConstructor
public class AuditPartner extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "审核状态1：待审核2审核通过3审核驳回4已撤回")
	private int auditStatus;
	@ApiModelProperty(value = "申请时间")
	private Date applyDate;
	@ApiModelProperty(value = "审核时间")
	private Date auditDate;
	@ApiModelProperty(value = "撤销合伙人说明")
	private String revokeMsg;
	@ApiModelProperty(value = "审核描述")
	private String auditMsg;
	@ApiModelProperty(value = "会员id")
	private String memberId;
	@ApiModelProperty(value = "合伙人Id")
	private String partnerId;
	@ApiModelProperty(value = "审核人Id")
	private String auditor;
	@ApiModelProperty(value = "退回金额")
	private BigDecimal backAmount;
	@ApiModelProperty(value = "收款人")
	private String backUser;
	@ApiModelProperty(value = "收款人手机")
	private String backMobile;
}
