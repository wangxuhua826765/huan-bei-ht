package cn.lili.modules.audit.service;

import cn.lili.modules.audit.entity.dos.AuditPartner;
import cn.lili.modules.audit.entity.dto.AuditDto;
import cn.lili.modules.audit.entity.dto.AuditPartnerDto;
import cn.lili.modules.audit.entity.dto.LowListDto;
import cn.lili.modules.audit.entity.vo.AuditPartnerVo;
import cn.lili.modules.goods.entity.dos.Brand;
import cn.lili.modules.goods.entity.dto.BrandPageDTO;
import cn.lili.modules.member.entity.dos.Member;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AuditPartnerService extends IService<AuditPartner> {

	IPage<AuditPartnerVo> getByPage(AuditPartnerDto partnerDto);

	Boolean audit(AuditDto auditDto);

	Boolean getBackMsg(AuditDto auditDto);

	IPage<LowListDto> getLowList(String memberId);

}
