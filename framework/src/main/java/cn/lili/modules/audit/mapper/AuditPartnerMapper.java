package cn.lili.modules.audit.mapper;

import cn.lili.modules.audit.entity.dos.AuditPartner;
import cn.lili.modules.audit.entity.dto.LowListDto;
import cn.lili.modules.audit.entity.vo.AuditPartnerVo;
import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.vo.MemberVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AuditPartnerMapper extends BaseMapper<AuditPartner> {
	@Select("SELECT * FROM li_audit_partner a WHERE a.member_id = ${memberId} and a.delete_flag = 0 and audit_status=${auditStatus}")
	AuditPartner getAllByMemberIdAndAuditStatusEquals(String memberId, int auditStatus);

	@Select("SELECT * FROM li_audit_partner a WHERE a.member_id = ${memberId} and a.delete_flag = 0 ORDER BY create_time desc LIMIT 1")
	AuditPartner verifyAllByMemberId(String memberId);

	@Select("SELECT a.id auditId,e.id memberId, a.create_time createTime, a.revoke_msg revokeMsg, a.audit_status auditStatus, e.mobile mobile, e.username username, e.id_card_number idCardNumber, "
			+ " e.nick_name nickName,e.real_name realName, e.location region,f.region as partnerRegion, f.partner_type partnerType,r.role_name partnerName, c.RECHARGE recharge, d.PROMOTE promote, b.recharge_money rechargeMoney,a.back_amount backAmount,a.back_user backUser,a.back_mobile backMobile "
			+ " FROM li_audit_partner a" + " LEFT JOIN li_member e ON a.member_id = e.id"
			+ " LEFT JOIN li_partner f ON f.member_id = a.member_id and f.delete_flag = 0 and f.partner_state = 0"
			+ " left join li_role r on r.id = f.role_id"
			+ " LEFT JOIN ( SELECT member_wallet AS RECHARGE, member_id FROM li_member_wallet WHERE `owner` = 'RECHARGE' ) c ON c.member_id = a.member_id"
			+ " LEFT JOIN ( SELECT member_wallet AS PROMOTE, member_id FROM li_member_wallet WHERE `owner` = 'PROMOTE' ) d ON a.member_id = d.member_id"
			+ " LEFT JOIN ( SELECT recharge_money, member_id FROM"
			+ " ( SELECT recharge_money, member_id FROM li_recharge WHERE recharge_type = '5' ORDER BY create_time DESC ) lr "
			+ " GROUP BY lr.member_id) b ON a.member_id = b.member_id  " + " ${ew.customSqlSegment}")
	IPage<AuditPartnerVo> getByPage(IPage<AuditPartnerVo> page,
			@Param(Constants.WRAPPER) Wrapper<AuditPartner> queryWrapper);

	@Select(" SELECT aa.mobile mobile,aa.nick_name nickName,d.`name` levelName,aa.location region FROM li_member a"
			+ " LEFT JOIN li_extension c on a.promotion_code = c.extension"
			+ " LEFT JOIN li_member aa ON aa.id = c.member_id"
			+ " LEFT JOIN li_membership_card d on d.id = aa.grade_id " + " ${ew.customSqlSegment}")
	IPage<LowListDto> getLowListPage(IPage<LowListDto> page,
			@Param(Constants.WRAPPER) Wrapper<AuditPartner> queryWrapper);

	@Select(" SELECT aa.mobile mobile,aa.nick_name nickName,d.`name` levelName,aa.location region FROM li_member a"
			+ " LEFT JOIN li_extension c on a.promotion_code = c.extension"
			+ " LEFT JOIN li_member aa ON aa.id = c.member_id"
			+ " LEFT JOIN li_membership_card d on d.id = aa.grade_id " + " ${ew.customSqlSegment}")
	List<LowListDto> getLowList(@Param(Constants.WRAPPER) Wrapper<AuditPartner> queryWrapper);

}
