package cn.lili.modules.audit.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LowListDto {

	private String mobile;
	@ApiModelProperty(value = "昵称")
	private String nickName;
	@ApiModelProperty(value = "会员等级")
	private String levelName;
	@ApiModelProperty(value = "区域")
	private String region;
}
