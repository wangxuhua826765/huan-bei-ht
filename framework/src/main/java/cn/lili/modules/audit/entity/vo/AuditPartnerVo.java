package cn.lili.modules.audit.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AuditPartnerVo {

	private String auditId;
	@ApiModelProperty(value = "会员Id")
	private String memberId;
	@ApiModelProperty(value = "创建时间")
	private String createTime;
	@ApiModelProperty(value = "注销原因")
	private String revokeMsg;
	@ApiModelProperty(value = "审核状态")
	private String auditStatus;
	@ApiModelProperty(value = "手机号")
	private String mobile;
	@ApiModelProperty(value = "会员名称")
	private String username;
	@ApiModelProperty(value = "会员身份证号")
	private String idCardNumber;
	@ApiModelProperty(value = "会员昵称")
	private String nickName;
	@ApiModelProperty(value = "真实姓名")
	private String realName;
	@ApiModelProperty(value = "地址")
	private String region;
	@ApiModelProperty(value = "合伙人类型   0 非合伙人  1 事业合伙人  2 天使合伙人  3 普通合伙人  4 城市代理商")
	private String partnerType;
	@ApiModelProperty(value = "合伙人名称")
	private String partnerName;
	@ApiModelProperty(value = "充值钱余额")
	private String recharge;
	@ApiModelProperty(value = "推广钱包余额")
	private String promote;
	@ApiModelProperty(value = "已缴端口费")
	private String rechargeMoney;
	@ApiModelProperty(value = "会员总数")
	private int memberNum;
	@ApiModelProperty(value = "退回金额")
	private BigDecimal backAmount;
	@ApiModelProperty(value = "收款人")
	private String backUser;
	@ApiModelProperty(value = "收款人手机")
	private String backMobile;
	@ApiModelProperty(value = "合伙人区域")
	private String partnerRegion;

}
