package cn.lili.modules.audit.enums;

import java.util.EnumSet;

public enum AuditPartnerStatus {

	AUDITING(1, "待审核"), PASS(2, "审核通过"), REFUSE(3, "审核拒绝"), REVOKE(4, "已撤回"),;

	private int code;
	private String value;

	AuditPartnerStatus(int code, String value) {
		this.code = code;
		this.value = value;
	}

	public static AuditPartnerStatus getStatus(int code) {
		for (AuditPartnerStatus status : AuditPartnerStatus.values()) {
			if (status.getCode() == code) {
				return status;
			}
		}
		throw new IllegalArgumentException("不支持的类型");
	}

	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public static EnumSet<AuditPartnerStatus> LIST = EnumSet.of(PASS, REFUSE);

}
