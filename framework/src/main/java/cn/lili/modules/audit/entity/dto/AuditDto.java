package cn.lili.modules.audit.entity.dto;

import cn.lili.common.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AuditDto extends PageVO {

	private String auditId;
	private String memberId;
	@ApiModelProperty(value = "审核描述")
	private String auditMsg;
	@ApiModelProperty(value = "审核状态1：待审核2审核通过3审核驳回4已撤回")
	private int auditStatus;
	@ApiModelProperty(value = "退回金额")
	private BigDecimal backAmount;
	@ApiModelProperty(value = "收款人")
	private String backUser;
	@ApiModelProperty(value = "收款人手机")
	private String backMobile;
}
