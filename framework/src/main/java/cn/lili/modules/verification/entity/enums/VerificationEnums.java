package cn.lili.modules.verification.entity.enums;

/**
 * VerificationEnums
 *
 * @author Chopper
 * @version v1.0 2020-12-22 19:11
 */
public enum VerificationEnums {

	/**
	 * 登录 注册 找回用户 修改密码 银行卡验证 支付钱包密码
	 */
	LOGIN, REGISTER, FIND_USER, UPDATE_PASSWORD, BANK_VERIFY, WALLET_PASSWORD;
}
