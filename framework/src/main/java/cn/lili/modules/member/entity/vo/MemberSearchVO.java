package cn.lili.modules.member.entity.vo;

import cn.lili.common.enums.SwitchEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 会员搜索VO
 *
 * @author Bulbasaur
 * @since 2020/12/15 10:48
 */
@Data
public class MemberSearchVO {

	@ApiModelProperty(value = "id")
	private String id;
	/*
	 * 合伙人Id
	 */
	@ApiModelProperty(value = "partnerId")
	private String partnerId;
	/*
	 * 合伙人Id
	 */
	@ApiModelProperty(value = "memberId")
	private String memberId;

	@ApiModelProperty(value = "用户名")
	private String username;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@ApiModelProperty(value = "用户手机号码")
	private String mobile;

	@ApiModelProperty(value = "查询合伙人类型  / STORE 商家 / AGENT 代理商 / ANGEL_PARTNER 天使合伙人 / BUSINESS_PARTNER 事业合伙人 / ORDINARY_USERS 推广员 / 普通用户")
	private String roleType;

	@ApiModelProperty(value = "用户类型  1 商家  2 小程序用户")
	private Integer type;

	@ApiModelProperty(value = "合伙人类型   0 非合伙人  1 事业合伙人  2 天使合伙人  3 推广员  4 小程序合伙人(普通+天使)")
	private Integer partnerType;

	@ApiModelProperty(value = "合伙人状态  0 启用   1  禁用")
	private Integer partnerState;
	/**
	 * @see SwitchEnum
	 */
	@ApiModelProperty(value = "会员状态 禁用 0 可用 1 不可用")
	private String disabled;

	@ApiModelProperty(value = "推广码")
	private String extension;

	@ApiModelProperty(value = "推广人姓名")
	private String nameExte;

	@ApiModelProperty(value = "推广人电话")
	private String mobileExte;

	@ApiModelProperty(value = "合伙人区域")
	private String adcode;
	@ApiModelProperty(value = "区域")
	private String region;
	@ApiModelProperty(value = "区域id")
	private String regionId;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "开始时间")
	private Date beginTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	/**
	 * PartnerStatusEnum
	 */
	@ApiModelProperty(value = "审核状态")
	private String auditStates;

	@ApiModelProperty(value = "会员等级")
	private String level;

	@ApiModelProperty(value = "店铺名")
	private String storeName;

	@ApiModelProperty(value = "店铺位置")
	private String storeAddressPath;
}
