package cn.lili.modules.member.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.vo.ExtensionVO;
import cn.lili.modules.member.entity.vo.MemberVO;
import cn.lili.modules.member.entity.vo.PartnerVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 风控管理
 */
public interface EextensionService extends IService<Extension> {

	/**
	 * 获推广管理分页
	 *
	 * @param page
	 *            分页
	 * @return 推广管理
	 */
	IPage<ExtensionVO> getExtensionPage(ExtensionVO extension, PageVO page);

	IPage<ExtensionVO> getByPageCode(ExtensionVO extension, PageVO page);

	/**
	 * 查询是否有重复的推广码
	 *
	 * @return 推广管理
	 */
	Boolean queryExtension(String promotionCode);

	/*
	 * *后台-修改会推广信息
	 *
	 * @param
	 * 
	 * @return 会员
	 */

	Extension updateExtension(Extension extension);

	/**
	 * 后台-添加推广管理
	 *
	 * @param extension
	 *            推广管理
	 * @return
	 */
	Extension addExtension(Extension extension);

	/**
	 * 删除推广
	 *
	 * @param id
	 *            收货地址ID
	 * @return 操作状态
	 */
	boolean removeExtension(String id);

	// 根据会员id查看推广人信息
	MemberVO findByMemberId(String memberId);

	// 根据推广码查看推广人信息
	MemberVO findByPromotionCode(String promotionCode);

	// 生成推广码
	String getExtension();

	/**
	 * 临时推广码转正式
	 * 
	 * @param memberId
	 * @return
	 */
	Boolean tempExToEx(String memberId, boolean isNull);

	Boolean bindTempEx(String memberId, String exCode);

	public Boolean bindEx(String memberId, String exCode);

	boolean updateByExtensionCode(String extensionCode);

	List<Extension> getUnbindCount(String extension);
	List<Extension> updateUnbind(String extension);

	PartnerVO getCertificate(String memberId);
}