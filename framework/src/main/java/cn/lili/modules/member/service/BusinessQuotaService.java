package cn.lili.modules.member.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.BusinessQuota;
import cn.lili.modules.member.entity.vo.GradeVO;
import cn.lili.modules.order.order.entity.dos.Order;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 风控管理
 */
public interface BusinessQuotaService extends IService<BusinessQuota> {

	/**
	 * 获取风控管理分页
	 *
	 * @param page
	 *            分页
	 * @return 风控管理
	 */
	IPage<BusinessQuota> getBusinessQuotaPage(BusinessQuota businessQuota, PageVO page);

	/**
	 * 获取风控管详情接口
	 *
	 * @return 风控管理
	 */
	IPage<Order> queryBusinessOrder(String memberId, String storeId, String testingStartTime, String testingEndTime,
			PageVO page);

	/**
	 * 是否限额
	 */
	BusinessQuota limitOrNot(BusinessQuota businessQuota);
}