package cn.lili.modules.member.entity.vo;

import cn.lili.common.utils.DateUtil;
import cn.lili.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/4/22
 */
@Data
public class FinanceAdminVO {
	@ApiModelProperty(value = "用户id")
	private String memberId;
	@ApiModelProperty(value = "区域id")
	private String regionId;
	@ApiModelProperty(value = "合伙人区域")
	private String adcode;
	@ApiModelProperty(value = "电话")
	private String mobile;
	@ApiModelProperty(value = "推荐人手机号")
	private String tjMobile;
	@ApiModelProperty(value = "等级")
	private String level;
	@ApiModelProperty(value = "店铺名称")
	private String storeName;
	@ApiModelProperty(value = "日期")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "开始时间")
	private Date startTime;
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM")
	@DateTimeFormat(pattern = "yyyy-MM")
	@ApiModelProperty(value = "查询时间")
	private Date searchTime;

	/**
	 * @see OrderStatusEnum
	 */
	@ApiModelProperty(value = "订单状态")
	private String orderStatus;

	@ApiModelProperty(value = "天使店铺")
	private String tsStoreName;

	public <T> QueryWrapper<T> queryWrapperNow() {
		QueryWrapper queryWrapperNow = new QueryWrapper<>();
		queryWrapperNow.eq(StringUtils.isNotEmpty(mobile), "a.mobile", mobile);
		queryWrapperNow.eq(StringUtils.isNotEmpty(tjMobile), "tj_mobile", tjMobile);
		queryWrapperNow.eq(StringUtils.isNotEmpty(level), "level", level);
		queryWrapperNow.like(StringUtils.isNotEmpty(storeName), "store_name", storeName);
		Date date = new Date();
		String dates = DateUtil.toString(date, DateUtil.STANDARD_DATE_FORMAT);
		queryWrapperNow.eq(StringUtils.isNotEmpty(dates), "DATE(a.create_time)", dates);
		return queryWrapperNow;
	}

	public <T> QueryWrapper<T> queryWrapperStart() {
		QueryWrapper queryWrapperStart = new QueryWrapper<>();
		queryWrapperStart.eq(StringUtils.isNotEmpty(mobile), "a.mobile", mobile);
		queryWrapperStart.eq(StringUtils.isNotEmpty(tjMobile), "tj_mobile", tjMobile);
		queryWrapperStart.eq(StringUtils.isNotEmpty(level), "level", level);
		queryWrapperStart.like(StringUtils.isNotEmpty(storeName), "store_name", storeName);
		String dates = DateUtil.toString(startTime, DateUtil.STANDARD_DATE_FORMAT);
		queryWrapperStart.eq(StringUtils.isNotEmpty(dates), "DATE(a.create_time)", dates);
		return queryWrapperStart;
	}

	public <T> QueryWrapper<T> queryWrapperEnd() {
		QueryWrapper queryWrapperEnd = new QueryWrapper<>();
		queryWrapperEnd.eq(StringUtils.isNotEmpty(mobile), "a.mobile", mobile);
		queryWrapperEnd.eq(StringUtils.isNotEmpty(tjMobile), "tj_mobile", tjMobile);
		queryWrapperEnd.eq(StringUtils.isNotEmpty(level), "level", level);
		queryWrapperEnd.like(StringUtils.isNotEmpty(storeName), "store_name", storeName);
		String dates = DateUtil.toString(endTime, DateUtil.STANDARD_DATE_FORMAT);
		queryWrapperEnd.eq(StringUtils.isNotEmpty(dates), "DATE(a.create_time)", dates);
		return queryWrapperEnd;
	}

	public <T> QueryWrapper<T> queryWrapperRegionId() {
		QueryWrapper queryWrapperRegionId = new QueryWrapper<>();
		Date date = new Date();
		String dates = DateUtil.toString(date, DateUtil.STANDARD_DATE_FORMAT);
		queryWrapperRegionId.eq(StringUtils.isNotEmpty(dates), "DATE(a.create_time)", dates);
		return queryWrapperRegionId;
	}

}
