package cn.lili.modules.member.entity.vo;

import cn.lili.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by yudan on 2022/4/22
 */
@Data
public class MemberReportSearchVO {

	@ApiModelProperty(value = "区域id")
	private String regionId;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.like(StringUtils.isNotEmpty(regionId), "region_id", regionId);
		return queryWrapper;
	}
}
