package cn.lili.modules.member.serviceimpl;

import cn.lili.modules.member.mapper.AccessTokenMapper;
import cn.lili.modules.member.service.AccessTokenService;
import cn.lili.modules.member.token.AccessToken;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * create by yudan on 2022/9/23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AccessTokenServiceImpl extends ServiceImpl<AccessTokenMapper, AccessToken> implements AccessTokenService {

	@Override
	public Boolean removeByAccessToken(String accessToken) {
		return this.baseMapper.removeByAccessToken(accessToken);
	}
}
