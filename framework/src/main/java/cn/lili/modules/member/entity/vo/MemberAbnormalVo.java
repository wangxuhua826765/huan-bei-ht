package cn.lili.modules.member.entity.vo;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 异常会员
 */
@Data
@NoArgsConstructor
public class MemberAbnormalVo extends BaseEntity {

	private String memberId;

	@ApiModelProperty(value = "会员昵称")
	private String nickName;

	@ApiModelProperty(value = "用户名称")
	private String username;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "会员等级")
	private String gradeIdName;

	@ApiModelProperty(value = "地区")
	private String locationName;

	@ApiModelProperty(value = "推广人手机号")
	private String mobileExte;

	@ApiModelProperty(value = "会员到期时间")
	private String memberEndTime;

	@ApiModelProperty(value = "充值钱包余额")
	private String rechargeMoney;

	@ApiModelProperty(value = "禁用时间")
	private String disableTime;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "注册时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date createTime;

	@ApiModelProperty(value = "地区定位")
	private String location;

	@ApiModelProperty(value = "开始时间")
	private String startDate;

	@ApiModelProperty(value = "结束时间")
	private String endDate;

	@ApiModelProperty(value = "用户类型  1 商家  2 小程序用户")
	private Integer type;

	@ApiModelProperty(value = "区域id")
	private String regionId;

	@ApiModelProperty(value = "充值会员等级（充值会员等级（0 普通会员  1  银卡会员   2  金卡会员  9999 商家会员））")
	private String level;
}
