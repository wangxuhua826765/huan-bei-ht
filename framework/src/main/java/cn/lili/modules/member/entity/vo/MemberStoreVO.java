package cn.lili.modules.member.entity.vo;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.enums.StoreStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author paulG
 * @since 2021/11/8
 **/
@Data
public class MemberStoreVO implements Serializable {

	private static final long serialVersionUID = 1810890757303309436L;

	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@ApiModelProperty(value = "详细地址")
	private String storeAddressDetail;

	@ApiModelProperty(value = "手机号码", required = true)
	@Sensitive(strategy = SensitiveStrategy.PHONE)
	private String mobile;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "会员Id")
	private String memberId;

	@ApiModelProperty(value = "店铺ID")
	private String storeId;

}
