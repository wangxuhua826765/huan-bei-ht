package cn.lili.modules.member.entity.vo;

import cn.lili.modules.member.entity.dos.GradeLevel;
import cn.lili.modules.member.entity.dos.Partner;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/3/2
 */
@Data
@NoArgsConstructor
public class GradeLevelVO extends GradeLevel {

}
