package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.MemberReceipt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * 会员发票数据层
 *
 * @author Chopper
 * @since 2021-03-29 14:10:16
 */
public interface MemberReceiptMapper extends BaseMapper<MemberReceipt> {

	@Select("SELECT * FROM li_member_receipt where member_id = #{memberId} and delete_flag = 0")
	MemberReceipt getByMemberId(String memberId);

	@Select("UPDATE li_member_receipt SET delete_flag = 1 where member_id = #{memberId} ")
	void delByMemberId(String memberId);

}