package cn.lili.modules.member.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/6/22
 */
@Data
@TableName("li_foot_store")
@ApiModel(value = "店铺浏览历史")
@NoArgsConstructor
@AllArgsConstructor
public class FootStore extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "会员ID")
	private String memberId;

	@ApiModelProperty(value = "商品ID")
	private String storeId;

}
