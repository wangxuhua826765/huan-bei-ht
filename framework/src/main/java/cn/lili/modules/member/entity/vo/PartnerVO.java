package cn.lili.modules.member.entity.vo;

import cn.lili.modules.member.entity.dos.Partner;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/2/18
 */
@Data
@NoArgsConstructor
public class PartnerVO extends Partner {

	/**
	 * 以下为用户信息表字段
	 */
	@ApiModelProperty(value = "会员用户名")
	private String username;

	@ApiModelProperty(value = "推广码")
	private String promotionCode;

	@ApiModelProperty(value = "推广太阳码")
	private String promotionImg;

	@ApiModelProperty(value = "手机号码")
	private String mobile;

	@ApiModelProperty(value = "推广人数量")
	private Integer quantity;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "头像")
	private String face;

	@ApiModelProperty(value = "地址")
	private String region;

	@ApiModelProperty(value = "推广人")
	private String extension;

	@ApiModelProperty(value = "证书编码")
	private String prefixCode;

	@ApiModelProperty(value = "合伙人区域")
	private String adcode;
}
