package cn.lili.modules.member.serviceimpl;

import cn.lili.cache.Cache;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.BusinessQuota;
import cn.lili.modules.member.mapper.BusinessQuotaMapper;
import cn.lili.modules.member.service.BusinessQuotaService;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 风控管理接口业务层实现
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BusinessQuotaServiceImpl extends ServiceImpl<BusinessQuotaMapper, BusinessQuota>
		implements
			BusinessQuotaService {

	/**
	 * 缓存
	 */
	@Autowired
	private Cache cache;

	public IPage<BusinessQuota> getBusinessQuotaPage(BusinessQuota businessQuota, PageVO page) {
		QueryWrapper<BusinessQuota> queryWrapper = Wrappers.query();
		// queryWrapper.eq("delete_flag", 0);
		// queryWrapper.orderByDesc("create_time");
		return this.baseMapper.pageByMemberVO(PageUtil.initPage(page), queryWrapper);
	}

	/**
	 * @param memberId
	 *            用户id
	 * @param storeId
	 *            店铺id
	 * @param testingStartTime
	 *            开始时间
	 * @param testingEndTime
	 *            结束时间
	 * @return
	 */
	public IPage<Order> queryBusinessOrder(String memberId, String storeId, String testingStartTime,
			String testingEndTime, PageVO page) {
		QueryWrapper<Order> queryWrapper = Wrappers.query();
		queryWrapper.eq("member_id", memberId);
		queryWrapper.eq("store_id", storeId);
		queryWrapper.ge("payment_time", testingStartTime);
		queryWrapper.le("payment_time", testingEndTime);
		return this.baseMapper.queryBusinessOrder(PageUtil.initPage(page), queryWrapper);
	}

	/**
	 * 修改
	 *
	 * @param businessQuota
	 * @return
	 */
	public BusinessQuota limitOrNot(BusinessQuota businessQuota) {
		// 判断是否用户登录并且会员ID为当前登录会员ID
		AuthUser tokenUser = UserContext.getCurrentUser();
		if (tokenUser == null) {
			throw new ServiceException(ResultCode.USER_NOT_LOGIN);
		}
		this.updateById(businessQuota);
		return businessQuota;
	}

}