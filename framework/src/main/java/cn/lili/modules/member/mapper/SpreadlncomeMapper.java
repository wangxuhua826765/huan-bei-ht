package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.dos.Spreadlncome;
import cn.lili.modules.member.entity.vo.PartnerVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * create by yudan on 2022/2/18
 */
public interface SpreadlncomeMapper extends BaseMapper<Spreadlncome> {

}
