package cn.lili.modules.member.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PaymentPwdVo {

	@ApiModelProperty(value = "是否使用密码  true 是  false 否")
	private int noSecret;

	@ApiModelProperty(value = "是否已设置密码  true 是  false 否")
	private Boolean setPwd;
}
