package cn.lili.modules.member.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 浏览历史
 *
 * @author Chopper
 * @since 2020/11/17 7:22 下午
 */
@Data
@TableName("li_recharge_flow")
@ApiModel(value = "充值流水")
@NoArgsConstructor
@AllArgsConstructor
public class RechargeFow extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "会员ID")
	private String memberId;

	@ApiModelProperty(value = "订单流水号")
	private String sn;

	@ApiModelProperty(value = "充值金額")
	private Double money;

	@ApiModelProperty(value = "焕呗数量")
	private Double huanBei;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "充值时间", hidden = true)
	private Date addTime;

	@ApiModelProperty(value = "1提现2充值年费会员3充值会员4普通充值 5 充值合伙人")
	private String rechargeType;

	@ApiModelProperty(value = "费率")
	private Double fee;

	@ApiModelProperty(value = "可提现现金额")
	private Double canMoney;

	@ApiModelProperty(value = "可提现焕呗额")
	private Double canHuanBei;

	@ApiModelProperty(value = "可提现金额是否提完,-1已提完  0未提现完")
	private String realization;

}