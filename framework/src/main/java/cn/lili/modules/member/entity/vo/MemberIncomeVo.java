package cn.lili.modules.member.entity.vo;

import cn.lili.mybatis.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 会员详情、收入
 */
@Data
@NoArgsConstructor
public class MemberIncomeVo extends BaseEntity {

	private String memberId;
	@ApiModelProperty(value = "会员昵称")
	private String nickName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "会员等级")
	private String gradeIdName;

	@ApiModelProperty(value = "地区")
	private String locationName;

	@ApiModelProperty(value = "第三方付款流水号")
	private String receivableNo;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "收入时间")
	private Date incomeTime;

	@ApiModelProperty(value = "收入类型")
	private String incomeType;

	@ApiModelProperty(value = "收入类型")
	private String incomeTypeName;

	@ApiModelProperty(value = "焕呗数量")
	private Double yiBei;

	@ApiModelProperty(value = "现金额")
	private Double cash;

	@ApiModelProperty(value = "开始时间")
	private String startDate;

	@ApiModelProperty(value = "结束时间")
	private String endDate;

	@ApiModelProperty(value = "地区定位")
	private String location;

	@ApiModelProperty(value = "充值状态 充值：CHARGE；转账：TURN；会员：MEETING"
			+ "推广状态 会员分佣:MEMBERCOMMISSION ;消费分佣:CONSUMPTIONCOMMISSION ; 退款:REFUND")
	private String start;

	// 推广钱包
	@ApiModelProperty(value = "分佣得到金额")
	private Double money;

	@ApiModelProperty(value = "分佣总金额")
	private Double flowPrice;

	@ApiModelProperty(value = "钱包类型")
	private String owner;

	// 销售钱包
	@ApiModelProperty(value = "运费")
	private Double freightPrice;

	@ApiModelProperty(value = "合计")
	private Double AllMoney;

}
