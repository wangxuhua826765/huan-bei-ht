package cn.lili.modules.member.entity.vo;

import cn.lili.mybatis.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 会员详情、支出
 */
@Data
@NoArgsConstructor
public class MemberExpenditureVo extends BaseEntity {

	private String memberId;

	@ApiModelProperty(value = "会员昵称")
	private String nickName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "会员等级")
	private String gradeIdName;

	@ApiModelProperty(value = "地区")
	private String locationName;

	@ApiModelProperty(value = "流水号")
	private String receivableNo;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "支出时间")
	private Date expenditureTime;

	@ApiModelProperty(value = "支出类型")
	private String expenditureType;

	@ApiModelProperty(value = "支出类型")
	private String expenditureTypeName;

	@ApiModelProperty(value = "焕呗数量")
	private String yiBei;

	@ApiModelProperty(value = "开始时间")
	private String startDate;

	@ApiModelProperty(value = "结束时间")
	private String endDate;

	@ApiModelProperty(value = "地区定位")
	private String location;

	@ApiModelProperty(value = "充值钱包状态 支出会员费：MEETING；线下顶订单：OFFLINE；线上订单：NORMAL；虚拟订单：VIRTUAL" + " 推广钱包状态 ")
	private String start;

	@ApiModelProperty(value = "焕贝额")
	private String money;

	@ApiModelProperty(value = "手续费")
	private String handlingMoney;

	@ApiModelProperty(value = "对应现金额")
	private String realMoney;

	@ApiModelProperty(value = "提现折扣")
	private String fee;

	@ApiModelProperty(value = "钱包类型 PROMOT-推广钱包 ; PROMOTE_HY-推广会员钱包; PROMOTE_FW 推广服务费钱包")
	private String owner;

}
