package cn.lili.modules.member.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class MemberPartnerVO {

	/**
	 * memberID
	 */
	@ApiModelProperty
	private String id;

	/**
	 * 昵称
	 */
	@ApiModelProperty
	private String nickName;

	/**
	 * 手机号
	 */
	@ApiModelProperty
	private String mobile;

	/**
	 * 手机号
	 */
	@ApiModelProperty
	private String region;

	/**
	 * 真实姓名
	 */
	@ApiModelProperty
	private String realName;

	@ApiModelProperty(value = "身份证号")
	private String idCardNumber;

	/**
	 * 联系方式
	 */
	@ApiModelProperty
	private String linkMobile;

	/**
	 * 地址
	 */
	@ApiModelProperty
	private String storeAddressldPath;
	@ApiModelProperty
	private String adPath;

	/**
	 * 地址
	 */
	@ApiModelProperty
	private String storeAddressPath;

	/**
	 * 地址
	 */
	@ApiModelProperty
	private String storeAddressPath_path;

	/**
	 * adCode
	 */
	@ApiModelProperty
	private String adCode;

	/**
	 * 合伙人类型 0 非合伙人 1 事业合伙人 2 天使合伙人 3 推广员 4 代理商
	 */
	@ApiModelProperty
	private Integer partnerType;

	/**
	 * 合伙人状态 0 启用 1 禁用
	 */
	@ApiModelProperty
	private Integer partnerState;

	/**
	 * 审核状态
	 */
	@ApiModelProperty
	private String auditState;

	/**
	 * 审核原因
	 */
	@ApiModelProperty
	private String auditRemark;

	/**
	 * 合伙人会费
	 */
	@ApiModelProperty
	private Double partnerPrice;

	/**
	 * 付费状态 0 未付费 1 已付费
	 */
	@ApiModelProperty(value = "付费状态 0 未付费 1 已付费")
	private Integer payState;

	/**
	 * 备注
	 */
	@ApiModelProperty
	private String remark;

	@ApiModelProperty(value = "充值订单号")
	private String sn;

	@ApiModelProperty(value = "人民币")
	private Double price;

	@ApiModelProperty(value = "焕贝")
	private Double hbPrice;

	@ApiModelProperty(value = "类型")
	private String itemText;

	@ApiModelProperty(value = "支付时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date payTime;

	@ApiModelProperty(value = "推荐人手机号")
	private String recommendPhone;
}
