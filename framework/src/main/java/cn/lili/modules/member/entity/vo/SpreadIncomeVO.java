package cn.lili.modules.member.entity.vo;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import cn.lili.common.utils.DateUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.member.entity.dos.Spreadlncome;
import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/4/22
 */
@Data
public class SpreadIncomeVO extends Spreadlncome {

	@ApiModelProperty(value = "用户id")
	private String memberId;
	@ApiModelProperty(value = "区域id")
	private String regionId;
	@ApiModelProperty(value = "电话")
	private String mobile;

	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@ApiModelProperty(value = "会员用户名")
	private String username;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@ApiModelProperty(value = "会员地址")
	private String region;

	@ApiModelProperty(value = "合伙人区域")
	private String partnerRegion;

	@ApiModelProperty(value = "合伙人管理区域")
	private String partnerRegionId;

	@ApiModelProperty(value = "会员头像")
	private String face;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "最后一次登录时间")
	private Date lastLoginDate;

	@ApiModelProperty(value = "会员等级ID")
	private String gradeId;
	@ApiModelProperty(value = "会员等级")
	private String gradeName;
	@ApiModelProperty(value = "会员等级名称")
	private String gradeText;
	@ApiModelProperty(value = "会员类型名称")
	private String roleName;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "会员结束时间")
	private Date gradeEndTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建时间", hidden = true)
	private Date createTime;

	@ApiModelProperty(value = "会员标记  true 是  false 否")
	private Boolean memberFlag;

	@ApiModelProperty(value = "推广码")
	private String promotionCode;

	@ApiModelProperty(value = "合伙人id")
	private String partnerId;

	@ApiModelProperty(value = "合伙人名称")
	private String partnerName;

	@ApiModelProperty(value = "合伙人类型   0 非合伙人  1 事业合伙人  2 天使合伙人  3 普通合伙人  4 城市代理商")
	private Integer partnerType;

	@ApiModelProperty(value = "合伙人状态  0 启用   1  禁用")
	private Integer partnerState;

	@ApiModelProperty(value = "合伙人状态  禁用 时间")
	private Date partnerStateTime;

	@ApiModelProperty(value = "审核状态")
	private String auditState;

	@ApiModelProperty(value = "审核原因")
	private String auditRemark;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "开始时间")
	private Date beginTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	@ApiModelProperty(value = "推广人数量")
	private Integer quantity;

	@ApiModelProperty(value = "会员到期时间")
	private String memberEndTime;

	@ApiModelProperty(value = "身份证号")
	private String idCardNumber;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "定位")
	private String location;

	@ApiModelProperty(value = "区域")
	private String locationName;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "审核时间")
	private Date auditTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "推广时间")
	private Date promoteDate;

}
