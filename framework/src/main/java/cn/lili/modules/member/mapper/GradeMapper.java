package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.vo.GradeVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 会员等级数据处理层
 *
 * @author Bulbasaur
 * @since 2020-02-25 14:10:16
 */
public interface GradeMapper extends BaseMapper<GradeVO> {

	@Select("select * from li_membership_card  ${ew.customSqlSegment}")
	IPage<GradeVO> pageByMemberVO(IPage<GradeVO> page, @Param(Constants.WRAPPER) Wrapper<GradeVO> queryWrapper);

	@Select("select mc.*,dp.content from li_membership_card mc"
			+ " left join sys_dict_picture dp on dp.code = mc.picture_code  ${ew.customSqlSegment}")
	List<GradeVO> findByMemberVO(@Param(Constants.WRAPPER) Wrapper<GradeVO> queryWrapper);

	@Select("select c.* from li_membership_card c" + " left join li_grade_level g on g.grade_id = c.id"
			+ " where g.member_id = #{memberId}"
			+ " and g.grade_state = 0 and g.delete_flag = 0 and g.owner like 'BUYER%' AND NOW( ) < DATE_FORMAT( g.end_time, '%Y-%m-%d' )")
	GradeVO findByGradeVO(String memberId);

}