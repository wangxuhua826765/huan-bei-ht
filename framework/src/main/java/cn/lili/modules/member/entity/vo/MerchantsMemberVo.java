package cn.lili.modules.member.entity.vo;

import cn.lili.mybatis.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 推广会员
 */
@Data
@NoArgsConstructor
public class MerchantsMemberVo extends BaseEntity {

	@ApiModelProperty(value = "会员用户名")
	private String username;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "会员等级")
	private String gradeIdName;

	@ApiModelProperty(value = "地区")
	private String locationName;

	@ApiModelProperty(value = "地区adCode")
	private String location;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
	@ApiModelProperty(value = "推广时间")
	private Date promoteDate;

	@ApiModelProperty(value = "会员费累计额")
	private Double membersCosting;

	@ApiModelProperty(value = "会员费分佣")
	private Double membersCommission;

	@ApiModelProperty(value = "消费焕贝总额")
	private Double yiBeiSum;

	@ApiModelProperty(value = "C端消费分佣")
	private Double cCommission;

	@ApiModelProperty(value = "c充值钱包焕贝余额")
	private Double cYiBeiCommission;

	@ApiModelProperty(value = "开始时间")
	private String startDate;

	@ApiModelProperty(value = "结束时间")
	private String endDate;
}
