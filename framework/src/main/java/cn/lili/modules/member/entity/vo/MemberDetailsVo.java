package cn.lili.modules.member.entity.vo;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * 会员详情
 */
@Data
@NoArgsConstructor
public class MemberDetailsVo extends BaseEntity {

	@ApiModelProperty(value = "会员用户名")
	private String username;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "最后一次登录时间")
	private Date lastLoginDate;

	@ApiModelProperty(value = "会员等级")
	private String gradeIdName;

	@ApiModelProperty(value = "地区")
	private String locationName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "推广人手机号")
	private String extensionMobile;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "注册时间")
	private Date createTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "会员到期时间")
	private Date memberEndTime;

	@ApiModelProperty(value = "充值钱包余额 RECHARGE")
	private Double rechargeMoney;

	@ApiModelProperty(value = "会员头像")
	private String face;

	// 合伙人-B商家需要的字段
	@ApiModelProperty(value = "店铺名称")
	private String storeName;
	@ApiModelProperty(value = "店铺ID")
	private String storeId;

	@ApiModelProperty(value = "销售钱包余额 SALE")
	private Double saleMoney;

	@ApiModelProperty(value = "推广钱包余额 PROMOTE")
	private Double promoteMoney;

	// 天使合伙人需要的字段
	@ApiModelProperty(value = "推广人姓名")
	private String extensionName;

	// 根据不同状态获取不同合伙人的端口费
	@ApiModelProperty(value = "状态 天使合伙人：ANGEL_PARTNER；事业合伙人：BUSINESS_PARTNER；城市合伙人（也叫：代理商）：AGENT")
	private String state;
	// 角色Id
	@ApiModelProperty(value = "角色Id")
	private String roleId;
	// 所有端口费所用的字段
	@ApiModelProperty(value = "现金额")
	private Double price;

	@ApiModelProperty(value = "易呗数量")
	private Double yiBei;

	@ApiModelProperty(value = "平台付款时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date payTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private Date beginTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	// 城市合伙人需要的字段
	@ApiModelProperty(value = "推广会员费钱包余额")
	private Double promoteHyMoney;

	@ApiModelProperty(value = "推广服务费钱包余额")
	private Double promoteFwMoney;

	@ApiModelProperty(value = "推广码")
	private String promotionCode;

}
