package cn.lili.modules.member.entity.vo;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.checkerframework.common.value.qual.DoubleVal;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author paulG
 * @since 2021/11/8
 **/
@Data
public class MemberVO implements Serializable {

	private static final long serialVersionUID = 1810890757303309436L;

	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@ApiModelProperty(value = "会员用户名")
	private String username;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@ApiModelProperty(value = "会员性别,1为男，0为女")
	private Integer sex;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "会员生日")
	private Date birthday;

	@ApiModelProperty(value = "会员地址ID")
	private String regionId;

	@ApiModelProperty(value = "会员地址")
	private String region;

	@ApiModelProperty(value = "合伙人区域")
	private String partnerRegion;

	@ApiModelProperty(value = "合伙人管理区域")
	private String partnerRegionId;

	@ApiModelProperty(value = "手机号码", required = true)
	@Sensitive(strategy = SensitiveStrategy.PHONE)
	private String mobile;

	@ApiModelProperty(value = "积分数量")
	private Long point;

	@ApiModelProperty(value = "积分总数量")
	private Long totalPoint;

	@ApiModelProperty(value = "会员头像")
	private String face;

	@ApiModelProperty(value = "会员状态")
	private Boolean disabled;

	@ApiModelProperty(value = "是否开通店铺")
	private Boolean haveStore;

	@ApiModelProperty(value = "店铺ID")
	private String storeId;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "店铺地址名称， '，'分割")
	private String storeAddressPath;

	@ApiModelProperty(value = "店铺详细地址")
	private String storeAddressDetail;

	@ApiModelProperty(value = "openId")
	private String openId;

	/**
	 * @see ClientTypeEnum
	 */
	@ApiModelProperty(value = "客户端")
	private String clientEnum;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "最后一次登录时间")
	private Date lastLoginDate;

	@ApiModelProperty(value = "会员等级ID")
	private String gradeId;
	@ApiModelProperty(value = "会员等级")
	private String gradeName;
	@ApiModelProperty(value = "会员等级名称")
	private String gradeText;
	@ApiModelProperty(value = "会员类型名称")
	private String roleName;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "会员结束时间")
	private Date gradeEndTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "合伙人到期时间")
	private Date partnerEndTime;

	@ApiModelProperty(value = "经验值数量")
	private Long experience;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建时间", hidden = true)
	private Date createTime;

	@ApiModelProperty(value = "会员标记  true 是  false 否")
	private Boolean memberFlag;

	@ApiModelProperty(value = "用户类型  1 商家  2 小程序用户")
	private Integer type;

	@ApiModelProperty(value = "推广码")
	private String promotionCode;

	@ApiModelProperty(value = "合伙人id")
	private String partnerId;

	@ApiModelProperty(value = "合伙人名称")
	private String partnerName;

	@ApiModelProperty(value = "合伙人类型   0 非合伙人  1 事业合伙人  2 天使合伙人  3 普通合伙人  4 城市代理商")
	private Integer partnerType;

	@ApiModelProperty(value = "合伙人状态  0 启用   1  禁用")
	private Integer partnerState;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "合伙人状态  禁用 时间")
	private Date partnerStateTime;

	@ApiModelProperty(value = "审核状态")
	private String auditState;

	@ApiModelProperty(value = "审核原因")
	private String auditRemark;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private Date beginTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	@ApiModelProperty(value = "推广人数量")
	private Integer quantity;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "会员到期时间")
	private Date memberEndTime;

	@ApiModelProperty(value = "充值钱包余额")
	private Double rechargeMoney;
	@ApiModelProperty(value = "推广钱包余额")
	private Double promoteMoney;
	@ApiModelProperty(value = "推广钱包余额")
	private Double promoteHyMoney;
	@ApiModelProperty(value = "推广钱包余额")
	private Double promoteFwMoney;
	@ApiModelProperty(value = "销售钱包余额")
	private Double saleMoney;

	@ApiModelProperty(value = "钱包总余额")
	private Double allMoney;

	@ApiModelProperty(value = "充值现金总额")
	private Double allRechargeMoney;

	@ApiModelProperty(value = "充值焕贝总额")
	private Double allYiBeiMoney;

	@ApiModelProperty(value = "身份证号")
	private String idCardNumber;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "密码")
	private String password;

	@ApiModelProperty(value = "密码")
	private String paymentPassword;

	@ApiModelProperty(value = "定位")
	private String location;

	@ApiModelProperty(value = "区域")
	private String locationName;

	@ApiModelProperty(value = "推广人手机号")
	private String mobileExte;
	@ApiModelProperty(value = "是否使用密码  true 是  false 否")
	private int noSecret;

	@ApiModelProperty(value = "推广人姓名")
	private String nameExte;

	@ApiModelProperty(value = "推荐人电话")
	private String recommendPhone;

	@ApiModelProperty(value = "推荐人姓名")
	private String recommendName;

	@ApiModelProperty(value = "推广人类型")
	private String partnerExte;

	@ApiModelProperty(value = "推广人级别")
	private String gradeNameExte;

	@ApiModelProperty(value = "端口费-焕呗")
	private Double duanKouFeiHB;

	@ApiModelProperty(value = "端口费-现金")
	private Double duanKouFeiXJ;

	@ApiModelProperty(value = "充值会员金额")
	private Double rechargeMember;

	@ApiModelProperty(value = "充值会员分佣金额")
	private Double rechargeMemberCommission;

	@ApiModelProperty(value = "消费金额")
	private Double consumption;

	@ApiModelProperty(value = "消费分佣金额")
	private Double consumptionCommission;

	@ApiModelProperty(value = "是否有合同")
	private String isHaveContract;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "审核时间")
	private Date auditTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "推广时间")
	private Date promoteDate;

}
