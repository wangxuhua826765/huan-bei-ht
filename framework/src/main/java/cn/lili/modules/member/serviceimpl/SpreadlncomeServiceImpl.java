package cn.lili.modules.member.serviceimpl;

import cn.lili.modules.member.entity.dos.Spreadlncome;
import cn.lili.modules.member.mapper.SpreadlncomeMapper;
import cn.lili.modules.member.service.SpreadlncomeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * create by yudan on 2022/2/18
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SpreadlncomeServiceImpl extends ServiceImpl<SpreadlncomeMapper, Spreadlncome>
		implements
			SpreadlncomeService {

}
