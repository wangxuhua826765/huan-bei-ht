package cn.lili.modules.member.entity.vo;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 *
 **/
@Data
@TableName("li_membership_card")
@ApiModel(value = "会员")
@NoArgsConstructor
public class GradeVO extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "会员等级")
	private String level;

	@ApiModelProperty(value = "等级名称")
	private String name;

	@ApiModelProperty(value = "享受折扣")
	private Double rebate;

	@ApiModelProperty(value = "会员金额")
	private Double money;

	@ApiModelProperty(value = "充值折扣")
	private Double rechargeRule;

	@ApiModelProperty("会员描述")
	private String description;

	@ApiModelProperty("图片编码")
	private String pictureCode;

	@ApiModelProperty("图片地址")
	@TableField(exist = false)
	private String content;

}
