package cn.lili.modules.member.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.RechargeFow;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 会员等级业务层
 */
public interface RechargeFowService extends IService<RechargeFow> {

	/**
	 * 获取会员等级分页
	 *
	 * @param page
	 *            分页
	 * @return 会员分页
	 */
	IPage<RechargeFow> getRechargeFowPage(RechargeFow rechargeFow, PageVO page);

	/**
	 * 推广钱包-会员费-扣款逻辑
	 */
	Map<String, Object> updatePromote(String sn, Double price, String memberId, String type);

	/**
	 * 推广钱包-会员费-退款逻辑
	 */
	Map<String, Object> updatePromoteRefund(String sn, Double price, String memberId, String type);

}