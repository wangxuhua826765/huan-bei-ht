package cn.lili.modules.member.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 浏览历史
 *
 * @author Chopper
 * @since 2020/11/17 7:22 下午
 */
@Data
@TableName("li_business_quota")
@ApiModel(value = "风控管理")
@NoArgsConstructor
@AllArgsConstructor
public class BusinessQuota extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "店铺ID")
	private String storeId;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "商品ID")
	private String goodsId;

	@ApiModelProperty(value = "变动易呗")
	private String changeAmount;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "检测开始时间")
	private Date testingStartTime;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "检测结束时间")
	private Date testingEndTime;

	@ApiModelProperty(value = "是否限额0是1否")
	private Integer quota;

	@ApiModelProperty(value = "小程序用户id")
	private String memberId;

}