package cn.lili.modules.member.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class AuthVerifyDto {
	@ApiModelProperty(value = "身份证号")
	private String idCardNumber;
	@ApiModelProperty(value = "姓名")
	private String realName;
	@ApiModelProperty(value = "性别")
	private Integer sex;
	@ApiModelProperty(value = "出生日期")
	private Date birthday;
	@ApiModelProperty(value = "户籍地")
	private String homeTown;
}
