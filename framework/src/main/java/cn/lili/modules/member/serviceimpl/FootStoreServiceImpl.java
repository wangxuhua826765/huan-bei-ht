package cn.lili.modules.member.serviceimpl;

import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.goods.entity.dos.GoodsSku;
import cn.lili.modules.goods.util.DateUtils;
import cn.lili.modules.member.entity.dos.FootStore;
import cn.lili.modules.member.mapper.FootStoreMapper;
import cn.lili.modules.member.service.FootStoreService;
import cn.lili.modules.search.service.EsGoodsSearchService;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.vos.StoreVO;
import cn.lili.modules.store.mapper.StoreMapper;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * create by yudan on 2022/6/22
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FootStoreServiceImpl extends ServiceImpl<FootStoreMapper, FootStore> implements FootStoreService {

	/**
	 * es商品业务层
	 */
	@Autowired
	private EsGoodsSearchService esGoodsSearchService;

	@Autowired
	private StoreMapper storeMapper;

	@Override
	public FootStore saveFootStore(FootStore footStore) {
		LambdaQueryWrapper<FootStore> queryWrapper = Wrappers.lambdaQuery();
		queryWrapper.eq(FootStore::getMemberId, footStore.getMemberId());
		queryWrapper.eq(FootStore::getStoreId, footStore.getStoreId());
		// 如果已存在某商品记录，则更新其修改时间
		// 如果不存在则添加记录
		List<FootStore> oldStores = list(queryWrapper);
		if (oldStores != null && !oldStores.isEmpty()) {
			FootStore oldStore = oldStores.get(0);
			this.updateById(oldStore);
			return oldStore;
		} else {
			footStore.setCreateTime(new Date());
			footStore.setUpdateTime(new Date());
			this.save(footStore);
			// 删除超过100条后的记录
			this.baseMapper.deleteLastFootStore(footStore.getMemberId());
			return footStore;
		}
	}

	@Override
	public boolean clean() {
		LambdaQueryWrapper<FootStore> lambdaQueryWrapper = Wrappers.lambdaQuery();
		lambdaQueryWrapper.eq(FootStore::getMemberId, UserContext.getCurrentUser().getId());
		return this.remove(lambdaQueryWrapper);
	}

	@Override
	public boolean deleteByIds(List<String> ids) {
		LambdaQueryWrapper<FootStore> lambdaQueryWrapper = Wrappers.lambdaQuery();
		lambdaQueryWrapper.eq(FootStore::getMemberId, UserContext.getCurrentUser().getId());
		lambdaQueryWrapper.in(FootStore::getStoreId, ids);
		this.remove(lambdaQueryWrapper);
		return true;
	}

	@Override
	public List<StoreVO> footStorePage(PageVO pageVO) {

		QueryWrapper<FootStore> queryWrapper = Wrappers.query();
		queryWrapper.eq("f.member_id", UserContext.getCurrentUser().getId());
		queryWrapper.eq("f.delete_flag", false);
		queryWrapper.gt("f.update_time", DateUtils.addDays(new Date(), -30));
		queryWrapper.isNotNull("s.id");
		queryWrapper.orderByDesc("f.update_time");
		List<StoreVO> storeIdList = this.baseMapper.footStoreSkuIdList(PageUtil.initPage(pageVO), queryWrapper);
		if (!storeIdList.isEmpty()) {
			// List<EsGoodsIndex> list = esGoodsSearchService.getEsGoodsBySkuIds(skuIdList);
			// List<Store> list = storeMapper.selectBatchIds(storeIdList);
			// 去除为空的商品数据
			// storeIdList.removeIf(Objects::isNull);
			return storeIdList;
		}
		return Collections.emptyList();
	}

	@Override
	public long getFootStoreNum() {
		LambdaQueryWrapper<FootStore> lambdaQueryWrapper = Wrappers.lambdaQuery();
		lambdaQueryWrapper.eq(FootStore::getMemberId, Objects.requireNonNull(UserContext.getCurrentUser()).getId());
		lambdaQueryWrapper.eq(FootStore::getDeleteFlag, false);
		return this.count(lambdaQueryWrapper);
	}
}
