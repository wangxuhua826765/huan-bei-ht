package cn.lili.modules.member.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.goods.entity.dos.GoodsSku;
import cn.lili.modules.member.entity.dos.FootStore;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.vos.StoreVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * create by yudan on 2022/6/22
 */
public interface FootStoreService extends IService<FootStore> {

	/**
	 * 保存浏览历史
	 *
	 * @param footStore
	 *            用户足迹
	 * @return 浏览历史
	 */
	FootStore saveFootStore(FootStore footStore);

	/**
	 * 清空当前会员的足迹
	 *
	 * @return 处理结果
	 */
	boolean clean();

	/**
	 * 根据ID进行清除会员的历史足迹
	 *
	 * @param ids
	 *            商品ID列表
	 * @return 处理结果
	 */
	boolean deleteByIds(List<String> ids);

	/**
	 * 获取会员浏览历史分页
	 *
	 * @param pageVO
	 *            分页
	 * @return 会员浏览历史列表
	 */
	List<StoreVO> footStorePage(PageVO pageVO);

	/**
	 * 获取当前会员的浏览记录数量
	 *
	 * @return 当前会员的浏览记录数量
	 */
	long getFootStoreNum();
}
