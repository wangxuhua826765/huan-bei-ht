package cn.lili.modules.member.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/2/18
 */
@Data
@TableName("li_partner")
@ApiModel(value = "合伙人")
@NoArgsConstructor
public class Partner extends BaseEntity {

	@ApiModelProperty(value = "用户id")
	private String memberId;

	@ApiModelProperty(value = "角色id")
	private String roleId;

	@ApiModelProperty(value = "合伙人类型   0 非合伙人  1 事业合伙人  2 天使合伙人  3 推广员 4 城市代理商")
	private Integer partnerType;

	@ApiModelProperty(value = "合伙人状态  0 启用   1  禁用")
	private Integer partnerState;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private Date beginTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	@ApiModelProperty(value = "区域")
	private String region;

	@ApiModelProperty(value = "区域id")
	private String regionId;
	/**
	 * PartnerStatusEnum
	 */
	@ApiModelProperty(value = "审核状态")
	private String auditState;

	@ApiModelProperty(value = "审核原因")
	private String auditRemark;

	@ApiModelProperty(value = "付费状态 0 未付费 1 已付费")
	private Integer payState;

	@ApiModelProperty(value = "备注")
	private String remark;

	@ApiModelProperty(value = "充值订单号")
	private String sn;

	@ApiModelProperty(value = "备注")
	private Integer code;

	@ApiModelProperty(value = "编码前缀")
	private String prefix;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "申请时间")
	private Date applicationTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "审核时间")
	private Date auditTime;

	@ApiModelProperty(value = "推荐人手机号")
	private String recommendPhone;

}
