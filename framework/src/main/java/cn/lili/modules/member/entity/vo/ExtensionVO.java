package cn.lili.modules.member.entity.vo;

import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import cn.lili.modules.member.entity.dos.Extension;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
public class ExtensionVO extends Extension {

	@ApiModelProperty(value = "会员用户名")
	private String username;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@ApiModelProperty(value = "会员名称")
	private String name;

	@ApiModelProperty(value = "实名认证名称")
	private String realName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "区域")
	private String locationName;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	@ApiModelProperty(value = "创建时间,会员注册时间", hidden = true)
	private Date createTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
	@ApiModelProperty(value = "会员到期时间")
	private String memberEndTime;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "头像")
	private String face;

	@ApiModelProperty(value = "是否有店铺")
	private String haveStore;

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "店铺地址")
	private String storeAddressPath;

	@ApiModelProperty(value = "详细地址")
	private String storeAddressDetail;

	@ApiModelProperty(value = "类型")
	private String type;

	@ApiModelProperty(value = "支付类型")
	private String payStatus;

	@ApiModelProperty(value = "结束关系时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	private Date endTime;

}