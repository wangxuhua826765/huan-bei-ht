package cn.lili.modules.member.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RevokeApplyVo {
	private String id;
	@ApiModelProperty(value = "会员id")
	private String memberId;
	@ApiModelProperty(value = "审核状态1：待审核2审核通过3审核驳回4已撤回")
	private int auditStatus;
	@ApiModelProperty(value = "审核时间")
	private String auditDate;
	@ApiModelProperty(value = "撤销合伙人说明")
	private String revokeMsg = "";
	@ApiModelProperty(value = "申请提交时间")
	private String createTime;
	@ApiModelProperty(value = "审核描述")
	private String auditMsg = "";
}
