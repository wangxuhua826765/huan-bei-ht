package cn.lili.modules.member.mapper;

import cn.lili.modules.member.token.AccessToken;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

/**
 * create by yudan on 2022/9/23
 */
public interface AccessTokenMapper extends BaseMapper<AccessToken> {

	@Delete("DELETE from li_access_token where access_token = #{accessToken}")
	Boolean removeByAccessToken(@Param("accessToken") String accessToken);
}
