package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.BusinessQuota;
import cn.lili.modules.order.order.entity.dos.Order;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 风控管理数据处理层
 *
 * @author Bulbasaur
 * @since 2020-02-25 14:10:16
 */
public interface BusinessQuotaMapper extends BaseMapper<BusinessQuota> {

	@Select("select a.*,b.store_name from li_business_quota a left join li_store b on a.store_id = b.id  ${ew.customSqlSegment}")
	IPage<BusinessQuota> pageByMemberVO(IPage<BusinessQuota> page,
			@Param(Constants.WRAPPER) Wrapper<BusinessQuota> queryWrapper);

	@Select("select * from li_order  ${ew.customSqlSegment}")
	IPage<Order> queryBusinessOrder(IPage<BusinessQuota> page, @Param(Constants.WRAPPER) Wrapper<Order> queryWrapper);

}