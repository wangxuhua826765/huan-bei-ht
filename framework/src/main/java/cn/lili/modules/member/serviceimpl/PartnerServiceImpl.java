package cn.lili.modules.member.serviceimpl;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.vo.PartnerVO;
import cn.lili.modules.member.mapper.PartnerMapper;
import cn.lili.modules.member.service.EextensionService;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.statistics.entity.vo.IndexStatisticsVO;
import cn.lili.modules.system.service.RegionService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/2/18
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PartnerServiceImpl extends ServiceImpl<PartnerMapper, Partner> implements PartnerService {

	@Autowired
	private EextensionService eextensionService;

	public IPage<PartnerVO> getPartnerPage(PartnerVO partner, PageVO page) {
		QueryWrapper<Partner> queryWrapper = Wrappers.query();
		queryWrapper.and(wrapper -> wrapper.isNull("p.partner_state").or().eq("p.partner_state", 0));
		queryWrapper.and(wrapper -> wrapper.isNull("p.delete_flag").or().eq("p.delete_flag", 0));
		queryWrapper.and(wrapper -> wrapper.isNull("p.end_time").or().gt("p.end_time", new Date()));
		if (partner.getPartnerType() != null) {
			queryWrapper.eq("p.partner_type", partner.getPartnerType());
		}
		if (partner.getMemberId() != null) {
			queryWrapper.eq("m.id", partner.getMemberId());
		}
		if (partner.getExtension() != null) {
			queryWrapper.eq("e1.extension", partner.getExtension());
		}
		// 根据code查询合伙人管理的区域Code
		if (cn.lili.common.utils.StringUtils.isNotEmpty(partner.getAdcode())) {
			List itemAdCod = regionService.getPartnerAdCode(partner.getAdcode(), "4");
			queryWrapper.in("m.location", itemAdCod);
		}
		queryWrapper.groupBy("m.id,m.promotion_code");
		return this.baseMapper.getPartnerPage(PageUtil.initPage(page), queryWrapper);
	}

	@Override
	public Partner updatePartner(Partner Partner) {
		// 判断是否用户登录并且会员ID为当前登录会员ID
		AuthUser tokenUser = UserContext.getCurrentUser();
		if (tokenUser == null) {
			throw new ServiceException(ResultCode.USER_NOT_LOGIN);
		}
		this.updateById(Partner);
		return Partner;
	}

	@Override
	public Partner seleGerByRoleMember(String memberId, String roleId) {
		return this.baseMapper.seleGerByRoleMember(memberId, roleId);
	}

	@Override
	public Long partnerNum() {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("audit_state", "APPLYING");
		queryWrapper.eq("partner_state", 0);
		queryWrapper.eq("delete_flag", false);
		return this.count(queryWrapper);
	}

	@Override public Long countPartner(QueryWrapper<Partner> wrapper) {
		return this.baseMapper.countPartner(wrapper);
	}

	@Override
	public Long getCountMember(List itemAdCod, String partnerType, boolean boo) {

		QueryWrapper<Partner> partnerQueryWrapper = new QueryWrapper<>();
		partnerQueryWrapper.eq("a.delete_flag", 0);
		partnerQueryWrapper.eq("b.delete_flag", 0);
		partnerQueryWrapper.eq("audit_state","PASS");
		partnerQueryWrapper.eq("b.disabled", 1);
		partnerQueryWrapper.eq("a.partner_type", partnerType);// 合伙人类型
		if (boo) {
			partnerQueryWrapper.eq("b.have_store", 1); // 是否带店铺
		}else{
			partnerQueryWrapper.eq("b.have_store", 0); // 是否带店铺
		}
		// 如果定位不为空则为城市合伙人登录
		if (itemAdCod.size() > 0) {
			partnerQueryWrapper.in("b.location", itemAdCod);
		}
		List<Member> list = baseMapper.selectBtianshiB(partnerQueryWrapper);

		List listAll = new ArrayList();
		list.forEach(item -> {
			listAll.add(item.getPromotionCode());
		});
		QueryWrapper<Extension> queryWrapper = new QueryWrapper<Extension>();
		if(CollectionUtils.isNotEmpty(listAll)) {
			queryWrapper.in("extension", listAll);
			return eextensionService.count(queryWrapper);
		}else{
			return 0L;
		}
	}

	@Autowired
	private RegionService regionService;

	@Override
	public Long getChengshi(List itemAdCod, String state) {
		QueryWrapper<Partner> partnerQueryWrapper = new QueryWrapper<>();
		partnerQueryWrapper.eq("a.delete_flag", 0);
		partnerQueryWrapper.eq("a.disabled", 1);
		if (StringUtils.isNotEmpty(state)) {
			partnerQueryWrapper.eq("b.delete_flag", 0);
			partnerQueryWrapper.eq("audit_state","PASS");
			if (state.equals("b")) {
				partnerQueryWrapper.eq("a.have_store", 1);// 是否有店铺
				partnerQueryWrapper.eq("b.partner_type", "2");// 合伙人类型
			} else {
				partnerQueryWrapper.eq("a.have_store", 0);// 是否有店铺
				partnerQueryWrapper.eq("b.partner_type", state);// 合伙人类型
			}
		}else{
			partnerQueryWrapper.apply("a.id not in (SELECT DISTINCT member_id from li_partner where partner_type in ('1','2','4') and partner_state = 0 and delete_flag = 0 and audit_state = 'PASS')");
			partnerQueryWrapper.eq("a.have_store", 0);// 是否有店铺
		}
		if (itemAdCod == null || itemAdCod.size() == 0) {
			List<Map> chengshi = baseMapper.getChengshi();
			chengshi.forEach(item -> {
				itemAdCod.addAll(regionService.getPartnerAdCode(item.get("location")+ "", "4"));
			});
		}
		partnerQueryWrapper.in("a.location", itemAdCod);
		if (itemAdCod == null || itemAdCod.size() == 0) {
			return 0L;
		}
		return baseMapper.getChengshiTg(partnerQueryWrapper);
	}
}
