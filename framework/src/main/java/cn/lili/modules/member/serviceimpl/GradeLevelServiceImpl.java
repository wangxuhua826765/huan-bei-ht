package cn.lili.modules.member.serviceimpl;

import cn.lili.common.utils.StringUtils;
import cn.lili.modules.member.entity.dos.GradeLevel;
import cn.lili.modules.member.mapper.GradeLevelMapper;
import cn.lili.modules.member.service.GradeLevelService;
import cn.lili.modules.statistics.entity.vo.IndexStatisticsVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * create by yudan on 2022/3/2
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GradeLevelServiceImpl extends ServiceImpl<GradeLevelMapper, GradeLevel> implements GradeLevelService {
	@Override
	public Long getSlMap(List itemAdCod, String level) {
		// 首页统计内容
		QueryWrapper<GradeLevel> objectQueryWrapper = new QueryWrapper<>();
		if (itemAdCod.size() > 0) {
			objectQueryWrapper.in("a.location", itemAdCod);
		}
		if (StringUtils.isNotEmpty(level)) {
			objectQueryWrapper.eq("b.level", level);
		}else{
			objectQueryWrapper.apply("a.id not in (SELECT DISTINCT member_id from li_partner where partner_type in ('1','2','4') and partner_state = 0 and delete_flag = 0 and audit_state = 'PASS')");
			objectQueryWrapper.eq("a.have_store", 0);
		}
		objectQueryWrapper.eq("g.delete_flag", 0);
		objectQueryWrapper.eq("g.grade_state", 0);
		objectQueryWrapper.ge("g.end_time", new Date());
		objectQueryWrapper.eq("a.delete_flag", 0);
		objectQueryWrapper.eq("a.disabled", 1);
		List<Map<String, Object>> slMap = this.baseMapper.getSlMap(objectQueryWrapper);
		return Long.parseLong(slMap.get(0).get("cun") + "");
	}
}
