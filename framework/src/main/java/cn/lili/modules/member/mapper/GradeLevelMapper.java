package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.dos.GradeLevel;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/3/2
 */
public interface GradeLevelMapper extends BaseMapper<GradeLevel> {

	// 根据会员id修改状态
	@Update("UPDATE li_grade_level set delete_flag = 1,grade_state = 1 where member_id = #{memberId} and owner like #{owner}")
	boolean updateByMemberId(String memberId, String owner);

	// 查询个个等级会员数量 提交
	@Select("select count(0) cun from li_member a"
		+ " left join li_grade_level g on g.member_id = a.id"
		+ " left join li_membership_card b on g.grade_id = b.id ${ew.customSqlSegment}")
	List<Map<String, Object>> getSlMap(@Param(Constants.WRAPPER) Wrapper<GradeLevel> queryWrapper);

}
