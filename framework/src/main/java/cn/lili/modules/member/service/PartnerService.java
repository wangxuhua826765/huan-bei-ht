package cn.lili.modules.member.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.vo.PartnerVO;
import cn.lili.modules.statistics.entity.vo.IndexStatisticsVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * create by yudan on 2022/2/18
 */
public interface PartnerService extends IService<Partner> {

	/**
	 * 获取会员等级分页
	 *
	 * @param page
	 *            分页
	 * @return 会员分页
	 */
	IPage<PartnerVO> getPartnerPage(PartnerVO partner, PageVO page);

	/**
	 * 后台-修改启用停用
	 *
	 * @return 会员
	 */
	Partner updatePartner(Partner partner);

	/**
	 * 根据membereId和RoleId获取开始时间结束时间
	 */
	Partner seleGerByRoleMember(String memberId, String roleId);

	/**
	 * 首页合伙人统计
	 */
	Long getCountMember(List itemAdCod, String partnerType, boolean boo);

	/**
	 * 首页合伙人统计
	 */
	Long getChengshi(List itemAdCod, String state);

	Long partnerNum();

	// 查询所有合伙人数量
	Long countPartner(QueryWrapper<Partner> wrapper);

}
