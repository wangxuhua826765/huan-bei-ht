package cn.lili.modules.member.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 支付密码修改入参dto
 */
@Data
public class PaymentPwdDto {
	@ApiModelProperty(value = "会员用户唯一标识")
	private String memberId;
	@ApiModelProperty(value = "手机号")
	private String mobile;
	@ApiModelProperty(value = "短信验证码")
	private String msgCode;
	@ApiModelProperty(value = "是否免密支付（0否 使用密码，1 是 免密支付）")
	private int noSecret;
	@ApiModelProperty(value = "新密码")
	private String newPassword;
	@ApiModelProperty(value = "确认密码")
	private String confirmPassword;
}
