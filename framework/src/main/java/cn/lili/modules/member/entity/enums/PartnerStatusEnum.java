package cn.lili.modules.member.entity.enums;

/**
 * 合伙人审核状态 create by yudan on 2022/3/31
 */
public enum PartnerStatusEnum {
	/**
	 * 申请开店
	 */
	PASS("审核通过"),
	/**
	 * 审核拒绝
	 */
	REFUSED("审核拒绝"),
	/**
	 * 申请中
	 */
	APPLYING("申请中，提交审核"),
	/**
	 * 待提交
	 */
	WAIT_SUBMIT("待提交");

	private final String description;

	PartnerStatusEnum(String des) {
		this.description = des;
	}

	public String description() {
		return this.description;
	}

	public String value() {
		return this.name();
	}
}
