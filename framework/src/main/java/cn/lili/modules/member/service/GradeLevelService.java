package cn.lili.modules.member.service;

import cn.lili.modules.member.entity.dos.GradeLevel;
import cn.lili.modules.statistics.entity.vo.IndexStatisticsVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/3/2
 */
public interface GradeLevelService extends IService<GradeLevel> {

	// 查询各个会员数量
	Long getSlMap(List itemAdCod, String leve);
}
