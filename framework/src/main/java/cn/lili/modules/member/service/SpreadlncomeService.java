package cn.lili.modules.member.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.dos.Spreadlncome;
import cn.lili.modules.member.entity.vo.PartnerVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * create by yudan on 2022/2/18
 */
public interface SpreadlncomeService extends IService<Spreadlncome> {

}
