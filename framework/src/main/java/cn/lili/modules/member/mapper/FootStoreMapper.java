package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.FootStore;
import cn.lili.modules.store.entity.vos.StoreVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * create by yudan on 2022/6/22
 */
public interface FootStoreMapper extends BaseMapper<FootStore> {

	/**
	 * 获取用户足迹的SkuId分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 用户足迹的SkuId分页
	 */
	@Select("select s.*,DATE_FORMAT(f.update_time,'%Y-%m-%d') as footTime from li_foot_store f "
			+ " left join li_store s on s.id = f.store_id " + " ${ew.customSqlSegment} ")
	List<StoreVO> footStoreSkuIdList(IPage<String> page, @Param(Constants.WRAPPER) Wrapper<FootStore> queryWrapper);

	/**
	 * 删除超过100条后的记录
	 *
	 * @param memberId
	 *            会员ID
	 */
	@Delete("DELETE FROM li_foot_store WHERE (SELECT COUNT(b.id) FROM ( SELECT * FROM li_foot_store WHERE member_id = #{memberId} ) b) >100 "
			+ " AND id =(SELECT a.id FROM ( SELECT * FROM li_foot_store WHERE member_id = #{memberId} ORDER BY create_time ASC LIMIT 1 ) AS a)")
	void deleteLastFootStore(String memberId);

}
