package cn.lili.modules.member.service;

import cn.lili.modules.member.token.AccessToken;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * create by yudan on 2022/9/23
 */
public interface AccessTokenService extends IService<AccessToken> {

	Boolean removeByAccessToken(String accessToken);
}
