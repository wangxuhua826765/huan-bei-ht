package cn.lili.modules.member.serviceimpl;

import cn.lili.cache.Cache;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.sensitive.SensitiveWordsFilter;
import cn.lili.common.utils.BeanUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.MemberAddress;
import cn.lili.modules.member.entity.dto.ManagerMemberEditDTO;
import cn.lili.modules.member.entity.dto.MemberAddDTO;
import cn.lili.modules.member.entity.vo.GradeVO;
import cn.lili.modules.member.mapper.GradeMapper;
import cn.lili.modules.member.service.GradeService;
import cn.lili.mybatis.util.PageUtil;
import cn.lili.rocketmq.RocketmqSendCallbackBuilder;
import cn.lili.rocketmq.tags.MemberTagsEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 会员等级接口业务层实现
 *
 * @author Chopper
 * @since 2021-03-29 14:10:16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GradeServiceImpl extends ServiceImpl<GradeMapper, GradeVO> implements GradeService {

	/**
	 * 缓存
	 */
	@Autowired
	private Cache cache;

	public IPage<GradeVO> getGradePage(GradeVO gradeVO, PageVO page) {
		QueryWrapper<GradeVO> queryWrapper = Wrappers.query();
		queryWrapper.eq("delete_flag", 0);
		queryWrapper.orderByDesc("create_time");
		IPage<GradeVO> gradeVOIPage = this.baseMapper.pageByMemberVO(PageUtil.initPage(page), queryWrapper);
		// 过滤富文本标签
		// if (CollectionUtils.isNotEmpty(gradeVOIPage.getRecords())) {
		// for (GradeVO grade : gradeVOIPage.getRecords()) {
		// if(StringUtils.isNotEmpty(grade.getDescription())){
		// grade.setDescription(Jsoup.parse(grade.getDescription()).text());
		// }
		// }
		// }
		return gradeVOIPage;
	}

	@Override
	public List<GradeVO> findByMemberVO(GradeVO gradeVO) {
		QueryWrapper<GradeVO> queryWrapper = Wrappers.query();
		queryWrapper.eq("mc.delete_flag", 0);
		queryWrapper.orderByDesc("mc.create_time");
		return this.baseMapper.findByMemberVO(queryWrapper);
	}

	@Override
	public GradeVO addGrade(GradeVO gradeVO) {
		// 添加会员等级
		this.save(gradeVO);
		return gradeVO;
	}

	@Override
	public GradeVO updateGrade(GradeVO gradeVO) {
		// 判断是否用户登录并且会员ID为当前登录会员ID
		AuthUser tokenUser = UserContext.getCurrentUser();
		if (tokenUser == null) {
			throw new ServiceException(ResultCode.USER_NOT_LOGIN);
		}
		this.updateById(gradeVO);
		return gradeVO;
	}

	@Override
	public boolean removeGrade(String id) {
		return this.remove(new QueryWrapper<GradeVO>().eq("id", id));
	}

	@Override
	public GradeVO findByGradeVO(String memberId) {
		return this.baseMapper.findByGradeVO(memberId);
	}

}