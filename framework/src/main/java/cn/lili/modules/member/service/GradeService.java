package cn.lili.modules.member.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dto.ManagerMemberEditDTO;
import cn.lili.modules.member.entity.dto.MemberAddDTO;
import cn.lili.modules.member.entity.vo.GradeVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 会员等级业务层
 */
public interface GradeService extends IService<GradeVO> {

	/**
	 * 获取会员等级分页
	 *
	 * @param page
	 *            分页
	 * @return 会员分页
	 */
	IPage<GradeVO> getGradePage(GradeVO gradeVO, PageVO page);

	List<GradeVO> findByMemberVO(GradeVO gradeVO);

	/**
	 * 后台-添加会员等级
	 *
	 * @param gradeVO
	 *            会员等级
	 * @return 会员
	 */
	GradeVO addGrade(GradeVO gradeVO);

	/**
	 * 后台-修改会员等级
	 *
	 * @param gradeVO
	 *            后台修改会员等级参数
	 * @return 会员
	 */
	GradeVO updateGrade(GradeVO gradeVO);

	/**
	 * 删除会员等级
	 *
	 * @param id
	 *            收货地址ID
	 * @return 操作状态
	 */
	boolean removeGrade(String id);

	/**
	 * 获取充值折扣
	 * 
	 * @param memberId
	 * @return
	 */
	GradeVO findByGradeVO(String memberId);
}