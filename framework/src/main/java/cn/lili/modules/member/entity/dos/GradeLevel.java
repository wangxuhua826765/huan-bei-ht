package cn.lili.modules.member.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/3/2
 */
@Data
@TableName("li_grade_level")
@ApiModel(value = "会员等级表")
@NoArgsConstructor
public class GradeLevel extends BaseEntity {

	@ApiModelProperty(value = "用户id")
	private String memberId;

	@ApiModelProperty(value = "会员等级id")
	private String gradeId;

	@ApiModelProperty(value = "会员状态  0 启用   1  禁用")
	private Integer gradeState;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private Date beginTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	@ApiModelProperty(value = "来源(用户、商家)")
	private String owner;

}
