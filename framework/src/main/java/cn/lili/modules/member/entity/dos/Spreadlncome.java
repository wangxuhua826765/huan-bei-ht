package cn.lili.modules.member.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/2/18
 */
@Data
@TableName("finance_admin_spreadlncome")
@ApiModel(value = "合伙人价差收益")
@NoArgsConstructor
public class Spreadlncome extends BaseEntity {

	@ApiModelProperty(value = "用户id")
	private String memberId;

	@ApiModelProperty(value = "会员地址")
	private String region;

	@ApiModelProperty(value = "区域id")
	private String regionId;

	@ApiModelProperty(value = "电话")
	private String mobile;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@ApiModelProperty(value = "会员头像")
	private String face;

	@ApiModelProperty(value = "会员类型名称")
	private String roleName;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建时间", hidden = true)
	private Date createTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "开始时间")
	private Date beginTime;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "结束时间")
	private Date endTime;

	@ApiModelProperty(value = "定位")
	private String location;

	@ApiModelProperty(value = "区域")
	private String locationName;

	// 全部下级充值钱包
	@ApiModelProperty(value = "充值焕贝额")
	private Double czHb;
	@ApiModelProperty(value = "充值现金额")
	private Double czXj;
	@ApiModelProperty(value = "充值折扣")
	private Double czZk;
	// 全部下级销售和推广钱包
	@ApiModelProperty(value = "销售推广现金额")
	private Double xsXj;
	@ApiModelProperty(value = "销售推广焕贝额")
	private Double xsHb;
	@ApiModelProperty(value = "提现折扣")
	private Double xsZk;
	// 总收益差值
	@ApiModelProperty(value = "总收益差值")
	private Double allocateAllMoney;
	// 区域所得
	// 分配折扣
	// 差价收益额（元）
	@ApiModelProperty(value = "分配折扣")
	private Double allocateMemberDiscount;
	@ApiModelProperty(value = "差价收益额")
	private Double allocateMemberMoney;
	// 平台所得
	// 分配折扣
	// 差价收益额（元）
	@ApiModelProperty(value = "分配折扣")
	private Double allocateAdminDiscount;
	@ApiModelProperty(value = "差价收益额")
	private Double allocateAdminMoney;

}
