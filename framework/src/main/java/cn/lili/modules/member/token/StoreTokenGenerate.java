package cn.lili.modules.member.token;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.security.token.Token;
import cn.lili.common.security.token.TokenUtil;
import cn.lili.common.security.token.base.AbstractTokenGenerate;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.permission.entity.dos.StoreUser;
import cn.lili.modules.permission.service.StoreUserService;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.service.StoreService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商家token生成
 *
 * @author Chopper
 * @version v4.0
 * @since 2020/11/16 10:51
 */
@Component
public class StoreTokenGenerate extends AbstractTokenGenerate {
	@Autowired
	private MemberService memberService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private TokenUtil tokenUtil;
	@Autowired
	private StoreUserService storeUserService;

	@Override
	public Token createToken(String username, Boolean longTerm) {
		// 生成token
		QueryWrapper<StoreUser> objectQueryWrapper = new QueryWrapper<>();
		objectQueryWrapper.eq("username", username);
		StoreUser one = storeUserService.getOne(objectQueryWrapper);
		// if (!member.getHaveStore()) {
		// throw new ServiceException(ResultCode.STORE_NOT_OPEN);
		// }
		Member member = memberService.getById(one.getMemberId());
		LambdaQueryWrapper<Store> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(Store::getMemberId, one.getMemberId());
		Store store = storeService.getOne(queryWrapper);
		AuthUser user = new AuthUser(one.getUsername(), one.getId(), one.getNickName(), store.getStoreLogo(),
				UserEnums.STORE, member.getMobile(), store.getId(), store.getStoreName(), member.getId());
		// user.setStoreId(store.getId());
		// user.setStoreName(store.getStoreName());
		return tokenUtil.createToken(username, user, longTerm, UserEnums.STORE);
	}

	@Override
	public Token refreshToken(String refreshToken) {
		return tokenUtil.refreshToken(refreshToken, UserEnums.STORE);
	}

}
