package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.RechargeFow;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 会员等级数据处理层
 *
 * @author Bulbasaur
 * @since 2020-02-25 14:10:16
 */
public interface RechargeFowMapper extends BaseMapper<RechargeFow> {

	@Select("select a.*,m.nick_name,m.mobile,m.type from li_recharge_flow a left join li_member m on a.member_id=m.id  ${ew.customSqlSegment}")
	IPage<RechargeFow> getRechargeFowPage(IPage<RechargeFow> page,
			@Param(Constants.WRAPPER) Wrapper<RechargeFow> queryWrapper);
}