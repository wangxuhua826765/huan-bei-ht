package cn.lili.modules.member.mapper;

import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.vo.PartnerVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/2/18
 */
public interface PartnerMapper extends BaseMapper<Partner> {
	/************
	 * 查询登录用户权限******修改计算实际到账金额和手续费************20220623
	 *****************************/
	@Select("select r.name from li_role r left join li_partner p on p.role_id = r.id "
			+ "where p.member_id = #{memberId} and p.partner_state = 0 and p.delete_flag = 0 AND NOW( ) < DATE_FORMAT( end_time, '%Y-%m-%d' )")
	String findByPartner(String id);

	@Select("SELECT * from (" + "select count(e.id) quantity,e1.extension,p.partner_state,p.partner_type,p.create_time,"
			+ "p.begin_time,p.end_time,m.username,m.id,m.face,m.region,m.promotion_img,m.promotion_code,m.mobile from li_member m "
			+ " left join li_extension e on e.extension = m.promotion_code"
			+ " left join li_partner p on p.member_id = m.id " + " LEFT JOIN li_extension e1 ON e1.member_id = m.id "
			+ " ${ew.customSqlSegment} ) a" + " ORDER BY a.quantity DESC ,a.partner_type DESC ")
	IPage<PartnerVO> getPartnerPage(IPage<Partner> page, @Param(Constants.WRAPPER) Wrapper<Partner> queryWrapper);

	// 根据会员id修改状态
	@Update("UPDATE li_partner set delete_flag = 1, partner_state = 1 ,update_time = Now() where member_id = #{memberId}")
	boolean updateByMemberId(String memberId);

	// 根据会员id修改状态
	@Update("UPDATE li_partner set delete_flag = 0, partner_state = 0 ,update_time = Now() where member_id = #{memberId} and audit_state=\"PASS\"  order by update_time DESC limit 1")
	boolean updateOpenByMemberId(String memberId);

	@Select("select * from li_partner "
			+ "where member_id = #{memberId} and partner_state = 0 and delete_flag = 0 AND NOW( ) < DATE_FORMAT( end_time, '%Y-%m-%d' )")
	Partner findPartnerByMemberId(String id);

	@Select("select * from li_partner "
			+ "where delete_flag = 1 and member_id = #{memberId} and role_id = #{roleId} group by end_time desc limit 1")
	Partner seleGerByRoleMember(String memberId, String roleId);

	@Select("select c.promotion_code code from li_partner a left join li_member c on a.member_id = c.id where a.delete_flag = false and a.partner_type = #{partnerType}  and  c.promotion_code is not null ")
	List<Map> selectTianshiOrShiYe(String partnerType);

	@Select("select a.region_id location from li_partner a left join li_member c on a.member_id = c.id where a.partner_type = 4 and a.audit_state = 'PASS' and a.partner_state = 0  ")
	List<Map> getChengshi();

	@Select("select count(*) con from li_extension a left join li_partner b on a.member_id = b.member_id left join li_member c on a.member_id = c.id   ${ew.customSqlSegment}   ")
	Map<String, Long> getChengshiTgB(@Param(Constants.WRAPPER) Wrapper<Extension> queryWrapper);

	@Select("select b.* from li_partner a left join li_member b on a.member_id = b.id  ${ew.customSqlSegment}    ")
	List<Member> selectBtianshiB(@Param(Constants.WRAPPER) Wrapper<Partner> queryWrapper);

	@Select("select count(0)  from li_member a left join li_partner b  on a.id = b.member_id ${ew.customSqlSegment} ")
	Long getChengshiTg(@Param(Constants.WRAPPER) Wrapper<Partner> queryWrapper);

	// 查询所有合伙人数量
	@Select("SELECT COUNT(*)  FROM li_member m inner join li_partner p on m.id = p.member_id ${ew.customSqlSegment}")
	Long countPartner(@Param(Constants.WRAPPER) Wrapper<Partner> queryWrapper);
}
