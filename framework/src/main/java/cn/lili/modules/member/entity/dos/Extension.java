package cn.lili.modules.member.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 浏览历史
 *
 * @author Chopper
 * @since 2020/11/17 7:22 下午
 */
@Data
@TableName("li_extension")
@ApiModel(value = "推广管理")
@NoArgsConstructor
@AllArgsConstructor
public class Extension extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "推广码")
	private String extension;

	@ApiModelProperty(value = "合伙人表id")
	private String partnerId;

	@ApiModelProperty(value = "临时推广码记录")
	private String tempExtension;

}