package cn.lili.modules.bank.mapper;

import cn.lili.modules.bank.entity.dos.MemberBank;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface MemberBankMapper extends BaseMapper<MemberBank> {
}
