package cn.lili.modules.bank.service;

import cn.lili.modules.bank.entity.dos.MemberBank;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface MemberBankService extends IService<MemberBank> {

	Boolean insert(MemberBank memberBank);

	MemberBank identify(String url) throws Exception;

	List<MemberBank> getBank(String memberId);

	Boolean delBank(String bankId);
}
