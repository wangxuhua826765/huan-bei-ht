package cn.lili.modules.bank.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@TableName("li_member_bank")
@ApiModel(value = "银行卡")
@NoArgsConstructor
public class MemberBank extends BaseEntity {

	@ApiModelProperty(value = "银行名称")
	private String bankName;
	@ApiModelProperty(value = "卡号")
	private String cardNo;
	@ApiModelProperty(value = "银行行号")
	private String bankCode;
	@ApiModelProperty(value = "地址ID")
	private String adCode;
	@ApiModelProperty(value = "地址名称")
	private String adCodeName;
	@ApiModelProperty(value = "开户行")
	private String openAccBank;
	@ApiModelProperty(value = "账户名")
	private String accountName;
	@ApiModelProperty(value = "手机号")
	private String mobile;
	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "短信验证码")
	@TableField(exist = false)
	private String msgCode;
}
