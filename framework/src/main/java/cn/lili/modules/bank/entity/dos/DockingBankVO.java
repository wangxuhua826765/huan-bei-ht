package cn.lili.modules.bank.entity.dos;

import cn.lili.common.utils.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@ApiModel(value = "对接银行")
@NoArgsConstructor
public class DockingBankVO {

	// 必传项
	@ApiModelProperty(value = "收款人账号")
	private String payeeAcctNo;
	@ApiModelProperty(value = "收款人名称")
	private String payeeName;
	@ApiModelProperty(value = "支付金额")
	private BigDecimal amount;

	@ApiModelProperty(value = "银行行号")
	private String bankCode;
	@ApiModelProperty(value = "地址ID")
	private String adCode;
	@ApiModelProperty(value = "地址名称")
	private String adCodeName;
	@ApiModelProperty(value = "开户行")
	private String openAccBank;

	// 非必传(需要用于判断是否是同行或者他行>在这里必传)
	@ApiModelProperty(value = "收款行名称")
	private String payeeBankName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "提现单号")
	private String withdrawalSn;

	@ApiModelProperty(value = "提现ID")
	private String withdrawalId;

	@ApiModelProperty(value = "备注")
	private String note;

	public boolean isNotNull() {
		if (amount.compareTo(BigDecimal.valueOf(0.0)) > 0 && StringUtils.isNotEmpty(payeeAcctNo)
				&& StringUtils.isNotEmpty(payeeName) && StringUtils.isNotEmpty(payeeBankName)) {
			return true;
		} else {
			return false;
		}
	}
}
