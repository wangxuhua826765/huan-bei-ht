package cn.lili.modules.bank.service.impl;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.bank.entity.dos.MemberBank;
import cn.lili.modules.bank.mapper.MemberBankMapper;
import cn.lili.modules.bank.service.MemberBankService;
import cn.lili.modules.promotion.entity.dos.PromotionGoods;
import cn.lili.modules.system.entity.dos.Setting;
import cn.lili.modules.system.entity.dto.SmsSetting;
import cn.lili.modules.system.entity.enums.SettingEnum;
import cn.lili.modules.system.service.SettingService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aliyun.tea.*;
import com.aliyun.ocr20191230.models.*;
import com.aliyun.teaopenapi.models.*;
import com.aliyun.teautil.models.*;

import java.util.List;

@Service
@Slf4j
public class MemberBankServiceImpl extends ServiceImpl<MemberBankMapper, MemberBank> implements MemberBankService {
	@Autowired
	private SettingService settingService;

	@Override
	public Boolean insert(MemberBank memberBank) {
		int num = 0;
		if (StringUtils.isEmpty(memberBank.getId())) {
			num = this.baseMapper.insert(memberBank);
		} else {
			num = this.baseMapper.updateById(memberBank);
		}
		if (num != 1) {
			throw new ServiceException(ResultCode.ERROR);
		}
		return Boolean.TRUE;
	}

	public MemberBank identify(String url) throws Exception {
		Setting setting = settingService.get(SettingEnum.SMS_SETTING.name());
		SmsSetting smsSetting = new Gson().fromJson(setting.getSettingValue(), SmsSetting.class);

		com.aliyun.ocr20191230.Client client = createClient(smsSetting.getAccessKeyId(), smsSetting.getAccessSecret());
		RecognizeBankCardRequest recognizeBankCardRequest = new RecognizeBankCardRequest();
		recognizeBankCardRequest.setImageURL(url);
		RuntimeOptions runtime = new RuntimeOptions();
		try {
			// 复制代码运行请自行打印 API 的返回值
			RecognizeBankCardResponse response = client.recognizeBankCardWithOptions(recognizeBankCardRequest, runtime);
			log.info("银行卡识别结果:{}", JSON.toJSONString(response));
			MemberBank memberBank = new MemberBank();
			if (response != null) {
				memberBank.setBankName(response.body.getData().getBankName());
				memberBank.setCardNo(response.body.getData().getCardNumber());
			}
			return memberBank;
		} catch (TeaException error) {
			// 如有需要，请打印 error
			com.aliyun.teautil.Common.assertAsString(error.message);
		} catch (Exception _error) {
			TeaException error = new TeaException(_error.getMessage(), _error);
			// 如有需要，请打印 error
			com.aliyun.teautil.Common.assertAsString(error.message);
		}
		return null;
	}

	@Override
	public List<MemberBank> getBank(String memberId) {
		QueryWrapper<MemberBank> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("delete_flag", 0);
		queryWrapper.eq("member_id", memberId);
		return this.baseMapper.selectList(queryWrapper);
	}

	@Override
	public Boolean delBank(String bankId) {
		MemberBank memberBank = this.baseMapper.selectById(bankId);
		if (memberBank != null) {
			memberBank.setDeleteFlag(Boolean.TRUE);
			this.baseMapper.updateById(memberBank);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public static com.aliyun.ocr20191230.Client createClient(String accessKeyId, String accessKeySecret)
			throws Exception {
		Config config = new Config()
				// 您的 AccessKey ID
				.setAccessKeyId(accessKeyId)
				// 您的 AccessKey Secret
				.setAccessKeySecret(accessKeySecret);
		// 访问的域名
		config.endpoint = "ocr.cn-shanghai.aliyuncs.com";
		return new com.aliyun.ocr20191230.Client(config);
	}
}
