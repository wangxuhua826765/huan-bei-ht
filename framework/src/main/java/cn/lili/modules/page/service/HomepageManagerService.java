package cn.lili.modules.page.service;

import cn.lili.modules.system.entity.dos.Homepage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

public interface HomepageManagerService extends IService<Homepage> {

}
