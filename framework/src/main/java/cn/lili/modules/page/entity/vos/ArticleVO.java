package cn.lili.modules.page.entity.vos;

import cn.lili.modules.page.entity.dos.Article;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 文章VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class ArticleVO extends Article {

	@ApiModelProperty(value = "文章ID")
	private String id;

	@ApiModelProperty(value = "文章标题")
	private String title;

	@ApiModelProperty(value = "分类名称")
	private String articleCategoryName;

	@ApiModelProperty(value = "文章排序")
	private Integer sort;

	@ApiModelProperty(value = "开启状态")
	private Boolean openStatus;

	@ApiModelProperty(value = "文章内容")
	private String content;
}
