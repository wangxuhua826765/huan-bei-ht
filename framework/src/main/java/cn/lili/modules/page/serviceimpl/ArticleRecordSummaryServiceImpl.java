package cn.lili.modules.page.serviceimpl;

import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.UuidUtils;
import cn.lili.modules.page.entity.dos.ArticleRecordSummary;
import cn.lili.modules.page.entity.vos.ArticleRecordSummaryVO;
import cn.lili.modules.page.mapper.ArticleRecordSummaryMapper;
import cn.lili.modules.page.service.ArticleRecordSummaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class ArticleRecordSummaryServiceImpl extends ServiceImpl<ArticleRecordSummaryMapper, ArticleRecordSummary>
		implements
			ArticleRecordSummaryService {

	@Override
	public Integer AddArticleRecordSummary(ArticleRecordSummary articleRecordSummary) {
		String id = UserContext.getCurrentUser().getId();
		articleRecordSummary.setUserId(id);
		String[] ids = articleRecordSummary.getArticleId().split(",");
		for (String s : ids) {
			articleRecordSummary.setId(UuidUtils.getUUID());
			articleRecordSummary.setArticleId(s);
			this.baseMapper.insert(articleRecordSummary);
		}
		return null;
	}
}
