package cn.lili.modules.page.serviceimpl;

import cn.lili.modules.message.entity.vos.JoinBoardVO;
import cn.lili.modules.page.entity.dos.JoinBoard;
import cn.lili.modules.page.mapper.JoinBoardMapper;
import cn.lili.modules.page.service.JoinBoardService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * JoinBoardServiceImpl.
 *
 * @author Li Bing
 * @since 2022.03.27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class JoinBoardServiceImpl extends ServiceImpl<JoinBoardMapper, JoinBoard> implements JoinBoardService {

	@Override
	public IPage<JoinBoard> pageJoinBoard(JoinBoardVO joinBoardVo) {
		return baseMapper.queryByParams(PageUtil.initPage(joinBoardVo), joinBoardVo.queryWrapper());
	}
}
