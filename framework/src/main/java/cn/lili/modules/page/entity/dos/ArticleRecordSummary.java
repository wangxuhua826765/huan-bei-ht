package cn.lili.modules.page.entity.dos;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@TableName("li_article_record_summary")
@NoArgsConstructor
public class ArticleRecordSummary {

	@ApiModelProperty(value = "主键")
	private String id;

	@ApiModelProperty(value = "用户id")
	private String userId;

	@ApiModelProperty(value = "文章分类")
	private String articleType;

	@ApiModelProperty(value = "查看状态")
	private Integer viewStatus;

	@ApiModelProperty(value = "文章id")
	private String articleId;

	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public String getArticleType() {
		return articleType;
	}

	public Integer getViewStatus() {
		return viewStatus;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}

	public void setViewStatus(Integer viewStatus) {
		this.viewStatus = viewStatus;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}
}
