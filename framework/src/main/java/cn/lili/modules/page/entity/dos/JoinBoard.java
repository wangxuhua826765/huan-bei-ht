package cn.lili.modules.page.entity.dos;

import cn.lili.common.security.sensitive.Sensitive;
import cn.lili.common.security.sensitive.enums.SensitiveStrategy;
import cn.lili.modules.page.entity.enums.JoinTypeEnum;
import cn.lili.mybatis.BaseIdEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * JoinBoard. 加盟意向留言板
 * 
 * @author Li Bing
 * @since 2022.03.27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("li_join_board")
@ApiModel(value = "加盟意向留言板")
public class JoinBoard extends BaseIdEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "申请人姓名")
	private String userName;

	@ApiModelProperty(value = "手机号")
	@Length(max = 11, message = "手机号不能超过11位")
	@Sensitive(strategy = SensitiveStrategy.PHONE)
	private String mobile;

	/**
	 * 加盟意向-推广员，天使合伙人，事业合伙人， 城市合伙人
	 *
	 * @see JoinTypeEnum
	 */
	@ApiModelProperty(value = "加盟意向", allowableValues = "EXTENSION_AGENT,ANGEL_PARTNER,CAREER_PARTNER，CITY_PARTNER")
	@NotEmpty(message = "加盟意向不能为空")
	private String type;

	@NotEmpty(message = "区域编码不能为空")
	@ApiModelProperty(value = "区域编码")
	private String adCode;

	@ApiModelProperty(value = "备注")
	@Length(max = 255, message = "备注不能超过255个字符")
	private String remark;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建时间", hidden = true)
	private Date createTime;

}
