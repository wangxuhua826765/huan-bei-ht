package cn.lili.modules.page.mapper;

import cn.lili.modules.page.entity.dos.ArticleRecordSummary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ArticleRecordSummaryMapper extends BaseMapper<ArticleRecordSummary> {
}
