package cn.lili.modules.page.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class ArticleDto {

	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@ApiModelProperty(value = "文章标题")
	@Length(max = 30, message = "文章标题不能超过30个字符")
	private String title;

	@ApiModelProperty(value = "文章内容")
	@NotEmpty(message = "文章内容不能为空")
	private String content;
}
