package cn.lili.modules.page.service;

import cn.lili.modules.page.entity.dos.ArticleRecordSummary;
import cn.lili.modules.page.entity.vos.ArticleRecordSummaryVO;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ArticleRecordSummaryService extends IService<ArticleRecordSummary> {

	// 插入已经阅读的用户
	Integer AddArticleRecordSummary(ArticleRecordSummary articleRecordSummary);

}
