package cn.lili.modules.page.serviceimpl;

import cn.hutool.core.util.StrUtil;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.BeanUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.order.order.entity.vo.OrderComplaintVO;
import cn.lili.modules.page.entity.vos.ArticleRecordSummaryVO;
import cn.lili.modules.page.entity.dto.ArticleCategoryDto;
import cn.lili.modules.system.entity.dos.Region;
import cn.lili.mybatis.util.PageUtil;
import cn.lili.modules.page.entity.dos.Article;
import cn.lili.modules.page.entity.dto.ArticleSearchParams;
import cn.lili.modules.page.entity.enums.ArticleEnum;
import cn.lili.modules.page.entity.vos.ArticleVO;
import cn.lili.modules.page.mapper.ArticleMapper;
import cn.lili.modules.page.service.ArticleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 文章业务层实现
 *
 * @author Chopper
 * @since 2020/11/18 11:40 上午
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {

	@Override
	public IPage<ArticleVO> managerArticlePage(ArticleSearchParams articleSearchParams, PageVO page) {
		articleSearchParams.setSort("a.sort");
		return this.baseMapper.getArticleList(PageUtil.initPage(page), articleSearchParams.queryWrapper());
	}

	@Override
	public IPage<ArticleVO> articlePage(ArticleSearchParams articleSearchParams) {
		// articleSearchParams.setSort("a.sort");
		QueryWrapper queryWrapper = articleSearchParams.queryWrapper();
		queryWrapper.eq("open_status", true);
		queryWrapper.orderByDesc("a.create_time");
		return this.baseMapper.getArticleList(PageUtil.initPage(articleSearchParams), queryWrapper);
	}

	@Override
	public List<Article> list(String categoryId) {

		QueryWrapper<Article> queryWrapper = Wrappers.query();
		queryWrapper.eq(StringUtils.isNotBlank(categoryId), "category_id", categoryId);
		return this.list(queryWrapper);
	}

	@Override
	public Article updateArticle(Article article) {
		Article oldArticle = this.getById(article.getId());
		BeanUtil.copyProperties(article, oldArticle);
		this.updateById(oldArticle);
		// 调用已读协议客户表删除已读数据
		String id = UserContext.getCurrentUser().getId();
		this.DeleteArticleRecordSummary(id);
		return oldArticle;
	}

	@Override
	public void customRemove(String id) {
		// 判断是否为默认文章
		if (this.getById(id).getType().equals(ArticleEnum.OTHER.name())) {
			this.removeById(id);
		} else {
			throw new ServiceException(ResultCode.ARTICLE_NO_DELETION);
		}
	}

	@Override
	public Article customGet(String id) {
		return this.getById(id);
		// return this.baseMapper.selectOne(new QueryWrapper<Article>()
		// .eq("type", id));
	}

	@Override
	public Article customGetByType(String type) {
		if (!StrUtil.equals(type, ArticleEnum.OTHER.name())) {
			return this.getOne(new LambdaUpdateWrapper<Article>().eq(Article::getType, type));
		}
		return null;
	}

	@Override
	public Boolean updateArticleStatus(String id, boolean status) {
		Article article = this.getById(id);
		article.setOpenStatus(status);
		return this.updateById(article);
	}

	@Override
	public List<ArticleCategoryDto> getPlatformAbout(ArticleSearchParams articleSearchParams) {
		return this.baseMapper.getPlatformAbout(articleSearchParams);
	}

	@Override
	public Article getDetail(String id) {
		return this.baseMapper.selectById(id);
	}

	@Override
	public List<ArticleRecordSummaryVO> QueryRecord() {
		String id = UserContext.getCurrentUser().getId();
		QueryWrapper<ArticleRecordSummaryVO> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", id);
		List<ArticleRecordSummaryVO> articleRecordSummaryVOS = this.baseMapper.QueryRecord(queryWrapper);
		return articleRecordSummaryVOS;
	}

	@Override
	public Integer DeleteArticleRecordSummary(String userId) {
		return this.baseMapper.DeleteArticleRecordSummary(userId);
	}

	@Override
	public List<ArticleRecordSummaryVO> getArticleRecordSummary(String Type) {
		if (!StrUtil.equals(Type, ArticleEnum.OTHER.name())) {
			QueryWrapper wrapper = new QueryWrapper();
			wrapper.eq("type", Type);
			List<ArticleRecordSummaryVO> list = this.list(wrapper);
			return list;
		}
		return null;
	}

}