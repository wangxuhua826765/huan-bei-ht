package cn.lili.modules.page.service;

import cn.lili.modules.message.entity.vos.JoinBoardVO;
import cn.lili.modules.page.entity.dos.JoinBoard;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * JoinBoardService. 加盟意向留言板
 * 
 * @author Li Bing
 * @since 2022.03.27
 */
public interface JoinBoardService extends IService<JoinBoard> {

	/**
	 * 查询加盟申请留言列表
	 * 
	 * @param joinBoardVo
	 *            分页和查询信息
	 * @return 留言分页信息
	 */
	IPage<JoinBoard> pageJoinBoard(JoinBoardVO joinBoardVo);
}
