package cn.lili.modules.page.entity.enums;

/**
 * JoinTypeEnum.
 *
 * @author Li Bing
 * @since 2022.03.27
 */
public enum JoinTypeEnum {

	/**
	 * 推广员
	 */
	EXTENSION_AGENT,

	/**
	 * 天事合伙人
	 */
	ANGEL_PARTNER,

	/**
	 * 事业合伙人
	 */
	CAREER_PARTNER,
	/**
	 * 城市合伙人
	 */
	CITY_PARTNER;

	public String value() {
		return this.name();
	}
}
