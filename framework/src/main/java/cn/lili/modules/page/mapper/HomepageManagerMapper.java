package cn.lili.modules.page.mapper;

import cn.lili.modules.system.entity.dos.Homepage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HomepageManagerMapper extends BaseMapper<Homepage> {
}
