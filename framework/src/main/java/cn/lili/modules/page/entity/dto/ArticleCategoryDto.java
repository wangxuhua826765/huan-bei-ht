package cn.lili.modules.page.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class ArticleCategoryDto {

	@ApiModelProperty(value = "唯一标识", hidden = true)
	private String id;

	@ApiModelProperty(value = "分类名称")
	private String articleCategoryName;

	@ApiModelProperty(value = "子集")
	private List<ArticleDto> articleDtos;

}
