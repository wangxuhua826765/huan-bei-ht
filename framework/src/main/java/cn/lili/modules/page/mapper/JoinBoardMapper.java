package cn.lili.modules.page.mapper;

import cn.lili.modules.page.entity.dos.JoinBoard;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * JoinBoardMapper. 加盟意向留言板
 * 
 * @author Li Bing
 * @since 2022.03.27
 */
public interface JoinBoardMapper extends BaseMapper<JoinBoard> {

	@Select("SELECT * FROM li_join_board ${ew.customSqlSegment}")
	IPage<JoinBoard> queryByParams(Page<Object> initPage, @Param(Constants.WRAPPER) QueryWrapper<Object> queryWrapper);
}
