package cn.lili.modules.page.serviceimpl;

import cn.lili.modules.page.mapper.HomepageManagerMapper;
import cn.lili.modules.page.service.HomepageManagerService;
import cn.lili.modules.system.entity.dos.Homepage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class HomepageManagerServiceImpl extends ServiceImpl<HomepageManagerMapper, Homepage>
		implements
			HomepageManagerService {
}
