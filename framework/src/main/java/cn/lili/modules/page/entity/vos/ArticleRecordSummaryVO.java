package cn.lili.modules.page.entity.vos;

import cn.lili.modules.page.entity.dos.Article;
import cn.lili.modules.page.entity.dos.ArticleRecordSummary;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 过滤已经查看过协议的用户VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
public class ArticleRecordSummaryVO extends ArticleRecordSummary {

}
