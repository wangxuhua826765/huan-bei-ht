package cn.lili.modules.page.mapper;

import cn.lili.modules.page.entity.dos.Article;
import cn.lili.modules.page.entity.dto.ArticleCategoryDto;
import cn.lili.modules.page.entity.dto.ArticleSearchParams;
import cn.lili.modules.page.entity.vos.ArticleRecordSummaryVO;
import cn.lili.modules.page.entity.vos.ArticleVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 文章数据处理层
 *
 * @author pikachu
 * @since 2020-05-06 15:18:56
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

	/**
	 * 获取文章VO分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文章VO分页
	 */
	@Select("select a.id,a.title,a.sort,ac.article_category_name,a.open_status,a.content,a.create_time from "
			+ "li_article as a inner join li_article_category ac on a.category_id=ac.id ${ew.customSqlSegment}")
	IPage<ArticleVO> getArticleList(IPage<ArticleVO> page, @Param(Constants.WRAPPER) Wrapper<ArticleVO> queryWrapper);

	List<ArticleCategoryDto> getPlatformAbout(ArticleSearchParams articleSearchParams);

	// 查询用户是否已读
	@Select("SELECT * from li_article la INNER JOIN li_article_record_summary rs on la.type=rs.article_type and la.delete_flag=0 ${ew.customSqlSegment}")
	List<ArticleRecordSummaryVO> QueryRecord(@Param(Constants.WRAPPER) Wrapper<ArticleRecordSummaryVO> queryWrapper);

	// 插入已经阅读的用户
	@Insert("INSERT INTO li_article_record_summary(id,user_id,article_type,view_status,article_id)VALUES(#{id},#{userId},#{articleType},0,#{ArticleId})")
	Integer AddArticleRecordSummary(ArticleRecordSummaryVO articleRecordSummaryVO);

	// 删除查看就类型的数据
	@Delete("DELETE from li_article_record_summary where user_id=#{userId}")
	Integer DeleteArticleRecordSummary(String userId);

}