package cn.lili.modules.system.entity.dos;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("li_homepage")
@ApiModel("开通会员好店推荐热销商品新品新店活动专区")
public class Homepage implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	@ApiModelProperty("id")
	private String id;

	/**
	 * 图片地址
	 */
	@ApiModelProperty("图片地址")
	private String url;

	/**
	 * 图片地址
	 */
	@ApiModelProperty("图片名称")
	private String urlName;

	/**
	 * 标题名称
	 */
	@ApiModelProperty("标题名称")
	private String name;

	/**
	 * 状态（1 开启 0 关闭）
	 */
	@ApiModelProperty("状态（1 开启 0 关闭）")
	private String status;
}
