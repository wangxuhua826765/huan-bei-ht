package cn.lili.modules.payment.kit.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * 支付参数
 *
 * @author Chopper
 * @since 2020/12/19 11:46
 */
@Data
@ToString
public class PayParam {

	@ApiModelProperty(value = "订单号")
	private String uuid;

	@NotNull
	@ApiModelProperty(value = "交易类型", allowableValues = "TRADE,ORDER,RECHARGE,DUES")
	private String orderType;

	// @NotNull
	@ApiModelProperty(value = "订单号")
	private String sn;

	@NotNull
	@ApiModelProperty(value = "客户端类型")
	private String clientType;

	/**
	 * WalletOwnerEnum
	 */
	@ApiModelProperty(value = "订单来源  STORE 商家端   BUYER 用户端")
	private String owner;

	@ApiModelProperty(value = "用户id")
	private String membeId;

	@ApiModelProperty(value = "邀请码")
	private String code;

}
