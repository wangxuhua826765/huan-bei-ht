package cn.lili.modules.payment.entity.vo;

import cn.lili.modules.payment.entity.dos.RecyclingManagement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * VO
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Data
public class RecyclingManagementVO extends RecyclingManagement {

}
