package cn.lili.modules.payment.service;

import cn.lili.modules.payment.entity.dos.CombinedPayment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * create by yudan on 2022/6/29
 */
public interface CombinedPaymentService extends IService<CombinedPayment> {
}
