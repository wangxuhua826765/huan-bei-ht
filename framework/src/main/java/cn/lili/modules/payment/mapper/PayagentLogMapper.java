package cn.lili.modules.payment.mapper;

import cn.lili.modules.payment.entity.PayagentLog;
import cn.lili.modules.payment.entity.PayagentLogVO;
import cn.lili.modules.payment.entity.PaybusinessLog;
import cn.lili.modules.payment.entity.PaybusinessLogVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 退款日志数据层
 * 
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface PayagentLogMapper extends BaseMapper<PayagentLog> {
	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select a.id,a.consumption_amount,a.b_name,a.b_id,a.create_time,a.b_contact,a.b_lastconsume_time,a.b_consume_type,a.b_accountbalance,a.b_extension_no,ac.dict_name from "
			+ "li_payagent_log as a inner join li_dictionary ac on a.b_consume_type=ac.id ${ew.customSqlSegment}")
	IPage<PayagentLogVO> getPaybusinessListGroupBid(IPage<PayagentLogVO> page,
			@Param(Constants.WRAPPER) Wrapper<PayagentLogVO> queryWrapper);

	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select a.id,a.consumption_amount,a.b_name,a.b_id,a.create_time,a.b_contact,a.b_lastconsume_time,a.b_consume_type,a.b_accountbalance,a.b_extension_no,ac.dict_name from "
			+ "li_payagent_log as a inner join li_dictionary ac on a.b_consume_type=ac.id ${ew.customSqlSegment}")
	IPage<PayagentLogVO> getPaybusinessList(IPage<PayagentLogVO> page,
			@Param(Constants.WRAPPER) Wrapper<PayagentLogVO> queryWrapper);
}