package cn.lili.modules.payment.mapper;

import cn.lili.modules.payment.entity.dos.FundManagement;
import cn.lili.modules.payment.entity.vo.FundManagementVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 资金管理
 * 
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface FundManagementMapper extends BaseMapper<FundManagement> {
	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select id,name,change_amount,change_time,user_type,change_details,bus_type from "
			+ "li_fund_management  ${ew.customSqlSegment}")
	IPage<FundManagementVO> getFundManagementList(IPage<FundManagementVO> page,
			@Param(Constants.WRAPPER) Wrapper<FundManagementVO> queryWrapper);
}