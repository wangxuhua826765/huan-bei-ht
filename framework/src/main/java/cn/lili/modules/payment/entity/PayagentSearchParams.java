package cn.lili.modules.payment.entity;

import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询条件
 *
 * @author pikachu
 * @since 2020-02-24 19:27:20
 */
@Data
public class PayagentSearchParams extends PageVO {

	@ApiModelProperty(value = "商家名称")
	private String bName;

	@ApiModelProperty(value = "商家联系方式")
	private String bContact;

	@ApiModelProperty(value = "消费类型")
	private String bConsumeType;

	@ApiModelProperty(value = "商家id")
	private String bId;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		// queryWrapper.eq(StringUtils.isNotBlank(bConsumeType+""), "b_consume_type",
		// bConsumeType);
		queryWrapper.like(StringUtils.isNotBlank(bContact), "b_contact", bContact);
		queryWrapper.like(StringUtils.isNotBlank(bName), "b_name", bName);
		queryWrapper.like(StringUtils.isNotBlank(bConsumeType), "b_consume_type", bConsumeType);
		queryWrapper.eq(StringUtils.isNotBlank(bId), "b_id", bId);

		return queryWrapper;
	}
}
