package cn.lili.modules.payment.serviceimpl;

import cn.lili.modules.page.entity.dto.ArticleSearchParams;
import cn.lili.modules.page.entity.vos.ArticleVO;
import cn.lili.modules.payment.entity.PaybusinessLog;
import cn.lili.modules.payment.entity.PaybusinessLogVO;
import cn.lili.modules.payment.entity.PaybusinessSearchParams;
import cn.lili.modules.payment.entity.RefundLog;
import cn.lili.modules.payment.kit.CashierSupport;
import cn.lili.modules.payment.kit.dto.PaymentSuccessParams;
import cn.lili.modules.payment.kit.params.CashierExecute;
import cn.lili.modules.payment.mapper.PaybusinessLogMapper;
import cn.lili.modules.payment.mapper.RefundLogMapper;
import cn.lili.modules.payment.service.PaybusinessLogService;
import cn.lili.modules.payment.service.PaymentService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class PaybusinessLogServiceImpl extends ServiceImpl<PaybusinessLogMapper, PaybusinessLog>
		implements
			PaybusinessLogService {

	@Override
	public IPage<PaybusinessLogVO> paybusinessLogPage(PaybusinessSearchParams paybusinessLogParams) {

		return this.baseMapper.getPaybusinessList(PageUtil.initPage(paybusinessLogParams),
				paybusinessLogParams.queryWrapper());
	}

	@Override
	public IPage<PaybusinessLogVO> getPaybusinessListGroupBid(PaybusinessSearchParams paybusinessLogParams) {

		QueryWrapper queryWrapper = paybusinessLogParams.queryWrapper();
		queryWrapper.groupBy("b_id");

		return this.baseMapper.getPaybusinessListGroupBid(PageUtil.initPage(paybusinessLogParams), queryWrapper);
	}

}