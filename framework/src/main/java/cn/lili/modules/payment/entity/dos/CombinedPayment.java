package cn.lili.modules.payment.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/6/29
 */
@Data
@TableName("li_combined_payment")
@ApiModel(value = "组合支付明细")
@NoArgsConstructor
public class CombinedPayment extends BaseEntity {

	@ApiModelProperty(value = "用户id")
	private String memberId;

	@ApiModelProperty(value = "订单号")
	private String sn;

	@ApiModelProperty(value = "订单金额")
	private Double money;

	@ApiModelProperty(value = "手续费")
	private Double commmissionMoney;

	@ApiModelProperty(value = "运费")
	private Double colMoney;

	@ApiModelProperty(value = "钱包类型")
	private String walletType;

	@ApiModelProperty(value = "扣款金额")
	private Double walletMoney;
}
