package cn.lili.modules.payment.mapper;

import cn.lili.modules.payment.entity.dos.CombinedPayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * create by yudan on 2022/6/29
 */
public interface CombinedPaymentMapper extends BaseMapper<CombinedPayment> {
}
