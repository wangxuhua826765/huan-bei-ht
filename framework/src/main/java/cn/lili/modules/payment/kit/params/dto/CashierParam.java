package cn.lili.modules.payment.kit.params.dto;

import cn.lili.common.enums.DictCodeEnum;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.utils.SysDictUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * 支付参数
 *
 * @author Chopper
 * @since 2021-01-25 19:09
 */
@Data
@ToString
public class CashierParam {

	@ApiModelProperty(value = "价格")
	private Double price;

	@ApiModelProperty(value = "商品价格")
	private Double goodsPrice;

	@ApiModelProperty(value = "运费金额")
	private Double freightPrice;

	/**
	 * 会员价
	 */
	@ApiModelProperty("会员价")
	private Double memberPrice;

	@ApiModelProperty(value = "B2B订单佣金")
	private Double commission;

	@ApiModelProperty(value = "佣金费率")
	private Double fee = Double.parseDouble(SysDictUtils.getValueString(DictCodeEnum.ORDER_FEE.dictCode()));

	@ApiModelProperty(value = "支付title")
	private String title;

	@ApiModelProperty(value = "支付详细描述")
	private String detail;

	@ApiModelProperty(value = "订单sn集合")
	private String orderSns;

	@ApiModelProperty(value = "支持支付方式")
	private List<String> support;

	@ApiModelProperty(value = "订单创建时间")
	private Date createTime;

	@ApiModelProperty(value = "支付自动结束时间")
	private Long autoCancel;

	@ApiModelProperty(value = "充值余额")
	private Double walletValue;

	@ApiModelProperty(value = "销售余额")
	private Double walletSaleValue;

	@ApiModelProperty(value = "会员佣金余额")
	private Double walletPromoteHyValue;

	@ApiModelProperty(value = "服务佣金余额")
	private Double walletPromoteFwValue;

	@ApiModelProperty(value = "佣金余额")
	private Double walletPromoteValue;

	public String getDetail() {
		if (StringUtils.isEmpty(detail)) {
			return "清单详细";
		}
		return StringUtils.filterSpecialChart(detail);
	}
}
