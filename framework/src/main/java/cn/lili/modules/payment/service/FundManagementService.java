package cn.lili.modules.payment.service;

import cn.lili.modules.payment.entity.PayagentLog;
import cn.lili.modules.payment.entity.PayagentLogVO;
import cn.lili.modules.payment.entity.PayagentSearchParams;
import cn.lili.modules.payment.entity.dos.FundManagement;
import cn.lili.modules.payment.entity.vo.FundManagementSearchParams;
import cn.lili.modules.payment.entity.vo.FundManagementVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 退款日志 业务层
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface FundManagementService extends IService<FundManagement> {

	public IPage<FundManagementVO> fundManagementPage(FundManagementSearchParams fundManagementSearchParams);

}
