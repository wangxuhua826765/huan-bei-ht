package cn.lili.modules.payment.service;

import cn.lili.modules.payment.entity.dos.RecyclingManagement;
import cn.lili.modules.payment.entity.vo.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 回收（提现）管理
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
public interface RecyclingManagementService extends IService<RecyclingManagement> {

	public IPage<RecyclingManagementVO> recyclingManagementPage(
			RecyclingManagementSearchParams recyclingManagementSearchParams);

	public boolean updateRecyclingManagement(RecyclingManagementVO recyclingManagementVO);

}
