package cn.lili.modules.payment.service;

import cn.lili.modules.payment.entity.dos.PaymentsCode;
import cn.lili.modules.payment.entity.dos.QuotaManagement;
import cn.lili.modules.payment.entity.vo.PaymentsCodeSearchParams;
import cn.lili.modules.payment.entity.vo.PaymentsCodeVO;
import cn.lili.modules.payment.entity.vo.QuotaManagementSearchParams;
import cn.lili.modules.payment.entity.vo.QuotaManagementVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 收款码
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
public interface PaymentsCodeService extends IService<PaymentsCode> {

	/**
	 * 分页查询
	 * 
	 * @param
	 * @return
	 */
	public IPage<PaymentsCodeVO> getPaymentsCodeList(PaymentsCodeSearchParams paymentsCodeSearchParams);

}
