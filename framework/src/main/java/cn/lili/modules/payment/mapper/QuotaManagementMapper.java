package cn.lili.modules.payment.mapper;

import cn.lili.modules.payment.entity.dos.FundManagement;
import cn.lili.modules.payment.entity.dos.QuotaManagement;
import cn.lili.modules.payment.entity.vo.FundManagementVO;
import cn.lili.modules.payment.entity.vo.QuotaManagementVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 回收（提现）管理
 * 
 * @author 贾送兵
 * @since 2022-02-11 09:25
 */
public interface QuotaManagementMapper extends BaseMapper<QuotaManagement> {

	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select * from   li_quota_management  ${ew.customSqlSegment}")
	IPage<QuotaManagementVO> getQuotaManagementList(IPage<QuotaManagementVO> page,
			@Param(Constants.WRAPPER) Wrapper<QuotaManagementVO> queryWrapper);

}