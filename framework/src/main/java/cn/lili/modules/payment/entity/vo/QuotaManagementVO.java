package cn.lili.modules.payment.entity.vo;

import cn.lili.modules.payment.entity.dos.QuotaManagement;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * VO
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Data
public class QuotaManagementVO extends QuotaManagement {

}
