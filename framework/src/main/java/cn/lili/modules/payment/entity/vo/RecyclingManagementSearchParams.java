package cn.lili.modules.payment.entity.vo;

import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询条件
 *
 * @author pikachu
 * @since 2020-02-24 19:27:20
 */
@Data
public class RecyclingManagementSearchParams extends PageVO {

	@ApiModelProperty(value = "提现状态")
	private String withdrawalStatus;

	@ApiModelProperty(value = "审核状态")
	private String auditStatus;

	@ApiModelProperty(value = "提现申请人")
	private String withdrawalBy;

	@ApiModelProperty(value = "审核时间")
	private String auditTimeStart;
	private String auditTimeEnd;

	@ApiModelProperty(value = "申请时间")
	private String applicationDateStart;
	private String applicationDateEnd;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.like(StringUtils.isNotBlank(withdrawalStatus), "withdrawal_status", withdrawalStatus);
		queryWrapper.like(StringUtils.isNotBlank(auditStatus), "audit_status", auditStatus);
		queryWrapper.like(StringUtils.isNotBlank(withdrawalBy), "withdrawal_by", withdrawalBy);
		queryWrapper.between(StringUtils.isNotBlank(auditTimeStart), "audit_time", auditTimeStart, auditTimeEnd);
		queryWrapper.between(StringUtils.isNotBlank(applicationDateStart), "application_date", applicationDateStart,
				applicationDateEnd);
		return queryWrapper;
	}
}
