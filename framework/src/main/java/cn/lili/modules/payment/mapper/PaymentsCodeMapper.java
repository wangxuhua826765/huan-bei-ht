package cn.lili.modules.payment.mapper;

import cn.lili.modules.payment.entity.dos.PaymentsCode;
import cn.lili.modules.payment.entity.vo.PaymentsCodeVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 收款码管理
 * 
 * @author 贾送兵
 * @since 2022-02-11 09:25
 */
public interface PaymentsCodeMapper extends BaseMapper<PaymentsCode> {

	/**
	 * 获取分页
	 *
	 * @param page
	 *            分页
	 * @param queryWrapper
	 *            查询条件
	 * @return 文分页
	 */
	@Select("select * from   li_payments_code  ${ew.customSqlSegment}")
	IPage<PaymentsCodeVO> getPaymentsCodeVOList(IPage<PaymentsCodeVO> page,
			@Param(Constants.WRAPPER) Wrapper<PaymentsCodeVO> queryWrapper);

}