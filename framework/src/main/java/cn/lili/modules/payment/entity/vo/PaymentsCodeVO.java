package cn.lili.modules.payment.entity.vo;

import cn.lili.modules.payment.entity.dos.PaymentsCode;
import lombok.Data;

/**
 * VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class PaymentsCodeVO extends PaymentsCode {

}
