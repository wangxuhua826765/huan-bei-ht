package cn.lili.modules.payment.serviceimpl;

import cn.lili.modules.payment.entity.dos.CombinedPayment;
import cn.lili.modules.payment.mapper.CombinedPaymentMapper;
import cn.lili.modules.payment.service.CombinedPaymentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * create by yudan on 2022/6/29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CombinedPaymentServiceImpl extends ServiceImpl<CombinedPaymentMapper, CombinedPayment>
		implements
			CombinedPaymentService {

}
