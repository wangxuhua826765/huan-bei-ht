package cn.lili.modules.payment.entity.vo;

import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Data
public class QuotaManagementSearchParams extends PageVO {

	@ApiModelProperty(value = "用户类型")
	private String userType;
	@ApiModelProperty(value = "业务类型")
	private String busType;
	@ApiModelProperty(value = "商家名称")
	private String name;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.like(StringUtils.isNotBlank(busType), "bus_type", busType);
		queryWrapper.like(StringUtils.isNotBlank(name), "name", name);
		return queryWrapper;
	}
}
