package cn.lili.modules.payment.serviceimpl;

import cn.lili.modules.payment.entity.PayagentLog;
import cn.lili.modules.payment.entity.PayagentLogVO;
import cn.lili.modules.payment.entity.PayagentSearchParams;
import cn.lili.modules.payment.entity.dos.FundManagement;
import cn.lili.modules.payment.entity.vo.FundManagementSearchParams;
import cn.lili.modules.payment.entity.vo.FundManagementVO;
import cn.lili.modules.payment.mapper.FundManagementMapper;
import cn.lili.modules.payment.mapper.PayagentLogMapper;
import cn.lili.modules.payment.service.FundManagementService;
import cn.lili.modules.payment.service.PayagentLogService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class FundManagementServiceImpl extends ServiceImpl<FundManagementMapper, FundManagement>
		implements
			FundManagementService {

	@Override
	public IPage<FundManagementVO> fundManagementPage(FundManagementSearchParams fundManagementSearchParams) {

		return this.baseMapper.getFundManagementList(PageUtil.initPage(fundManagementSearchParams),
				fundManagementSearchParams.queryWrapper());
	}

}