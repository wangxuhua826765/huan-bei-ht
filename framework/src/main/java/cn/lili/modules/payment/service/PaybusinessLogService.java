package cn.lili.modules.payment.service;

import cn.lili.modules.payment.entity.PaybusinessLog;
import cn.lili.modules.payment.entity.PaybusinessLogVO;
import cn.lili.modules.payment.entity.PaybusinessSearchParams;
import cn.lili.modules.payment.entity.RefundLog;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 退款日志 业务层
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface PaybusinessLogService extends IService<PaybusinessLog> {

	public IPage<PaybusinessLogVO> paybusinessLogPage(PaybusinessSearchParams paybusinessLogParams);

	public IPage<PaybusinessLogVO> getPaybusinessListGroupBid(PaybusinessSearchParams paybusinessLogParams);

}
