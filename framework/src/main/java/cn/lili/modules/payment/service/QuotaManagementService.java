package cn.lili.modules.payment.service;

import cn.lili.modules.goods.entity.vos.BrandVO;
import cn.lili.modules.payment.entity.dos.QuotaManagement;
import cn.lili.modules.payment.entity.vo.FundManagementSearchParams;
import cn.lili.modules.payment.entity.vo.FundManagementVO;
import cn.lili.modules.payment.entity.vo.QuotaManagementSearchParams;
import cn.lili.modules.payment.entity.vo.QuotaManagementVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 限额管理
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
public interface QuotaManagementService extends IService<QuotaManagement> {

	/**
	 * 分页查询
	 * 
	 * @param quotaManagementSearchParams
	 * @return
	 */
	public IPage<QuotaManagementVO> getQuotaManagementList(QuotaManagementSearchParams quotaManagementSearchParams);

	public boolean updateQuotaManagement(QuotaManagementVO quotaManagementVO);

}
