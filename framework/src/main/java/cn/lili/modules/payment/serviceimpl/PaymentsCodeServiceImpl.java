package cn.lili.modules.payment.serviceimpl;

import cn.lili.modules.payment.entity.dos.PaymentsCode;
import cn.lili.modules.payment.entity.vo.PaymentsCodeSearchParams;
import cn.lili.modules.payment.entity.vo.PaymentsCodeVO;
import cn.lili.modules.payment.mapper.PaymentsCodeMapper;
import cn.lili.modules.payment.service.FundManagementService;
import cn.lili.modules.payment.service.PaymentsCodeService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class PaymentsCodeServiceImpl extends ServiceImpl<PaymentsCodeMapper, PaymentsCode>
		implements
			PaymentsCodeService {

	@Override
	public IPage<PaymentsCodeVO> getPaymentsCodeList(PaymentsCodeSearchParams paymentsCodeSearchParams) {
		return this.baseMapper.getPaymentsCodeVOList(PageUtil.initPage(paymentsCodeSearchParams),
				paymentsCodeSearchParams.queryWrapper());
	}

}