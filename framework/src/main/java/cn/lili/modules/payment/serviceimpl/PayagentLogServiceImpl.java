package cn.lili.modules.payment.serviceimpl;

import cn.lili.modules.payment.entity.*;
import cn.lili.modules.payment.mapper.PayagentLogMapper;
import cn.lili.modules.payment.mapper.PaybusinessLogMapper;
import cn.lili.modules.payment.service.PayagentLogService;
import cn.lili.modules.payment.service.PaybusinessLogService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付日志 业务实现
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class PayagentLogServiceImpl extends ServiceImpl<PayagentLogMapper, PayagentLog> implements PayagentLogService {

	@Override
	public IPage<PayagentLogVO> paybusinessLogPage(PayagentSearchParams payagentSearchParams) {

		return this.baseMapper.getPaybusinessList(PageUtil.initPage(payagentSearchParams),
				payagentSearchParams.queryWrapper());
	}

	@Override
	public IPage<PayagentLogVO> getPaybusinessListGroupBid(PayagentSearchParams payagentSearchParams) {

		QueryWrapper queryWrapper = payagentSearchParams.queryWrapper();
		queryWrapper.groupBy("b_id");

		return this.baseMapper.getPaybusinessListGroupBid(PageUtil.initPage(payagentSearchParams), queryWrapper);
	}

}