package cn.lili.modules.payment.entity.vo;

import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询条件
 *
 * @author pikachu
 * @since 2020-02-24 19:27:20
 */
@Data
public class FundManagementSearchParams extends PageVO {

	@ApiModelProperty(value = "用户名称")
	private String name;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.like(StringUtils.isNotBlank(name), "name", name);
		return queryWrapper;
	}
}
