package cn.lili.modules.payment.service;

import cn.lili.modules.payment.entity.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 退款日志 业务层
 *
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface PayagentLogService extends IService<PayagentLog> {

	public IPage<PayagentLogVO> paybusinessLogPage(PayagentSearchParams payagentSearchParams);

	public IPage<PayagentLogVO> getPaybusinessListGroupBid(PayagentSearchParams payagentSearchParams);

}
