package cn.lili.modules.payment.serviceimpl;

import cn.lili.modules.payment.entity.dos.RecyclingManagement;
import cn.lili.modules.payment.entity.vo.RecyclingManagementSearchParams;
import cn.lili.modules.payment.entity.vo.RecyclingManagementVO;
import cn.lili.modules.payment.mapper.RecyclingManagementMapper;
import cn.lili.modules.payment.service.RecyclingManagementService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 回收（提现）管理
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class RecyclingManagementServiceImpl extends ServiceImpl<RecyclingManagementMapper, RecyclingManagement>
		implements
			RecyclingManagementService {

	@Override
	public IPage<RecyclingManagementVO> recyclingManagementPage(
			RecyclingManagementSearchParams recyclingManagementSearchParams) {
		return this.baseMapper.getRecyclingList(PageUtil.initPage(recyclingManagementSearchParams),
				recyclingManagementSearchParams.queryWrapper());
	}

	@Override
	public boolean updateRecyclingManagement(RecyclingManagementVO recyclingManagementVO) {
		return this.updateById(recyclingManagementVO);
	}
}