package cn.lili.modules.payment.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class PaybusinessLogVO {

	@ApiModelProperty(value = "ID")
	private String id;

	@ApiModelProperty(value = "ID")
	private String bId;

	@ApiModelProperty(value = "商家名称")
	private String bName;

	@ApiModelProperty(value = "商家联系方式")
	private String bContact;

	@ApiModelProperty(value = "消费类型")
	private String bConsumeType;

	@ApiModelProperty(value = "消费类型名称")
	private String dictName;

	@ApiModelProperty(value = "账号余额")
	private Double bAccountbalance;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "推广号")
	private String bExtensionNo;

	@ApiModelProperty(value = "最后消费时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date bLastconsumeTime;
	@ApiModelProperty(value = "消费金额")
	private Double consumptionAmount;
}
