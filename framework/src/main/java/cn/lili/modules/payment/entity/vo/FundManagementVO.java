package cn.lili.modules.payment.entity.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * VO
 *
 * @author Chopper
 * @since 2021-03-26 11:32
 */
@Data
public class FundManagementVO {

	@ApiModelProperty(value = "ID")
	private String id;
	@ApiModelProperty(value = "用户名称")
	private String name;

	@ApiModelProperty(value = "变动金额")
	private String changeAmount;

	@ApiModelProperty(value = "用户类型")
	private String userType;
	@ApiModelProperty(value = "业务类型")
	private String busType;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "变动时间", hidden = true)
	private Date changeTime;

	@ApiModelProperty(value = "变动明细")
	private Double changeDetails;

}
