package cn.lili.modules.payment.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 限额管理
 *
 * @author 贾送兵
 * @since
 */
@Data
@TableName("li_quota_management")
@ApiModel(value = "限额管理")
public class QuotaManagement extends BaseEntity {

	private static final long serialVersionUID = -5339221840646353417L;

	@ApiModelProperty(value = "商家名称")
	private String name;
	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "变动时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date changeTime;
	@ApiModelProperty(value = "用户类型")
	private String userType;
	@ApiModelProperty(value = "业务类型")
	private String busType;
	@ApiModelProperty(value = "变动明细")
	private String changeDetails;

	@ApiModelProperty(value = "额度限制")
	private Double limitAmount;

	@ApiModelProperty(value = "转账路径")
	private String transferPath;
	@ApiModelProperty(value = "入口")
	private String entrance;
	@ApiModelProperty(value = "出口")
	private String export;
	@ApiModelProperty(value = "名头")
	private String renown;
	@ApiModelProperty(value = "项目")
	private String pro;
	@ApiModelProperty(value = "单笔资金量")
	private Double singleFund;
	@ApiModelProperty(value = "日资金量")
	private Double dailyFund;

	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "是否启用 true/false 启用/未启用 0启用 1不启用", hidden = true)
	private Boolean enableFlag;

}