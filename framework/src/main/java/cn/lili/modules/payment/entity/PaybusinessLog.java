package cn.lili.modules.payment.entity;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付管理-查询商家
 *
 * @author 贾送兵
 * @since
 */
@Data
@TableName("li_paybusiness_log")
@ApiModel(value = "查询商家")
public class PaybusinessLog extends BaseEntity {

	private static final long serialVersionUID = -5339221840646353416L;

	@ApiModelProperty(value = "商家id")
	private String bId;

	@ApiModelProperty(value = "商家名称")
	private String bName;

	@ApiModelProperty(value = "商家联系方式")
	private String bContact;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "最后一次消费时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date bLastconsumeTime;

	@ApiModelProperty(value = "消费类型")
	private String bConsumeType;

	@ApiModelProperty(value = "账号余额")
	private Double bAccountbalance;

	@ApiModelProperty(value = "推广号")
	private String bExtensionNo;

	@ApiModelProperty(value = "账号余额")
	private BigDecimal consumption_amount;

}