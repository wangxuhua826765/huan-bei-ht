package cn.lili.modules.payment.serviceimpl;

import cn.lili.modules.payment.entity.dos.QuotaManagement;
import cn.lili.modules.payment.entity.vo.QuotaManagementSearchParams;
import cn.lili.modules.payment.entity.vo.QuotaManagementVO;
import cn.lili.modules.payment.mapper.QuotaManagementMapper;
import cn.lili.modules.payment.service.QuotaManagementService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 限额管理
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class QuotaManagementServiceImpl extends ServiceImpl<QuotaManagementMapper, QuotaManagement>
		implements
			QuotaManagementService {

	@Override
	public IPage<QuotaManagementVO> getQuotaManagementList(QuotaManagementSearchParams quotaManagementSearchParams) {
		return this.baseMapper.getQuotaManagementList(PageUtil.initPage(quotaManagementSearchParams),
				quotaManagementSearchParams.queryWrapper());
	}

	@Override
	public boolean updateQuotaManagement(QuotaManagementVO quotaManagementVO) {
		return this.updateById(quotaManagementVO);
	}
}