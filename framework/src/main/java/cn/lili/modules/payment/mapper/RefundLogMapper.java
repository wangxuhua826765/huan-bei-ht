package cn.lili.modules.payment.mapper;

import cn.lili.modules.order.order.entity.vo.PaymentLog;
import cn.lili.modules.payment.entity.RefundLog;
import cn.lili.modules.payment.entity.vo.RefundLogVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 退款日志数据层
 * 
 * @author Chopper
 * @since 2020-12-19 09:25
 */
public interface RefundLogMapper extends BaseMapper<RefundLog> {

	@Select("select DISTINCT r.*,m.mobile from li_refund_log r " + " left join li_member m on m.id = r.member_id "
			+ " left join li_extension e on e.member_id = r.member_id " + " left join li_order o on o.sn = r.order_sn"
			+ " LEFT JOIN li_store s ON s.id = o.store_id  ${ew.customSqlSegment} ")
	// @Select("select DISTINCT o.*,m.mobile " +
	// "from li_order o" +
	// " left join li_member m on m.id = o.member_id" +
	// " left join li_extension e on e.member_id = o.member_id" +
	// " ${ew.customSqlSegment} ")
	IPage<RefundLogVO> getPageList12(IPage<RefundLog> pageVO,
			@Param(Constants.WRAPPER) Wrapper<RefundLogVO> queryWrapper);

}