package cn.lili.modules.payment.entity.vo;

import cn.lili.modules.payment.entity.RefundLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/6/8
 */
@Data
@NoArgsConstructor
@ApiModel(value = "退款订单")
public class RefundLogVO extends RefundLog {

	private static final long serialVersionUID = 5820637554656388777L;

	@ApiModelProperty(value = "手机号")
	private String mobile;

}
