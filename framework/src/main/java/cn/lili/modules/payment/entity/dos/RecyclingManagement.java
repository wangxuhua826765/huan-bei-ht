package cn.lili.modules.payment.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 回收管理
 *
 * @author 贾送兵
 * @since
 */
@Data
@TableName("li_recycling_management")
@ApiModel(value = "回收管理")
public class RecyclingManagement extends BaseEntity {

	private static final long serialVersionUID = -5339221840646353417L;

	@ApiModelProperty(value = "提现申请人")
	private String withdrawalBy;

	@ApiModelProperty(value = "提现金额")
	private Double withdrawalAmount;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	@ApiModelProperty(value = "申请日期")
	private Date applicationDate;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "审核时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date auditTime;

	@ApiModelProperty(value = "提现状态")
	private String withdrawalStatus;

	@ApiModelProperty(value = "提现说明")
	private String withdrawalRemark;

	@ApiModelProperty(value = "备注")
	private String remark;

	@ApiModelProperty(value = "审核状态")
	private String auditStatus;

	@ApiModelProperty(value = "审核人1")
	private String auditBy;
	@ApiModelProperty(value = "审核说明")
	private String auditRemark;
	@ApiModelProperty(value = "1商家端2用户端")
	private String status;

}