package cn.lili.modules.payment.entity.enums;

/**
 * 类型
 *
 * @author Chopper
 * @since 2020-12-19 11:50
 */
public enum QuotaEnum {

	ONE("用户类型1"), TWO("用户类型2"), BUSONE("业务类型1"), BUSTWO("业务类型2");// 记住要用分号结束

	private final String description;

	QuotaEnum(String str) {
		this.description = str;
	}

	public String description() {
		return description;
	}

}
