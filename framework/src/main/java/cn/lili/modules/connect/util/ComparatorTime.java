package cn.lili.modules.connect.util;

import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.dos.WalletDetail;

import java.util.Comparator;

public class ComparatorTime implements Comparator {
	/**
	 * 
	 * TODO 以对象Time判断两个list对象排序（可选）.
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object arg0, Object arg1) {
		WalletDetail cb;
		Recharge rd;
		WalletDetail cb1;
		Recharge rd1;
		if (arg0 instanceof WalletDetail) {
			cb = (WalletDetail) arg0;
			if (arg1 instanceof Recharge) {
				rd = (Recharge) arg1;
				return cb.getCreateTime().compareTo(rd.getCreateTime());
			} else {
				cb1 = (WalletDetail) arg1;
				return cb.getCreateTime().compareTo(cb1.getCreateTime());
			}
		} else {
			rd1 = (Recharge) arg0;
			if (arg1 instanceof Recharge) {
				rd = (Recharge) arg1;
				return rd1.getCreateTime().compareTo(rd.getCreateTime());
			} else {
				cb = (WalletDetail) arg1;
				return rd1.getCreateTime().compareTo(cb.getCreateTime());
			}
		}
	}

}