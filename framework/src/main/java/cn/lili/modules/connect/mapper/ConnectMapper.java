package cn.lili.modules.connect.mapper;

import cn.lili.modules.connect.entity.Connect;
import cn.lili.modules.member.entity.dos.Member;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 联合登陆数据处理层
 *
 * @author Chopper
 */
public interface ConnectMapper extends BaseMapper<Connect> {

    // 通过用户查询openid
    @Select("SELECT c.* FROM li_connect c left join li_member m on m.id = c.user_id ${ew.customSqlSegment}")
    List<Connect> findByMember(@Param(Constants.WRAPPER) Wrapper<Member> queryWrapper);
}