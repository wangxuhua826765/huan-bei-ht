package cn.lili.modules.walletAll.service;

import java.util.List;
import java.util.Map;

/**
 * 会员预存款业务层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface AllWalletService {

	List<Map> getAllWallet(String owner);

}