package cn.lili.modules.walletAll.serviceimpl;

import java.util.*;

import cn.lili.modules.walletAll.mapper.AllWalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import cn.lili.modules.walletAll.service.AllWalletService;

import lombok.extern.slf4j.Slf4j;

/**
 * 会员余额业务层实现
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class AllWalletServiceImpl implements AllWalletService {

    @Autowired
    private AllWalletMapper allWalletMapper;


	@Override
	public List<Map> getAllWallet(String owner) {
		return allWalletMapper.getAllWallet("%" + owner);
	}
}
