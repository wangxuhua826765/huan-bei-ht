package cn.lili.modules.walletAll.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AllWalletMapper{

	@Select("SELECT " +
			" ROUND(SUM( money ),5) AS money, " +
			" b.member_id  " +
			"FROM " +
			" ( " +
			" SELECT " +
			"  - SUM( money ) AS money,  " +
			"  a.member_id AS member_id  " +
			" FROM " +
			"  ( " +
			"  SELECT " +
			"   SUM( IFNULL( money, 0 ) ) AS money,  " +
			"   payee AS member_id  " +
			"  FROM " +
			"   li_wallet_detail  " +
			"  WHERE " +
			"   payee_owner like #{owner}  " +
			"  GROUP BY " +
			"   payee UNION ALL " +
			"  SELECT " +
			"   - SUM( IFNULL( money, 0 ) ) AS money,  " +
			"   payer AS member_id  " +
			"  FROM " +
			"   li_wallet_detail  " +
			"  WHERE " +
			"   payer_owner like #{owner}  " +
			"  GROUP BY " +
			"   payer  " +
			"  ) a  " +
			" GROUP BY " +
			"  a.member_id UNION ALL " +
			" SELECT " +
			"  member_wallet AS money, " +
			"  member_id AS member_id  " +
			" FROM " +
			"  li_member_wallet  " +
			" WHERE " +
			"  `owner` like #{owner} " +
			" ) b  " +
			"GROUP BY " +
			" b.member_id " +
			" HAVING money  !=0 ")
	List<Map> getAllWallet(@Param("owner") String owner);
}
