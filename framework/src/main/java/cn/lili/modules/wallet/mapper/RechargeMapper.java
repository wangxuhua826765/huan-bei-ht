package cn.lili.modules.wallet.mapper;

import cn.lili.modules.member.entity.dos.RechargeFow;
import cn.lili.modules.order.trade.entity.vo.RechargeQueryVO;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.vo.RechargeVO;
import cn.lili.modules.whitebar.entity.vo.RateSettingVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * 预存款充值记录数据处理层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface RechargeMapper extends BaseMapper<Recharge> {

	@Select(" " + "SELECT" + " ifNull(lm.mobile,'') AS mobile," + " ifNull(lm.nick_name,'') AS nickName,"
			+ " ifNull(lm.location,'') AS location," + " ifNull(lmc.NAME,'') AS typeName,"
			+ " ifNull(lr.recharge_money,0) AS rechargeMoney," + " ifNull(lr.yi_bei,0) AS huanbeiMoney,"
			+ " ifNull( DATE_FORMAT(lr.pay_time,'%Y-%m-%d %H:%i:%S') ,'') AS payTime,"
			+ " ifNull(lr.recharge_sn,'') AS rechargeSn," + " (" + " CASE" + "   lr.recharge_type " + "   WHEN '1' THEN"
			+ "   '提现' " + "   WHEN '2' THEN" + "   '充值年费会员' " + "   WHEN '3' THEN" + "   '充值会员' " + "   WHEN '4' THEN"
			+ "   '普通充值' " + "   WHEN '5' THEN" + "   '充值合伙人' " + "   WHEN '42' THEN" + "   '充值并交年费' "
			+ "   WHEN '43' THEN" + "   '充值并交银卡会员' " + "   WHEN '44' THEN" + "   '充值并交金卡会员' " + "   WHEN '45' THEN"
			+ "   '充值并成为合伙人' " + "   WHEN '46' THEN" + "   '充值并消费' " + "   WHEN '6' THEN" + "   '消费' ELSE '' "
			+ "  END " + "  ) AS rechargeTypeName ," + "  IFNULL(lro.name,'')AS roleName,"
			+ "  ifNull(lm.real_name,'') AS name," + "  ifNull(lm.id_card_number,'') AS code" + " FROM"
			+ "  li_recharge lr" + " LEFT JOIN li_member lm ON lr.member_id = lm.id"
			+ " LEFT JOIN li_grade_level lgl ON lgl.member_id = lm.id"
			+ " LEFT JOIN li_membership_card lmc ON lmc.id = lgl.grade_id"
			+ " LEFT JOIN li_partner lp ON lp.member_id = lm.id" + " LEFT JOIN li_role lro ON lro.id = lp.role_id"
			+ "   ${ew.customSqlSegment}")
	IPage<Map> getRechargeFowPage(IPage<RechargeQueryVO> page,
			@Param(Constants.WRAPPER) Wrapper<RechargeQueryVO> queryWrapper);

	@Select("select r.*,m.mobile,wd.payee_after_money" + " from li_recharge r  "
			+ " left join li_member m on m.id = r.member_id" + " left join li_wallet_detail wd on wd.sn = r.recharge_sn"
			+ " ${ew.customSqlSegment}")
	IPage<RechargeVO> findList(IPage<RechargeVO> page, @Param(Constants.WRAPPER) Wrapper<RechargeVO> queryWrapper);

	@Select("select * from li_recharge where pay_status = 'PAID' and recharge_type = 5 and  member_id = #{memberId} group by pay_time desc limit 1")
	RechargeVO getRechargeMember(String memberId);

	@Select("SELECT  IFNULL(sum(yi_bei),0) FROM li_recharge ${ew.customSqlSegment}")
	Double getByPageSum(@Param(Constants.WRAPPER) QueryWrapper<Recharge> queryWrapper);

	// 总充值现金，焕呗数
	@Select("SELECT sum( recharge_money ) recharge_money,member_id,sum( price ) price,sum( yi_bei ) yi_bei "
		+ "FROM li_recharge WHERE pay_status = 'PAID' and member_id = #{memberId}")
	Map<String ,Object> getAllMoneyByMember(@Param("memberId") String memberId);

}