package cn.lili.modules.wallet.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.order.order.entity.vo.OrderVO;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * create by yudan on 2022/3/31
 */
public interface WalletDetailService extends IService<WalletDetail> {

	IPage<WalletDetailVO> getOutlayList(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper);

	IPage<WalletDetailVO> getIncomeList(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper);

	// 查询明细
	IPage<WalletDetailVO> getList(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper);

	IPage<WalletDetailVO> getListAdminIn(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper);

	List<WalletDetailVO> getListAdminInList(QueryWrapper<WalletDetailVO> queryWrapper);

	IPage<WalletDetailVO> getListAdminOut(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper);

	List<WalletDetailVO> getListAdminOutList(QueryWrapper<WalletDetailVO> queryWrapper);

	// 查询合伙人年收益
	IPage<WalletDetailVO> findYear(PageVO page, String memberId);

	// 查询合伙人月收益
	// IPage<WalletDetailVO> findMonth(PageVO page,QueryWrapper<WalletDetailVO>
	// queryWrapper);
	IPage<WalletDetailVO> findMonth(PageVO page, String memberId, String format);

	// 查询合伙人日收益
	IPage<WalletDetailVO> findDay(PageVO page, String memberId, String format);
	// IPage<WalletDetailVO> findDay(PageVO page, QueryWrapper<WalletDetailVO>
	// queryWrapper);

	Double getCommission(QueryWrapper<WalletDetail> queryWrapper);

	IPage<WalletDetailVO> getByPage(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper, String memberId,
			String type, String owner);

	Double getByPageSum(QueryWrapper<WalletDetail> queryWrapper);

	// 根据sn查询明细
	List<WalletDetailVO> getListBySn(QueryWrapper<WalletDetailVO> queryWrapper, String memberId, String type,
			String owner);

	// 查询销售钱包明细
	IPage<OrderVO> getSaleMoney(PageVO page, QueryWrapper<OrderVO> queryWrapper);

	Double getCommissionYear(String memberId, String format);

	Double getCommissionMonth(String memberId, String format);

	Double getCommissionDay(String memberId, String format);

	Double getCommissionAll(String memberId, String format);

	//临时   查询未退款的提现订单
	List<WalletDetail> getUnRefund();
	//临时  查询转账订单
	List<WalletDetailVO> getTransfer();
}
