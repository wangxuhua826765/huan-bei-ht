package cn.lili.modules.wallet.serviceimpl;

import cn.lili.common.enums.DictCodeEnum;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.DateUtil;
import cn.lili.common.utils.SysDictUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.vo.FinanceAdminVO;
import cn.lili.modules.member.entity.vo.MemberReportSearchVO;
import cn.lili.modules.member.entity.vo.SpreadIncomeVO;
import cn.lili.modules.order.order.entity.vo.OrderVO;
import cn.lili.modules.order.order.service.OrderService;
import cn.lili.modules.payment.entity.enums.CashierEnum;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.mapper.StoreMapper;
import cn.lili.modules.wallet.entity.dos.WalletLogDetail;
import cn.lili.modules.wallet.entity.dto.WalletLogDetailSearchParams;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import cn.lili.modules.wallet.entity.vo.WalletLogDetailVO;
import cn.lili.modules.wallet.mapper.WalletLogDetailMapper;
import cn.lili.modules.wallet.mapper.WalletLogDetailMappering;
import cn.lili.modules.wallet.service.WalletLogDetailService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mchange.lang.DoubleUtils;
import org.apache.http.client.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * create by yudan on 2022/3/31
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WalletLogDetailServiceImpl extends ServiceImpl<WalletLogDetailMapper, WalletLogDetail>
		implements
			WalletLogDetailService {
	@Autowired
	private WalletLogDetailMappering walletLogDetailMappering;
	@Autowired
	private OrderService orderService;
	@Autowired
	private StoreMapper storeMapper;

	@Override
	public IPage<WalletLogDetailVO> getList(PageVO page, QueryWrapper<WalletLogDetailVO> queryWrapper) {
		IPage<WalletLogDetailVO> list = baseMapper.getList(PageUtil.initPage(page), queryWrapper);
		if (null != list.getRecords()) {
			for (WalletLogDetailVO walletLogDetailVO : list.getRecords()) {
				String storeName = null;
				Double commission = 0D;
				List<String> snList = Arrays.asList(walletLogDetailVO.getSn().split(","));
				if (CollectionUtils.isNotEmpty(snList)) {
					for (String sn : snList) {
						OrderVO bySn = orderService.getBySn(sn);
						if (null != bySn) {
							Store store = storeMapper.selectById(bySn.getStoreId());
							if (null != store) {
								storeName = (storeName != null ? storeName : "") + store.getStoreName() + ",";
								commission = commission + (bySn.getCommission() != null ? bySn.getCommission() : 0D);
							}
						}
					}
				}
				walletLogDetailVO.setStoreName(storeName);
				walletLogDetailVO.setCommission(commission);
			}
		}
		return list;
	}

	@Override
	public IPage<WalletLogDetailVO> findYear(PageVO page, String memberId) {
		return baseMapper.findYear(PageUtil.initPage(page), memberId);
	}

	@Override
	public IPage<WalletLogDetailVO> findMonth(PageVO page, String memberId) {
		return baseMapper.findMonth(PageUtil.initPage(page), memberId);
	}

	@Override
	public IPage<WalletLogDetailVO> findDay(PageVO page, String memberId) {
		return baseMapper.findDay(PageUtil.initPage(page), memberId);
	}

	@Override
	public IPage<Map<String, Object>> getCMember(PageVO page, QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getCMember(PageUtil.initPage(page), queryWrapper);
	}

	@Override
	public Map<String, Object> getCMemberAll(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getCMemberAll(queryWrapper);
	}

	@Override
	public IPage<Map<String, Object>> getBMember(PageVO page, QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getBMember(PageUtil.initPage(page), queryWrapper);
	}

	@Override
	public Map<String, Object> getBMemberAll(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getBMemberAll(queryWrapper);
	}

	@Override
	public IPage<Map<String, Object>> getTSMember(PageVO page, QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getTSMember(PageUtil.initPage(page), queryWrapper);
	}

	@Override
	public Map<String, Object> getTSMemberAll(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getTSMemberAll(queryWrapper);
	}

	@Override
	public IPage<Map<String, Object>> getSYMember(PageVO page, QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getSYMember(PageUtil.initPage(page), queryWrapper);
	}

	@Override
	public Map<String, Object> getSYMemberAll(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getSYMemberAll(queryWrapper);
	}

	@Override
	public IPage<Map<String, Object>> getQYMember(PageVO page, QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getQYMember(PageUtil.initPage(page), queryWrapper);
	}

	@Override
	public Map<String, Object> getQYMemberAll(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getQYMemberAll(queryWrapper);
	}

	@Override
	public Map<String, Object> getPTMemberAlltoHB(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getPTMemberAlltoHB(queryWrapper);
	}

	@Override
	public Map<String, Object> getPTMemberAlltoHBYE(String memberId) {
		return walletLogDetailMappering.getPTMemberAlltoHBYE(memberId);
	}

	@Override
	public Map<String, Object> getPTMemberAlltoXJ(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getPTMemberAlltoXJ(queryWrapper);
	}

	@Override
	public Map<String, Object> getPTMemberAlltoTXXJ(String memberId) {
		return walletLogDetailMappering.getPTMemberAlltoTXXJ(memberId);
	}

	@Override
	public Map<String, Object> getPTMemberAlltoKTxXj(QueryWrapper queryWrapper) {
		return walletLogDetailMappering.getPTMemberAlltoKTxXj(queryWrapper);
	}

	@Override
	public SpreadIncomeVO getSpreadIncomeAll(QueryWrapper queryWrapper, Date date) {
		QueryWrapper q1 = new QueryWrapper();
		q1 = queryWrapper;
		QueryWrapper q2 = new QueryWrapper();
		q2 = queryWrapper;
		q1.lt("a.create_time", date);
		q2.gt("a.create_time", DateUtil.getN_MONTH_Time(date, -1));
		// Map<String, Object> p1 = walletLogDetailMappering.getSpreadIncomeAll(q1);
		Map<String, Object> p2 = walletLogDetailMappering.getSpreadIncomeAll(q2);
		Map<String, Object> all = new HashMap();
		// Double czhb =
		// CurrencyUtil.sub(Double.valueOf(p2.get("czhb").toString()),
		// Double.valueOf(p1.get("czhb").toString()));
		// Double czxj =
		// CurrencyUtil.sub(Double.valueOf(p2.get("czxj").toString()),
		// Double.valueOf(p1.get("czxj").toString()));
		// Double xshb =
		// CurrencyUtil.sub(Double.valueOf(p2.get("xshb").toString()),
		// Double.valueOf(p1.get("xshb").toString()));
		// Double xsxj =
		// CurrencyUtil.sub(Double.valueOf(p2.get("xsxj").toString()),
		// Double.valueOf(p1.get("xsxj").toString()));
		Double wxCzHb = Double.valueOf(p2.get("wxCzHb").toString());
		Double wxCzXj = Double.valueOf(p2.get("wxCzXj").toString());
		Double wxTxHb = Double.valueOf(p2.get("wxTxHb").toString());
		Double wxTxXj = Double.valueOf(p2.get("wxTxXj").toString());
		all.put("wxCzHb", wxCzHb);
		all.put("wxCzXj", wxCzXj);
		all.put("wxTxHb", wxTxHb);
		all.put("wxTxXj", wxTxXj);
		SpreadIncomeVO spreadIncomeVO = new SpreadIncomeVO();
		spreadIncomeVO.setCzHb(CurrencyUtil.roundDown(Double.valueOf(all.get("wxCzHb").toString()), 2));
		spreadIncomeVO.setCzXj(CurrencyUtil.roundDown(Double.valueOf(all.get("wxCzXj").toString()), 2));
		spreadIncomeVO.setCzZk(CurrencyUtil.roundDown(CurrencyUtil.div(Double.valueOf(all.get("wxCzXj").toString()),
				Double.valueOf(all.get("wxCzHb").toString())), 2));
		spreadIncomeVO.setXsHb(CurrencyUtil.roundDown(Double.valueOf(all.get("wxTxHb").toString()), 2));
		spreadIncomeVO.setXsXj(CurrencyUtil.roundDown(Double.valueOf(all.get("wxTxXj").toString()), 2));
		spreadIncomeVO.setXsZk(CurrencyUtil.roundDown(CurrencyUtil.div(Double.valueOf(all.get("wxTxXj").toString()),
				Double.valueOf(all.get("wxTxHb").toString())), 2));

		// 总收益差异
//		Double allocateAllMoney = CurrencyUtil.sub(Double.valueOf(all.get("wxCzXj").toString()),
//				Double.valueOf(all.get("wxTxXj").toString()));
		Double allocateAllMoney =CurrencyUtil.mul(wxTxXj,
			CurrencyUtil.sub(spreadIncomeVO.getCzZk(),spreadIncomeVO.getXsZk()));
		spreadIncomeVO.setAllocateAllMoney(CurrencyUtil.roundDown(allocateAllMoney, 2));
		Double allocateMemberDiscount = Double
				.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_QY.dictCode()));
		Double allocateMemberMoney = CurrencyUtil.mul(allocateAllMoney, allocateMemberDiscount);
		Double allocateAdminDiscount = Double
				.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_PT.dictCode()));
		Double allocateAdminMoney = CurrencyUtil.mul(allocateAllMoney, allocateAdminDiscount);

		spreadIncomeVO.setAllocateMemberDiscount(CurrencyUtil.roundDown(allocateMemberDiscount, 2));
		spreadIncomeVO.setAllocateMemberMoney(CurrencyUtil.roundDown(allocateMemberMoney, 2));
		spreadIncomeVO.setAllocateAdminDiscount(CurrencyUtil.roundDown(allocateAdminDiscount, 2));
		spreadIncomeVO.setAllocateAdminMoney(CurrencyUtil.roundDown(allocateAdminMoney, 2));
		return spreadIncomeVO;
	}

}
