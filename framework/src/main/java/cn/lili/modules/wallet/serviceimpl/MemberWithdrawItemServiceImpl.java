package cn.lili.modules.wallet.serviceimpl;

import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.dos.RechargeFow;
import cn.lili.modules.order.order.entity.vo.OrderItemVO;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.wallet.entity.dos.*;
import cn.lili.modules.wallet.mapper.MemberWalletItemMapper;
import cn.lili.modules.wallet.service.*;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会员余额业务层实现
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class MemberWithdrawItemServiceImpl extends ServiceImpl<MemberWalletItemMapper, MemberWithdrawItem>
		implements
			MemberWithdrawItemService {

	@Autowired
	private RegionService regionService;

	@Autowired
	private MemberWalletItemMapper memberWalletItemMapper;

	@Override
	public void AddItem(MemberWithdrawItem messages) {
		this.baseMapper.addWalletItem(messages);
	}

	@Override
	public List<MemberWithdrawItem> findItemDtoBySn(String sn) {
		String id = UserContext.getCurrentUser().getId();
		return this.baseMapper.findItemDtoBySn(sn, id);
	}

	@Override
	public IPage<OrderItemVO> findItemBySn(PageVO pageVO, String sn) {
		IPage<OrderItemVO> itemBySn = this.baseMapper.findItemBySn(PageUtil.initPage(pageVO), sn);
		if (CollectionUtils.isNotEmpty(itemBySn.getRecords())) {
			for (OrderItemVO orderItemVO : itemBySn.getRecords()) {
				orderItemVO.setRealMoney(
						BigDecimal.valueOf(orderItemVO.getRealMoney() != null ? orderItemVO.getRealMoney() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setApplyFee(
						BigDecimal.valueOf(orderItemVO.getApplyFee() != null ? orderItemVO.getApplyFee() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setGoodsPrice(
						BigDecimal.valueOf(orderItemVO.getGoodsPrice() != null ? orderItemVO.getGoodsPrice() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setFlowPrice(
						BigDecimal.valueOf(orderItemVO.getFlowPrice() != null ? orderItemVO.getFlowPrice() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO
						.setDeposit(BigDecimal.valueOf(orderItemVO.getDeposit() != null ? orderItemVO.getDeposit() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
			}
		}
		return itemBySn;
	}

	@Override
	public IPage<OrderItemVO> findItemByOrderSn(PageVO pageVO, String sn) {
		IPage<OrderItemVO> itemBySn = this.baseMapper.findItemByOrderSn(PageUtil.initPage(pageVO), sn);
		if (CollectionUtils.isNotEmpty(itemBySn.getRecords())) {
			for (OrderItemVO orderItemVO : itemBySn.getRecords()) {
				orderItemVO.setRealMoney(
						BigDecimal.valueOf(orderItemVO.getRealMoney() != null ? orderItemVO.getRealMoney() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setApplyFee(
						BigDecimal.valueOf(orderItemVO.getApplyFee() != null ? orderItemVO.getApplyFee() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setGoodsPrice(
						BigDecimal.valueOf(orderItemVO.getGoodsPrice() != null ? orderItemVO.getGoodsPrice() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setFlowPrice(
						BigDecimal.valueOf(orderItemVO.getFlowPrice() != null ? orderItemVO.getFlowPrice() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO
						.setDeposit(BigDecimal.valueOf(orderItemVO.getDeposit() != null ? orderItemVO.getDeposit() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
			}
		}
		return itemBySn;
	}

	@Override
	public IPage<OrderItemVO> findItemByTransferSn(PageVO pageVO, String sn) {
		IPage<OrderItemVO> itemBySn = this.baseMapper.findItemByTransferSn(PageUtil.initPage(pageVO), sn);
		if (CollectionUtils.isNotEmpty(itemBySn.getRecords())) {
			for (OrderItemVO orderItemVO : itemBySn.getRecords()) {
				orderItemVO.setRealMoney(
						BigDecimal.valueOf(orderItemVO.getRealMoney() != null ? orderItemVO.getRealMoney() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setApplyFee(
						BigDecimal.valueOf(orderItemVO.getApplyFee() != null ? orderItemVO.getApplyFee() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setGoodsPrice(
						BigDecimal.valueOf(orderItemVO.getGoodsPrice() != null ? orderItemVO.getGoodsPrice() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO.setFlowPrice(
						BigDecimal.valueOf(orderItemVO.getFlowPrice() != null ? orderItemVO.getFlowPrice() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				orderItemVO
						.setDeposit(BigDecimal.valueOf(orderItemVO.getDeposit() != null ? orderItemVO.getDeposit() : 0D)
								.setScale(2, RoundingMode.DOWN).doubleValue());
				if (StringUtils.isNotEmpty(orderItemVO.getRegion())) {
					orderItemVO
							.setRegion(regionService.getItemAdCodOrName(StringUtils.toString(orderItemVO.getRegion())));
				}
			}
		}
		return itemBySn;
	}

	@Override
	public void batchUpdatemoney(String status, BigDecimal money, String id, String surplus) {
		this.baseMapper.batchUpdatemoney(status, money, id, surplus);
	}

	@Override
	public Map<String, Object> updateSaleRefund(String sn, Double price, String memberId, String type) {
		Map<String, Object> map = new HashMap<>();
		List<MemberWithdrawItem> itemDtoBySnList = memberWalletItemMapper.findItemDtoBySn(sn, memberId);// 所有需要退款的记录
		Double alreadyMoney = 0D;// 已退还现金
		Double alreadyPrice = 0D;// 已退还焕呗

		if (CollectionUtils.isNotEmpty(itemDtoBySnList)) {
			for (MemberWithdrawItem item : itemDtoBySnList) {
				// 循环出每一次的扣款记录
				// 应扣除焕呗额大于等于可扣除焕呗额，把整条记录都扣完
				alreadyMoney = CurrencyUtil.add(alreadyMoney, item.getRealMoney());// 增加已退还现金
				alreadyPrice = CurrencyUtil.add(alreadyPrice, item.getApplyMoney());// 增加已退还焕呗
				price = CurrencyUtil.sub(price, item.getApplyMoney());// 应扣除焕呗额减少

				// 修改充值明细表信息
				batchUpdatemoney("0", BigDecimal.valueOf(item.getApplyMoney()), item.getOrderItemId(),
						item.getRealMoney().toString());

			}
		}
		map.put("alreadyPrice", alreadyPrice);
		map.put("alreadyMoney", alreadyMoney);
		return map;
	}

	@Override
	public List<MemberWithdrawItem> findItemDtoBySnAll(String sn) {
		return this.baseMapper.findItemDtoBySnAll(sn);
	}

	@Override
	public List<MemberWithdrawItem> findItemDtoBySnDeliver(String sn, String memberId) {
		return this.baseMapper.findItemDtoBySnDeliver(sn, memberId);
	}
}
