package cn.lili.modules.wallet.serviceimpl;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.order.order.entity.vo.OrderVO;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import cn.lili.modules.wallet.entity.vo.WalletLogDetailVO;
import cn.lili.modules.wallet.mapper.WalletDetailMapper;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class WalletDetailServiceImpl extends ServiceImpl<WalletDetailMapper, WalletDetail>
		implements
			WalletDetailService {

	@Override
	public IPage<WalletDetailVO> getOutlayList(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper) {
		IPage<WalletDetailVO> list = baseMapper.getOutlayList(PageUtil.initPage(page), queryWrapper);
		return list;
	}

	@Override
	public IPage<WalletDetailVO> getIncomeList(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper) {
		IPage<WalletDetailVO> list = baseMapper.getIncomeList(PageUtil.initPage(page), queryWrapper);
		return list;
	}

	@Override
	public IPage<WalletDetailVO> getList(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper) {
		IPage<WalletDetailVO> list = baseMapper.getList(PageUtil.initPage(page), queryWrapper);
		return list;
	}

	@Override
	public IPage<WalletDetailVO> getListAdminIn(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper) {
		IPage<WalletDetailVO> list = baseMapper.getListAdminIn(PageUtil.initPage(page), queryWrapper);
		return list;
	}

	@Override
	public List<WalletDetailVO> getListAdminInList(QueryWrapper<WalletDetailVO> queryWrapper) {
		List<WalletDetailVO> list = baseMapper.getListAdminInList(queryWrapper);
		return list;
	}

	@Override
	public IPage<WalletDetailVO> getListAdminOut(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper) {
		IPage<WalletDetailVO> list = baseMapper.getListAdminOut(PageUtil.initPage(page), queryWrapper);
		return list;
	}

	@Override
	public List<WalletDetailVO> getListAdminOutList(QueryWrapper<WalletDetailVO> queryWrapper) {
		List<WalletDetailVO> list = baseMapper.getListAdminOutList(queryWrapper);
		return list;
	}

	@Override
	public IPage<WalletDetailVO> findYear(PageVO page, String memberId) {
		return baseMapper.findYear(PageUtil.initPage(page), memberId);
	}

	@Override
	public IPage<WalletDetailVO> findMonth(PageVO page, String memberId, String format) {
		return baseMapper.findMonth(PageUtil.initPage(page), memberId, format);
	}

	@Override
	public IPage<WalletDetailVO> findDay(PageVO page, String memberId, String format) {
		return baseMapper.findDay(PageUtil.initPage(page), memberId, format);
	}

	@Override
	public Double getCommission(QueryWrapper<WalletDetail> queryWrapper) {
		return baseMapper.getCommission(queryWrapper);
	}

	@Override
	public IPage<WalletDetailVO> getByPage(PageVO page, QueryWrapper<WalletDetailVO> queryWrapper, String memberId,
			String type, String owner) {
		queryWrapper.ne("d.commission_flag", 1);
		if (StringUtils.isNotEmpty(type) && type.equals("in")) {
			return baseMapper.getByPageIn(PageUtil.initPage(page), queryWrapper, memberId, owner);
		}
		if (StringUtils.isNotEmpty(type) && type.equals("out")) {
			return baseMapper.getByPageOut(PageUtil.initPage(page), queryWrapper, memberId, owner);
		}
		queryWrapper.ne("d.sumMoney", 0);
		return baseMapper.getByPageAll(PageUtil.initPage(page), queryWrapper, memberId, owner);
	}

	@Override
	public Double getByPageSum(QueryWrapper<WalletDetail> queryWrapper) {
		return baseMapper.getByPageSum(queryWrapper);
	}

	@Override
	public List<WalletDetailVO> getListBySn(QueryWrapper<WalletDetailVO> queryWrapper, String memberId, String type,
			String owner) {
		if (StringUtils.isNotEmpty(type) && type.equals("out")) {
			return baseMapper.getListBySnOut(queryWrapper, memberId);
		}
		if (StringUtils.isNotEmpty(type) && type.equals("in")) {
			return baseMapper.getListBySnIn(queryWrapper, memberId);
		}
		queryWrapper.ne("d.sumMoney", 0);
		return baseMapper.getListBySnAll(queryWrapper, memberId, owner);
	}

	@Override
	public IPage<OrderVO> getSaleMoney(PageVO page, QueryWrapper<OrderVO> queryWrapper) {
		return baseMapper.getSaleMoney(PageUtil.initPage(page), queryWrapper);
	}

	@Override
	public Double getCommissionAll(String memberId, String format) {
		return baseMapper.getCommissionAll(memberId, format);
	}

	@Override public List<WalletDetail> getUnRefund() {
		return baseMapper.getUnRefund();
	}
	@Override public List<WalletDetailVO> getTransfer() {
		return baseMapper.getTransfer();
	}

	@Override
	public Double getCommissionYear(String memberId, String format) {
		return baseMapper.getCommissionYear(memberId, format);
	}

	@Override
	public Double getCommissionMonth(String memberId, String format) {
		return baseMapper.getCommissionMonth(memberId, format);
	}

	@Override
	public Double getCommissionDay(String memberId, String format) {
		return baseMapper.getCommissionDay(memberId, format);
	}
}
