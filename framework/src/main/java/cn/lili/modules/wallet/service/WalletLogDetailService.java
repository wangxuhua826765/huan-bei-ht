package cn.lili.modules.wallet.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.member.entity.vo.FinanceAdminVO;
import cn.lili.modules.member.entity.vo.MemberReportSearchVO;
import cn.lili.modules.member.entity.vo.SpreadIncomeVO;
import cn.lili.modules.wallet.entity.dos.WalletLogDetail;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import cn.lili.modules.wallet.entity.vo.WalletLogDetailVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/3/31
 */
public interface WalletLogDetailService extends IService<WalletLogDetail> {

	// 查询明细
	IPage<WalletLogDetailVO> getList(PageVO page, QueryWrapper<WalletLogDetailVO> queryWrapper);

	// 查询合伙人年收益
	IPage<WalletLogDetailVO> findYear(PageVO page, String memberId);

	// 查询合伙人月收益
	IPage<WalletLogDetailVO> findMonth(PageVO page, String memberId);

	// 查询合伙人日收益
	IPage<WalletLogDetailVO> findDay(PageVO page, String memberId);

	// c端会员统计
	IPage<Map<String, Object>> getCMember(PageVO page, QueryWrapper queryWrapper);

	Map<String, Object> getCMemberAll(QueryWrapper queryWrapper);

	// b端会员统计
	IPage<Map<String, Object>> getBMember(PageVO page, QueryWrapper queryWrapper);

	Map<String, Object> getBMemberAll(QueryWrapper queryWrapper);

	// 天使端会员统计
	IPage<Map<String, Object>> getTSMember(PageVO page, QueryWrapper queryWrapper);

	Map<String, Object> getTSMemberAll(QueryWrapper queryWrapper);

	// 事业端会员统计
	IPage<Map<String, Object>> getSYMember(PageVO page, QueryWrapper queryWrapperEnd);

	Map<String, Object> getSYMemberAll(QueryWrapper queryWrapperEnd);

	// 区域端会员统计
	IPage<Map<String, Object>> getQYMember(PageVO page, QueryWrapper queryWrapper);

	Map<String, Object> getQYMemberAll(QueryWrapper queryWrapper);

	// 平台汇总数据
	Map<String, Object> getPTMemberAlltoHB(QueryWrapper queryWrapper);

	Map<String, Object> getPTMemberAlltoHBYE(String memberId);

	// 平台汇总数据
	Map<String, Object> getPTMemberAlltoXJ(QueryWrapper queryWrapper);

	Map<String, Object> getPTMemberAlltoTXXJ(String memberId);

	// 平台汇总数据应体现数据
	Map<String, Object> getPTMemberAlltoKTxXj(QueryWrapper queryWrapper);

	// 获取价差收益区域所得和平台所得
	SpreadIncomeVO getSpreadIncomeAll(QueryWrapper queryWrapper, Date searchTime);

}
