package cn.lili.modules.wallet.entity.dto;

import lombok.Data;

@Data
public class CommissionData {

	private Double money = 0D;

	private Double comMoney = 0D;

	private Double deductMoney = 0D;

	private Double deductcomMoney = 0D;

	public CommissionData() {
	};

	public CommissionData(Double money, Double comMoney) {
		this.money = money;
		this.comMoney = comMoney;
	}

	/**
	 * 扣除
	 * 
	 * @param dm
	 */
	public void deductMoney(Double dm) {
		deductMoney = deductMoney + dm;
	}

	public void deductcomMoney(Double dm) {
		deductcomMoney = deductcomMoney + dm;
	}

}
