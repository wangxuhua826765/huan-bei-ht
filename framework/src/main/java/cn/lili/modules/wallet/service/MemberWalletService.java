package cn.lili.modules.wallet.service;

import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.wallet.entity.dos.*;
import cn.lili.modules.wallet.entity.dto.MemberWalletDTO;
import cn.lili.modules.wallet.entity.dto.MemberWalletUpdateDTO;
import cn.lili.modules.wallet.entity.vo.MemberWalletSearchParams;
import cn.lili.modules.wallet.entity.vo.MemberWalletVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 会员预存款业务层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface MemberWalletService extends IService<MemberWallet> {

	/**
	 * 查询会员的预存款
	 *
	 * @param memberId
	 *            会员id
	 * @param owner
	 * @return 会员预存款VO
	 */
	MemberWalletVO getMemberWallet(String memberId, String owner);

	/**
	 * 根据会员Id查询所有钱包
	 *
	 * @param memberId
	 *            会员id
	 * @return 会员预存款VO
	 */
	List<MemberWallet> getMemberWalletById(String memberId);

	/**
	 * 增加用户预存款余额
	 *
	 * @param memberWalletUpdateDTO
	 *            变动模型
	 * @return 返回增加结果 true:成功 false:失败
	 */
	Boolean increase(MemberWalletUpdateDTO memberWalletUpdateDTO, WalletLogDetail walletLogDetail);

	/**
	 * 增加冻结款余额
	 *
	 * @param memberWalletUpdateDTO
	 *            变动模型
	 * @return 返回增加结果 true:成功 false:失败
	 */
	Boolean inFrozen(MemberWalletUpdateDTO memberWalletUpdateDTO);

	/**
	 * 从冻结金额到余额
	 *
	 * @param memberWalletUpdateDTO
	 *            变动模型
	 * @return 返回冻结结果 true:成功 false:失败
	 */
	Boolean increaseWithdrawal(MemberWalletUpdateDTO memberWalletUpdateDTO, WalletLogDetail walletLogDetail);

	/**
	 * 扣减用户预存款余额
	 *
	 * @param memberWalletUpdateDTO
	 *            变动模型
	 * @return 操作状态 true:成功 false:失败
	 */
	Boolean reduce(MemberWalletUpdateDTO memberWalletUpdateDTO, WalletLogDetail walletLogDetail);

	/**
	 * 提现扣减余额到冻结金额
	 *
	 * @param memberWalletUpdateDTO
	 *            变动模型
	 * @return 操作状态 true:成功 false:失败
	 */
	Boolean reduceWithdrawal(MemberWalletUpdateDTO memberWalletUpdateDTO, WalletLogDetail walletLogDetail);

	/**
	 * 提现扣减冻结金额
	 *
	 * @param memberWalletUpdateDTO
	 *            变动模型
	 * @return 操作状态
	 */
	Boolean reduceFrozen(MemberWalletUpdateDTO memberWalletUpdateDTO);

	/**
	 * 设置支付密码
	 *
	 * @param member
	 *            会员id
	 * @param password
	 *            支付密码
	 */
	void setMemberWalletPassword(Member member, String password);

	/**
	 * 检查当前会员是否设置过预存款密码
	 *
	 * @return 操作状态
	 */
	Boolean checkPassword();

	/**
	 * 会员注册添加会员预存款
	 *
	 * @param memberId
	 *            会员id
	 * @param memberName
	 *            会员名称
	 * @return 操作结果
	 */
	MemberWallet save(String memberId, String memberName, String owner);

	/**
	 * 用户提现
	 *
	 * @param price
	 *            提现金额
	 * @param owner
	 * @return 是否提现成功
	 */
	MemberWithdrawApply applyWithdrawal(Double price, String owner, String cardId);

	// 计算预扣税
	public Double withholdingAdvance(Double money);

	/**
	 * 提现公共方法，此方法供前端用户提现和后端提现使用
	 *
	 * @param memberWithdrawApply
	 *            会员零钱提现申请
	 * @return 操作状态
	 */
	Boolean withdrawal(MemberWithdrawApply memberWithdrawApply);

	/**
	 * 根据店铺成员id，修改解冻资金
	 * 
	 * @param memberId
	 *            成员id
	 * @param flowPrice
	 *            需要解冻的资金数额
	 * @param owner
	 *            WalletOwnerEnum
	 * @return 执行结果
	 */
	Boolean updateFrozenWallet(String memberId, Double flowPrice, String owner);

	// 平台钱包收入
	IPage<MemberWalletDTO> findMemberWalletDTO(MemberWalletSearchParams memberWalletSearchParams);

	// 商家钱包收入
	IPage<MemberWalletDTO> findMemberWalletDTOStore(MemberWalletSearchParams memberWalletSearchParams);

	// 商家钱包支出
	IPage<MemberWalletDTO> findCommissionVOStore(MemberWalletSearchParams memberWalletSearchParams);

	// 充值
	void updateWallet(Recharge recharge);

	/**
	 * 支付订单
	 * 
	 * @param sn
	 * @param from
	 * @param to
	 * @param money
	 * @param comMoney
	 * @return
	 */
	public boolean payOrder(String sn, MemberWallet from, MemberWallet to, Double money, Double colMoney,
			Double comMoney);

	// 组合支付
	public boolean combinedPayOrder(String sn, String owner, String memberId, MemberWallet to, Double money,
			Double colMoney);

	/**
	 * 授信
	 * 
	 * @param sn
	 *            订单号
	 * @param from
	 *            转出钱包
	 * @param to
	 *            转入钱包
	 * @param money
	 *            交易金额
	 * @return
	 */
	public boolean departmentOrder(String sn, MemberWallet from, MemberWallet to, Double money);

	/**
	 * 还款
	 * 
	 * @param sn
	 *            订单号
	 * @param from
	 *            转出钱包
	 * @param to
	 *            转入钱包
	 * @param money
	 *            交易金额
	 * @return
	 */
	@Transactional
	public boolean repayOrder(String sn, MemberWallet from, MemberWallet to, Double money);

	/**
	 * 提现
	 * 
	 * @param sn
	 *            订单号
	 * @param from
	 *            转出钱包
	 * @param to
	 *            转入钱包
	 * @param money
	 *            交易金额
	 * @return
	 */
	@Transactional
	public boolean withdrawApply(String sn, MemberWallet from, MemberWallet to, Double money, String remark);

	/**
	 * 提现退回
	 * 
	 * @param sn
	 *            订单号
	 * @param from
	 *            转出钱包
	 * @param to
	 *            转入钱包
	 * @param money
	 *            交易金额
	 * @return
	 */
	@Transactional
	public boolean withdrawApplyRefund(String sn, MemberWallet from, MemberWallet to, Double money, String remark);

	/**
	 * 完成订单
	 * 
	 * @param sn
	 * @param to
	 * @param money
	 * @return
	 */
	public Boolean complateOrder(String sn, MemberWallet to, Double money, Double colMoney);

	/**
	 * 退款
	 * 
	 * @param sn
	 * @return
	 */
	public Boolean cancelOrder(String sn, boolean colFlag, String afterSaleNo);
	/**
	 * 退款
	 *
	 * @param sn
	 * @return
	 */
	public Boolean confirmOrder(String sn);

	/**
	 * 扫码付款
	 * 
	 * @param sn
	 * @param from
	 * @param to
	 * @param money
	 * @param comMoney
	 * @return
	 */
	public boolean scanPay(String sn, MemberWallet from, MemberWallet to, Double money, Double comMoney);

	// 转账
	public boolean transferPay(String sn, MemberWallet from, MemberWallet to, Double money, Double comMoney,
			String type);
	// 转账组合支付
	public boolean combinedTransferPay(String sn, String owner, String memberId, MemberWallet to, Double money);

	// 扫码付款组合支付
	public boolean combinedPay(String sn, String owner, String memberId, MemberWallet to, Double money);

	public MemberWallet getMemberWalletInfo(String memberId, String owner);

	public MemberWallet getMemberWalletInfo(String memberId, String owner, String defultMemberName);

	// 充值会员
	public boolean upgradHy(String sn, MemberWallet from, Double money);

	// 充值合伙人
	public boolean upgradHhr(String sn, MemberWallet from, Double money);

	public Integer getMemberType(String memberId);

}