package cn.lili.modules.wallet.entity.vo;

import cn.lili.modules.order.order.entity.enums.OrderStatusEnum;
import cn.lili.modules.wallet.entity.dos.WalletFrozenDetail;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * create by yudan on 2022/8/24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class WalletFrozenDetailVO extends WalletFrozenDetail {

	@ApiModelProperty(value = "时间")
	private String time;
	private String endTime;
	private String startTime;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "区域")
	private String location;
	@ApiModelProperty(value = "会员地址名称")
	private String locationName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "姓名")
	private String nickName;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "姓名")
	private String cardName;

	@ApiModelProperty(value = "收益")
	private String payeeMoney;

	@ApiModelProperty(value = "支出")
	private String payerMoney;

	@ApiModelProperty(value = "订单号首字母")
	private String sn;

	@ApiModelProperty(value = "类型")
	private String type;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "提现金额")
	private Double realMoney;

	@ApiModelProperty(value = "提现对应现金")
	private Double applyMoney;

	@ApiModelProperty(value = "金额")
	private Double sumMoney;

	@ApiModelProperty(value = "明细")
	private List<WalletFrozenDetailVO> WalletFrozenDetailList;

	@ApiModelProperty(value = "支付状态 OFFLINE线下  其余线上")
	private String orderType;

	@ApiModelProperty(value = "头像")
	private String face;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "店铺联系方式")
	private String storePhone;

	@ApiModelProperty(value = "转账类型")
	private String transferType;

	@ApiModelProperty(value = "区域")
	private String region;
	private String regionId;

	@ApiModelProperty(value = "收款人级别")
	private String payeeGradeName;

	@ApiModelProperty(value = "付款人钱包")
	private String payerOwner;

	@ApiModelProperty(value = "收款人钱包")
	private String payeeOwner;

	@ApiModelProperty(value = "天使合伙人金额")
	private double tsMoney;
	@ApiModelProperty(value = "区域合伙人金额")
	private double qyMoney;
	@ApiModelProperty(value = "事业胡好人金额")
	private double syMoney;
	@ApiModelProperty(value = "平台金额")
	private double adminMoney;
	@ApiModelProperty(value = "天使合伙人昵称")
	private String tsRealName;
	@ApiModelProperty(value = "区域合伙人昵称")
	private String qyRealName;
	@ApiModelProperty(value = "事业合伙人昵称")
	private String syRealName;
	@ApiModelProperty(value = "天使焕商店铺名称")
	private String tsStoreName;
	@ApiModelProperty(value = "天使合伙人电话")
	private String tsMobile;
	@ApiModelProperty(value = "区域合伙人电话")
	private String qyMobile;
	@ApiModelProperty(value = "事业合伙人电话")
	private String syMobile;
	@ApiModelProperty(value = "订单金额")
	private double flowPrice;
	@ApiModelProperty(value = "手续费")
	private double commission;

	/**
	 * @see OrderStatusEnum
	 */
	@ApiModelProperty(value = "订单状态")
	private String orderStatus;

	@ApiModelProperty(value = "订单支付时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date paymentTime;

	@ApiModelProperty(value = "收货时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date receivingTime;

	@ApiModelProperty(value = "合伙人标识")
	private String name;
	@ApiModelProperty(value = "合伙人名称")
	private String roleName;

}
