package cn.lili.modules.wallet.entity.enums;

/**
 * 提现申请状态枚举类
 *
 * @author pikachu
 * @since 2020-11-06
 */
public enum WithdrawStatusEnum {
	/**
	 * 申请中
	 */
	APPLY("申请中"),
	/**
	 * 取消申请
	 */
	CANCEL("取消申请"),
	/**
	 * 审核成功即提现成功
	 */
	VIA_AUDITING("审核通过"),
	/**
	 * 打款中
	 */
	INPAYMENT("打款中"),
	/**
	 * 已完成
	 */
	FINISH("已完成"),
	/**
	 * 注销结算完成
	 */
	CLOSE_FINISH("注销结算完成"),
	/**
	 * 审核未通过
	 */
	FAIL_AUDITING("审核未通过");

	private String description;

	public String description() {
		return description;
	}

	WithdrawStatusEnum(String description) {
		this.description = description;
	}

}
