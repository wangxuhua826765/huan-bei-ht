package cn.lili.modules.wallet.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.order.order.entity.vo.OrderItemVO;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawApply;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawItem;
import cn.lili.modules.wallet.entity.dto.MemberWithdrawItemDTO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawItemVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 会员提现申请业务层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface MemberWithdrawItemService extends IService<MemberWithdrawItem> {

	/**
	 * 保存总金额
	 * 
	 * @param messages
	 *            消息
	 * @return
	 */
	void AddItem(MemberWithdrawItem messages);

	List<MemberWithdrawItem> findItemDtoBySn(String sn);

	// 扣率信息-提现
	IPage<OrderItemVO> findItemBySn(PageVO pageVO, String sn);

	// 扣率信息-订单
	IPage<OrderItemVO> findItemByOrderSn(PageVO pageVO, String sn);

	// 扣率信息-转账
	IPage<OrderItemVO> findItemByTransferSn(PageVO pageVO, String sn);

	void batchUpdatemoney(String status, BigDecimal money, String id, String surplus);
	// 销售钱包明细退回
	Map<String, Object> updateSaleRefund(String sn, Double price, String memberId, String type);

	List<MemberWithdrawItem> findItemDtoBySnAll(String sn);

	List<MemberWithdrawItem> findItemDtoBySnDeliver(String sn, String memberId);
}