package cn.lili.modules.wallet.entity.vo;

import cn.lili.modules.wallet.entity.dos.WalletLogDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by yudan on 2022/3/31
 */
@Data
public class WalletLogDetailVO extends WalletLogDetail {

	@ApiModelProperty(value = "时间")
	private String time;
	private String endTime;
	private String startTime;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "手续费")
	private Double commission;

	@ApiModelProperty(value = "区域")
	private String regionId;

}
