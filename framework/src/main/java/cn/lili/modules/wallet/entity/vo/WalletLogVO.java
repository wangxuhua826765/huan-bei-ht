package cn.lili.modules.wallet.entity.vo;

import cn.lili.modules.wallet.entity.dos.WalletLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by yudan on 2022/3/28
 */
@Data
public class WalletLogVO extends WalletLog {

	@ApiModelProperty(value = "商家名称")
	private String storeName;

	@ApiModelProperty(value = "推广钱包余额")
	private String promote;

	@ApiModelProperty(value = "销售钱包余额")
	private String sale;

	@ApiModelProperty(value = "手机号")
	private String mobile;
}
