package cn.lili.modules.wallet.mapper;

import cn.lili.modules.order.order.entity.vo.OrderItemVO;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.util.List;

public interface MemberWalletItemMapper extends BaseMapper<MemberWithdrawItem> {
	// 小程序查看自己提现订单明细
	@Select("SELECT * from li_member_withdraw_item where sn = #{sn} and member_id=#{memberId}")
	List<MemberWithdrawItem> findItemDtoBySn(String sn, String memberId);

	@Select("SELECT s.store_name,s.store_address_path as region,i.apply_money,i.real_money,i.fee as applyFee,"
			+ "oi.goods_id,oi.goods_name,oi.goods_price,oi.num,oi.flow_price,oi.deposit "
			+ " from li_member_withdraw_item i" + " left join li_member_withdraw_apply a on a.sn = i.sn"
			+ " left join li_store s on s.member_id = a.member_id"
			+ " left join li_order_item oi on oi.id = i.order_item_id" + " where i.sn = #{sn} order by oi.fee desc")
	IPage<OrderItemVO> findItemBySn(IPage<OrderItemVO> page, String sn);

	@Select("SELECT s.store_name,s.store_address_path as region,i.real_money,i.fee as applyFee,"
			+ "oi.goods_id,oi.goods_name,oi.goods_price,oi.num,oi.flow_price,oi.deposit "
			+ " from li_member_withdraw_item i" + " LEFT JOIN li_order o ON o.sn = i.sn"
			+ " left join li_store s on s.id = o.store_id" + " left join li_order_item oi on oi.id = i.order_item_id"
			+ " where i.sn = #{sn} order by oi.fee desc")
	IPage<OrderItemVO> findItemByOrderSn(IPage<OrderItemVO> page, String sn);

	@Select("SELECT m.nick_name,m.location AS region,i.real_money,i.fee as applyFee,"
			+ "oi.goods_id,oi.goods_name,oi.goods_price,oi.num,oi.flow_price,oi.deposit "
			+ " from li_member_withdraw_item i" + " LEFT JOIN li_transfer t ON t.sn = i.sn"
			+ " LEFT JOIN li_member m ON m.id = t.payee " + " left join li_order_item oi on oi.id = i.order_item_id"
			+ " where i.sn = #{sn} order by oi.fee desc")
	IPage<OrderItemVO> findItemByTransferSn(IPage<OrderItemVO> page, String sn);

	// 撤销提现订单
	@Update("update li_order_item set realization=#{status},surplus =surplus+#{surplus},surplus_price =surplus_price+#{money}  where id = #{id}")
	void batchUpdatemoney(String status, BigDecimal money, String id, String surplus);

	@Insert("INSERT INTO li_member_withdraw_item (id,apply_money, real_money, order_item_id, sn, fee,create_time,member_id,type) "
			+ "VALUES (#{id},#{applyMoney},#{realMoney},#{orderItemId},#{sn},#{fee},#{createTime},#{memberId},#{type})")
	void addWalletItem(MemberWithdrawItem dto);

	@Select("SELECT * from li_member_withdraw_item where sn = #{sn}")
	List<MemberWithdrawItem> findItemDtoBySnAll(String sn);

	// 不退邮费
	@Select("SELECT * from li_member_withdraw_item where sn = #{sn} and member_id=#{memberId} and type!='13''")
	List<MemberWithdrawItem> findItemDtoBySnDeliver(String sn, String memberId);
}
