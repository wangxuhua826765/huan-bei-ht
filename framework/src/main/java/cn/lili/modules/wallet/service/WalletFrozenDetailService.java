package cn.lili.modules.wallet.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.wallet.entity.dos.WalletFrozenDetail;
import cn.lili.modules.wallet.entity.dto.CommissionDistributionSale;
import cn.lili.modules.wallet.entity.vo.WalletFrozenDetailVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * create by yudan on 2022/8/24
 */
public interface WalletFrozenDetailService extends IService<WalletFrozenDetail> {

	Boolean updateBySn(String sn);

	// 获取冻结钱包分佣明细
	IPage<WalletFrozenDetailVO> getWalletFrozenDetail(QueryWrapper queryWrapper, PageVO page);

	// 退款明细
	List<WalletFrozenDetailVO> refundDetailList(String sn);
}
