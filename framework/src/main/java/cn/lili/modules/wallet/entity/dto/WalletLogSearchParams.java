package cn.lili.modules.wallet.entity.dto;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Calendar;

/**
 * create by yudan on 2022/3/28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WalletLogSearchParams extends PageVO {

	@ApiModelProperty(value = "商家名称")
	private String storeName;

	@ApiModelProperty(value = "时间")
	private String endTime;
	@ApiModelProperty(value = "时间")
	private String startTime;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq(StringUtils.isNotBlank(storeName), "s.store_name ", storeName);
		if (StringUtils.isNotBlank(startTime)) {
			queryWrapper.ge("l.create_time", startTime);
			queryWrapper.le("l.create_time", endTime);
		}
		queryWrapper.in("l.owner", WalletOwnerEnum.PROMOTE.name(), WalletOwnerEnum.PROMOTE_HY.name(),
				WalletOwnerEnum.PROMOTE_FW.name(), WalletOwnerEnum.SALE.name());
		queryWrapper.orderByDesc("l.create_time");
		return queryWrapper;
	}

}
