package cn.lili.modules.wallet.serviceimpl;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.wallet.entity.dos.WalletFrozenDetail;
import cn.lili.modules.wallet.entity.vo.WalletFrozenDetailVO;
import cn.lili.modules.wallet.mapper.WalletFrozenDetailMapper;
import cn.lili.modules.wallet.service.WalletFrozenDetailService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * create by yudan on 2022/8/24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WalletFrozenDetailServiceImpl extends ServiceImpl<WalletFrozenDetailMapper, WalletFrozenDetail>
		implements
			WalletFrozenDetailService {
	@Override
	public Boolean updateBySn(String sn) {
		return baseMapper.updateBySn(sn);
	}

	@Override
	public IPage<WalletFrozenDetailVO> getWalletFrozenDetail(QueryWrapper queryWrapper, PageVO page) {
		return baseMapper.getWalletFrozenDetail(queryWrapper, PageUtil.initPage(page));
	}

	@Override
	public List<WalletFrozenDetailVO> refundDetailList(String sn) {
		return baseMapper.refundDetailList(sn);
	}
}
