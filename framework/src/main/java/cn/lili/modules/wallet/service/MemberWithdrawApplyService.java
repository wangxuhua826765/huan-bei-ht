package cn.lili.modules.wallet.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.message.entity.dos.MemberMessage;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawApply;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawItem;
import cn.lili.modules.wallet.entity.vo.MemberSalesWithdrawVO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyQueryVO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 会员提现申请业务层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface MemberWithdrawApplyService extends IService<MemberWithdrawApply> {

	/**
	 * 平台审核提现申请，申请成功后直接扣款
	 *
	 * @param applyId
	 *            审核id
	 * @param result
	 *            审核结构
	 * @param remark
	 *            备注
	 * @return 操作状态
	 */
	Boolean audit(String applyId, String result, String remark);

	/**
	 * 提现记录列表
	 *
	 * @param pageVO
	 *            分页条件
	 * @param memberWithdrawApplyQueryVO
	 *            提现记录查询条件
	 * @return 提现记录分页
	 */
	IPage<MemberWithdrawApplyVO> getMemberWithdrawPage(PageVO pageVO,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO);

	IPage<MemberWithdrawApplyVO> getList(PageVO pageVO, MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO);

	IPage<MemberWithdrawApplyVO> getListPage(PageVO pageVO, QueryWrapper<MemberWithdrawApply> queryWrapper);

	List<MemberWithdrawApplyVO> getListAll(QueryWrapper<MemberWithdrawApply> queryWrapper);

	/**
	 * 获取当前人店铺提现金额
	 */
	IPage<MemberSalesWithdrawVO> getdeposit(PageVO pageVO, MemberSalesWithdrawVO memberWithdrawApplyQueryVO);

	void batchUpdate(String status, String id, Double surplus, Double surplusPrice);

	List<MemberSalesWithdrawVO> getFeesum(String id);

	List<MemberWithdrawApply> getList(String ids);

	/**
	 * 保存总金额
	 * 
	 * @param messages
	 *            消息
	 * @return
	 */
	boolean save(List<MemberWithdrawApply> messages);

	/**
	 * 修改审核状态
	 */
	Integer UpdateApplyStaytus(String status, String idArr);

	// 提现列表
	List<MemberWithdrawApply> queryList();

	// 提现列表
	IPage<MemberWithdrawApply> queryWithdrawApplyList(PageVO pageVO, QueryWrapper<MemberWithdrawApply> queryWrapper);

	List<MemberWithdrawApply> queryMemberWithdrawApplyList(String memberId);

	// 提现实现方法
	Map<String, Object> txList(Double totalMoney, String sn, String id, String type);

	// PC提现明细
	IPage<MemberWithdrawItem> queryItemLists(PageVO pageVO, QueryWrapper<MemberWithdrawItem> queryWrapper);

	Long saleNum();

	Long promoteNum();
}