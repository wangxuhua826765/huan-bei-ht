package cn.lili.modules.wallet.entity.vo;

import cn.lili.modules.wallet.entity.dos.MemberWithdrawApply;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "提现")
@AllArgsConstructor
@NoArgsConstructor
public class MemberWithdrawApplyVO extends MemberWithdrawApply {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "申请提现金额")
	private Double applyMoney;

	@ApiModelProperty(value = "实际提现金额")
	private Double realMoney;

	@ApiModelProperty(value = "手续费")
	private Double handlingMoney;

	@ApiModelProperty(value = "提现状态")
	private String applyStatus;

	@ApiModelProperty(value = "会员id")
	private String memberId;
	// @ApiModelProperty(value = "合伙人类型")
	// private String partnerType;

	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@ApiModelProperty(value = "审核备注")
	private String inspectRemark;

	@ApiModelProperty(value = "审核时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	private Date inspectTime;

	@ApiModelProperty(value = "创建时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

	@ApiModelProperty(value = "sn")
	private String sn;

	/**
	 * WalletOwnerEnum
	 */
	@ApiModelProperty(value = "订单来源  STORE 商家端   BUYER 用户端")
	private String owner;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "变动前金额")
	private Double payerBeforeMoney;

	@ApiModelProperty(value = "变动后金额")
	private Double payerAfterMoney;

	@ApiModelProperty(value = "人员的身份类别")
	private String partnerName;

	@ApiModelProperty(value = "区域")
	private String region;
	@ApiModelProperty(value = "区域id")
	private String regionId;

	@ApiModelProperty(value = "是否有店铺")
	private Boolean haveStore;

	// 银行信息
	@ApiModelProperty(value = "银行")
	private String bankName;
	@ApiModelProperty(value = "开户行")
	private String openAccBank;
	@ApiModelProperty(value = "账户名")
	private String accountName;
	@ApiModelProperty(value = "收款账户")
	private String cardNo;
	@ApiModelProperty(value = "电话")
	private String bankMobile;

	@ApiModelProperty(value = "银行地址")
	private String adCodeName;
	@ApiModelProperty(value = "银行行号")
	private String bankCode;

	@ApiModelProperty(value = "商家名称")
	private String storeName;

	@ApiModelProperty(value = "会员等级")
	private String gradeName;

}
