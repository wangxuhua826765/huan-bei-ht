package cn.lili.modules.wallet.entity.dos;

import cn.lili.mybatis.BaseIdEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/3/31
 */
@Data
@TableName("li_wallet_log_detail")
@ApiModel(value = "钱包变动日志关联表")
@NoArgsConstructor
public class WalletLogDetail extends BaseIdEntity {

	@CreatedBy
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建者", hidden = true)
	private String createBy;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date createTime;

	@ApiModelProperty(value = "日志表id")
	private String logId;

	@ApiModelProperty(value = "区域名称")
	private String storeAddressPath;

	@ApiModelProperty(value = "区域id")
	private String storeAddressIdPath;

	@ApiModelProperty(value = "交易类型（0 余额充值 1消费  2佣金  3销售  4 年费充值 5合伙人充值 6 提现 7 退款 8退佣金）")
	private String transactionType;

	@ApiModelProperty(value = "订单号")
	private String sn;

	@ApiModelProperty(value = "付款人id")
	private String payer;

	@ApiModelProperty(value = "付款人姓名")
	private String payerName;

	@ApiModelProperty(value = "付款人钱包")
	private String payerOwner;

	@ApiModelProperty(value = "收款人")
	private String payee;

	@ApiModelProperty(value = "收款人姓名")
	private String payeeName;

	@ApiModelProperty(value = "收款人钱包")
	private String payeeOwner;

	@ApiModelProperty(value = "充值会员等级（充值会员等级（0 普通会员  1  银卡会员   2  金卡会员  9999 商家会员））")
	private String gradeLevel;

	@ApiModelProperty(value = "是否二次激活")
	private String secondActivation;

	@ApiModelProperty(value = "金额占比")
	private String percentage;

	@ApiModelProperty(value = "交易金额")
	private Double money;

	public WalletLogDetail(String storeAddressPath, String storeAddressIdPath, String transactionType, String sn,
			String payer, String payerName, String payee, String payeeName, String gradeLevel, String secondActivation,
			String percentage, Double money) {
		this.storeAddressPath = storeAddressPath;
		this.storeAddressIdPath = storeAddressIdPath;
		this.transactionType = transactionType;
		this.sn = sn;
		this.payer = payer;
		this.payerName = payerName;
		this.payee = payee;
		this.payeeName = payeeName;
		this.gradeLevel = gradeLevel;
		this.secondActivation = secondActivation;
		this.percentage = percentage;
		this.money = money;
	}

}
