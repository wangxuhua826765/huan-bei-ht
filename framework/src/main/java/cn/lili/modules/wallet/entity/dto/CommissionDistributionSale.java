package cn.lili.modules.wallet.entity.dto;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/3/28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CommissionDistributionSale extends PageVO {

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	@ApiModelProperty(value = "订单号")
	private String sn;
	@ApiModelProperty(value = "会员电话")
	private String mobile;
	@ApiModelProperty(value = "会员昵称")
	private String realName;
	@ApiModelProperty(value = "会员地址")
	private String location;
	@ApiModelProperty(value = "会员类型")
	private String roleName;
	@ApiModelProperty(value = "会员地址名称")
	private String locationName;
	@ApiModelProperty(value = "会员级别")
	private String gradeName;
	@ApiModelProperty(value = "订单金额")
	private double flowPrice;
	@ApiModelProperty(value = "手续费")
	private double commission;
	@ApiModelProperty(value = "订单金额")
	private double goodsPrice;
	@ApiModelProperty(value = "支付类型")
	private String transactionType;
	@ApiModelProperty(value = "分佣类型")
	private String typeName;

	@ApiModelProperty(value = "天使合伙人金额")
	private double tsMoney;
	@ApiModelProperty(value = "区域合伙人金额")
	private double qyMoney;
	@ApiModelProperty(value = "事业胡好人金额")
	private double syMoney;
	@ApiModelProperty(value = "平台金额")
	private double adminMoney;
	@ApiModelProperty(value = "天使合伙人昵称")
	private String tsRealName;
	@ApiModelProperty(value = "区域合伙人昵称")
	private String qyRealName;
	@ApiModelProperty(value = "事业合伙人昵称")
	private String syRealName;
	@ApiModelProperty(value = "天使焕商店铺名称")
	private String tsStoreName;
	@ApiModelProperty(value = "天使合伙人电话")
	private String tsMobile;
	@ApiModelProperty(value = "区域合伙人电话")
	private String qyMobile;
	@ApiModelProperty(value = "事业合伙人电话")
	private String syMobile;

}
