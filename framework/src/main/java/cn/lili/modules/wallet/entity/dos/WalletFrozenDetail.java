package cn.lili.modules.wallet.entity.dos;

import cn.lili.mybatis.BaseIdEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/8/24
 */
@Data
@TableName("li_wallet_frozen_detail")
@ApiModel(value = "冻结钱包交易记录表")
@NoArgsConstructor
public class WalletFrozenDetail extends BaseIdEntity {

	@CreatedBy
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建者", hidden = true)
	private String createBy;

	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建时间", hidden = true)
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd HH:mm:ss|| yyyy/MM/dd ||epoch_millis")
	private Date createTime;

	/**
	 * DepositServiceTypeEnum
	 */
	@ApiModelProperty(value = "交易类型")
	private String transactionType;

	@ApiModelProperty(value = "订单号")
	private String sn;

	@ApiModelProperty(value = "付款人id")
	private String payer;

	@ApiModelProperty(value = "付款人姓名")
	private String payerName;

	@ApiModelProperty(value = "付款人钱包")
	private String payerOwner;

	@ApiModelProperty(value = "变动前金额")
	private Double payerBeforeMoney;

	@ApiModelProperty(value = "变动后金额")
	private Double payerAfterMoney;

	@ApiModelProperty(value = "收款人")
	private String payee;

	@ApiModelProperty(value = "收款人姓名")
	private String payeeName;

	@ApiModelProperty(value = "收款人钱包")
	private String payeeOwner;

	@ApiModelProperty(value = "收款人变动前金额")
	private Double payeeBeforeMoney;

	@ApiModelProperty(value = "收款人变动后金额")
	private Double payeeAfterMoney;

	@ApiModelProperty(value = "交易金额")
	private Double money;

	@ApiModelProperty(value = "备注")
	private String remark;

	@ApiModelProperty(value = "是否已分佣  0否1是")
	private String commissionFlag;

}
