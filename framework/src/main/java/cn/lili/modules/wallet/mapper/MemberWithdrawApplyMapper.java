package cn.lili.modules.wallet.mapper;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawApply;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawItem;
import cn.lili.modules.wallet.entity.vo.MemberSalesWithdrawVO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyQueryVO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.poi.ss.formula.functions.T;

import java.math.BigDecimal;
import java.util.List;

/**
 * 会员提现申请数据处理层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface MemberWithdrawApplyMapper extends BaseMapper<MemberWithdrawApply> {

	@Select("SELECT a.*,p.partner_type from li_member_withdraw_apply a "
			+ "left join li_partner p on p.member_id = a.member_id and p.partner_state = 0 and p.delete_flag = 0 ${ew.customSqlSegment}")
	IPage<MemberWithdrawApply> selectPage(PageVO page, @Param("ew") Wrapper<MemberWithdrawApply> queryWrapper);

	@Select("SELECT member.mobile,role.name as partnerName, wallet.payer_before_money as payerBeforeMoney,"
			+ "wallet.payer_after_money as payerAfterMoney,apply.* FROM li_member_withdraw_apply apply"
			+ " LEFT JOIN li_wallet_detail wallet ON apply.sn = wallet.sn and wallet.payer = apply.member_id "
			+ " LEFT JOIN li_partner partner ON partner.member_id = apply.member_id and partner.delete_flag = 0 and partner.partner_state= 0"
			+ " left join li_role role on role.id = partner.role_id"
			+ " LEFT JOIN li_member member on member.id = apply.member_id ${ew.customSqlSegment}")
	IPage<MemberWithdrawApplyVO> getMemberWithdrawPage(Page<Object> initPage,
			@Param(Constants.WRAPPER) QueryWrapper<MemberWithdrawApply> queryWrapper);

	@Select("SELECT member.mobile,member.have_store,role.role_name as partnerName,mc.name gradeName, member.location as regionId, wallet.payer_before_money as payerBeforeMoney,"
			+ "wallet.payer_after_money as payerAfterMoney,apply.*, " + " ifnull(bank.bank_name,'') AS bankName , "
			+ " ifnull(bank.open_acc_bank,'') AS openAccBank , " + " ifnull(bank.account_name,'') AS accountName , "
			+ " ifnull(bank.card_no,'') AS cardNo , " + " ifnull(bank.mobile,'') AS bankMobile, "
			+ " ifnull(bank.bank_code,'') AS bankCode, " + " ifnull(bank.ad_code_name,'') AS adCodeName,s.store_name "
			+ " FROM li_member_withdraw_apply apply"
			+ " LEFT JOIN li_wallet_detail wallet ON apply.sn = wallet.sn and wallet.payer = apply.member_id "
			+ " LEFT JOIN li_partner partner ON partner.member_id = apply.member_id and partner.delete_flag = 0 and partner.partner_state= 0"
			+ " left join li_role role on role.id = partner.role_id"
			+ " LEFT JOIN li_member member on member.id = apply.member_id"
			+ " left join li_grade_level b on member.id = b.member_id AND b.delete_flag = 0 left join li_membership_card mc on mc.id = b.grade_id "
			+ " LEFT JOIN li_member_bank bank ON bank.id = apply.card_id "
			+ " LEFT JOIN li_store s ON s.member_id = member.id  ${ew.customSqlSegment}")
	IPage<MemberWithdrawApplyVO> getSaleList(Page<Object> initPage,
			@Param(Constants.WRAPPER) QueryWrapper<MemberWithdrawApply> queryWrapper);

	@Select("SELECT member.mobile,member.have_store,role.role_name as partnerName, member.location as regionId, wallet.payer_before_money as payerBeforeMoney,"
			+ "wallet.payer_after_money as payerAfterMoney,apply.* FROM li_member_withdraw_apply apply"
			+ " LEFT JOIN li_wallet_detail wallet ON apply.sn = wallet.sn and wallet.payer = apply.member_id "
			+ " LEFT JOIN li_partner partner ON partner.member_id = apply.member_id and partner.delete_flag = 0 and partner.partner_state= 0"
			+ " left join li_role role on role.id = partner.role_id"
			+ " LEFT JOIN li_member member on member.id = apply.member_id "
			+ " LEFT JOIN li_store s ON s.member_id = member.id ${ew.customSqlSegment}")
	List<MemberWithdrawApplyVO> getListAll(@Param(Constants.WRAPPER) QueryWrapper<MemberWithdrawApply> queryWrapper);

	// 查询销售商品可提现金额
	@Select("SELECT id,sum( surplus ) surplus,sum( surplus_price ) surplus_price,deposit,"
			+ "fee,flow_price,store_id from (SELECT odm.id,odm.surplus,odm.surplus_price,,deposit,"
			+ "fee,odm.flow_price,store_id  FROM li_order od  INNER JOIN "
			+ "li_order_item odm where  od.sn=odm.order_sn and odm.realization='0' "
			+ " and od.order_status in ('COMPLETED','AFTERSALEFINISHED') )" + " as a   ${ew.customSqlSegment}")
	IPage<MemberSalesWithdrawVO> getdeposit(Page<Object> initPage,
			@Param(Constants.WRAPPER) Wrapper<MemberSalesWithdrawVO> queryWrapper);

	// @Update("<script>" +
	// "update li_order_item_copy1 set realization=-1 " +
	// "where id in" +
	// "<foreach collection='ids' item='item' open='(' separator=',' close=')'> " +
	// "${item}"+
	// "</foreach>" +
	// "</script>")
	// void batchUpdate(String [] ids);

	@Update("update li_order_item set realization=#{status},surplus = #{surplus},surplus_price = #{surplusPrice}  where id = #{id}")
	void batchUpdate(String status, String id, Double surplus, Double surplusPrice);

	@Select("SELECT * from (SELECT odm.id,surplus,odm.surplus_price,fee,odm.flow_price,store_id,odm.deposit "
			+ " FROM li_order od  INNER JOIN li_order_item odm "
			+ " where  od.sn=odm.order_sn and odm.realization='0' and odm.surplus > 0"
			+ "  and odm.delete_flag=0 and od.delete_flag=0 AND od.order_status IN ( 'COMPLETED', 'AFTERSALEFINISHED' ) "
			+ ") as a where a.store_id=#{storeId}  ORDER BY fee ")
	List<MemberSalesWithdrawVO> getFeesum(String storeId);

	// 根据id查询详细
	@Select("SELECT * from li_member_withdraw_apply where id in (#{ids}) and delete_flag=0")
	List<MemberWithdrawApply> getList(String ids);

	/**
	 * 修改提现状态
	 */
	@Update("update li_member_withdraw_apply set apply_status=#{status} where id=#{idArr}")
	Integer UpdateApplyStaytus(String status, String idArr);

	// 根据当前用户人查询提现详细
	@Select("SELECT * from li_member_withdraw_apply where  member_id=#{memberId} and delete_flag=0 and DATE_FORMAT(create_time,'%Y%m') = DATE_FORMAT(CURDATE(),'%Y%m')")
	List<MemberWithdrawApply> queryList(String memberId);

	@Select("SELECT * from li_member_withdraw_apply  ${ew.customSqlSegment}")
	IPage<MemberWithdrawApply> queryLists(Page<Object> initPage,
			@Param("ew") Wrapper<MemberWithdrawApply> queryWrapper);

	// 根据当前用户人查询提现详细
	@Select("SELECT * from li_member_withdraw_apply where member_id=#{memberId} and delete_flag=0")
	List<MemberWithdrawApplyVO> queryMemberWithdrawApplyList(String memberId);

	// 平台提现详情
	@Select("SELECT * from li_member_withdraw_item  ${ew.customSqlSegment}")
	IPage<MemberWithdrawItem> queryItemLists(Page<Object> initPage,
			@Param("ew") Wrapper<MemberWithdrawItem> queryWrapper);

}