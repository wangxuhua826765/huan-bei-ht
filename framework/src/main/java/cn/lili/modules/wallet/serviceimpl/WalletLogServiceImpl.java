package cn.lili.modules.wallet.serviceimpl;

import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.order.trade.entity.vo.DepositQueryVO;
import cn.lili.modules.wallet.entity.dos.WalletLog;
import cn.lili.modules.wallet.entity.dto.WalletLogSearchParams;
import cn.lili.modules.wallet.entity.vo.WalletLogVO;
import cn.lili.modules.wallet.mapper.WalletLogMapper;
import cn.lili.modules.wallet.service.WalletLogService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 预存款日志业务层实现
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WalletLogServiceImpl extends ServiceImpl<WalletLogMapper, WalletLog> implements WalletLogService {

	@Override
	public IPage<WalletLogVO> depositLogPage(PageVO page, DepositQueryVO depositQueryVO) {
		// 构建查询条件
		QueryWrapper<WalletLogVO> depositLogQueryWrapper = new QueryWrapper<>();
		// 会员名称
		depositLogQueryWrapper.like(!StringUtils.isEmpty(depositQueryVO.getMemberName()), "l.member_name",
				depositQueryVO.getMemberName());
		// 会员手机号
		depositLogQueryWrapper.like(!StringUtils.isEmpty(depositQueryVO.getMobile()), "m.mobile",
				depositQueryVO.getMobile());
		// 会员id
		depositLogQueryWrapper.eq(!StringUtils.isEmpty(depositQueryVO.getMemberId()), "l.member_id",
				depositQueryVO.getMemberId());
		// 会员来源
		depositLogQueryWrapper.eq(!StringUtils.isEmpty(depositQueryVO.getOwner()), "l.owner",
				depositQueryVO.getOwner());
		// 开始时间和结束时间
		if (!StringUtils.isEmpty(depositQueryVO.getStartDate()) && !StringUtils.isEmpty(depositQueryVO.getEndDate())) {
			Date start = cn.hutool.core.date.DateUtil.parse(depositQueryVO.getStartDate());
			Date end = cn.hutool.core.date.DateUtil.parse(depositQueryVO.getEndDate());
			depositLogQueryWrapper.between("l.create_time", start, end);
		}
		// 查询返回数据
		return this.baseMapper.findList(PageUtil.initPage(page), depositLogQueryWrapper);
	}

	@Override
	public IPage<WalletLogVO> findStoreLog(WalletLogSearchParams walletLogSearchParams) {
		return this.baseMapper.findStoreLog(PageUtil.initPage(walletLogSearchParams),
				walletLogSearchParams.queryWrapper());
	}
}