package cn.lili.modules.wallet.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 会员提现申请
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
@Data
@TableName("li_member_withdraw_apply")
@ApiModel(value = "会员提现申请")
public class MemberWithdrawApply extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "申请提现焕呗金额")
	private Double applyPrice;

	@ApiModelProperty(value = "申请提现现金额")
	private Double applyMoney;

	@ApiModelProperty(value = "实际提现现金额")
	private Double realMoney;

	@ApiModelProperty(value = "手续费")
	private Double handlingMoney;

	@ApiModelProperty(value = "提现状态")
	private String applyStatus;

	@ApiModelProperty(value = "会员id")
	private String memberId;
	// @ApiModelProperty(value = "合伙人类型")
	// private String partnerType;

	@ApiModelProperty(value = "会员名称")
	private String memberName;

	@ApiModelProperty(value = "审核备注")
	private String inspectRemark;

	@ApiModelProperty(value = "审核时间")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	private Date inspectTime;

	@ApiModelProperty(value = "sn")
	private String sn;

	/**
	 * WalletOwnerEnum
	 */
	@ApiModelProperty(value = "订单来源  STORE 商家端   BUYER 用户端")
	private String owner;

	@ApiModelProperty(value = "银行卡表主键")
	private String cardId;

	@ApiModelProperty(value = "预扣预缴税额")
	private Double withholdingAdvance;

	@ApiModelProperty(value = "收款人id")
	private String payee;

	@ApiModelProperty(value = "收款人钱包")
	private String payeeOwner;

	@ApiModelProperty(value = "折扣率")
	private Double fee;

	@ApiModelProperty(value = "提现前余额")
	private Double beforeMoney;

	@ApiModelProperty(value = "完成时间", hidden = true)
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	private Date finishTime;
}
