package cn.lili.modules.wallet.entity.enums;

/**
 * 预存款变动日志业务类型
 *
 * @author Chopper
 * @since 2020/8/20 15:39
 */

public enum DepositServiceTypeEnum {
	/**
	 * 预存款变动日志业务类型枚举
	 */
	WALLET_WITHDRAWAL("余额提现"), WALLET_WITHDRAWALREFUND("余额提现退回"), WALLET_PAY("余额支付"),

	WALLET_REFUND("余额退款"), WALLET_RECHARGE("余额充值"),

	WALLET_COMMISSION("旧数据使用"), WALLET_COMMISSIONREFUND("旧数据使用"),

	WALLET_COMMISSION_HY("佣金提成-会员费"), // 城市
	WALLET_COMMISSION_FW("佣金提成-服务费"), // 城市
	WALLET_COMMISSION_TS("佣金提成-天使"), // 天使
	WALLET_COMMISSION_SY("佣金提成-事业"), // 事业

	WALLET_COMMISSION_HY_CONFIRM("佣金提成确认-会员费"), WALLET_COMMISSION_FW_CONFIRM(
			"佣金提成确认-服务费"), WALLET_COMMISSION_TS_CONFIRM("佣金提成确认-天使"), WALLET_COMMISSION_SY_CONFIRM("佣金提成确认-事业"),

	WALLET_COMMISSION_HY_REFUND("佣金提成退款-会员费"), WALLET_COMMISSION_FW_REFUND("佣金提成退款-服务费"), WALLET_COMMISSION_TS_REFUND(
			"佣金提成退款-天使"), WALLET_COMMISSION_SY_REFUND("佣金提成退款-事业"),

	WALLET_DEPARTMENT("授信拨款"), WALLET_REPAY("授信还款"),

	WALLET_COLLECT("运费"), WALLET_COLLECT_REFUND("运费");

	private final String description;

	DepositServiceTypeEnum(String description) {
		this.description = description;
	}

	public String description() {
		return description;
	}

}
