package cn.lili.modules.wallet.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * create by yudan on 2022/7/5
 */
@Data
@TableName("li_member_withdraw_item")
@ApiModel(value = "提现明细表")
public class MemberWithdrawItem extends BaseEntity {

	// @ApiModelProperty(value = "主键")
	// private String id;

	@ApiModelProperty(value = "焕呗总额")
	private Double applyMoney;
	@ApiModelProperty(value = "人民币")
	private Double realMoney;
	@ApiModelProperty(value = "订单明细表id")
	private String orderItemId;
	@ApiModelProperty(value = "流水号")
	private String sn;
	@ApiModelProperty(value = "提现主表主键")
	private String memberId;
	@ApiModelProperty(value = "汇率")
	private Double fee;
	// @ApiModelProperty(value = "手续费")
	// private Double handlingMoney;
	@ApiModelProperty(value = "类型  11 销售-提现 12销售-订单额 13销售-运费  14销售-服务费  16销售-转账 17销售-转账手续费"
			+ "  21 推广会员-提现 22推广会员-订单额 23推广会员-运费  24推广会员-服务费 25 推广会员-佣金  26推广会员-转账  27推广会员-转账手续费")
	private String type;
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "审核时间")
	private Date inspectTime;
	// @ApiModelProperty(value = "提现状态")
	// private String applyStatus;
}
