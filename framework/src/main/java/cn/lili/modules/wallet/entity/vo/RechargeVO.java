package cn.lili.modules.wallet.entity.vo;

import cn.lili.modules.order.order.entity.enums.PayStatusEnum;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.mybatis.BaseIdEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * 预存款充值记录
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
@Data
@ApiModel(value = "充值")
@AllArgsConstructor
@NoArgsConstructor
public class RechargeVO extends Recharge {

	@ApiModelProperty(value = "用户类型  1  提现   2    充值年费会员   3    充值会员   4  普通充值   5  充值合伙人   42    充值并交年费   43   充值并交银卡会员   44 充值并交金卡会员   45 充值并成为合伙人   46 充值并消费   6 消费")
	private Integer type;

	@ApiModelProperty(value = "会员昵称")
	private String nickName;

	@ApiModelProperty(value = "手机号")
	private String mobile;
	/**
	 * 区域id
	 */
	@ApiModelProperty(value = "名称")
	private String locationName;
	/**
	 * 区域id
	 */
	@ApiModelProperty(value = "区域id")
	private String location;
	/**
	 * 区域id
	 */
	@ApiModelProperty(value = "类型名称")
	private String typeName;

	@ApiModelProperty(value = "充值后余额")
	private Double payeeAfterMoney;

}