package cn.lili.modules.wallet.entity.dto;

import cn.lili.modules.wallet.entity.dos.MemberWithdrawItem;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel(value = "提现明细表")
public class MemberWithdrawItemDTO extends MemberWithdrawItem {

}
