package cn.lili.modules.wallet.entity.vo;

import cn.hutool.core.text.CharSequenceUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Calendar;

/**
 * create by yudan on 2022/3/26
 */
@Data
public class MemberWalletSearchParams extends PageVO {

	@ApiModelProperty(value = "订单号")
	private String sn;

	@ApiModelProperty(value = "时间")
	private String endTime;
	private String startTime;

	@ApiModelProperty(value = "类型")
	private String owner;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq(StringUtils.isNotBlank(sn), "aaa.sn", sn);
		// 1，商家B2B手续费
		// 2，商家提现手续费
		// 3，用户提现手续费
		// 4，商家会员年费
		// 5，用户会员年费
		// 6，合伙人年费
		queryWrapper.eq(StringUtils.isNotBlank(owner), "aaa.owner", owner);
		queryWrapper.eq(StringUtils.isNotBlank(memberId), "aaa.member_id", memberId);
		if (StringUtils.isNotBlank(startTime)) {
			queryWrapper.ge("aaa.create_time", startTime);
			queryWrapper.le("aaa.create_time", endTime);
		} else {
			// 获取当前月份
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			String m = year + "-" + (month < 10 ? "0" + month : month);
			queryWrapper.eq("DATE_FORMAT(aaa.create_time,'%Y-%m')", m);
		}
		queryWrapper.orderByDesc("aaa.create_time");
		return queryWrapper;
	}

}
