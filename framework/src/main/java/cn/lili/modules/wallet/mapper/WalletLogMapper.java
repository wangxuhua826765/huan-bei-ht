package cn.lili.modules.wallet.mapper;

import cn.lili.modules.wallet.entity.dos.WalletLog;
import cn.lili.modules.wallet.entity.vo.WalletLogVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 预存款日志数据处理层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface WalletLogMapper extends BaseMapper<WalletLog> {

	@Select("SELECT s.store_name,w1.member_wallet promote,w2.member_wallet sale,l.owner,l.money,l.before_money,l.after_money,l.create_time,l.service_type,l.detail from li_wallet_log l"
			+ " left join li_store s on s.member_id = l.member_id"
			+ " left join li_member_wallet w1 on w1.member_id = l.member_id and w1.owner = 'PROMOTE'"
			+ " left join li_member_wallet w2 on w2.member_id = l.member_id and w2.owner = 'SALE'"
			+ " ${ew.customSqlSegment} ")
	IPage<WalletLogVO> findStoreLog(IPage<WalletLogVO> page,
			@Param(Constants.WRAPPER) Wrapper<WalletLogVO> queryWrapper);

	@Select("SELECT l.*,m.mobile from li_wallet_log l" + " left join li_member m on m.id = l.member_id"
			+ " ${ew.customSqlSegment} ")
	IPage<WalletLogVO> findList(IPage<WalletLogVO> page, @Param(Constants.WRAPPER) Wrapper<WalletLogVO> queryWrapper);

}