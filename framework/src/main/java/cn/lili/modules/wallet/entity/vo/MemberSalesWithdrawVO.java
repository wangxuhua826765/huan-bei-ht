package cn.lili.modules.wallet.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.math.BigDecimal;
import java.util.Date;

// 销售提现金额返回类
@Data
public class MemberSalesWithdrawVO implements Serializable, Comparable<MemberSalesWithdrawVO> {

	@ApiModelProperty(value = "店铺id")
	private String storeId;

	@ApiModelProperty(value = "可提现金额")
	private Double deposit;

	@ApiModelProperty(value = "折扣率")
	private Double fee;

	// @ApiModelProperty(value = "订单明细ID")
	// private String id;

	@ApiModelProperty(value = "是否提现 -1已提现，0默认未提现")
	private Integer realization;

	@ApiModelProperty(value = "明细表id")
	private String ids;

	@ApiModelProperty(value = "当前人登录人id")
	private String id;

	@ApiModelProperty(value = "可提现总焕呗金额")
	private Double surplusPrice;

	@ApiModelProperty(value = "可提现总金额")
	private Double surplus;

	@ApiModelProperty(value = "换呗金额")
	private Double flowprice;

	@ApiModelProperty(value = "换呗总金额")
	private BigDecimal flowpriceSum;
	@ApiModelProperty(value = "可提现金钱")
	private BigDecimal surplusSum;
	@ApiModelProperty("平均折扣率")
	private Double FeeSum;

	@ApiModelProperty("提现时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@ApiModelProperty("本次提现金额")
	private Object payPrice;

	@ApiModelProperty("流水号")
	private String txsn;

	@ApiModelProperty("手续费")
	@TableField(exist = false)
	private Double sxf;

	@Override
	public int compareTo(MemberSalesWithdrawVO o) {
		return 0;
	}
}
