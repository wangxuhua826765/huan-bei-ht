package cn.lili.modules.wallet.entity.vo;

import cn.lili.modules.wallet.entity.dos.WalletDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import java.util.List;

/**
 * create by yudan on 2022/5/24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class WalletDetailVO extends WalletDetail {

	@ApiModelProperty(value = "时间")
	private String time;
	private String endTime;
	private String startTime;

	@ApiModelProperty(value = "会员id")
	private String memberId;

	@ApiModelProperty(value = "区域")
	private String location;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "姓名")
	private String nickName;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "姓名")
	private String cardName;

	@ApiModelProperty(value = "收益")
	private String payeeMoney;

	@ApiModelProperty(value = "支出")
	private String payerMoney;

	@ApiModelProperty(value = "订单号首字母")
	private String sn;

	@ApiModelProperty(value = "类型")
	private String type;

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "提现金额")
	private Double realMoney;

	@ApiModelProperty(value = "提现对应现金")
	private Double applyMoney;

	@ApiModelProperty(value = "金额")
	private Double sumMoney;

	@ApiModelProperty(value = "明细")
	private List<WalletDetailVO> WalletDetailList;

	@ApiModelProperty(value = "支付状态 OFFLINE线下  其余线上")
	private String orderType;

	@ApiModelProperty(value = "头像")
	private String face;

	@ApiModelProperty(value = "店铺名称")
	private String storeName;

	@ApiModelProperty(value = "店铺logo")
	private String storeLogo;

	@ApiModelProperty(value = "店铺联系方式")
	private String storePhone;

	@ApiModelProperty(value = "转账类型")
	private String transferType;

	@ApiModelProperty(value = "区域")
	private String region;
	private String regionId;

	@ApiModelProperty(value = "收款人级别")
	private String payeeGradeName;

	@ApiModelProperty(value = "付款人钱包")
	private String payerOwner;

	@ApiModelProperty(value = "收款人钱包")
	private String payeeOwner;
}
