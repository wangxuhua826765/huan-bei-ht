package cn.lili.modules.wallet.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by yudan on 2022/3/26
 */
@Data
@ApiModel(value = "钱包")
@NoArgsConstructor
@AllArgsConstructor
public class MemberWalletDTO {

	@ApiModelProperty(value = "订单号")
	private String sn;

	@ApiModelProperty(value = "金额")
	private Double money;

	@ApiModelProperty(value = "创建时间")
	private String createTime;

	@ApiModelProperty(value = "订单类型")
	private String owner;

}
