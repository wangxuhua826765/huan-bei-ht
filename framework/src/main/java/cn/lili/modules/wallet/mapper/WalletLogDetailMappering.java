package cn.lili.modules.wallet.mapper;

import cn.lili.modules.member.entity.vo.FinanceAdminVO;
import cn.lili.modules.member.entity.vo.SpreadIncomeVO;
import cn.lili.modules.wallet.entity.dos.WalletLogDetail;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/4/22
 */
@Mapper
public interface WalletLogDetailMappering extends BaseMapper<WalletLogDetail> {

	// c端会员统计
	IPage<Map<String, Object>> getCMember(IPage<FinanceAdminVO> page,
			@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	Map<String, Object> getCMemberAll(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	// B端会员统计
	IPage<Map<String, Object>> getBMember(IPage<FinanceAdminVO> page,
			@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	Map<String, Object> getBMemberAll(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	// 天使端数据统计
	IPage<Map<String, Object>> getTSMember(Page<Object> initPage,
			@Param(Constants.WRAPPER) Wrapper<Object> queryWrapper);

	Map<String, Object> getTSMemberAll(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	// 事业端数据统计
	IPage<Map<String, Object>> getSYMember(Page<Object> initPage,
			@Param(Constants.WRAPPER) Wrapper<Object> queryWrapper);

	Map<String, Object> getSYMemberAll(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	// 区域端数据统计
	IPage<Map<String, Object>> getQYMember(Page<Object> initPage,
			@Param(Constants.WRAPPER) Wrapper<Object> queryWrapper);

	Map<String, Object> getQYMemberAll(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	// 平台汇总信息 焕贝信息
	Map<String, Object> getPTMemberAlltoHB(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	Map<String, Object> getPTMemberAlltoHBYE(String memberId);
	// 平台汇总信息 现金信息

	Map<String, Object> getPTMemberAlltoXJ(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	Map<String, Object> getPTMemberAlltoTXXJ(String memberId);

	// 平台汇总信息 可以提现现金
	Map<String, Object> getPTMemberAlltoKTxXj(@Param(Constants.WRAPPER) Wrapper<FinanceAdminVO> queryWrapper);

	// 平台汇总信息 价差收益
	Map<String, Object> getSpreadIncomeAll(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
