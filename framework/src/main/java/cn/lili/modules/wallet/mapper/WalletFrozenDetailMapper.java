package cn.lili.modules.wallet.mapper;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.wallet.entity.dos.WalletFrozenDetail;
import cn.lili.modules.wallet.entity.vo.WalletFrozenDetailVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;

import java.util.List;

/**
 * create by yudan on 2022/8/24
 */
public interface WalletFrozenDetailMapper extends BaseMapper<WalletFrozenDetail> {

	@Update("update li_wallet_frozen_detail set commission_flag = 1 where sn = #{sn}")
	Boolean updateBySn(@Param("sn") String sn);

	// 获取冻结钱包分佣明细
	@Select("SELECT a.createTime AS createTime, a.sn AS sn, a.mobile AS mobile, "
			+ " a.nick_name AS realName, a.location AS location, ifnull(lr.role_name,'') AS roleName, "
			+ " a.NAME AS gradeName, RoundDown(a.flow_price,2) AS flowPrice, "
			+ " RoundDown(a.commission,2) AS commission, a.transaction_type AS transactionType, "
			+ " a.typeName AS typeName,a.order_status,a.receiving_time,a.payment_time, RoundDown(sum( a.tsMoney ),2) AS tsMoney, "
			+ " RoundDown(sum( a.qyMoney ),2) AS qyMoney, RoundDown(sum( a.syMoney ),2) AS syMoney, "
			+ " RoundDown(sum( a.adminMoney ),2) AS adminMoney, "
			+ " GROUP_CONCAT( DISTINCT IFNULL( a.tsRealName, '' ) SEPARATOR '' ) AS tsRealName, "
			+ " GROUP_CONCAT( DISTINCT IFNULL( a.qyRealName, '' ) SEPARATOR '' ) AS qyRealName, "
			+ " GROUP_CONCAT( DISTINCT IFNULL( a.syRealName, '' ) SEPARATOR '' ) AS syRealName, "
			+ " GROUP_CONCAT( DISTINCT IFNULL( a.tsMobile, '' ) SEPARATOR '' ) AS tsMobile, "
			+ " GROUP_CONCAT( DISTINCT IFNULL( a.qyMobile, '' ) SEPARATOR '' ) AS qyMobile, "
			+ " GROUP_CONCAT( DISTINCT IFNULL( a.syMobile, '' ) SEPARATOR '' ) AS syMobile, "
			+ " GROUP_CONCAT( DISTINCT IFNULL( a.tsStoreName, '' ) SEPARATOR '' ) AS tsStoreName,a.store_name FROM ( "
			+ " SELECT lo.create_time AS createTime, lo.sn, lm.id, lm.mobile, "
			+ " lm.nick_name, lm.location, lmc.NAME, lo.goods_price AS flow_price, "
			+ " lo.commission, lwd.transaction_type, '订单费' AS typeName,lo.order_status,lo.receiving_time,lo.payment_time, "
			+ " IF ( lwd.transaction_type = 'WALLET_COMMISSION_TS_REFUND', - abs( lwd.money ), 0 ) +IF( lwd.transaction_type = 'WALLET_COMMISSION_TS', lwd.money, 0 ) tsMoney, "
			+ " IF ( lwd.transaction_type = 'WALLET_COMMISSION_SY_REFUND', - abs( lwd.money ), 0 ) +IF( lwd.transaction_type = 'WALLET_COMMISSION_SY', lwd.money, 0 ) syMoney, "
			+ " IF ( lwd.payer <> 'admin' AND lwd.payee <> 'admin' AND lwd.transaction_type IN ( 'WALLET_COMMISSION_TS', 'WALLET_COMMISSION_SY' ),- abs( lwd.money ), 0 ) + IF ( lwd.transaction_type IN ( 'WALLET_COMMISSION_FW', 'WALLET_COMMISSION_TS_REFUND', 'WALLET_COMMISSION_SY_REFUND' ) AND lwd.payee <> 'admin' , lwd.money, 0 ) qyMoney, "
			+ " IF ( lwd.payer = 'admin', - abs( lwd.money ), 0 ) + IF ( lwd.payee = 'admin', lwd.money, 0 ) AS adminMoney, "
			+ " IF ( lwd.transaction_type = 'WALLET_COMMISSION_TS', lm1.nick_name, NULL ) tsRealName, "
			+ " IF ( lwd.payer <> 'admin' AND lwd.payee <> 'admin' AND lwd.transaction_type NOT LIKE '%REFUND' AND lwd.transaction_type IN ( 'WALLET_COMMISSION_TS', 'WALLET_COMMISSION_SY' ), "
			+ "lm2.nick_name,IF ( lwd.transaction_type = 'WALLET_COMMISSION_FW' AND lwd.payee <> 'admin', lm1.nick_name, NULL ) ) qyRealName, "
			+ " IF ( lwd.transaction_type = 'WALLET_COMMISSION_SY', lm1.nick_name, NULL ) syRealName, "
			+ " IF ( lwd.transaction_type = 'WALLET_COMMISSION_TS', ls.store_name, NULL ) tsStoreName, ls.store_name,"
			+ " IF ( lwd.transaction_type = 'WALLET_COMMISSION_TS', lm1.mobile, NULL ) tsMobile, "
			+ " IF ( lwd.payer <> 'admin' AND lwd.payee <> 'admin' AND lwd.transaction_type NOT LIKE '%_REFUND' AND lwd.transaction_type IN ( 'WALLET_COMMISSION_TS', 'WALLET_COMMISSION_SY' ), "
			+ "lm2.mobile, IF ( lwd.transaction_type = 'WALLET_COMMISSION_FW' AND lwd.payee <> 'admin', lm1.mobile, NULL ) ) qyMobile, "
			+ " IF ( lwd.transaction_type = 'WALLET_COMMISSION_SY', lm1.mobile, NULL ) syMobile FROM "
			+ " li_order lo LEFT JOIN li_member lm ON lo.member_id = lm.id "
			+ " LEFT JOIN li_grade_level lgl ON lgl.member_id = lm.id and lgl.delete_flag = 0 "
			+ " LEFT JOIN li_membership_card lmc ON lmc.id = lgl.grade_id "
			+ " LEFT JOIN li_wallet_frozen_detail lwd ON lo.sn = lwd.sn AND lwd.transaction_type <> 'WALLET_PAY' "
			+ " LEFT JOIN li_member lm1 ON lwd.payee = lm1.id LEFT JOIN li_member lm2 ON lwd.payer = lm2.id "
			+ " LEFT JOIN li_store ls ON lo.store_id = ls.id WHERE lo.pay_status = 'PAID' "
			+ " AND lwd.commission_flag = 0 AND lm.id is not null " + " ) a "
			+ " LEFT JOIN li_partner lp on lp.member_id = a.id AND lp.partner_state = '0' AND lp.delete_flag = '0' and lp.audit_state = 'PASS' "
			+ " LEFT JOIN li_role lr on lr.id = lp.role_id ${ew.customSqlSegment}")
	IPage<WalletFrozenDetailVO> getWalletFrozenDetail(@Param(Constants.WRAPPER) QueryWrapper queryWrapper,
			Page<Object> initPage);

	// 退款明细
	@Select("SELECT sum(a.money) sumMoney,a.sn, a.member_id, a.member_name nickName,m.mobile,s.store_name,"
			+ "ifnull(r.name, '平台' ) AS name,ifnull(r.role_name, '平台' ) AS roleName from ("
			+ "SELECT sum(-d.money) money,"
			+ "d.transaction_type,LEFT ( d.sn, 1 ) AS STATUS,d.create_time,d.sn,d.payer member_id,d.payer_name member_name"
			+ " FROM li_wallet_frozen_detail d WHERE d.sn = #{sn}"
			+ " and d.transaction_type like '%_REFUND' GROUP BY payer " + "union all " + "SELECT sum(d.money) money,"
			+ "d.transaction_type,LEFT ( d.sn, 1 ) AS STATUS,d.create_time,d.sn,d.payee member_id,d.payee_name member_name"
			+ " FROM li_wallet_frozen_detail d WHERE d.sn = #{sn}"
			+ "and d.transaction_type like '%_REFUND' GROUP BY payee) a "
			+ " left join li_member m on m.id = a.member_id" + " left join li_store s on s.member_id = m.id"
			+ " LEFT JOIN li_partner p ON p.member_id = a.member_id AND p.partner_state = 0 AND p.delete_flag = 0"
			+ " LEFT JOIN li_role r ON r.id = p.role_id " + "GROUP BY a.member_id ")
	List<WalletFrozenDetailVO> refundDetailList(@Param("sn") String sn);
}
