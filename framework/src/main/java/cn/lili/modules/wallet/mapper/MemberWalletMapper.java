package cn.lili.modules.wallet.mapper;

import cn.lili.modules.wallet.entity.dos.MemberWallet;
import cn.lili.modules.wallet.entity.dto.MemberWalletDTO;
import cn.lili.modules.wallet.entity.vo.MemberWalletSearchParams;
import cn.lili.modules.whitebar.entity.vo.CommissionVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 会员预存款数据处理层
 *
 * @author pikachu
 * @since 2020-02-25 14:10:16
 */
public interface MemberWalletMapper extends BaseMapper<MemberWallet> {

	/**
	 * 执行资金解冻，单sql执行，规避脏数据问题
	 * 
	 * @param memberId
	 *            会员id
	 * @param flowPrice
	 *            解冻金额
	 * @return
	 */
	@Update("UPDATE li_member_wallet SET member_frozen_wallet=member_frozen_wallet-#{flowPrice},member_wallet = member_wallet+#{flowPrice} WHERE member_id=#{memberId} and owner=#{owner}")
	int updateFrozenWallet(@Param("memberId") String memberId, @Param("flowPrice") Double flowPrice,
			@Param("owner") String owner);

	@Select("SELECT * from" + "(SELECT o.sn,o.commission as money,o.payment_time create_time,'1' owner"
			+ " from li_order o left join li_member m on m.id = o.member_id "
			+ " where m.have_store = '1' and o.pay_status = 'PAID'" + " UNION ALL "
			+ " SELECT sn,handling_money as money,inspect_time create_time,'2' owner"
			+ " FROM li_member_withdraw_apply where apply_status = 'FAIL_AUDITING'" + " UNION ALL"
			+ " SELECT recharge_sn sn,recharge_money as money,pay_time create_time,'4' AS owner from li_recharge where recharge_type = '2' and pay_status = 'PAID'"
			+ " UNION ALL"
			+ " SELECT recharge_sn sn,IFNULL(recharge_money*0.5,0) as money,pay_time create_time,'5' AS owner from li_recharge where recharge_type = '3' and pay_status = 'PAID'"
			+ " UNION ALL"
			+ " SELECT recharge_sn sn,recharge_money as money,pay_time create_time,'6' AS owner from li_recharge where recharge_type = '5' and pay_status = 'PAID'"
			+ " ) aaa ${ew.customSqlSegment}")
	IPage<MemberWalletDTO> findMemberWalletDTO(IPage<MemberWalletSearchParams> page,
			@Param(Constants.WRAPPER) Wrapper<MemberWalletSearchParams> queryWrapper);

	@Select("SELECT * from ("
			+ " SELECT o.sn,o.flow_price money,o.payment_time as create_time,'1' as owner,s.member_id from li_order o "
			+ " left join li_store s on s.id = o.store_id  where o.payment_time is not null and pay_status = 'PAID'"
			+ " UNION ALL "
			+ " SELECT serial_number sn,IFNULL(member,transaction_flow) as money,transaction_time as create_time,if(member = null,'3','2') owner,"
			+ " user_id from li_commission where transaction_time is not null" + " ) aaa ${ew.customSqlSegment}")
	IPage<MemberWalletDTO> findMemberWalletDTOStore(IPage<MemberWalletSearchParams> page,
			@Param(Constants.WRAPPER) Wrapper<MemberWalletSearchParams> queryWrapper);

	@Select("SELECT * from (" + " SELECT o.sn,o.commission as money,o.payment_time create_time,'1' owner,o.member_id"
			+ " from li_order o left join li_member m on m.id = o.member_id "
			+ " where m.have_store = '1' and o.pay_status = 'PAID'  and payment_time is not null" + " UNION ALL "
			+ " SELECT recharge_sn sn,recharge_money as money,pay_time create_time,'2' as owner,member_id from li_recharge where recharge_type = '2' and pay_status = 'PAID'"
			+ " ) aaa ${ew.customSqlSegment}")
	IPage<MemberWalletDTO> findCommissionVOStore(IPage<MemberWalletSearchParams> page,
			@Param(Constants.WRAPPER) Wrapper<MemberWalletSearchParams> queryWrapper);

}