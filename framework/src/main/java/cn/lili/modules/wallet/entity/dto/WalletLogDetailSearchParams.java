package cn.lili.modules.wallet.entity.dto;

import cn.lili.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * create by yudan on 2022/4/1
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WalletLogDetailSearchParams extends PageVO {

	@ApiModelProperty(value = "会员id")
	private String memberId;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		return queryWrapper;
	}

}
