package cn.lili.modules.wallet.entity.vo;

import cn.lili.modules.wallet.entity.dos.MemberWithdrawItem;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * create by yudan on 2022/7/5
 */
@Data
@ApiModel(value = "提现明细表")
@AllArgsConstructor
public class MemberWithdrawItemVO extends MemberWithdrawItem {
}
