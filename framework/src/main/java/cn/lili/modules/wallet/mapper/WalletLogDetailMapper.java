package cn.lili.modules.wallet.mapper;

import cn.lili.modules.distribution.entity.vos.DistributionGoodsVO;
import cn.lili.modules.wallet.entity.dos.WalletLog;
import cn.lili.modules.wallet.entity.dos.WalletLogDetail;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import cn.lili.modules.wallet.entity.vo.WalletLogDetailVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * create by yudan on 2022/3/31
 */
public interface WalletLogDetailMapper extends BaseMapper<WalletLogDetail> {

	// 查询明细
	@Select("SELECT d.*,s.store_name,o.commission from li_wallet_log_detail d" + " left join li_order o on o.sn = d.sn"
			+ " left join li_store s on s.id = o.store_id" + " ${ew.customSqlSegment} order by d.create_time desc")
	IPage<WalletLogDetailVO> getList(IPage<WalletLogDetailVO> page,
			@Param(Constants.WRAPPER) Wrapper<WalletLogDetailVO> queryWrapper);

	// 查询合伙人年收益
	@Select("SELECT DATE_FORMAT(create_time,'%Y') time,sum(money) money" + " from li_wallet_log_detail"
			+ " WHERE payee = #{memberId} and transaction_type = '2'" + " GROUP BY DATE_FORMAT(create_time,'%Y') desc")
	IPage<WalletLogDetailVO> findYear(IPage<WalletLogDetailVO> page, String memberId);

	// 查询合伙人月收益
	@Select("SELECT DATE_FORMAT(create_time,'%Y-%m') time,sum(money) money" + " from li_wallet_log_detail"
			+ " WHERE payee = #{memberId} and transaction_type = '2'"
			+ " GROUP BY DATE_FORMAT(create_time,'%Y-%m') desc")
	IPage<WalletLogDetailVO> findMonth(IPage<WalletLogDetailVO> page, String memberId);

	// 查询合伙人日收益
	@Select("SELECT DATE_FORMAT(create_time,'%Y-%m-%d') time,sum(money) money" + " from li_wallet_log_detail"
			+ " WHERE payee = #{memberId} and transaction_type = '2'"
			+ " GROUP BY DATE_FORMAT(create_time,'%Y-%m-%d') desc")
	IPage<WalletLogDetailVO> findDay(IPage<WalletLogDetailVO> page, String memberId);

}
