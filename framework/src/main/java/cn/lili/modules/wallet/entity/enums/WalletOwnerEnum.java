package cn.lili.modules.wallet.entity.enums;

/**
 * 钱包类型枚举 create by yudan on 2022/3/18
 */
public enum WalletOwnerEnum {
	/**
	 * 预存款变动日志业务类型枚举
	 */
	// BUYER_RECHARGE("用户充值钱包"),
	// BUYER_PROMOTE("用户推广钱包"),
	// STORE_RECHARGE("商家充值钱包"),
	// STORE_PROMOTE("商家推广钱包"),
	// STORE_SALE("商家销售钱包"),
	RECHARGE("充值钱包"), PROMOTE("推广钱包"), PROMOTE_FW("推广钱包-服务费"), PROMOTE_HY("推广钱包-会员费"), SALE("销售钱包");

	private final String description;

	WalletOwnerEnum(String description) {
		this.description = description;
	}

	public String description() {
		return description;
	}

}
