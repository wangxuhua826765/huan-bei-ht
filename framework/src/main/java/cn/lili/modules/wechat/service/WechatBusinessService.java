package cn.lili.modules.wechat.service;

import cn.lili.modules.wechat.entity.dos.WechatBusinessStore;

public interface WechatBusinessService {

	Boolean createBusinessStore(WechatBusinessStore param);
}
