package cn.lili.modules.wechat.entity.dos;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@TableName("li_wechat_business")
@ApiModel(value = "微信消息")
public class WechatBusinessStore extends BaseEntity {

	private String account_name;

	private String nickname;

	private String icon_media_id;

	private String business_id;

}
