package cn.lili.modules.activity.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.awt.print.Book;
import java.io.Serializable;
import java.util.Date;

/**
 * @description 活动表
 * @author jwj
 * @date 2022-07-18
 */
@Data
@TableName("li_activity")
public class Activity implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	private String id;

	/**
	 * 活动名称
	 */
	private String name;

	/**
	 * 活动说明
	 */
	// @NotNull(message = "活动说明不能为空")
	private String content;

	/**
	 * 活动开始时间
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "活动开始时间", hidden = true)
	private Date activityStartTime;

	/**
	 * 活动结束时间
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "活动结束时间", hidden = true)
	private Date activityEndTime;

	/**
	 * 报名开始时间
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "报名开始时间", hidden = true)
	private Date registrationStartTime;

	/**
	 * 报名结束时间
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "报名结束时间", hidden = true)
	private Date registrationEndTime;

	/**
	 * 审核状态
	 */
	private String auditStatus;

	/**
	 * 投放区域(0全国投放 1区域投放)
	 */
	private String salesArea;

	/**
	 * 投放区域地址ID
	 */
	private String regionId;

	/**
	 * 投放区域
	 */
	private String launchArea;

	/**
	 * adcode
	 */
	private String adCode;

	/**
	 * 海报
	 */
	private String posterUrl;

	/**
	 * 发布人
	 */
	private String publisher;

	/**
	 * 活动类型
	 */
	private String activityType;

	/**
	 * 上架状态（0未上架 1已上架）
	 */
	private Boolean status;

	/**
	 * 提交人类型（0平台端 1城市合伙人 2商家端）
	 */
	private String submitType;

	/**
	 * 备注
	 */
	private String remarks;

	/**
	 * 是否限购（0不限购 1限购）
	 */
	private Boolean quantityFlag;
	/**
	 * 限购数量
	 */
	private Integer quantity;

	/**
	 * 店铺ID
	 */
	private String storeId;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@CreatedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建时间", hidden = true)
	private Date createTime;

	/**
	 * 删除标志 true/false 删除/未删除
	 */
	private Boolean deleteFlag;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间
	 */
	@LastModifiedDate
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新时间", hidden = true)
	private Date updateTime;

}