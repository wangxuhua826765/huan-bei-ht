package cn.lili.modules.activity.service.Impl;

import cn.hutool.core.util.ObjectUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.ActivityGoods;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import cn.lili.modules.activity.mapper.ActivityGoodsMapper;
import cn.lili.modules.activity.mapper.ActivityMapper;
import cn.lili.modules.activity.service.ActivityService;
import cn.lili.modules.goods.entity.vos.GoodsSkuVO;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.goods.mapper.GoodsMapper;
import cn.lili.modules.goods.service.GoodsService;
import cn.lili.modules.goods.service.GoodsSkuService;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.mapper.AdminUserMapper;
import cn.lili.modules.system.entity.dos.Region;
import cn.lili.modules.system.mapper.RegionMapper;
import cn.lili.modules.system.service.RegionService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements ActivityService {

	@Autowired
	private ActivityMapper activityMapper;
	@Autowired
	private ActivityGoodsMapper activityGoodsMapper;

	@Autowired
	private AdminUserMapper adminUserMapper;

	@Autowired
	private RegionMapper regionMapper;
	@Autowired
	private GoodsSkuService goodsSkuService;
	@Autowired
	private GoodsMapper goodsMapper;

	@Autowired
	private RegionService regionService;

	@Override
	public IPage<ActivityVO> selectPage(ActivityVO activityVO, PageVO page) {
		IPage<ActivityVO> activityVOIPage = null;
		// activityType 等于0 的时候是商品活动
		if (activityVO.getActivityType().equals("0")) {
			activityVOIPage = activityMapper.selectGoodsIPage(PageUtil.initPage(page),
					activityVO.queryWrapper(activityVO));
		} else {
			activityVOIPage = activityMapper.selectIPage(PageUtil.initPage(page), activityVO.queryWrapper(activityVO));
		}
		return activityVOIPage;
	}

	@Override
	public ActivityVO getActivityById(QueryWrapper<Activity> queryWrapper) {
		ActivityVO activity = activityMapper.getActivityById(queryWrapper);
		String id = activity.getId();
		QueryWrapper qu = new QueryWrapper();
		qu.eq("activity_id", id);
		List<ActivityGoods> list = activityGoodsMapper.selectList(qu);
		StringBuilder goods = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			if (i < list.size() - 1) {
				goods.append(list.get(i).getGoodsId()).append(",");
			} else {
				goods.append(list.get(i).getGoodsId());
			}
		}
		activity.setGoodsList(goods.toString());
		return activity;
	}

	@Override
	public IPage<ActivityVO> getIPageActivity(ActivityVO activityVO, PageVO page) {
		AuthUser authUser = UserContext.getCurrentUser();
		IPage<ActivityVO> activityVOIPage = new Page<>();
		if (ObjectUtil.isNotEmpty(authUser)) {
			UserEnums role = authUser.getRole();
			if (role.getRole().equals("代理商")) {
				List<String> stringList1 = Arrays.asList("2");
				activityVO.setSubmitType1(stringList1);
				// 0 待审核 1 已审核 2 已完成
				if (StringUtils.isNotEmpty(activityVO.getAuditStatus()) && activityVO.getAuditStatus().equals("0")) {
					List<String> stringList = Arrays.asList("0");
					activityVO.setStatusList(stringList);
				} else if (StringUtils.isNotEmpty(activityVO.getAuditStatus())
						&& activityVO.getAuditStatus().equals("1")) {
					List<String> stringList = Arrays.asList("1", "2");
					activityVO.setStatusList(stringList);
				} else if (StringUtils.isNotEmpty(activityVO.getAuditStatus())
						&& activityVO.getAuditStatus().equals("2")) {
					List<String> stringList = Arrays.asList("3", "4");
					activityVO.setStatusList(stringList);
				}
				// 根据登录者id查询城市合伙人所管理的区域
				AdminUser adminUser = adminUserMapper.selectById(authUser.getId());
				String adcode = adminUser.getRegionId();
				// Region region = regionMapper.getAdRegionId(adcode);
				List listByPath = regionService.getPartnerAdCode(adcode, "4");
				// List<String> listByPath = regionMapper.getListByPath(region.getId());
				activityVO.setAdcodeList(listByPath);
			} else if (role.getRole().equals("管理员")) {
				List<String> stringList1 = Arrays.asList("1", "2");
				activityVO.setSubmitType1(stringList1);
				if (StringUtils.isNotEmpty(activityVO.getAuditStatus()) && activityVO.getAuditStatus().equals("0")) {
					List<String> stringList = Arrays.asList("1");
					activityVO.setStatusList(stringList);
				} else if (StringUtils.isNotEmpty(activityVO.getAuditStatus())
						&& activityVO.getAuditStatus().equals("1")) {
					List<String> stringList = Arrays.asList("3", "4");
					activityVO.setStatusList(stringList);
				} else {
					List<String> stringList = Arrays.asList("1", "3", "4");
					activityVO.setStatusList(stringList);
				}
			}
			activityVOIPage = activityMapper.getIPageActivity(PageUtil.initPage(page),
					activityVO.queryWrapper(activityVO));
			return activityVOIPage;
		}
		return activityMapper.getIPageActivity(PageUtil.initPage(page), activityVO.queryWrapper(activityVO));
	}

	@Override
	public IPage<ActivityVO> selectAppIPage(ActivityVO activityVO, PageVO page) {
		activityVO.setStatus(false);
		activityVO.setAuditStatus1("3");
		IPage<ActivityVO> activityVOIPage = activityMapper.selectAppIPage(PageUtil.initPage(page),
				activityVO.queryWrapper(activityVO));
		if (StringUtils.isNotEmpty(activityVO.getChooseCode())) {
			// 查询平台发布的
			List<String> list1 = Arrays.asList("0");
			activityVO.setSubmitType1(list1);
			List<ActivityVO> activityVOS = new ArrayList<>();
			IPage<ActivityVO> activityVOIPage1 = activityMapper.selectAppIPage(PageUtil.initPage(page),
					activityVO.queryWrapper(activityVO));
			activityVOS.addAll(activityVOIPage1.getRecords());
			List<ActivityVO> records = activityVOIPage1.getRecords();
			page.setPageSize(page.getPageSize() - records.size());
			String regionId = null;
			if (page.getPageSize() > 0) {
				// 根据adcode查询投放区域一致的
				Region region = regionMapper.getAdRegionId(activityVO.getChooseCode());
				regionId = region.getId();
				activityVO.setRegionId(regionId);
				List<String> list = Arrays.asList("1", "2");
				activityVO.setSubmitType1(list);
				IPage<ActivityVO> activityVOIPage2 = activityMapper.selectAppIPage(PageUtil.initPage(page),
						activityVO.queryWrapper(activityVO));
				page.setPageSize(page.getPageSize() - activityVOIPage2.getRecords().size());
				activityVOS.addAll(activityVOIPage2.getRecords());
			}
			if (page.getPageSize() > 0) {
				// 其他
				Region region = regionMapper.getAdRegionId(activityVO.getChooseCode());
				String id = region.getId();
				activityVO.setRegionId("");
				activityVO.setNotRegionId(id);
				List<String> list = Arrays.asList("1", "2");
				activityVO.setSubmitType1(list);
				IPage<ActivityVO> activityVOIPage3 = activityMapper.selectAppIPage(PageUtil.initPage(page),
						activityVO.queryWrapper(activityVO));
				activityVOS.addAll(activityVOIPage3.getRecords());
			}
			activityVOIPage.setRecords(activityVOS);
		}

		return activityVOIPage;
	}

	@Override
	public ActivityVO selectAppActDetails(String id) {
		return activityMapper.selectAppActDetails(id);
	}

	@Override
	public IPage<GoodsVO> selectAppGoodsDetails(String id, PageVO page, String categoryId) {
		IPage<GoodsVO> goodsVOIPage = activityMapper.selectAppGoodsDetails(PageUtil.initPage(page), id, categoryId);
		List<GoodsVO> records = goodsVOIPage.getRecords();
		for (GoodsVO record : records) {
			String goodsId = record.getId();
			// 商品sku赋值
			List<GoodsSkuVO> goodsListByGoodsId = goodsSkuService.getGoodsListByGoodsId(goodsId);
			record.setSkuList(goodsListByGoodsId);
		}
		goodsVOIPage.setRecords(records);
		return goodsVOIPage;
	}

	@Override
	public IPage<ActivityVO> joinActivityList(ActivityVO activityVO, PageVO page) {
		AuthUser authUser = UserContext.getCurrentUser();
		if (StringUtils.isNotEmpty(authUser.getStoreId())) {
			return activityMapper.joinActivityList(PageUtil.initPage(page), activityVO.queryWrapper(activityVO),
					authUser.getStoreId());
		}
		return activityMapper.joinActivityList(PageUtil.initPage(page), activityVO.queryWrapper(activityVO), null);
	}

	@Override
	public ActivityVO selectActDetailsByGoodsId(String goodsId) {
		return activityMapper.selectActDetailsByGoodsId(goodsId);
	}

	@Override
	public Boolean updateGoodsStatus() {
		List<String> activityList = activityMapper.getListActivity();
		Boolean b = goodsMapper.updateGoodsStatus(activityList);
		return b;
	}

}
