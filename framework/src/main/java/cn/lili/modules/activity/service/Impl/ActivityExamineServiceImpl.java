package cn.lili.modules.activity.service.Impl;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.ActivityExamine;
import cn.lili.modules.activity.entity.vo.ActivityExamineVO;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import cn.lili.modules.activity.mapper.ActivityExamineMapper;
import cn.lili.modules.activity.mapper.ActivityMapper;
import cn.lili.modules.activity.service.ActivityExamineService;
import cn.lili.modules.activity.service.ActivityService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ActivityExamineServiceImpl extends ServiceImpl<ActivityExamineMapper, ActivityExamine>
		implements
			ActivityExamineService {

	@Autowired
	private ActivityExamineMapper activityExamineMapper;

	@Override
	public List<ActivityExamineVO> getByActivityId(String activityId) {
		return activityExamineMapper.getActivityById(activityId);
	}
}
