package cn.lili.modules.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @description 活动审核详情
 * @author jwj
 * @date 2022-07-19
 */

@Data
@TableName("li_activity_examine")
@ApiModel("活动审核详情")
public class ActivityExamine implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	@ApiModelProperty("id")
	private String id;

	/**
	 * 活动ID
	 */
	@ApiModelProperty("活动ID")
	private String activityId;

	/**
	 * 原因
	 */
	@ApiModelProperty("原因")
	private String reason;

	/**
	 * 审核状态
	 */
	@ApiModelProperty("审核状态")
	private String status;

	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建时间", hidden = true)
	private Date createTime;

}