package cn.lili.modules.activity.mapper;

import cn.lili.modules.activity.entity.ActivityGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ActivityGoodsMapper extends BaseMapper<ActivityGoods> {
}
