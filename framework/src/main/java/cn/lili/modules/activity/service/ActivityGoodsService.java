package cn.lili.modules.activity.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.ActivityGoods;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

@Service
public interface ActivityGoodsService extends IService<ActivityGoods> {

}
