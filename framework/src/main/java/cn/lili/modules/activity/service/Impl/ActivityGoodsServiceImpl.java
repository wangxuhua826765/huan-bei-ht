package cn.lili.modules.activity.service.Impl;

import cn.lili.modules.activity.entity.ActivityGoods;
import cn.lili.modules.activity.mapper.ActivityGoodsMapper;
import cn.lili.modules.activity.service.ActivityGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ActivityGoodsServiceImpl extends ServiceImpl<ActivityGoodsMapper, ActivityGoods>
		implements
			ActivityGoodsService {

}
