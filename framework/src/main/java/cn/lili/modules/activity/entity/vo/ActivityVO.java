package cn.lili.modules.activity.entity.vo;

import cn.hutool.core.date.DateUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.activity.entity.Activity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ActivityVO extends Activity {

	/**
	 * 活动名称
	 */
	private String name;
	private String activityId;

	/**
	 * 创建者名称
	 */
	private String nickName;

	/**
	 * 活动状态
	 */
	private String activityStatus;
	/**
	 * 上架状态
	 */
	private String shelfStatus;

	/**
	 * 创建者的手机号
	 */
	private String mobile;

	/**
	 * 活动类型
	 */
	private String activityType;

	/**
	 * 参与店铺数量
	 */
	private Integer storeNum;

	/**
	 * 参与商品数量
	 */
	private Integer goodsNum;

	/**
	 * 提交人类型（0平台端 1城市合伙人 2商家端）
	 */
	private List<String> submitType1;
	private String submitType;

	/**
	 * 审核状态
	 */
	private String auditStatus;

	/**
	 * 审核状态集合
	 */
	private List<String> statusList;
	/**
	 * 城市合伙人管理区域adcode集合
	 */
	private List<String> adcodeList;
	/**
	 * 商品的集合（以逗号分割）
	 */
	private String goodsList;
	/**
	 * 店铺名称
	 */
	private String storeName;
	/**
	 * 店铺ID
	 */
	private String storeId;
	/**
	 * chooseCode 定位、选择、默认定位
	 */
	private String chooseCode;
	/**
	 * 发布开始时间
	 */
	private String createStartTime;
	/**
	 * 发布截至时间
	 */
	private String createEndTime;
	/**
	 * 投放区域
	 */
	private String salesArea;
	/**
	 * 排除本区以外的投放区域
	 */
	private String notRegionId;
	/**
	 * 商品数量
	 */
	private Integer isJoin;

	private String AuditStatus1;

	/**
	 * 发布人
	 */
	private String publisher;

	public <T> QueryWrapper<T> queryWrapper(ActivityVO activityVO) {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (StringUtils.isNotEmpty(name)) {
			queryWrapper.like("name", name);
		}
		if (StringUtils.isNotEmpty(publisher)) {
			queryWrapper.eq("publisher", publisher);
		}
		if (null != activityStatus && "null" != activityStatus && activityStatus.equals("0")) {
			queryWrapper.gt("a.registration_start_time", new Date());
		}
		if (null != activityStatus && "null" != activityStatus && activityStatus.equals("1")) {
			queryWrapper.lt("a.registration_start_time", new Date()).gt("a.registration_end_time", new Date());
		}
		if (null != activityStatus && "null" != activityStatus && activityStatus.equals("2")) {
			queryWrapper.lt("a.registration_end_time", new Date()).gt("a.activity_start_time", new Date());
		}
		if (null != activityStatus && "null" != activityStatus && activityStatus.equals("3")) {
			queryWrapper.lt("a.activity_start_time", new Date()).gt("a.activity_end_time", new Date());
		}
		if (null != activityStatus && "null" != activityStatus && activityStatus.equals("4")) {
			queryWrapper.lt("a.activity_end_time", new Date());
		}
		if (null != activityType && StringUtils.isNotEmpty(activityType)) {
			queryWrapper.eq("a.activity_type", activityType);
		}
		if (null != submitType1 && submitType1.size() > 0) {
			queryWrapper.in("a.submit_type", submitType1);
		}
		if (null != submitType && StringUtils.isNotEmpty(submitType)) {
			queryWrapper.eq("a.submit_type", submitType);
		}
		if (statusList != null && statusList.size() > 0) {
			queryWrapper.in("a.audit_status", statusList);
		}
		if (adcodeList != null && adcodeList.size() > 0) {
			queryWrapper.in("s.ad_code", adcodeList);
		}
		if (storeId != null && StringUtils.isNotEmpty(storeId)) {
			queryWrapper.eq("a.store_id", storeId);
		}
		if (createEndTime != null && createStartTime != null) {
			queryWrapper.lt("a.create_time", createEndTime).gt("a.create_time", createStartTime);
		}
		if (salesArea != null) {
			queryWrapper.eq("a.sales_area", salesArea);
		}
		if (storeName != null && StringUtils.isNotEmpty(storeName)) {
			queryWrapper.like("s.store_name", storeName);
		}
		if (activityVO.getRegionId() != null && StringUtils.isNotEmpty(activityVO.getRegionId())) {
			queryWrapper.like("a.region_id", activityVO.getRegionId());
		}
		if (activityVO.getStatus() != null && !activityVO.getStatus()) {
			queryWrapper.lt("a.activity_start_time", new Date()).gt("a.activity_end_time", new Date()).eq("a.status",
					"0");
		}
		if (notRegionId != null && StringUtils.isNotEmpty(notRegionId)) {
			queryWrapper.notLike("a.region_id", notRegionId);
		}
		if (activityVO.getCreateTime() != null) {
			queryWrapper.like("a.create_time", DateUtil.formatDate(activityVO.getCreateTime()));
		}
		if (AuditStatus1 != null) {
			queryWrapper.eq("a.audit_status", activityVO.getAuditStatus1());
		}
		queryWrapper.eq("a.delete_flag", false);
		return queryWrapper;
	}

}
