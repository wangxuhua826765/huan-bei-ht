package cn.lili.modules.activity.entity.vo;

import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.ActivityExamine;
import lombok.Data;

@Data
public class ActivityExamineVO extends ActivityExamine {

	/**
	 * 创建者名称
	 */
	private String nickName;

	/**
	 * 创建者的手机号
	 */
	private String mobile;

	/**
	 * 角色名称
	 */
	private String roleName;

}
