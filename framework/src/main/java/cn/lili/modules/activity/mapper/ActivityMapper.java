package cn.lili.modules.activity.mapper;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import cn.lili.modules.goods.entity.dto.GoodsSearchParams;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.page.entity.vos.ArticleRecordSummaryVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ActivityMapper extends BaseMapper<Activity> {

	@Select("select a.*,( CASE WHEN NOW() < a.registration_start_time THEN 0 "
			+ " WHEN NOW() > a.registration_start_time and NOW() < a.registration_end_time THEN 1 "
			+ " WHEN NOW() > a.registration_end_time and NOW() < a.activity_start_time THEN 2 "
			+ " WHEN NOW() > a.activity_start_time and NOW() < a.activity_end_time THEN 3 "
			+ " WHEN NOW() > a.activity_end_time THEN 4 "
			+ " END )  as activityStatus,(CASE WHEN NOW() < a.activity_start_time THEN \"未上架\" "
			+ " WHEN NOW() > a.activity_start_time AND NOW() < a.activity_end_time THEN \"已上架\" "
			+ " WHEN NOW() > a.activity_end_time THEN \"已失效\" "
			+ " END ) AS shelfStatus,(SELECT count(id) from  li_activity_goods where activity_id =a.id ) as goodsNum , "
			+ "(SELECT count(DISTINCT(store_id)) from  li_activity_goods where activity_id =a.id ) as storeNum from li_activity a ${ew.customSqlSegment}")
	IPage<ActivityVO> selectGoodsIPage(IPage<Activity> page, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper);

	@Select("select a.*,( CASE WHEN NOW() < a.activity_start_time THEN 2 "
			+ " WHEN NOW() > a.activity_start_time and NOW() < a.activity_end_time THEN 3 "
			+ " WHEN NOW() > a.activity_end_time THEN 4 "
			+ " END )  as activityStatus,(CASE WHEN NOW() < a.activity_start_time THEN \"未上架\" "
			+ " WHEN NOW() > a.activity_start_time AND NOW() < a.activity_end_time THEN \"已上架\" "
			+ " WHEN NOW() > a.activity_end_time THEN \"已失效\" "
			+ " END ) AS shelfStatus,(SELECT count(id) from  li_activity_goods where activity_id =a.id ) as goodsNum , "
			+ "(SELECT count(DISTINCT(store_id)) from  li_activity_goods where activity_id =a.id ) as storeNum from li_activity a ${ew.customSqlSegment}")
	IPage<ActivityVO> selectIPage(IPage<Activity> page, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper);

	@Select("SELECT a.* ,IFNULL(au.mobile,su.mobile) as mobile,IFNULL(au.nick_name,su.nick_name) as nickName FROM li_activity a "
			+ "LEFT JOIN li_admin_user au on au.id = a.create_by "
			+ "LEFT JOIN li_store_user su on su.id = a.create_by  ${ew.customSqlSegment}")
	ActivityVO getActivityById(@Param(Constants.WRAPPER) QueryWrapper<Activity> queryWrapper);

	@Select("SELECT IFNULL(au.nick_name,su.nick_name) AS nickName, IFNULL(au.mobile,su.mobile) AS mobile,(CASE WHEN NOW() < a.registration_start_time THEN 0 WHEN NOW() > a.registration_start_time AND NOW() < a.registration_end_time THEN 1 WHEN NOW() > a.registration_end_time AND NOW() < a.activity_start_time THEN 2 WHEN NOW() > a.activity_start_time AND NOW() < a.activity_end_time THEN 3 WHEN NOW() > a.activity_end_time THEN 4 END) AS activityStatus,"
			+ " s.store_name as storeName,(CASE WHEN NOW() < a.activity_start_time THEN \"未上架\" WHEN NOW() > a.activity_start_time AND NOW() < a.activity_end_time THEN \"已上架\" \n"
			+ " WHEN NOW() > a.activity_end_time THEN \"已失效\" END ) AS shelfStatus,a.* FROM li_activity a "
			+ " LEFT JOIN li_admin_user au on a.create_by = au.id LEFT JOIN li_store s on s.id = a.store_id"
			+ " LEFT JOIN li_store_user su on su.id = a.create_by  ${ew.customSqlSegment} ")
	IPage<ActivityVO> getIPageActivity(IPage<Activity> page, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper);

	@Select("SELECT IFNULL(au.nick_name,su.nick_name) AS nickName, IFNULL(au.mobile,su.mobile) AS mobile,s.store_name as storeName,a.* FROM li_activity a "
			+ " LEFT JOIN li_admin_user au on a.create_by = au.id "
			+ " LEFT JOIN li_store_user su on su.id = a.create_by "
			+ " LEFT JOIN li_store s on s.id = a.store_id  ${ew.customSqlSegment} ")
	IPage<ActivityVO> selectAppIPage(IPage<Activity> page, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper);

	@Select("SELECT IFNULL(au.mobile,su.mobile) as mobile,IFNULL(au.nick_name,su.nick_name) as nickName,s.store_name AS storeName,a.* "
			+ " FROM li_activity a LEFT JOIN li_admin_user au ON a.create_by = au.id "
			+ " LEFT JOIN li_store_user su on su.id = a.create_by LEFT JOIN li_store s ON s.id = a.store_id where a.id= #{id}")
	ActivityVO selectAppActDetails(String id);

	@Select("<script>"
			+ "SELECT a.activity_end_time as activityEndTime,a.quantity,a.quantity_flag as quantityFlag,g.original as goodsUrl, "
			+ " IFNULL((select SUM(num) FROM li_order_item WHERE goods_id = g.id and after_sale_status='NOT_APPLIED'),0) as monthSales, "
			+ " FORMAT(IFNULL((select SUM(grade)/COUNT(grade) FROM li_member_evaluation WHERE goods_id = g.id),5),1) as score ,g.* "
			+ "FROM li_activity_goods ag LEFT JOIN li_activity a on a.id = ag.activity_id "
			+ " LEFT JOIN li_goods g ON g.id = ag.goods_id  WHERE a.id =  #{id}"
			+ "<if test=\"categoryId != null and categoryId != ''\">"
			+ " and g.category_path LIKE concat('%',#{categoryId},'%')" + "</if>" + "</script>")
	IPage<GoodsVO> selectAppGoodsDetails(IPage<Activity> page, @Param("id") String id,
			@Param("categoryId") String categoryId);

	@Select(" select (SELECT COUNT(id) from li_activity_goods where activity_id = a.id and store_id = #{storeId} ) as isJoin,( CASE WHEN NOW() < a.registration_start_time THEN 0 WHEN NOW() > a.registration_start_time "
			+ " AND NOW() < a.registration_end_time THEN 1 WHEN NOW() > a.registration_end_time "
			+ " AND NOW() < a.activity_start_time THEN 2 WHEN NOW() > a.activity_start_time "
			+ " AND NOW() < a.activity_end_time THEN 3 WHEN NOW() > a.activity_end_time THEN 4 END ) AS activityStatus,a.* from li_activity a  ${ew.customSqlSegment} ")
	IPage<ActivityVO> joinActivityList(IPage<Activity> page, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper,
			@Param("storeId") String storeId);

	@Select("SELECT a.* FROM li_activity a LEFT JOIN li_activity_goods ag ON a.id = ag.activity_id "
			+ "WHERE a.activity_start_time < NOW() and a.activity_end_time >NOW() and delete_flag = false "
			+ "and ag.goods_id = #{goodsId}")
	ActivityVO selectActDetailsByGoodsId(String goodsId);

	@Select(" SELECT DISTINCT(ag.goods_id) " + " FROM li_activity_goods ag "
			+ " LEFT JOIN li_activity a on a.id = ag.activity_id "
			+ " WHERE now() >= a.activity_end_time AND a.delete_flag = false")
	List<String> getListActivity();

	@Select("SELECT a.* FROM `li_activity` a "
			+ " LEFT JOIN li_activity_goods ag on a.id = ag.activity_id  ${ew.customSqlSegment}")
	ActivityVO getAppActivityById(@Param(Constants.WRAPPER) QueryWrapper<Activity> queryWrapper);

}
