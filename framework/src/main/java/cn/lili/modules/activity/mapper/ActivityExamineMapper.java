package cn.lili.modules.activity.mapper;

import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.ActivityExamine;
import cn.lili.modules.activity.entity.vo.ActivityExamineVO;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ActivityExamineMapper extends BaseMapper<ActivityExamine> {

	@Select("SELECT r.name as roleName,ae.*,au.nick_name ,au.mobile " + " FROM li_activity_examine ae "
			+ " LEFT JOIN li_admin_user au on ae.create_by = au.id "
			+ " LEFT JOIN li_user_role ur on ur.user_id = au.id "
			+ " LEFT JOIN li_role r on r.id = ur.role_id   where ae.activity_id = #{activityId}")
	List<ActivityExamineVO> getActivityById(String activityId);

}
