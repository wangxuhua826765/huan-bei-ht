package cn.lili.modules.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 活动商品关系
 * @author Liao cifeng
 * @date 2022-07-20
 */
@Data
@TableName("li_activity_goods")
@ApiModel("活动商品关系")
public class ActivityGoods implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty("id")
	private String id;

	/**
	 * 活动id
	 */
	@ApiModelProperty("活动id")
	private String activityId;

	/**
	 * 商品id
	 */
	@ApiModelProperty("商品id")
	private String goodsId;

	/**
	 * 店铺id
	 */
	@ApiModelProperty("店铺id")
	private String storeId;

}