package cn.lili.modules.activity.service;

import cn.lili.modules.activity.entity.ActivityExamine;
import cn.lili.modules.activity.entity.vo.ActivityExamineVO;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ActivityExamineService extends IService<ActivityExamine> {
	List<ActivityExamineVO> getByActivityId(String activityId);
}
