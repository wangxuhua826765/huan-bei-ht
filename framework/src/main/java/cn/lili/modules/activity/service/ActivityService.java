package cn.lili.modules.activity.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ActivityService extends IService<Activity> {
	IPage<ActivityVO> selectPage(ActivityVO activityVO, PageVO page);

	ActivityVO getActivityById(QueryWrapper<Activity> queryWrapper);

	IPage<ActivityVO> getIPageActivity(ActivityVO activityVO, PageVO page);

	IPage<ActivityVO> selectAppIPage(ActivityVO activityVO, PageVO page);

	ActivityVO selectAppActDetails(String id);

	IPage<GoodsVO> selectAppGoodsDetails(String id, PageVO page, String categoryId);

	IPage<ActivityVO> joinActivityList(ActivityVO activityVO, PageVO page);

	ActivityVO selectActDetailsByGoodsId(String goodsId);

	Boolean updateGoodsStatus();
}
