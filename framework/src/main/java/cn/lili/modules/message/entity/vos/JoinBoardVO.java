package cn.lili.modules.message.entity.vos;

import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.modules.page.entity.enums.JoinTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * JoinBoardVO.
 *
 * @author Li Bing
 * @since 2022.03.27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "加盟留言")
public class JoinBoardVO extends PageVO {

	@ApiModelProperty(value = "申请人姓名")
	private String userName;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	/**
	 * 加盟意向-推广员，天使合伙人，事业合伙人， 城市合伙人
	 *
	 * @see JoinTypeEnum
	 */
	@ApiModelProperty(value = "加盟意向", allowableValues = "EXTENSION_AGENT,ANGEL_PARTNER,CAREER_PARTNER，CITY_PARTNER")
	private String type;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private Date startDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private Date endDate;

	public <T> QueryWrapper<T> queryWrapper() {
		QueryWrapper<T> queryWrapper = new QueryWrapper<>();
		if (StringUtils.isNotBlank(userName)) {
			queryWrapper.like("user_name", userName);
		}
		if (StringUtils.isNotBlank(mobile)) {
			queryWrapper.like("mobile", mobile);
		}
		if (StringUtils.isNotBlank(type)) {
			queryWrapper.eq("type", type);
		}
		// 按时间查询
		if (startDate != null) {
			queryWrapper.ge("create_time", startDate);
		}
		if (endDate != null) {
			queryWrapper.le("create_time", endDate);
		}
		return queryWrapper;
	}

}
