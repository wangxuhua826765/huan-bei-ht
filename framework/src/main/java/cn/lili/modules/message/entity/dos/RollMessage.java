package cn.lili.modules.message.entity.dos;

import cn.lili.mybatis.BaseEntity;
import cn.lili.mybatis.BaseIdEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import java.util.Date;

@Data
@TableName("sys_message")
@ApiModel(value = "滚动消息")
public class RollMessage extends BaseIdEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 滚动消息
	 */
	private String content;

	/**
	 * 分类名称
	 */
	private String name;

	/**
	 * 0自定义 1成为会员 2成为合伙人 3店铺入住
	 */
	private Integer type;

	/**
	 * 排序值
	 */
	private Integer sort;

	/**
	 * 状态（1启用 0不启用）
	 */
	private Integer status;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 删除标志 true/false 1删除/0未删除
	 */
	private Boolean delFlag;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间
	 */
	private Date updateTime;

}
