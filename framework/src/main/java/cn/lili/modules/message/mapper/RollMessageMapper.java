package cn.lili.modules.message.mapper;

import cn.lili.modules.message.entity.dos.RollMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RollMessageMapper extends BaseMapper<RollMessage> {

}
