package cn.lili.modules.message.serviceimpl;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.message.entity.dos.RollMessage;
import cn.lili.modules.message.mapper.RollMessageMapper;
import cn.lili.modules.message.service.RollMessageService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class RollMessageServiceImpl extends ServiceImpl<RollMessageMapper, RollMessage> implements RollMessageService {

	@Autowired
	private RollMessageMapper rollMessageMapper;

	@Override
	public IPage<RollMessage> selectList(RollMessage rollMessage, PageVO pageVo) {
		QueryWrapper<RollMessage> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("del_flag", 0);
		queryWrapper.eq("type", 0);
		queryWrapper.orderByDesc("create_time");
		queryWrapper.like(rollMessage.getContent() != null, "content", rollMessage.getContent());
		IPage<RollMessage> page = rollMessageMapper.selectPage(PageUtil.initPage(pageVo), queryWrapper);
		return page;
	}
}
