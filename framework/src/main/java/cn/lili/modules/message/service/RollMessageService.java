package cn.lili.modules.message.service;

import cn.lili.common.vo.PageVO;
import cn.lili.modules.message.entity.dos.RollMessage;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

public interface RollMessageService extends IService<RollMessage> {

	IPage<RollMessage> selectList(RollMessage rollMessage, PageVO pageVo);
}
