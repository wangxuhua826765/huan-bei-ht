package cn.lili.controller.other;

import cn.lili.common.enums.DictCodeEnum;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.SysDictUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.vo.GradeVO;
import cn.lili.modules.member.service.GradeService;
import cn.lili.modules.whitebar.entity.vo.RateSettingSearchParams;
import cn.lili.modules.whitebar.entity.vo.RateSettingVO;
import cn.lili.modules.whitebar.service.RateSettingService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 买家端,费率接口
 *
 * create by yudan on 2022/2/25
 */
@RestController
@Api(tags = "商家端,费率")
@RequestMapping("/business/rateSetting")
@Transactional(rollbackFor = Exception.class)
public class RateSettingBusinessController {

	/**
	 * 费率管理
	 */
	@Autowired
	private RateSettingService rateSettingService;

	@Autowired
	private GradeService gradeService;

	@GetMapping
	@ApiOperation(value = "费率")
	public ResultMessage<Map<String, Object>> create() {
		// RateSettingSearchParams rateSettingSearchParams = new
		// RateSettingSearchParams();
		Map<String, Object> map = new HashMap<>();
		// rateSettingSearchParams.setPageSize(999999999);
		// IPage<RateSettingVO> rateSettingVOIPage =
		// rateSettingService.rateSettingPage(rateSettingSearchParams);
		// if (rateSettingVOIPage != null) {
		// for (RateSettingVO vo : rateSettingVOIPage.getRecords()) {
		// if (vo.getRoleName().equals("天使合伙人")) {
		// map.put("tsFlowRate", CurrencyUtil.mul(vo.getFlowRate(), new Double(100)));
		// }
		// }
		// }
		map.put("tsFlowRate",
				CurrencyUtil.mul(
						Double.valueOf(SysDictUtils.getValueString(DictCodeEnum.FLOW_RATE.dictCode(), "天使合伙人")),
						new Double(100)));
		return ResultUtil.data(map);
	}

	@RequestMapping(value = "/member")
	@ApiOperation(value = "会员折扣")
	public ResultMessage<List<GradeVO>> member() {

		return ResultUtil.data(gradeService.findByMemberVO(new GradeVO()));
	}
}
