package cn.lili.controller.member;

import cn.lili.common.enums.ClientTypeEnum;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.vo.ExtensionVO;
import cn.lili.modules.member.service.EextensionService;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.wechat.util.WechatMpCodeUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * create by yudan on 2022/2/28
 */
@RestController
@Api(tags = "商家端,推广")
@RequestMapping("/business/extension")
public class ExtensionBusinessController {

	@Autowired
	public WechatMpCodeUtil wechatMpCodeUtil;
	@Autowired
	private MemberService memberService;

	@Autowired
	private EextensionService eextensionService;

	@GetMapping
	@ApiOperation(value = "查询我推广的人")
	public ResultMessage<IPage<ExtensionVO>> extension(PageVO page, String scene) {
		ExtensionVO extension = new ExtensionVO();
		Member member = memberService.getUserInfo();
		if (StringUtils.isEmpty(scene)) {
			scene = member.getPromotionCode();
		}
		extension.setExtension(scene);
		return ResultUtil.data(eextensionService.getExtensionPage(extension, page));
	}

	@RequestMapping("/mp/unlimited")
	@ApiOperation(value = "小程序太阳码生成：不限制数量，但是限制长度，只能存放32为长度")
	public ResultMessage<Map<String, Object>> unlimited(String page, String scene) {
		Map<String, Object> map = new HashMap<>();
		Member member = memberService.getUserInfo();
		if (StringUtils.isEmpty(scene)) {
			scene = member.getPromotionCode();
		}
		map.put("sunCode", wechatMpCodeUtil.createCode(page, scene, ClientTypeEnum.WECHAT_MP2));
		map.put("img", "https://wangningkeji.oss-cn-beijing.aliyuncs.com/6e986516ce66410996dc609e7cdf261d.jpg");
		return ResultUtil.data(map);
	}

}
