package cn.lili.test.order;

import cn.lili.modules.order.order.service.OrderService;
import cn.lili.modules.wallet.mapper.WalletLogDetailMappering;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

/**
 * @author paulG
 * @since 2020/12/1
 **/
@ExtendWith(SpringExtension.class)
@SpringBootTest
class OrderServiceTest {

	@Autowired
	private OrderService orderService;
	@Autowired
	private WalletLogDetailMappering walletLogDetailMappering;

	@Test
	void QueryParam() {
		// OrderSearchParams orderSearchParams = new OrderSearchParams();
		// orderSearchParams.setPageSize(0);
		// orderSearchParams.setPageNumber(10);
		// IPage<OrderSimpleVO> orderVOIPage =
		// orderService.queryByParams(orderSearchParams);
		// Assertions.assertNotNull(orderVOIPage);
		// orderVOIPage.getRecords().forEach(System.out::println);
	}

	@Test
	void testQuery() {

		QueryWrapper queryWrapper = new QueryWrapper();

		IPage<Map<String, Object>> datas = walletLogDetailMappering.getBMember(new Page<>(0, 10), queryWrapper);
		System.out.println(datas);
	}

}
