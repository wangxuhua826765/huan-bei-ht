package cn.lili.controller.other;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.message.entity.vos.JoinBoardVO;
import cn.lili.modules.page.entity.dos.JoinBoard;
import cn.lili.modules.page.service.JoinBoardService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * JoinBoardManagerController.
 *
 * @author Li Bing
 * @since 2022.03.27
 */
@RestController
@Api(tags = "管理端,加盟留言接口")
@RequestMapping("/manager/joinboard")
public class JoinBoardManagerController {

	@Autowired
	private JoinBoardService joinBoardService;

	@ApiOperation(value = "查询加盟留言列表")
	@GetMapping
	public ResultMessage<IPage<JoinBoard>> page(JoinBoardVO joinBoardVo) {
		return ResultUtil.data(joinBoardService.pageJoinBoard(joinBoardVo));
	}

	@ApiOperation(value = "查看加盟留言")
	@ApiImplicitParam(name = "id", value = "加盟留言ID", required = true, dataType = "String", paramType = "path")
	@RequestMapping(value = "/{id}")
	public ResultMessage<JoinBoard> getJoinBoard(@PathVariable String id) {
		return ResultUtil.data(joinBoardService.getById(id));
	}

}
