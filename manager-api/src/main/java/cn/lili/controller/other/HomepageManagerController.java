package cn.lili.controller.other;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.page.entity.dos.Feedback;
import cn.lili.modules.page.entity.dos.Special;
import cn.lili.modules.page.service.HomepageManagerService;
import cn.lili.modules.page.service.JoinBoardService;
import cn.lili.modules.system.entity.dos.Homepage;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(tags = "管理端,首页模块管理")
@RequestMapping("/manager/homepage")
public class HomepageManagerController {

	@Autowired
	private HomepageManagerService homepageManagerService;

	@ApiOperation(value = "查询首页模块接口")
	@GetMapping
	public ResultMessage<List<Homepage>> list() {
		return ResultUtil.data(homepageManagerService.list());
	}

	@ApiOperation(value = "修改首页模块接口")
	@PutMapping("/{id}")
	public ResultMessage edit(@PathVariable String id, Homepage homepage) {
		homepage.setId(id);
		if (StringUtils.isNotEmpty(homepage.getUrl())) {
			String url = homepage.getUrl();
			String[] split = url.split("/");
			String s = split[split.length - 1];
			homepage.setUrlName(s);
		}
		return ResultUtil.data(homepageManagerService.updateById(homepage));
	}

	@ApiOperation(value = "修改首页模块上下架")
	@PutMapping("/status/{id}")
	public ResultMessage editStatus(@PathVariable String id, String status) {
		Homepage homepage = new Homepage();
		homepage.setId(id);
		homepage.setStatus(status);
		return ResultUtil.data(homepageManagerService.updateById(homepage));
	}

	@ApiOperation(value = "首页模块上下架")
	@RequestMapping("/{id}")
	public ResultMessage getById(@PathVariable String id) {
		return ResultUtil.data(homepageManagerService.getById(id));
	}

}
