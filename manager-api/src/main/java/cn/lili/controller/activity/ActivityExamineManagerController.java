package cn.lili.controller.activity;

import cn.hutool.core.util.ObjectUtil;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.ActivityExamine;
import cn.lili.modules.activity.entity.vo.ActivityExamineVO;
import cn.lili.modules.activity.service.ActivityExamineService;
import cn.lili.modules.activity.service.ActivityService;
import cn.lili.modules.system.aspect.annotation.SystemLogPoint;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "管理端-活动审核管理")
@RequestMapping("/manager/activity/examine")
public class ActivityExamineManagerController {

	@Autowired
	private ActivityExamineService activityExamineService;

	@Autowired
	private ActivityService activityService;

	/**
	 * 根据活动id查询审核详情
	 */
	@RequestMapping("/activityId")
	public ResultMessage getGoodsByActivityId(String activityId) {
		List<ActivityExamineVO> activityExamines = activityExamineService.getByActivityId(activityId);
		return ResultUtil.data(activityExamines);
	}

	@PutMapping("/status")
	@SystemLogPoint(description = "审核活动", customerLog = "'审核活动请求:'+#username", type = "2")
	public ResultMessage editAuditStatus(ActivityExamine activityExamine) {
		AuthUser authUser = UserContext.getCurrentUser();
		if (ObjectUtil.isNotEmpty(authUser)) {
			Activity activity = new Activity();
			activity.setId(activityExamine.getActivityId());
			UserEnums role = authUser.getRole();
			if (role.getRole().equals("代理商")) {
				// 0 审核通过 1 驳回
				if (activityExamine.getStatus().equals("0")) {
					activity.setAuditStatus("1");
				} else if (activityExamine.getStatus().equals("1")) {
					activity.setAuditStatus("2");
				}
			} else if (role.getRole().equals("管理员")) {
				if (activityExamine.getStatus().equals("0")) {
					activity.setAuditStatus("3");
				} else if (activityExamine.getStatus().equals("1")) {
					activity.setAuditStatus("4");
				}
			}
			activity.setUpdateBy(authUser.getId());
			activity.setUpdateTime(new Date());
			activity.setRemarks(activityExamine.getReason());
			boolean result = activityService.updateById(activity);
			activityExamine.setCreateBy(authUser.getId());
			activityExamine.setCreateTime(new Date());
			activityExamineService.save(activityExamine);
			// 0 审核通过 1 驳回
			if (activityExamine.getStatus().equals("0")) {
				return ResultUtil.data("审核通过");
			} else {
				return ResultUtil.data("驳回成功 ");
			}
		}
		return ResultUtil.data(ResultCode.TOKEN_OVER);
	}

}
