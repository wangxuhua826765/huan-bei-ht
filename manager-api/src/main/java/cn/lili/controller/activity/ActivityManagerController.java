package cn.lili.controller.activity;

import cn.hutool.core.util.ObjectUtil;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.activity.entity.Activity;
import cn.lili.modules.activity.entity.vo.ActivityVO;
import cn.lili.modules.activity.service.ActivityExamineService;
import cn.lili.modules.activity.service.ActivityService;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.goods.service.GoodsService;
import cn.lili.modules.system.aspect.annotation.SystemLogPoint;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "管理端-活动管理")
@RequestMapping("/manager/activity")
public class ActivityManagerController {

	@Autowired
	private ActivityService activityService;

	@Autowired
	private GoodsService goodsService;

	@Autowired
	private ActivityExamineService activityExamineService;

	/**
	 * 新增
	 */
	@RequestMapping("/save")
	@SystemLogPoint(description = "新增活动", customerLog = "'新增活动请求", type = "2")
	public ResultMessage save(Activity activity) {
		AuthUser authUser = UserContext.getCurrentUser();
		if (ObjectUtil.isNotEmpty(authUser)) {
			if (activityService.getOne(new QueryWrapper<Activity>().eq("name", activity.getName())
					.eq("delete_flag", false).eq("activity_type", activity.getActivityType())) != null) {
				return ResultUtil.data("保存失败，名字重复");
			}
			if ("0".equals(activity.getActivityType())) {
				if (StringUtils.isNotEmpty(activity.getRegistrationEndTime().toString())
						&& StringUtils.isNotEmpty(activity.getRegistrationStartTime().toString())
						&& StringUtils.isNotEmpty(activity.getActivityStartTime().toString())
						&& StringUtils.isNotEmpty(activity.getActivityEndTime().toString())) {
					if (activity.getActivityStartTime().before(activity.getRegistrationEndTime())) {
						return ResultUtil.error(2001, "活动开始时间应该在活动报名结束时间之后");
					}
				}
			}
			UserEnums role = authUser.getRole();
			if (StringUtils.isNotEmpty(role.getRole()) && role.getRole().equals("代理商")) {
				activity.setSubmitType("1");
				activity.setAuditStatus("1");
			} else if (StringUtils.isNotEmpty(role.getRole()) && role.getRole().equals("管理员")) {
				activity.setSubmitType("0");
				activity.setAuditStatus("3");
			}
			activity.setDeleteFlag(false);
			activity.setCreateTime(new Date());
			activity.setCreateBy(authUser.getId());
			activity.setPublisher(authUser.getId());
			activity.setStatus(false);
			if (activity.getRegistrationStartTime() == null) {
				activity.setRegistrationStartTime(new Date(0));
			}
			if (activity.getRegistrationEndTime() == null) {
				activity.setRegistrationEndTime(new Date(0));
			}
			activity.setQuantityFlag(false);
			activity.setQuantity(0);
			activityService.save(activity);
			return ResultUtil.data("保存成功");
		}
		return ResultUtil.data(ResultCode.TOKEN_OVER);

	}

	/**
	 * 删除
	 */
	@DeleteMapping("/{id}")
	@SystemLogPoint(description = "删除活动", customerLog = "'删除活动请求", type = "2")
	public ResultMessage delete(@PathVariable String id) {
		Activity activity = activityService.getOne(new QueryWrapper<Activity>().eq("id", id));
		if (activity != null) {
			Activity act = new Activity();
			act.setDeleteFlag(true);
			act.setId(id);
			return ResultUtil.data(activityService.updateById(act));
		} else {
			return ResultUtil.data("没有找到该对象");
		}
	}

	/**
	 * 查询
	 */
	@RequestMapping("/{id}")
	public ResultMessage find(@PathVariable String id) {
		ActivityVO activityVO = activityService.getActivityById(new QueryWrapper<Activity>().eq("a.id", id));
		if (ObjectUtil.isNotEmpty(activityVO)) {
			return ResultUtil.data(activityVO);
		} else {
			return ResultUtil.data("没有找到该对象");
		}
	}

	/**
	 * 自动分页查询
	 */
	@RequestMapping("/list")
	public ResultMessage<IPage<ActivityVO>> list(ActivityVO activityVO, PageVO page) {
		AuthUser authUser = UserContext.getCurrentUser();
		IPage<ActivityVO> activityVOIPage = new Page<>();
		if (ObjectUtil.isNotEmpty(authUser)) {
			UserEnums role = authUser.getRole();
			if (role.getRole().equals("代理商")) {
				activityVO.setSubmitType("1");
				activityVO.setPublisher(authUser.getId());
			} else if (role.getRole().equals("管理员")) {
				activityVO.setSubmitType("0");
			}
			activityVOIPage = activityService.selectPage(activityVO, page);
			return ResultUtil.data(activityVOIPage);
		}
		return ResultUtil.data(activityVOIPage);
	}

	@PutMapping("/{id}")
	@SystemLogPoint(description = "修改活动", customerLog = "'修改活动请求", type = "2")
	public ResultMessage edit(@PathVariable String id, Activity activity) {
		AuthUser authUser = UserContext.getCurrentUser();
		if (ObjectUtil.isNotEmpty(authUser)) {
			if (authUser.getRole().getRole().equals("代理商")) {
				activity.setAuditStatus("1");
			}
			if (cn.lili.common.utils.StringUtils.isNotEmpty(activity.getRegistrationEndTime().toString())
					&& cn.lili.common.utils.StringUtils.isNotEmpty(activity.getRegistrationStartTime().toString())
					&& cn.lili.common.utils.StringUtils.isNotEmpty(activity.getActivityStartTime().toString())
					&& cn.lili.common.utils.StringUtils.isNotEmpty(activity.getActivityEndTime().toString())) {
				if (activity.getActivityStartTime().before(activity.getRegistrationEndTime())) {
					return ResultUtil.error(2001, "活动开始时间应该在活动报名结束时间之后");
				}
			}
			activity.setId(id);
			activity.setStatus(false);
			activity.setUpdateBy(authUser.getId());
			activity.setUpdateTime(new Date());
			boolean result = activityService.updateById(activity);
			return ResultUtil.data(result);
		}
		return ResultUtil.data(ResultCode.TOKEN_OVER);
	}

	/**
	 * 审核管理-分页查询
	 */
	@RequestMapping("/status")
	public ResultMessage<IPage<ActivityVO>> getListActivity(ActivityVO activityVO, PageVO page) {
		return ResultUtil.data(activityService.getIPageActivity(activityVO, page));
	}

	/**
	 * 下架操作
	 */
	@PutMapping("/status/{id}")
	@SystemLogPoint(description = "下架活动", customerLog = "'下架活动请求", type = "2")
	public ResultMessage edit(@PathVariable String id) {
		Activity activity = new Activity();
		AuthUser authUser = UserContext.getCurrentUser();
		if (ObjectUtil.isNotEmpty(authUser)) {
			activity.setId(id);
			activity.setStatus(true);
			activity.setUpdateBy(authUser.getId());
			activity.setUpdateTime(new Date());
			boolean result = activityService.updateById(activity);
			return ResultUtil.data(result);
		}
		return ResultUtil.data(ResultCode.TOKEN_OVER);
	}

}
