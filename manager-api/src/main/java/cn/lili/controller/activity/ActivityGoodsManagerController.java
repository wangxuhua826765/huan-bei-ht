package cn.lili.controller.activity;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.goods.service.GoodsService;
import cn.lili.modules.store.entity.vos.StoreVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "管理端-活动管理")
@RequestMapping("/manager/activity/goods")
public class ActivityGoodsManagerController {

	@Autowired
	private GoodsService goodsService;

	/**
	 * 根据活动id查询商品详情
	 */
	@RequestMapping("/goods")
	public ResultMessage getGoodsByActivityId(String activityId) {
		List<StoreVO> storeVOList = goodsService.getStoreByActivityId(activityId);
		for (StoreVO storeVO : storeVOList) {
			List<GoodsVO> goodsVOList = goodsService.getGoodsByActivityId(activityId, storeVO.getId());
			storeVO.setGoodsVOList(goodsVOList);
		}
		return ResultUtil.data(storeVOList);
	}

	/**
	 * 根据活动id查询商品详情
	 */
	@RequestMapping("/store")
	public ResultMessage getStoreByActivityId(String activityId) {
		List<StoreVO> storeVOList = goodsService.getStoreByActivityId(activityId);
		return ResultUtil.data(storeVOList);
	}

}
