package cn.lili.controller.finance;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.finance.entity.dos.FinancialFunds;
import cn.lili.modules.finance.entity.vo.FinancialFundsSearchParams;
import cn.lili.modules.finance.entity.vo.FinancialFundsVO;
import cn.lili.modules.finance.service.FinancialFundsService;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.service.CreditManagementService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 授信管理
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "授信管理")
@RequestMapping("/manager/finance/financialfunds")
public class FinanceController {

	/**
	 * 授信管理
	 */
	@Autowired
	private FinancialFundsService financialFundsService;

	@ApiOperation(value = "授信管理")
	@ApiImplicitParams({@ApiImplicitParam(name = "bName", value = "商家名称", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<FinancialFundsVO>> getByPage(FinancialFundsSearchParams financialFundsSearchParams) {

		return ResultUtil.data(financialFundsService.creditManagementPage(financialFundsSearchParams));
	}

	@PostMapping
	@ApiOperation(value = "新增")
	public ResultMessage<FinancialFunds> save(FinancialFunds financialFunds) {
		financialFundsService.save(financialFunds);
		return ResultUtil.data(financialFunds);
	}

	@PutMapping("/{id}")
	@ApiOperation(value = "更新")
	public ResultMessage<FinancialFunds> update(@PathVariable String id, FinancialFunds financialFunds) {
		financialFundsService.updateById(financialFunds);
		return ResultUtil.data(financialFunds);
	}

	@DeleteMapping(value = "/{ids}")
	@ApiOperation(value = "删除")
	public ResultMessage<Object> delAllByIds(@PathVariable List<String> ids) {
		financialFundsService.deleteByIds(ids);
		return ResultUtil.success();
	}

}
