package cn.lili.controller.message;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.message.entity.dos.RollMessage;
import cn.lili.modules.message.service.RollMessageService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/manager/message")
public class RollMessageManagerController {

	@Autowired
	private RollMessageService rollMessageService;

	/**
	 * 新增或编辑
	 */
	@RequestMapping("/save")
	public ResultMessage<Object> save(RollMessage rollMessage) {
		rollMessage.setCreateTime(new Date());
		rollMessage.setCreateBy(UserContext.getCurrentUser().getId());
		rollMessage.setUpdateTime(new Date());
		rollMessage.setUpdateBy(UserContext.getCurrentUser().getId());
		rollMessage.setDelFlag(false);
		rollMessage.setType(0);
		boolean b = rollMessageService.save(rollMessage);
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	/**
	 * 新增或编辑
	 */
	@PutMapping("/update/{id}")
	public ResultMessage<Object> update(@PathVariable String id, RollMessage rollMessage) {
		rollMessage.setId(id);
		rollMessage.setUpdateTime(new Date());
		rollMessage.setUpdateBy(UserContext.getCurrentUser().getId());
		rollMessage.setDelFlag(false);
		boolean b = rollMessageService.updateById(rollMessage);
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	/**
	 * 查询所有自定义的
	 */
	@RequestMapping("/list")
	public ResultMessage<Object> selectList(RollMessage rollMessage, PageVO pageVo) {
		IPage<RollMessage> page = rollMessageService.selectList(rollMessage, pageVo);

		return ResultUtil.data(page);
	}

	/**
	 * 查询自定义详情
	 */
	@RequestMapping("/{id}")
	public ResultMessage<Object> selectById(@PathVariable String id) {
		RollMessage rollMessage = rollMessageService.getById(id);
		return ResultUtil.data(rollMessage);
	}

	/**
	 * 删除
	 */
	@DeleteMapping("/roll/{id}")
	public ResultMessage<Object> deleteMessage(@PathVariable String id) {
		RollMessage rollMessage = new RollMessage();
		rollMessage.setId(id);
		rollMessage.setDelFlag(true);
		boolean b = rollMessageService.updateById(rollMessage);
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	/**
	 * 修改状态
	 */
	@PutMapping("/{id}")
	public ResultMessage<Object> updateMessageState(@PathVariable String id, Integer status) {
		RollMessage rollMessage = new RollMessage();
		rollMessage.setStatus(status);
		rollMessage.setId(id);
		this.rollMessageService.updateById(rollMessage);
		return ResultUtil.success(ResultCode.SUCCESS);
	}

}
