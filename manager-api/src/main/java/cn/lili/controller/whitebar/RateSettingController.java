package cn.lili.controller.whitebar;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.system.entity.dos.Setting;
import cn.lili.modules.system.entity.enums.SettingEnum;
import cn.lili.modules.system.service.SettingService;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.dos.RateSetting;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.entity.vo.RateSettingSearchParams;
import cn.lili.modules.whitebar.entity.vo.RateSettingVO;
import cn.lili.modules.whitebar.service.CreditManagementService;
import cn.lili.modules.whitebar.service.RateSettingService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 费率管理
 *
 * @author 贾送兵
 * @since 2022-02-17 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "费率管理")
@RequestMapping("/manager/whitebar/rateSetting")
public class RateSettingController {

	/**
	 * 费率管理
	 */
	@Autowired
	private RateSettingService rateSettingService;

	// 系统配置
	@Autowired
	private SettingService settingService;

	@ApiOperation(value = "费率管理")
	@ApiImplicitParams({@ApiImplicitParam(name = "bName", value = "商家名称", paramType = "query"),
			@ApiImplicitParam(name = "roleName", value = "费率名称", paramType = "query")})
	@GetMapping
	public ResultMessage<IPage<RateSettingVO>> list(RateSettingSearchParams rateSettingSearchParams) {
		IPage<RateSettingVO> rateSettingVOIPage = rateSettingService.rateSettingPage(rateSettingSearchParams);
		return ResultUtil.data(rateSettingVOIPage);
	}

	@ApiOperation(value = "费率管理")
	@ApiImplicitParams({@ApiImplicitParam(name = "bName", value = "商家名称", paramType = "query"),
			@ApiImplicitParam(name = "roleName", value = "费率名称", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<Map<String, Object>> getByPage(RateSettingSearchParams rateSettingSearchParams) {
		Map<String, Object> map = new HashMap<>();
		IPage<RateSettingVO> rateSettingVOIPage = rateSettingService.rateSettingPage(rateSettingSearchParams);
		map.put("rateSetting", rateSettingVOIPage.getRecords());
		Setting setting = settingService.get(SettingEnum.ORDER_SETTING.toString());
		map.put("setting", setting.getSettingValue());
		return ResultUtil.data(map);
	}

	@RequestMapping(value = "/add")
	@ApiOperation(value = "新增")
	public ResultMessage<RateSetting> save(RateSetting rateSetting) {
		rateSettingService.save(rateSetting);
		return ResultUtil.data(rateSetting);
	}

	@PutMapping("/update/{id}")
	@ApiOperation(value = "更新")
	public ResultMessage<RateSetting> update(@PathVariable String id, RateSetting rateSetting) {
		rateSettingService.updateById(rateSetting);
		return ResultUtil.data(rateSetting);
	}

	@DeleteMapping(value = "/delete/{ids}")
	@ApiOperation(value = "删除")
	public ResultMessage<Object> delAllByIds(@PathVariable List<String> ids) {
		rateSettingService.deleteByIds(ids);
		return ResultUtil.success();
	}

}
