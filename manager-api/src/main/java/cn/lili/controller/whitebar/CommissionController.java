package cn.lili.controller.whitebar;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.whitebar.entity.dos.Commission;
import cn.lili.modules.whitebar.entity.vo.CommissionSearchParams;
import cn.lili.modules.whitebar.entity.vo.CommissionVO;
import cn.lili.modules.whitebar.service.CommissionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 佣金管理
 *
 * @author 贾送兵
 * @since 2022-02-17 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "佣金管理")
@RequestMapping("/manager/whitebar/commission")
public class CommissionController {

	/**
	 * 佣金管理
	 */
	@Autowired
	private CommissionService commissionService;

	@ApiOperation(value = "佣金管理")
	@ApiImplicitParams({@ApiImplicitParam(name = "userName", value = "名称", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<CommissionVO>> getByPage(CommissionSearchParams commissionSearchParams) {

		return ResultUtil.data(commissionService.commissionPage(commissionSearchParams));
	}

	@RequestMapping(value = "/add")
	@ApiOperation(value = "新增")
	public ResultMessage<Commission> save(Commission commission) {
		commissionService.save(commission);
		return ResultUtil.data(commission);
	}

	@PutMapping("/update/{id}")
	@ApiOperation(value = "更新")
	public ResultMessage<Commission> update(@PathVariable String id, Commission commission) {
		commissionService.updateById(commission);
		return ResultUtil.data(commission);
	}

	@DeleteMapping(value = "/delete/{ids}")
	@ApiOperation(value = "删除")
	public ResultMessage<Object> delAllByIds(@PathVariable List<String> ids) {
		commissionService.deleteByIds(ids);
		return ResultUtil.success();
	}

}
