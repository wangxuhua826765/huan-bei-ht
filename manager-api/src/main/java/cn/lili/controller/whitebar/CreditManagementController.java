package cn.lili.controller.whitebar;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.payment.entity.vo.FundManagementSearchParams;
import cn.lili.modules.payment.entity.vo.FundManagementVO;
import cn.lili.modules.payment.service.FundManagementService;
import cn.lili.modules.permission.entity.dos.Department;
import cn.lili.modules.wallet.entity.dos.MemberWallet;
import cn.lili.modules.wallet.entity.dos.WalletLogDetail;
import cn.lili.modules.wallet.entity.dto.MemberWalletUpdateDTO;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.service.MemberWalletService;
import cn.lili.modules.whitebar.entity.dos.CreditManagement;
import cn.lili.modules.whitebar.entity.vo.CreditManagementSearchParams;
import cn.lili.modules.whitebar.entity.vo.CreditManagementVO;
import cn.lili.modules.whitebar.service.CreditManagementService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 授信管理
 *
 * @author 贾送兵
 * @since 2022-02-17 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "授信管理")
@RequestMapping("/manager/whitebar/credit")
public class CreditManagementController {

	/**
	 * 授信管理
	 */
	@Autowired
	private CreditManagementService creditManagementService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberWalletService memberWalletService;

	@ApiOperation(value = "授信管理")
	@ApiImplicitParams({@ApiImplicitParam(name = "bName", value = "商家名称", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<CreditManagementVO>> getByPage(
			CreditManagementSearchParams creditManagementSearchParams) {
		// List<DictionaryVO> lsitdict = this.dictionaryService.allChildren();

		return ResultUtil.data(creditManagementService.creditManagementPage(creditManagementSearchParams));
	}

	@RequestMapping(value = "/add")
	@ApiOperation(value = "新增")
	public ResultMessage<CreditManagement> save(CreditManagement creditManagement) {
		creditManagementService.save(creditManagement);
		return ResultUtil.data(creditManagement);
	}

	@PutMapping("/update/{id}")
	@ApiOperation(value = "更新")
	public ResultMessage<CreditManagement> update(@PathVariable String id, CreditManagement department) {
		creditManagementService.updateById(department);
		return ResultUtil.data(department);
	}

	@PutMapping("/audit/{id}")
	@ApiOperation(value = "审核")
	public ResultMessage<CreditManagement> audit(@PathVariable String id, CreditManagement department) {

		if ("1".equals(department.getAuditStatus())) {
			department.setCreditDate(new Date());
		} else if ("2".equals(department.getAuditStatus())) {
			department.setOperatorId(UserContext.getCurrentUser().getId());
			department.setOperatorName(UserContext.getCurrentUser().getUsername());
		}
		boolean status = creditManagementService.updateById(department);
		Member member = memberService.getById(department.getMemberId());
		if (status && "1".equals(department.getAuditStatus())) {
			MemberWallet to = memberWalletService.getMemberWalletInfo(member.getId(), WalletOwnerEnum.SALE.name());
			memberWalletService.departmentOrder(id, null, to, department.getCreditLine());
			// memberWalletService.increase(new MemberWalletUpdateDTO(
			// department.getCreditLine(),
			// member.getId(),
			// "授信订单【" + id + "】获得金额[" + department.getCreditLine() + "]",
			// DepositServiceTypeEnum.WALLET_PAY.name(), WalletOwnerEnum.SALE.name()),new
			// WalletLogDetail(
			// null,
			// null,
			// "10",
			// null,
			// null,
			// null,
			// member.getId(),
			// member.getNickName(),
			// null,
			// null,
			// null,
			// department.getCreditLine()
			// ));
		}
		return ResultUtil.data(department);
	}

	@DeleteMapping(value = "/delete/{ids}")
	@ApiOperation(value = "删除")
	public ResultMessage<Object> delAllByIds(@PathVariable List<String> ids) {
		creditManagementService.deleteByIds(ids);
		return ResultUtil.success();
	}

}
