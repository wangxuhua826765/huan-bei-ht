package cn.lili.controller.goods;

import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.dos.GoodsSku;
import cn.lili.modules.goods.entity.dto.GoodsSearchParams;
import cn.lili.modules.goods.entity.enums.GoodsAuthEnum;
import cn.lili.modules.goods.entity.enums.GoodsStatusEnum;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.goods.service.GoodsService;
import cn.lili.modules.goods.service.GoodsSkuService;
import cn.lili.modules.system.aspect.annotation.SystemLogPoint;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.Arrays;
import java.util.List;

/**
 * 管理端,商品管理接口
 *
 * @author pikachu
 * @since 2020-02-23 15:18:56
 */
@RestController
@Api(tags = "管理端,商品管理接口")
@RequestMapping("/manager/goods")
public class GoodsManagerController {
	/**
	 * 商品
	 */
	@Autowired
	private GoodsService goodsService;
	/**
	 * 规格商品
	 */
	@Autowired
	private GoodsSkuService goodsSkuService;

	@ApiOperation(value = "分页获取")
	@RequestMapping(value = "/list")
	public IPage<GoodsVO> getByPage(GoodsSearchParams goodsSearchParams) {
		return goodsService.queryByParams(goodsSearchParams);
	}

	@ApiOperation(value = "分页获取商品列表")
	@RequestMapping(value = "/sku/list")
	public ResultMessage<IPage<GoodsSku>> getSkuByPage(GoodsSearchParams goodsSearchParams) {
		return ResultUtil.data(goodsSkuService.getGoodsSkuByPage(goodsSearchParams));
	}

	@ApiOperation(value = "分页获取待审核商品")
	@RequestMapping(value = "/auth/list")
	public IPage<GoodsVO> getAuthPage(GoodsSearchParams goodsSearchParams) {

		goodsSearchParams.setAuthFlag(GoodsAuthEnum.TOBEAUDITED.name());
		return goodsService.queryByParams(goodsSearchParams);
	}

	@ApiOperation(value = "管理员下架商品", notes = "管理员下架商品时使用")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goodsId", value = "商品ID", required = true, paramType = "query", allowMultiple = true),
			@ApiImplicitParam(name = "reason", value = "下架理由", required = true, paramType = "query")})
	@PutMapping(value = "/{goodsId}/under")
	@SystemLogPoint(description = "下架商品", customerLog = "'下架商品请求", type = "2")
	public ResultMessage<Object> underGoods(@PathVariable String goodsId,
			@NotEmpty(message = "下架原因不能为空") @RequestParam String reason) {
		List<String> goodsIds = Arrays.asList(goodsId.split(","));
		if (Boolean.TRUE.equals(goodsService.managerUpdateGoodsMarketAble(goodsIds, GoodsStatusEnum.DOWN, reason))) {
			return ResultUtil.success();
		}
		throw new ServiceException(ResultCode.GOODS_UNDER_ERROR);
	}

	@ApiOperation(value = "管理员审核商品", notes = "管理员审核商品")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goodsIds", value = "商品ID", required = true, paramType = "path", allowMultiple = true, dataType = "int"),
			@ApiImplicitParam(name = "authFlag", value = "审核结果", required = true, paramType = "query", dataType = "string")})
	@PutMapping(value = "{goodsIds}/auth")
	@SystemLogPoint(description = "审核商品", customerLog = "'审核商品请求", type = "2")
	public ResultMessage<Object> auth(@PathVariable List<String> goodsIds, @RequestParam String authFlag,
			@RequestParam String authMessage) {
		// 校验商品是否存在
		if (goodsService.auditGoods(goodsIds, GoodsAuthEnum.valueOf(authFlag), authMessage)) {
			return ResultUtil.success();
		}
		throw new ServiceException(ResultCode.GOODS_AUTH_ERROR);
	}

	@ApiOperation(value = "管理员上架商品", notes = "管理员上架商品时使用")
	@PutMapping(value = "/{goodsId}/up")
	@ApiImplicitParams({@ApiImplicitParam(name = "goodsId", value = "商品ID", required = true, allowMultiple = true)})
	@SystemLogPoint(description = "上架商品", customerLog = "'上架商品请求", type = "2")
	public ResultMessage<Object> unpGoods(@PathVariable List<String> goodsId) {
		if (goodsService.updateGoodsMarketAble(goodsId, GoodsStatusEnum.UPPER, "")) {
			return ResultUtil.success();
		}
		throw new ServiceException(ResultCode.GOODS_UPPER_ERROR);
	}

	@ApiOperation(value = "通过id获取商品详情")
	@RequestMapping(value = "/get/{id}")
	public ResultMessage<GoodsVO> get(@PathVariable String id) {
		GoodsVO goods = goodsService.getGoodsVO(id);
		return ResultUtil.data(goods);
	}

	@ApiOperation(value = "商品推荐", notes = "商品推荐")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goodsId", value = "商品ID", required = true, paramType = "query", allowMultiple = true),
			@ApiImplicitParam(name = "recommend", value = "是否推荐", required = true, paramType = "query"),
			@ApiImplicitParam(name = "recommendRanking", value = "推荐排名", required = true, paramType = "query")})
	@PutMapping(value = "/recommend")
	public ResultMessage<Boolean> recommendGoods(@RequestParam String goodsId, @RequestParam Boolean recommend,
			@RequestParam int recommendRanking) {

		return ResultUtil.data(goodsService.updateRecommend(goodsId, recommend, recommendRanking));
	}

	@ApiOperation(value = "商品排名", notes = "商品排名")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goodsId", value = "商品ID", required = true, paramType = "query", allowMultiple = true),
			@ApiImplicitParam(name = "sort", value = "排名", required = true, paramType = "query")})
	@PutMapping(value = "/sort")
	public ResultMessage<Boolean> sort(@RequestParam String goodsId, @RequestParam Long sort) {

		return ResultUtil.data(goodsService.updateSort(goodsId, sort));
	}

	@ApiOperation(value = "分页获取商品初审")
	@RequestMapping(value = "/firstTrial/list")
	public IPage<GoodsVO> getfirstTrialPage(GoodsSearchParams goodsSearchParams) {
		goodsSearchParams.setAuthFlag(GoodsAuthEnum.FIRSTTRIAL.name());
		return goodsService.getfirstTrialPage(goodsSearchParams);
	}

	@ApiOperation(value = "城市合伙人审核商品", notes = "城市合伙人审核商品")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goodsIds", value = "商品ID", required = true, paramType = "path", allowMultiple = true, dataType = "int"),
			@ApiImplicitParam(name = "authFlag", value = "审核结果", required = true, paramType = "query", dataType = "string")})
	@PutMapping(value = "{goodsIds}/firstTrial")
	public ResultMessage<Object> firstTrial(@PathVariable List<String> goodsIds, @RequestParam String authFlag,
			@RequestParam String authMessage) {
		// 校验商品是否存在
		if (goodsService.firstTrialGoods(goodsIds, GoodsAuthEnum.valueOf(authFlag), authMessage)) {
			return ResultUtil.success();
		}
		throw new ServiceException(ResultCode.GOODS_AUTH_ERROR);
	}
}
