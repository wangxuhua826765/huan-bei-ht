package cn.lili.controller.statistics;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.statistics.entity.dto.GoodsStatisticsQueryParam;
import cn.lili.modules.statistics.entity.dto.StatisticsQueryParam;
import cn.lili.modules.statistics.entity.enums.StatisticsQuery;
import cn.lili.modules.statistics.entity.vo.GoodsStatisticsDataVO;
import cn.lili.modules.statistics.entity.vo.IndexNoticeVO;
import cn.lili.modules.statistics.entity.vo.IndexStatisticsVO;
import cn.lili.modules.statistics.entity.vo.StoreStatisticsDataVO;
import cn.lili.modules.statistics.service.IndexStatisticsService;
import cn.lili.modules.system.service.RegionService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 管理端,首页统计数据接口
 *
 * @author Bulbasaur
 * @since 2020/12/15 17:53
 */
@Slf4j
@Api(tags = "管理端,首页统计数据接口")
@RestController
@RequestMapping("/manager/statistics/index")
public class IndexStatisticsManagerController {

	/**
	 * 首页统计
	 */
	@Autowired
	private IndexStatisticsService indexStatisticsService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private RegionService regionService;

	@ApiOperation(value = "获取首页查询数据")
	@GetMapping
	public ResultMessage<IndexStatisticsVO> index() {
		try {
			return ResultUtil.data(indexStatisticsService.indexStatistics());
		} catch (Exception e) {
			log.error("获取首页查询数据错误", e);
		}
		return null;
	}

	@ApiOperation(value = "获取首页查询热卖商品TOP10")
	@RequestMapping("/goodsStatistics")
	public ResultMessage<List<GoodsStatisticsDataVO>> goodsStatistics(
			GoodsStatisticsQueryParam goodsStatisticsQueryParam) {
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				goodsStatisticsQueryParam.setAdCodeList(regionService.getPartnerAdCode(partner.getRegionId(), "4"));
			}
		}
		// 按照金额查询
		goodsStatisticsQueryParam.setType(StatisticsQuery.NUM.name());
		return ResultUtil.data(indexStatisticsService.goodsStatistics(goodsStatisticsQueryParam));
	}

	@ApiOperation(value = "获取首页查询热卖店铺TOP10")
	@RequestMapping("/storeStatistics")
	public ResultMessage<List<StoreStatisticsDataVO>> storeStatistics(StatisticsQueryParam statisticsQueryParam) {
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				statisticsQueryParam.setAdCodeList(regionService.getPartnerAdCode(partner.getRegionId(), "4"));
			}
		}
		return ResultUtil.data(indexStatisticsService.storeStatistics(statisticsQueryParam));
	}

	@ApiOperation(value = "通知提示信息")
	@RequestMapping("/notice")
	public ResultMessage<IndexNoticeVO> notice() {
		return ResultUtil.data(indexStatisticsService.indexNotice());
	}

	@ApiOperation(value = "首页人员统计图表人员")
	@RequestMapping("/chart")
	public ResultMessage<Map<String, List>> chart(GoodsStatisticsQueryParam goodsStatisticsQueryParam) {
		return ResultUtil.data(indexStatisticsService.chart(goodsStatisticsQueryParam));
	}

	@ApiOperation(value = "首页人员统计图表，金额")
	@RequestMapping("/chartMoney")
	public ResultMessage<Map<String, List>> chartMoney(GoodsStatisticsQueryParam goodsStatisticsQueryParam) {
		return ResultUtil.data(indexStatisticsService.chartMoney(goodsStatisticsQueryParam));
	}
}
