package cn.lili.controller.walletAll;

import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.lili.common.utils.CurrencyUtil;
import cn.lili.modules.member.entity.dos.GradeLevel;
import cn.lili.modules.member.entity.vo.GradeVO;
import cn.lili.modules.member.service.GradeLevelService;
import cn.lili.modules.member.service.GradeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.lili.common.utils.SnowFlake;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.RechargeFow;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.member.service.RechargeFowService;
import cn.lili.modules.order.order.entity.enums.PayStatusEnum;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.service.RechargeService;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.modules.walletAll.service.AllWalletService;

@RestController
@RequestMapping("/manager/member/wallet")
public class MemberWalletController {

    /**
     * 会员 PROMOTE_HY
     */

    @Autowired
    WalletDetailService walletDetailService;

    @Autowired
    RechargeService rechargeService;

    @Autowired
    RechargeFowService rechargeFowService;

    @Autowired
    AllWalletService allWalletService;

    @Autowired
    MemberService memberService;

    @Autowired
    GradeService gradeService;

    @GetMapping
    public Object get() {
        String owner = "PROMOTE_HY";
        List<Map> list = allWalletService.getAllWallet(owner);
        for (Map m : list) {
            String memberId = m.get("member_id").toString();
            Double money = Double.parseDouble(m.get("money").toString());
            // 添加历史流水
            WalletDetail detail = new WalletDetail();

            if (!StringUtils.equals("admin", memberId)) {
                // 获取会员信息
                Member member = memberService.getById(memberId);
//                if (member == null) {
//                    continue;
//                }
                // 查询会员身份
                GradeVO gradeVO = gradeService.findByGradeVO(memberId);
                Double fee = gradeVO != null ? gradeVO.getRechargeRule() : 0.96D;
                Double xianjian = CurrencyUtil.mul(money, fee);

                Recharge recharge = new Recharge();
                //recharge.setId();//编码
                //recharge.setCreateBy();//创建者
                //recharge.setCreateTime();//创建时间
//                recharge.setMemberId(memberId);//会员id
//                recharge.setMemberName(member.getNickName());//会员名称
                recharge.setPayStatus(PayStatusEnum.PAID.name());//支付状态
                //recharge.setPaymentPluginId();//支付插件id
                //TODO : 易呗数量冲突吗
                recharge.setRechargeMoney(money);//充值金额
                //TODO : 生成方式
                String sn = "Y" + SnowFlake.getId();
                recharge.setRechargeSn(sn);//充值订单编号
//                recharge.setRechargeWay("WECHAT");//充值方式
                //recharge.setPayTime();//支付时间
                //recharge.setReceivableNo();//第三方流水
                //TODO : 充值金额冲突吗
//                recharge.setYiBei(money);//易呗数量
                recharge.setRechargeType("3");//1提现2充值年费会员3充值会员4普通充值 5 充值合伙人42 充值并交年费  43 充值并交银卡会员 44 充值并交金卡会员 45 充值并成为合伙人
                //TODO : 订单来源?
                recharge.setOwner("RECHARGE");//订单来源  STORE 商家端   BUYER 用户端
                recharge.setPrice(xianjian);//扣除金额
                //recharge.setOrderSn();//订单号（只有消费时才会有）
                //添加数据
                rechargeService.save(recharge);


                RechargeFow rechargeFow = new RechargeFow();
                //rechargeFow.setId();//ID
                //rechargeFow.setCreateBy();//创建者
                //rechargeFow.setCreateTime();//创建时间
                //rechargeFow.setDeleteFlag();//删除标志 true/false 删除/未删除
                //rechargeFow.setUpdateBy();//更新者
                //rechargeFow.setUpdateTime();//更新时间
                rechargeFow.setMemberId(memberId);//用户id
                rechargeFow.setSn(sn);//编号
                //TODO : 焕呗额冲突吗
                rechargeFow.setMoney(xianjian);//现金额
                //TODO : 现金额冲突吗
                rechargeFow.setHuanBei(money);//焕呗额
                rechargeFow.setAddTime(new Date());//充值时间
                rechargeFow.setRechargeType("3");//1提现2充值年费会员3充值会员4普通充值 5 充值合伙人
                rechargeFow.setFee(fee);//费率
                rechargeFow.setCanMoney(xianjian);//可提现现金额
                rechargeFow.setCanHuanBei(money);//可提现焕呗额
                rechargeFow.setRealization("0");//可提现金额是否提完,-1已提完  0未提现完
                //添加数据
                rechargeFowService.save(rechargeFow);


                // 封装钱包流水
                // TODO:订单号怎么处理？
                detail.setSn(sn);
                detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_HY.name());
                detail.setPayee(memberId);
                detail.setPayeeName(member != null ? member.getNickName() : null);
                detail.setPayeeOwner(owner);
                detail.setPayeeBeforeMoney(null);
                detail.setMoney(money);
                // 添加钱包流水
                 walletDetailService.save(detail);
                System.out.println(detail.toString());

            }
        }
        return list;
    }
}
