package cn.lili.controller.walletAll;

import cn.lili.common.utils.StringUtils;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.modules.walletAll.service.AllWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/manager/servicecharge/wallet")
public class ServiceChargeWalletController {

    /**
     * 服务费 PROMOTE_FW
     */

    @Autowired
    WalletDetailService walletDetailService;

    @Autowired
    AllWalletService allWalletService;

    @Autowired
    MemberService memberService;

    @GetMapping
    public Object get() {
        String owner = "PROMOTE_FW";
        List<Map> list = allWalletService.getAllWallet(owner);
        for (Map m : list) {
            String memberId = m.get("member_id").toString();
            Double money = Double.parseDouble(m.get("money").toString());
            // 添加历史流水
            WalletDetail detail = new WalletDetail();

            if (!StringUtils.equals("admin", memberId)) {
                // 获取会员信息
                Member member = memberService.getById(memberId);
//                if (member==null){
//                    continue;
//                }
                // 封装钱包流水
                // TODO:订单号怎么处理？
                detail.setSn(null);
                detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_FW.name());
                detail.setPayee(memberId);
                detail.setPayeeName(member != null ? member.getNickName() : null);
                detail.setPayeeOwner(owner);
                detail.setPayeeBeforeMoney(null);
                detail.setMoney(money);
                // 添加钱包流水
                 walletDetailService.save(detail);
                System.out.println(detail.toString());
            }
            m.put("detail",detail);
        }
        return list;
    }

}
