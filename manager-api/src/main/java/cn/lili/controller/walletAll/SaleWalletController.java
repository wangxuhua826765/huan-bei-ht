package cn.lili.controller.walletAll;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.SnowFlake;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.order.order.entity.dos.Order;
import cn.lili.modules.order.order.entity.dos.OrderItem;
import cn.lili.modules.order.order.entity.enums.OrderStatusEnum;
import cn.lili.modules.order.order.entity.enums.OrderTypeEnum;
import cn.lili.modules.order.order.entity.enums.PayStatusEnum;
import cn.lili.modules.order.order.service.OrderItemService;
import cn.lili.modules.order.order.service.OrderService;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.service.StoreService;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.modules.walletAll.service.AllWalletService;

@RestController
@RequestMapping("/manager/sale/wallet")
public class SaleWalletController {

    /**
     * 销售 SALE
     */

    @Autowired
    MemberService memberService;
    @Autowired
    WalletDetailService walletDetailService;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderItemService orderItemService;

    @Autowired
    AllWalletService allWalletService;
    @Autowired
    StoreService storeService;

    @GetMapping
    public Object get() {
        String owner = "SALE";
        List<Map> list = allWalletService.getAllWallet(owner);
        for (Map m : list) {
            String memberId = m.get("member_id").toString();
            Double money = Double.parseDouble(m.get("money").toString());
            // 添加历史流水
            WalletDetail detail = new WalletDetail();
            // 添加订单
            Order order = new Order();
            // 添加订单
            OrderItem orderItem = new OrderItem();

            if (!StringUtils.equals("admin", memberId)) {
                // 获取会员信息
                Member member = memberService.getById(memberId);
                // 获取会员店铺信息
                Store store = new Store();
                if (member != null) {
                    if (member.getStoreId() != null && StringUtils.isNotEmpty(member.getStoreId())) {
                        store = storeService.getById(member.getStoreId());
                    }
                }
                // 封装订单信息
                order.setOwner(owner);
//                order.setMemberId(memberId);
//                order.setMemberName(member.getNickName());
                order.setStoreId(member != null ? member.getStoreId() : null);
                order.setFlowPrice(money);// 总价
                order.setGoodsPrice(money);// 商品价格
                // TODO:订单号怎么处理？
                order.setTradeSn(null);
                order.setOrderStatus(OrderStatusEnum.COMPLETED.name());
                order.setPayStatus(PayStatusEnum.PAID.name());
                // TODO:订单号类型处理？
                order.setOrderType(OrderTypeEnum.OFFLINE.name());
                order.setSn(SnowFlake.createStr("O"));
                 orderService.save(order);
                // 封装订单Item信息
                orderItem.setOrderSn(order.getSn());// 线下sn
                orderItem.setTradeSn(order.getSn());
                orderItem.setGoodsPrice(order.getGoodsPrice());
                orderItem.setFlowPrice(order.getFlowPrice());// 焕呗总额
                // 有合同 并且 有折扣率
                if (store != null && "1".equals(store.getIsHaveContract())
                    && StringUtils.isNotEmpty(store.getDiscount()) && store.getDiscount() != "null") {
                    orderItem.setFee(CurrencyUtil.div(Double.valueOf(store.getDiscount()), 100D));// 折现率
                    orderItem.setDeposit(CurrencyUtil.mul(orderItem.getFlowPrice(), orderItem.getFee()));// 计算可提现金额
                    orderItem.setSurplus(orderItem.getDeposit());// 剩余可提现金额
                } else {
                    orderItem.setFee(0D);// 折现率
                    orderItem.setDeposit(0D);// 计算可提现金额
                    orderItem.setSurplus(0D);// 剩余可提现金额
                }
                orderItem.setRealization(0);
                orderItem.setSurplusPrice(order.getFlowPrice());
                 orderItemService.save(orderItem);
                // 封装钱包流水
                // TODO:订单号怎么处理？
                detail.setSn(order.getSn());
                detail.setTransactionType(DepositServiceTypeEnum.WALLET_PAY.name());
                detail.setPayee(memberId);
                detail.setPayeeName(member != null ? member.getNickName() : null);
                detail.setPayeeOwner(owner);
                detail.setPayeeBeforeMoney(null);
                detail.setMoney(money);
                // 添加钱包流水
                 walletDetailService.save(detail);
                System.out.println(detail.toString());

            }
            m.put("order", order);
            m.put("orderItem", orderItem);
            m.put("detail", detail);
        }
        return list;
    }

}
