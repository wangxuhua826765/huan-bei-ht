package cn.lili.controller.walletAll;


import java.util.List;
import java.util.Map;

import cn.lili.modules.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.lili.common.utils.SnowFlake;
import cn.lili.common.utils.StringUtils;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.transfer.entity.Transfer;
import cn.lili.modules.transfer.service.TransferService;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.modules.walletAll.service.AllWalletService;

@RestController
@RequestMapping("/manager/rechargeable/wallet")
public class RechargeableWalletController {

    /**
     * 充值 RECHARGE
     */

    @Autowired
    AllWalletService allWalletService;

    @Autowired
    TransferService transferService;

    @Autowired
    WalletDetailService walletDetailService;
    @Autowired
    MemberService memberService;

    @GetMapping
    public Object get() {
        String owner = "RECHARGE";
       List<Map> list = allWalletService.getAllWallet(owner);
       for (Map m : list) {
           String memberId = m.get("member_id").toString();
           Double money = Double.parseDouble(m.get("money").toString());
           // 添加转账订单
           Transfer transfer = new Transfer();
           // 添加历史流水
           WalletDetail detail = new WalletDetail();

           if (!StringUtils.equals("admin", memberId)) {
               // 获取会员信息
               Member member = memberService.getById(memberId);
//               if (member==null){
//                   continue;
//               }
               // 封装转账记录
               transfer.setPayee(memberId);
               transfer.setType("TRANSFER");
               transfer.setState("success");
               transfer.setPayeeName(member != null ? member.getNickName() : null);
               transfer.setPayeePhone(member != null ? member.getMobile() : null);
               transfer.setPayeeImg(member != null ? member.getFace() : null);
               transfer.setTransferMoney(money);
               transfer.setCommission(0.0);
               transfer.setPaymentWallet(null);
               transfer.setPayer(null);
               transfer.setPayerPhone(null);
               transfer.setPayeeCard(null);
               // TODO:订单号怎么处理？
               transfer.setTradeSn(null);
               transfer.setSn(SnowFlake.createStr("Z"));
               // 添加转账记录
                transferService.save(transfer);
               System.out.println(transfer.toString());
               // 封装钱包流水
               detail.setSn(transfer.getSn());
               detail.setTransactionType(DepositServiceTypeEnum.WALLET_PAY.name());
               detail.setPayee(memberId);
               detail.setPayeeName(member != null ? member.getNickName() : null);
               detail.setPayeeOwner(owner);
               detail.setPayeeBeforeMoney(null);
               detail.setMoney(money);
               // 添加钱包流水
                walletDetailService.save(detail);
               System.out.println(detail.toString());
           }
           m.put("detail",detail);
           m.put("transfer",transfer);
       }
        return list;
    }

}
