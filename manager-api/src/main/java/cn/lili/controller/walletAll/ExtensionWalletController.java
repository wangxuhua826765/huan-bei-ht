package cn.lili.controller.walletAll;

import java.util.List;
import java.util.Map;

import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.service.PartnerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.lili.common.utils.StringUtils;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.modules.walletAll.service.AllWalletService;

@RestController
@RequestMapping("/manager/extension/wallet")
public class ExtensionWalletController {

    /**
     * 推广 PROMOTE
     */

    @Autowired
    WalletDetailService walletDetailService;

    @Autowired
    AllWalletService allWalletService;

    @Autowired
    MemberService memberService;

    @Autowired
    PartnerService partnerService;

    @GetMapping
    public Object get() {
        String owner = "PROMOTE";
        List<Map> list = allWalletService.getAllWallet(owner);
        for (Map m : list) {
            String memberId = m.get("member_id").toString();
            Double money = Double.parseDouble(m.get("money").toString());
            // 添加历史流水
            WalletDetail detail = new WalletDetail();

            if (!StringUtils.equals("admin", memberId)) {
                // 获取会员信息
                Member member = memberService.getById(memberId);
//                if (member == null) {
//                    continue;
//                }
                // 封装钱包流水
                // TODO:订单号怎么处理？
                detail.setSn(null);
                QueryWrapper<Partner> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("member_id",memberId);
                queryWrapper.eq("delete_flag",0);
                queryWrapper.eq("partner_state",0);
                queryWrapper.eq("audit_state","PASS");
                Partner one = partnerService.getOne(queryWrapper);
                // 合伙人类型   0 非合伙人  1 事业合伙人  2 天使合伙人  3 普通合伙人  4 城市代理商
                if(one != null) {
                    switch (one.getPartnerType()) {
                        case 0:
                            detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
                        case 1:
                            detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_SY.name());
                        case 2:
                            detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
                        case 3:
                            detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
                        case 4:
                            detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_FW.name());
                        default:
                            detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
                    }
                }else{
                    detail.setTransactionType(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
                }
                detail.setPayee(memberId);
                detail.setPayeeName(member != null ? member.getNickName() : null);
                detail.setPayeeOwner(owner);
                detail.setPayeeBeforeMoney(null);
                detail.setMoney(money);
                // 添加钱包流水
                 walletDetailService.save(detail);
                System.out.println(detail.toString());
            }
            m.put("detail", detail);
        }
        return list;
    }


}
