package cn.lili.controller.sysdict;

import cn.lili.cache.Cache;
import cn.lili.cache.CachePrefix;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.UuidUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.SysDictItemVO;
import cn.lili.modules.dict.entity.vos.SysDictVO;
import cn.lili.modules.dict.service.SysDictItemService;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据字典
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "管理端,数据字典接口")
@RequestMapping("/manager/sysDictItem")
public class SysDictItemController {

	/**
	 * 字典Value
	 */
	@Autowired
	private SysDictItemService sysDictItemService;

	@Autowired
	private Cache cache;

	@ApiOperation(value = "查询数据字典value列表")
	@ApiImplicitParam(name = "id", value = "字典id", required = true, paramType = "path")
	@RequestMapping(value = "/findValueSById/{id}")
	public ResultMessage<List<SysDictItem>> findValueSById(
			@NotNull(message = "字典ID不能为空") @PathVariable("id") String id) {
		try {
			return ResultUtil.data(this.sysDictItemService.findValueSById(id));
		} catch (Exception e) {
			log.error("查询数据字典列表错误", e);
		}
		return null;
	}

	@ApiOperation(value = "查询数据字典value列表")
	@ApiImplicitParam(name = "id", value = "字典id", required = true, paramType = "path")
	@RequestMapping(value = "/findValueSByDictCode/{dictCode}")
	public ResultMessage<List<SysDictItemVO>> findValueSByDictCode(
			@NotNull(message = "字典dictCode不能为空") @PathVariable("dictCode") String dictCode) {
		try {
			return ResultUtil.data(this.sysDictItemService.findValueSByDictCode(dictCode));
		} catch (Exception e) {
			log.error("查询数据字典列表错误", e);
		}
		return null;
	}

	@ApiOperation(value = "保存数据字典Key")
	@RequestMapping(value = "/add")
	public ResultMessage<SysDictItem> add(SysDictItemVO sysDictItemVO) {
		SysDictItem sysDictItem = new SysDictItem();
		sysDictItem.setDictId(sysDictItemVO.getParentId());
		sysDictItem.setDescription(sysDictItemVO.getDescription());
		sysDictItem.setItemText(sysDictItemVO.getDictName());
		sysDictItem.setItemValue(sysDictItemVO.getDictCode());
		sysDictItem.setStatus(sysDictItemVO.getStatus());
		sysDictItem.setSort(sysDictItemVO.getSort());
		// 查询当前字典Id对应的所有Value值
		List<SysDictItem> sysDictItems = sysDictItemService.findValueSById(sysDictItem.getDictId());
		for (SysDictItem item : sysDictItems) {
			if (item != null && item.getItemValue().equals(sysDictItem.getItemValue())
					&& item.getItemText().equals(sysDictItem.getItemText())) {
				return ResultUtil.error(ResultCode.DICTI_LI_VALUE_EXIST);
			}
		}
		sysDictItem.setId(UuidUtils.getUUID());
		sysDictItem.setDelFlag(false);
		this.sysDictItemService.save(sysDictItem);
		cache.remove(CachePrefix.SYS_DICT.getPrefix());
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	@ApiOperation(value = "修改数据字典item")
	@PutMapping(value = "/update/{id}")
	public ResultMessage<SysDictItem> update(@PathVariable String id, SysDictItemVO sysDictItemVO) {
		SysDictItem sysDictItem = new SysDictItem();
		sysDictItem.setDescription(sysDictItemVO.getDescription());
		sysDictItem.setItemText(sysDictItemVO.getDictName());
		sysDictItem.setItemValue(sysDictItemVO.getDictCode());
		sysDictItem.setStatus(sysDictItemVO.getStatus());
		sysDictItem.setSort(sysDictItemVO.getSort());
		UpdateWrapper<SysDictItem> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id).set("item_text", sysDictItem.getItemText())
				.set("item_value", sysDictItem.getItemValue()).set("description", sysDictItem.getDescription())
				.set("sort", sysDictItem.getSort()).set("status", sysDictItem.getStatus());
		this.sysDictItemService.update(sysDictItem, updateWrapper);
		cache.remove(CachePrefix.SYS_DICT.getPrefix());
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	@ApiOperation(value = "修改是否使用(1启用/0不启用)")
	@RequestMapping(value = "/updateStatus")
	public ResultMessage<SysDictItem> updateStatus(SysDictItem sysDictItem) {
		UpdateWrapper<SysDictItem> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", sysDictItem.getId()).set("status", sysDictItem.getDescription());
		this.sysDictItemService.update(sysDictItem, updateWrapper);
		return ResultUtil.data(sysDictItem);
	}

	@ApiOperation(value = "查询数据字典value列表")
	@ApiImplicitParam(name = "id", value = "字典id", required = true, paramType = "path")
	@RequestMapping(value = "/findValueSByDictCode2")
	public ResultMessage<Object> findValueSByDictCode2() {
		try {
			List<SysDictVO> vo = new ArrayList<>();
			/*
			 * vo.add(SysDictUtils.getSysDictVO(DictCodeEnum.RECHARGE_RULE.dictCode(),
			 * DictCodeEnum.AGENT.dictCode()));
			 * vo.add(SysDictUtils.getSysDictVO(DictCodeEnum.MEMBERSHIP_RATE.dictCode(),
			 * DictCodeEnum.BUSINESS.dictCode()));
			 * vo.add(SysDictUtils.getSysDictVO(DictCodeEnum.FLOW_RATE.dictCode(),
			 * DictCodeEnum.BUSINESS_PARTNER.dictCode()));
			 * vo.add(SysDictUtils.getSysDictVO(DictCodeEnum.PARTNER_RATE.dictCode(),
			 * DictCodeEnum.ORDINARY_USERS.dictCode()));
			 * vo.add(SysDictUtils.getSysDictVO(DictCodeEnum.PARTNER_PRICE.dictCode(),
			 * DictCodeEnum.PROMOTER.dictCode()));
			 */
			return ResultUtil.data(vo);
		} catch (Exception e) {
			log.error("查询数据字典列表错误", e);
		}
		return null;
	}

	@ApiOperation(value = "删除")
	@DeleteMapping(value = "/{id}")
	public ResultMessage<SysDictItem> del(@PathVariable String id) {
		SysDictItem sysDictItem = new SysDictItem();
		sysDictItem.setId(id);
		UpdateWrapper<SysDictItem> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id).set("del_flag", "1");
		this.sysDictItemService.update(sysDictItem, updateWrapper);
		cache.remove(CachePrefix.SYS_DICT.getPrefix());
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	@ApiOperation(value = "查询详情")
	@RequestMapping(value = "/id")
	public ResultMessage<SysDictItem> selectById(String id) {

		SysDictItem sysDictItem = sysDictItemService.getById(id);
		return ResultUtil.data(sysDictItem);
	}

}
