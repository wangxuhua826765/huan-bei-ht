package cn.lili.controller.sysdict;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.page.entity.dos.ArticleCategory;
import cn.lili.modules.page.entity.vos.ArticleCategoryVO;
import cn.lili.modules.page.service.ArticleCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 数据字典
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "管理端,数据字典接口")
@RequestMapping("/manager/dictionary")
public class DictController {

	/**
	 * 文章分类
	 */
	@Autowired
	private DictionaryService dictionaryService;

	@ApiOperation(value = "查询数据字典列表")
	@RequestMapping(value = "/all-children")
	public ResultMessage<List<DictionaryVO>> allChildren() {
		try {
			return ResultUtil.data(this.dictionaryService.allChildren());
		} catch (Exception e) {
			log.error("查询数据字典列表错误", e);
		}
		return null;
	}

	@ApiOperation(value = "查看数据字典分类")
	@ApiImplicitParam(name = "id", value = "数据字典分类ID", required = true, dataType = "String", paramType = "path")
	@RequestMapping(value = "/{id}")
	public ResultMessage<DictionaryLi> getArticleCategory(@PathVariable String id) {
		return ResultUtil.data(this.dictionaryService.getById(id));
	}

	@ApiOperation(value = "根据dictCode查询")
	@ApiImplicitParam(name = "id", value = "数据字典分类ID", required = true, dataType = "String", paramType = "path")
	@RequestMapping(value = "/getDict/{dictCode}")
	public ResultMessage<List<DictionaryVO>> getDictList(@PathVariable("dictCode") String dictCode) {

		System.out.println("dictCode==================" + dictCode);
		return ResultUtil.data(this.dictionaryService.getDictionaryLiListVo(dictCode));
	}

	@ApiOperation(value = "保存数据字典分类")
	@PostMapping
	public ResultMessage<DictionaryLi> save(@Valid DictionaryLi articleCategory) {
		if (articleCategory.getLevel() == null) {
			articleCategory.setLevel(0);
		}
		if (articleCategory.getSort() == null) {
			articleCategory.setSort(0);
		}

		return ResultUtil.data(dictionaryService.saveDictionaryLi(articleCategory));
	}

	@ApiOperation(value = "修改数据字典分类")
	@ApiImplicitParam(name = "id", value = "数据字典分类ID", required = true, dataType = "String", paramType = "path")
	@PutMapping("/update/{id}")
	public ResultMessage<DictionaryLi> update(@Valid DictionaryLi articleCategory, @PathVariable("id") String id) {

		if (articleCategory.getLevel() == null) {
			articleCategory.setLevel(0);
		}
		if (articleCategory.getSort() == null) {
			articleCategory.setSort(0);
		}

		articleCategory.setId(id);
		return ResultUtil.data(dictionaryService.updateDictionaryLi(articleCategory));
	}

	@ApiOperation(value = "删除数据字典分类")
	@ApiImplicitParam(name = "id", value = "数据字典分类ID", required = true, dataType = "String", paramType = "path")
	@DeleteMapping("/{id}")
	public ResultMessage<DictionaryLi> deleteById(@PathVariable String id) {
		dictionaryService.deleteById(id);
		return ResultUtil.success();
	}
}
