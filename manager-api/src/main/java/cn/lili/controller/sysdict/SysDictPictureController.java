package cn.lili.controller.sysdict;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.entity.dos.SysDictPicture;
import cn.lili.modules.dict.service.SysDictPictureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@Api(tags = "管理端,图片配置接口")
@RequestMapping("/manager/sysDictPicture")
public class SysDictPictureController {

    @Autowired
    private SysDictPictureService sysDictPictureService;


    @ApiOperation(value = "查询列表")
    @RequestMapping()
    public ResultMessage<List<SysDictPicture>> list() {

        List<SysDictPicture> list = sysDictPictureService.list();
        return ResultUtil.data(list);
    }

    @ApiOperation(value = "查询详情")
    @RequestMapping(value = "/{id}")
    public ResultMessage<SysDictPicture> selectById(@PathVariable String id) {

        SysDictPicture sysDictPicture = sysDictPictureService.getById(id);
        return ResultUtil.data(sysDictPicture);
    }

    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update")
    public ResultMessage<Boolean> update(SysDictPicture sysDictPicture) {

        return ResultUtil.data(sysDictPictureService.updateById(sysDictPicture));
    }

    @ApiOperation(value = "查询详情")
    @RequestMapping(value = "/listByCode")
    public ResultMessage<List<SysDictPicture>> listByCode(String code) {

        List<SysDictPicture> list = sysDictPictureService.listByCode(code);
        return ResultUtil.data(list);
    }
}
