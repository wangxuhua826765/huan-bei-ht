package cn.lili.controller.sysdict;

import cn.lili.cache.Cache;
import cn.lili.cache.CachePrefix;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.DateUtil;
import cn.lili.common.utils.UserAgentUtils;
import cn.lili.common.utils.UuidUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.entity.dos.DictionaryLi;
import cn.lili.modules.dict.entity.dos.SysDict;
import cn.lili.modules.dict.entity.dos.SysDictItem;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.dict.entity.vos.SysDictVO;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.dict.service.SysDictItemService;
import cn.lili.modules.dict.service.SysDictService;
import cn.lili.modules.goods.entity.dos.Category;
import cn.lili.modules.store.entity.dos.Store;
import cn.lili.modules.store.entity.dto.AdminStoreApplyDTO;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 数据字典
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "管理端,数据字典接口")
@RequestMapping("/manager/sysDict")
public class SysDictController {

	/**
	 * 文章分类
	 */
	@Autowired
	private SysDictService sysDictService;

	@Autowired
	private SysDictItemService sysDictItemService;

	@Autowired
	private Cache cache;

	@ApiOperation(value = "查询数据字典K列表")
	@RequestMapping(value = "/allKey")
	public ResultMessage<List<SysDict>> allKey() {
		try {
			return ResultUtil.data(this.sysDictService.allKey());
		} catch (Exception e) {
			log.error("查询数据字典列表错误", e);
		}
		return null;
	}

	@ApiOperation(value = "保存数据字典Key")
	@RequestMapping(value = "/add")
	public ResultMessage<SysDict> add(SysDict sysDict) {
		// 所有字典数据
		List<SysDict> SysDictS = this.sysDictService.allKey();
		// 判断字典编码是否已存在
		for (SysDict i : SysDictS) {
			if (i != null && sysDict.getDictCode().equals(i.getDictCode())) {
				return ResultUtil.error(ResultCode.DICT_LI_NAME_EXIST);
			}
		}
		sysDict.setId(UuidUtils.getUUID());
		sysDict.setDelFlag(false);
		this.sysDictService.save(sysDict);
		cache.remove(CachePrefix.SYS_DICT.getPrefix());
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	@ApiOperation(value = "查询详情")
	@RequestMapping(value = "/id")
	public ResultMessage<SysDict> selectById(String id) {

		SysDict sysDict = sysDictService.getById(id);
		return ResultUtil.data(sysDict);
	}

	@ApiOperation(value = "修改数据字典")
	@PutMapping(value = "/update/{id}")
	public ResultMessage<SysDict> update(@PathVariable String id, SysDict sysDict) {
		UpdateWrapper<SysDict> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id).set("dict_name", sysDict.getDictName())
				// .set("dict_code",sysDict.getDictCode())
				.set("description", sysDict.getDescription());
		this.sysDictService.update(sysDict, updateWrapper);
		cache.remove(CachePrefix.SYS_DICT.getPrefix());
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	@ApiOperation(value = "作废字典(0正常/1作废)")
	@DeleteMapping(value = "/del/{id}")
	public ResultMessage<SysDict> del(@PathVariable String id) {
		SysDict sysDict = new SysDict();
		sysDict.setId(id);
		// 字典下所有可用数据
		List<SysDictItem> items = sysDictItemService.findValueSById(id);
		int isUser = 0;
		// 判断字典下是否有启动可用数据
		for (SysDictItem i : items) {
			if (i != null && "1".equals(i.getStatus())) {
				return ResultUtil.error(ResultCode.DICTI_LI_NO_DELETION);
			}
		}
		UpdateWrapper<SysDict> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id).set("del_flag", "1");
		this.sysDictService.update(sysDict, updateWrapper);
		cache.remove(CachePrefix.SYS_DICT.getPrefix());
		return ResultUtil.success(ResultCode.SUCCESS);
	}

	@ApiOperation(value = "查询数据字典列表")
	@RequestMapping(value = "/all-children")
	public ResultMessage<List<SysDictVO>> allChildren() {
		try {
			return ResultUtil.data(this.sysDictService.allChildren());
		} catch (Exception e) {
			log.error("查询数据字典列表错误", e);
		}
		return ResultUtil.error(ResultCode.ERROR);
	}
}
