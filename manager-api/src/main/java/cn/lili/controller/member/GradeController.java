package cn.lili.controller.member;

import cn.lili.common.aop.annotation.DemoSite;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dto.ManagerMemberEditDTO;
import cn.lili.modules.member.entity.dto.MemberAddDTO;
import cn.lili.modules.member.entity.vo.GradeVO;
import cn.lili.modules.member.service.GradeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

/**
 * 等级管理
 */
@RestController
@Api(tags = "管理端,会员等级管理")
@RequestMapping("/manager/grade")
public class GradeController {
	@Autowired
	private GradeService gradeService;

	@ApiOperation(value = "会员等级分页列表")
	@GetMapping
	public ResultMessage<IPage<GradeVO>> getByPage(GradeVO gradeVO, PageVO page) {
		return ResultUtil.data(gradeService.getGradePage(gradeVO, page));
	}

	@ApiOperation(value = "编辑或添加会员等级")
	@PostMapping
	public ResultMessage<GradeVO> save(@Valid GradeVO gradeVO) {
		if (StringUtils.isNotBlank(gradeVO.getId())) {
			gradeVO.setUpdateTime(new Date());
			return ResultUtil.data(gradeService.updateGrade(gradeVO));
		}
		gradeVO.setCreateTime(new Date());
		return ResultUtil.data(gradeService.addGrade(gradeVO));
	}

	@ApiOperation(value = "通过ID获取会员等级信息")
	@ApiImplicitParam(name = "id", value = "会员ID", required = true, dataType = "String", paramType = "path")
	@RequestMapping(value = "/{id}")
	public ResultMessage<GradeVO> get(@PathVariable String id) {

		return ResultUtil.data(gradeService.getById(id));
	}

	@ApiOperation(value = "删除会员等级")
	@ApiImplicitParam(name = "id", value = "会员地址ID", dataType = "String", paramType = "path")
	@DeleteMapping(value = "/delById/{id}")
	public ResultMessage<Object> delShippingAddressById(@PathVariable String id) {
		gradeService.removeGrade(id);
		return ResultUtil.success();
	}
}
