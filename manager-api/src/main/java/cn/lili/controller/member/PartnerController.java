package cn.lili.controller.member;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.vo.ExtensionVO;
import cn.lili.modules.member.entity.vo.GradeVO;
import cn.lili.modules.member.entity.vo.PartnerVO;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.service.AdminUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

/**
 * 等级管理
 */
@RestController
@Api(tags = "管理端,合伙人推广管理")
@RequestMapping("/manager/partner")
public class PartnerController {
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private AdminUserService adminUserService;

	@ApiOperation(value = "推广管理分页列表")
	@GetMapping
	public ResultMessage<IPage<PartnerVO>> getByPage(PartnerVO partner, PageVO page) {
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// if (null != adminUser) {
			// partner.setExtension(adminUser.getPromotionCode());
			// }
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner1 = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner1 && StringUtils.isNotEmpty(partner1.getRegionId())) {
				partner.setAdcode(partner1.getRegionId());
			}
		}
		return ResultUtil.data(partnerService.getPartnerPage(partner, page));
	}

	@ApiOperation(value = "编辑修改状态")
	@PostMapping
	public ResultMessage<Partner> save(@Valid Partner partner) {
		return ResultUtil.data(partnerService.updatePartner(partner));
	}

	@ApiOperation(value = "通过ID获取会员信息")
	@ApiImplicitParam(name = "id", value = "会员ID", required = true, dataType = "String", paramType = "path")
	@RequestMapping(value = "/{id}")
	public ResultMessage<Member> get(@PathVariable String id) {
		// Partner byId = partnerService.getById(id);
		Member byId1 = memberService.getById(id);
		// byId1.setMemberFlag(byId.getPartnerState() == 0 ? true : false);
		return ResultUtil.data(byId1);
	}

}
