package cn.lili.controller.member;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.RechargeFow;
import cn.lili.modules.member.service.RechargeFowService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流水管理
 */
@RestController
@Api(tags = "管理端,流水管理")
@RequestMapping("/manager/RechargeFow")
public class RechargeFowController {
	@Autowired
	private RechargeFowService rechargeFowService;

	@ApiOperation(value = "会员等级分页列表")
	@GetMapping
	public ResultMessage<IPage<RechargeFow>> getByPage(RechargeFow rechargeFow, PageVO page) {
		return ResultUtil.data(rechargeFowService.getRechargeFowPage(rechargeFow, page));
	}

	// @ApiOperation(value = "通过ID获取会员等级信息")
	// @ApiImplicitParam(name = "id", value = "会员ID", required = true, dataType =
	// "String", paramType = "path")
	// @RequestMapping(value = "/{id}")
	// public ResultMessage<RechargeFow> get(@PathVariable String id) {
	//
	// return ResultUtil.data(gradeService.getById(id));
	// }

}
