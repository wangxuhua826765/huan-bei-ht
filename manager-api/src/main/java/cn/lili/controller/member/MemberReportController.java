package cn.lili.controller.member;

import cn.lili.common.enums.DictCodeEnum;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.*;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.entity.dos.Spreadlncome;
import cn.lili.modules.member.entity.vo.*;
import cn.lili.modules.member.service.MemberService;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.member.service.SpreadlncomeService;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.wallet.entity.dto.CommissionDistributionSale;
import cn.lili.modules.wallet.service.WalletLogDetailService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * create by yudan on 2022/4/22
 */
@RestController
@Api(tags = "管理端,财务报表接口")
@RequestMapping("/manager/report")
@EnableScheduling
public class MemberReportController {

	@Autowired
	private WalletLogDetailService walletLogDetailService;
	@Autowired
	private RegionService regionService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private SpreadlncomeService spreadlncomeService;

	@ApiOperation(value = "c端会员统计")
	@RequestMapping(value = "/getCMember")
	public ResultMessage<Map<String, Object>> getCMember(FinanceAdminVO financeAdminVO, PageVO page) {

		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}

		// 返回数据
		Map<String, Object> map = new HashMap<>();
		// 返回统计数据
		Map<String, Object> All = new HashMap<>();
		// 结束时间统计数据
		Map<String, Object> All1 = new HashMap<>();
		// 开始时间统计数据
		Map<String, Object> All2 = new HashMap<>();
		// 结束时间统计数据
		IPage<Map<String, Object>> mapIPage1 = null;
		// 开始时间统计数据
		IPage<Map<String, Object>> mapIPage2 = null;
		// 无时间统计数据
		IPage<Map<String, Object>> mapIPage3 = null;
		// 返回统计数据统计数据
		IPage<Map<String, Object>> mapIPageRetern = null;
		// 获取查询条件
		QueryWrapper queryWrapperNow = financeAdminVO.queryWrapperNow();
		QueryWrapper queryWrapperStart = financeAdminVO.queryWrapperStart();
		QueryWrapper queryWrapperEnd = financeAdminVO.queryWrapperEnd();

		queryWrapperNow.orderByDesc("m.create_time");
		queryWrapperStart.orderByDesc("m.create_time");
		queryWrapperEnd.orderByDesc("m.create_time");

		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 查询信息
		// 有时间区间
		if (financeAdminVO.getStartTime() != null && financeAdminVO.getEndTime() != null) {
			// 查询结束时间数据
			All1 = walletLogDetailService.getCMemberAll(queryWrapperEnd);
			mapIPage1 = walletLogDetailService.getCMember(page, queryWrapperEnd);
			// 根据结束时间查到的memberID 查询开始时间数据;
			All2 = walletLogDetailService.getCMemberAll(queryWrapperStart);
			List<String> memberIdList = new ArrayList<>();
			if (mapIPage1 != null && mapIPage1.getRecords().size() > 0) {
				for (Map m : mapIPage1.getRecords()) {
					memberIdList.add(m.get("memberId").toString());
				}
				QueryWrapper queryWrapper = financeAdminVO.queryWrapperStart();
				queryWrapper.in("member_id", memberIdList);
				mapIPage2 = walletLogDetailService.getCMember(page, queryWrapper);
			}
			// 以结束时间数据为基准
			mapIPageRetern = mapIPage1;
		} else {
			// 没有开始时间结束时间查询截止时间
			mapIPage3 = walletLogDetailService.getCMember(page, queryWrapperNow);
			// 统计合计数据
			All = walletLogDetailService.getCMemberAll(queryWrapperNow);
			mapIPageRetern = mapIPage3;
		}
		// 整理开始结束时间数据和合并数据
		// 区间数据为开始数据有结束时间的差值
		if (mapIPage1 != null && mapIPage2 != null) {
			List<Map<String, Object>> l1 = mapIPage1.getRecords();
			List<Map<String, Object>> l2 = mapIPage2.getRecords();
			List<Map<String, Object>> records = new ArrayList<>();
			for (Map m : l1) {
				Map member = new HashMap();
				member.putAll(m);
				for (Map m2 : l2) {
					if (StringUtils.equals(m.get("memberId").toString(), m2.get("memberId").toString())) {
						member.put("dkfHb", (Double.parseDouble(m.get("dkfHb").toString())
								- Double.parseDouble(m2.get("dkfHb").toString())));
						member.put("dkfXj", (Double.parseDouble(m.get("dkfXj").toString())
								- Double.parseDouble(m2.get("dkfXj").toString())));
						member.put("czSrXj", (Double.parseDouble(m.get("czSrXj").toString())
								- Double.parseDouble(m2.get("czSrXj").toString())));
						member.put("czSrHb", (Double.parseDouble(m.get("czSrHb").toString())
								- Double.parseDouble(m2.get("czSrHb").toString())));
						member.put("czSrZr", (Double.parseDouble(m.get("czSrZr").toString())
								- Double.parseDouble(m2.get("czSrZr").toString())));
						member.put("czZcXf", (Double.parseDouble(m.get("czZcXf").toString())
								- Double.parseDouble(m2.get("czZcXf").toString())));
						member.put("czYe", (Double.parseDouble(m.get("czYe").toString())
								- Double.parseDouble(m2.get("czYe").toString())));
					}
				}
				records.add(member);
			}
			mapIPageRetern.setRecords(records);
			if (All1 != null && All2 != null) {
				All.put("dkfHb", (Double.parseDouble(All1.get("dkfHb").toString())
						- Double.parseDouble(All2.get("dkfHb").toString())));
				All.put("dkfXj", (Double.parseDouble(All1.get("dkfXj").toString())
						- Double.parseDouble(All2.get("dkfXj").toString())));
				All.put("czSrXj", (Double.parseDouble(All1.get("czSrXj").toString())
						- Double.parseDouble(All2.get("czSrXj").toString())));
				All.put("czSrHb", (Double.parseDouble(All1.get("czSrHb").toString())
						- Double.parseDouble(All2.get("czSrHb").toString())));
				All.put("czSrZr", (Double.parseDouble(All1.get("czSrZr").toString())
						- Double.parseDouble(All2.get("czSrZr").toString())));
				All.put("czZcXf", (Double.parseDouble(All1.get("czZcXf").toString())
						- Double.parseDouble(All2.get("czZcXf").toString())));
				All.put("czYe", (Double.parseDouble(All1.get("czYe").toString())
						- Double.parseDouble(All2.get("czYe").toString())));
				All.put("endTime", (All1.get("endTime")));
				All.put("startTime", (All2.get("startTime")));

			} else {
				if (All1 != null) {
					All.putAll(All1);
				} else {
					All.putAll(All2);
				}
			}
		}
		if (mapIPageRetern == null || mapIPageRetern.getRecords() == null || mapIPageRetern.getRecords().size() == 0
				|| All == null || All.size() == 0) {
			mapIPageRetern.setRecords(new ArrayList<>());
		} else {
			for (Map m : mapIPageRetern.getRecords()) {
				if (!StringUtils.equals("", m.get("location").toString()))
					m.put("region", regionService.getItemAdCodOrName(StringUtils.toString(m.get("location"))));
			}
		}
		map.put("page", mapIPageRetern);
		map.put("All", All);
		return ResultUtil.data(map);

	}

	@ApiOperation(value = "b端会员统计")
	@RequestMapping(value = "/getBMember")

	public ResultMessage<Map<String, Object>> getBMember(FinanceAdminVO financeAdminVO, PageVO page) {

		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}
		// 返回数据
		Map<String, Object> map = new HashMap<>();
		// 返回统计数据
		Map<String, Object> All = new HashMap<>();
		// 结束时间统计数据
		Map<String, Object> All1 = new HashMap<>();
		// 开始时间统计数据
		Map<String, Object> All2 = new HashMap<>();
		// 结束时间统计数据
		IPage<Map<String, Object>> mapIPage1 = null;
		// 开始时间统计数据
		IPage<Map<String, Object>> mapIPage2 = null;
		// 无时间统计数据
		IPage<Map<String, Object>> mapIPage3 = null;
		// 返回统计数据统计数据
		IPage<Map<String, Object>> mapIPageRetern = null;
		// 获取查询条件
		QueryWrapper queryWrapperNow = financeAdminVO.queryWrapperNow();
		QueryWrapper queryWrapperStart = financeAdminVO.queryWrapperStart();
		QueryWrapper queryWrapperEnd = financeAdminVO.queryWrapperEnd();

		queryWrapperNow.orderByDesc("m.create_time");
		queryWrapperStart.orderByDesc("m.create_time");
		queryWrapperEnd.orderByDesc("m.create_time");

		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 查询信息
		// 有时间区间
		if (financeAdminVO.getStartTime() != null && financeAdminVO.getEndTime() != null) {
			// 查询结束时间数据
			All1 = walletLogDetailService.getBMemberAll(queryWrapperEnd);
			mapIPage1 = walletLogDetailService.getBMember(page, queryWrapperEnd);
			// 根据结束时间查到的memberID 查询开始时间数据;
			All2 = walletLogDetailService.getBMemberAll(queryWrapperStart);
			List<String> memberIdList = new ArrayList<>();
			if (mapIPage1 != null && mapIPage1.getRecords().size() > 0) {
				for (Map m : mapIPage1.getRecords()) {
					memberIdList.add(m.get("memberId").toString());
				}
				QueryWrapper queryWrapper = financeAdminVO.queryWrapperStart();
				queryWrapper.in("member_id", memberIdList);
				mapIPage2 = walletLogDetailService.getBMember(page, queryWrapper);
			}
			// 以结束时间数据为基准
			mapIPageRetern = mapIPage1;
		} else {
			// 没有开始时间结束时间查询截止时间
			mapIPage3 = walletLogDetailService.getBMember(page, queryWrapperNow);
			// 统计合计数据
			All = walletLogDetailService.getBMemberAll(queryWrapperNow);
			mapIPageRetern = mapIPage3;
		}
		// 整理开始结束时间数据和合并数据
		// 区间数据为开始数据有结束时间的差值
		if (mapIPage1 != null && mapIPage2 != null) {
			List<Map<String, Object>> l1 = mapIPage1.getRecords();
			List<Map<String, Object>> l2 = mapIPage2.getRecords();
			List<Map<String, Object>> records = new ArrayList<>();
			for (Map m : l1) {
				Map member = new HashMap();
				member.putAll(m);
				for (Map m2 : l2) {
					if (StringUtils.equals(m.get("memberId").toString(), m2.get("memberId").toString())) {
						member.put("memberNum", (Double.parseDouble(m.get("memberNum").toString())
								- Double.parseDouble(m2.get("memberNum").toString())));
						member.put("dkfHb", (Double.parseDouble(m.get("dkfHb").toString())
								- Double.parseDouble(m2.get("dkfHb").toString())));
						member.put("dkfXj", (Double.parseDouble(m.get("dkfXj").toString())
								- Double.parseDouble(m2.get("dkfXj").toString())));
						member.put("czSrXj", (Double.parseDouble(m.get("czSrXj").toString())
								- Double.parseDouble(m2.get("czSrXj").toString())));
						member.put("czSrHb", (Double.parseDouble(m.get("czSrHb").toString())
								- Double.parseDouble(m2.get("czSrHb").toString())));
						member.put("czSrZr", (Double.parseDouble(m.get("czSrZr").toString())
								- Double.parseDouble(m2.get("czSrZr").toString())));
						member.put("czZcXf", (Double.parseDouble(m.get("czZcXf").toString())
								- Double.parseDouble(m2.get("czZcXf").toString())));
						member.put("czYe", (Double.parseDouble(m.get("czYe").toString())
								- Double.parseDouble(m2.get("czYe").toString())));
						member.put("tgSrXffy", (Double.parseDouble(m.get("tgSrXffy").toString())
								- Double.parseDouble(m2.get("tgSrXffy").toString())));
						member.put("tgSrHyfy", (Double.parseDouble(m.get("tgSrHyfy").toString())
								- Double.parseDouble(m2.get("tgSrHyfy").toString())));
						member.put("tgZcXf", (Double.parseDouble(m.get("tgZcXf").toString())
								- Double.parseDouble(m2.get("tgZcXf").toString())));
						member.put("tgZcFwf", (Double.parseDouble(m.get("tgZcFwf").toString())
								- Double.parseDouble(m2.get("tgZcFwf").toString())));
						member.put("tgYe", (Double.parseDouble(m.get("tgYe").toString())
								- Double.parseDouble(m2.get("tgYe").toString())));
						member.put("xsYe", (Double.parseDouble(m.get("xsYe").toString())
								- Double.parseDouble(m2.get("xsYe").toString())));
						member.put("xsSr", (Double.parseDouble(m.get("xsSr").toString())
								- Double.parseDouble(m2.get("xsSr").toString())));
						member.put("xsZcXf", (Double.parseDouble(m.get("xsZcXf").toString())
								- Double.parseDouble(m2.get("xsZcXf").toString())));
						member.put("xsZcFwf", (Double.parseDouble(m.get("xsZcFwf").toString())
								- Double.parseDouble(m2.get("xsZcFwf").toString())));
						member.put("xsZcZc", (Double.parseDouble(m.get("xsZcZc").toString())
								- Double.parseDouble(m2.get("xsZcZc").toString())));
						member.put("xsZcJf", (Double.parseDouble(m.get("xsZcJf").toString())
								- Double.parseDouble(m2.get("xsZcJf").toString())));
						member.put("tgTxXj", (Double.parseDouble(m.get("tgTxXj").toString())
								- Double.parseDouble(m2.get("tgTxXj").toString())));
						member.put("tgTxHb", (Double.parseDouble(m.get("tgTxHb").toString())
								- Double.parseDouble(m2.get("tgTxHb").toString())));
						member.put("xsTxXj", (Double.parseDouble(m.get("xsTxXj").toString())
								- Double.parseDouble(m2.get("xsTxXj").toString())));
						member.put("xsTxHb", (Double.parseDouble(m.get("xsTxHb").toString())
								- Double.parseDouble(m2.get("xsTxHb").toString())));
						member.put("yeHj", (Double.parseDouble(m.get("yeHj").toString())
								- Double.parseDouble(m2.get("yeHj").toString())));
					}
				}
				records.add(member);
			}
			mapIPageRetern.setRecords(records);
			if (All1 != null && All2 != null) {
				All.put("memberNum", (Double.parseDouble(All1.get("memberNum").toString())
						- Double.parseDouble(All2.get("memberNum").toString())));
				All.put("dkfHb", (Double.parseDouble(All1.get("dkfHb").toString())
						- Double.parseDouble(All2.get("dkfHb").toString())));
				All.put("dkfXj", (Double.parseDouble(All1.get("dkfXj").toString())
						- Double.parseDouble(All2.get("dkfXj").toString())));
				All.put("czSrXj", (Double.parseDouble(All1.get("czSrXj").toString())
						- Double.parseDouble(All2.get("czSrXj").toString())));
				All.put("czSrHb", (Double.parseDouble(All1.get("czSrHb").toString())
						- Double.parseDouble(All2.get("czSrHb").toString())));
				All.put("czSrZr", (Double.parseDouble(All1.get("czSrZr").toString())
						- Double.parseDouble(All2.get("czSrZr").toString())));
				All.put("czZcXf", (Double.parseDouble(All1.get("czZcXf").toString())
						- Double.parseDouble(All2.get("czZcXf").toString())));
				All.put("czYe", (Double.parseDouble(All1.get("czYe").toString())
						- Double.parseDouble(All2.get("czYe").toString())));
				All.put("tgSrXffy", (Double.parseDouble(All1.get("tgSrXffy").toString())
						- Double.parseDouble(All2.get("tgSrXffy").toString())));
				All.put("tgSrHyfy", (Double.parseDouble(All1.get("tgSrHyfy").toString())
						- Double.parseDouble(All2.get("tgSrHyfy").toString())));
				All.put("tgZcXf", (Double.parseDouble(All1.get("tgZcXf").toString())
						- Double.parseDouble(All2.get("tgZcXf").toString())));
				All.put("tgZcFwf", (Double.parseDouble(All1.get("tgZcFwf").toString())
						- Double.parseDouble(All2.get("tgZcFwf").toString())));
				All.put("tgYe", (Double.parseDouble(All1.get("tgYe").toString())
						- Double.parseDouble(All2.get("tgYe").toString())));
				All.put("xsYe", (Double.parseDouble(All1.get("xsYe").toString())
						- Double.parseDouble(All2.get("xsYe").toString())));
				All.put("xsSr", (Double.parseDouble(All1.get("xsSr").toString())
						- Double.parseDouble(All2.get("xsSr").toString())));
				All.put("xsZcXf", (Double.parseDouble(All1.get("xsZcXf").toString())
						- Double.parseDouble(All2.get("xsZcXf").toString())));
				All.put("xsZcFwf", (Double.parseDouble(All1.get("xsZcFwf").toString())
						- Double.parseDouble(All2.get("xsZcFwf").toString())));
				All.put("xsZcZc", (Double.parseDouble(All1.get("xsZcZc").toString())
						- Double.parseDouble(All2.get("xsZcZc").toString())));
				All.put("xsZcJf", (Double.parseDouble(All1.get("xsZcJf").toString())
						- Double.parseDouble(All2.get("xsZcJf").toString())));
				All.put("tgTxXj", (Double.parseDouble(All1.get("tgTxXj").toString())
						- Double.parseDouble(All2.get("tgTxXj").toString())));
				All.put("tgTxHb", (Double.parseDouble(All1.get("tgTxHb").toString())
						- Double.parseDouble(All2.get("tgTxHb").toString())));
				All.put("xsTxXj", (Double.parseDouble(All1.get("xsTxXj").toString())
						- Double.parseDouble(All2.get("xsTxXj").toString())));
				All.put("xsTxHb", (Double.parseDouble(All1.get("xsTxHb").toString())
						- Double.parseDouble(All2.get("xsTxHb").toString())));
				All.put("yeHj", (Double.parseDouble(All1.get("yeHj").toString())
						- Double.parseDouble(All2.get("yeHj").toString())));
				All.put("endTime", (All1.get("endTime")));
				All.put("startTime", (All2.get("startTime")));

			} else {
				if (All1 != null) {
					All.putAll(All1);
				} else {
					All.putAll(All2);
				}
			}
		}
		if (mapIPageRetern.getRecords() == null || mapIPageRetern.getRecords().size() == 0 || All == null
				|| All.size() == 0) {
			mapIPageRetern.setRecords(new ArrayList<>());
		} else {
			for (Map m : mapIPageRetern.getRecords()) {
				if (!StringUtils.equals("", m.get("location").toString()))
					m.put("region", regionService.getItemAdCodOrName(StringUtils.toString(m.get("location"))));
			}
		}
		map.put("page", mapIPageRetern);
		map.put("All", All);
		return ResultUtil.data(map);

	}

	@ApiOperation(value = "天使端会员统计")
	@RequestMapping(value = "/getTSMember")
	public ResultMessage<Map<String, Object>> getTSMember(FinanceAdminVO financeAdminVO, PageVO page) {

		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}
		// 返回数据
		Map<String, Object> map = new HashMap<>();
		// 返回统计数据
		Map<String, Object> All = new HashMap<>();
		// 结束时间统计数据
		Map<String, Object> All1 = new HashMap<>();
		// 开始时间统计数据
		Map<String, Object> All2 = new HashMap<>();
		// 结束时间统计数据
		IPage<Map<String, Object>> mapIPage1 = null;
		// 开始时间统计数据
		IPage<Map<String, Object>> mapIPage2 = null;
		// 无时间统计数据
		IPage<Map<String, Object>> mapIPage3 = null;
		// 返回统计数据统计数据
		IPage<Map<String, Object>> mapIPageRetern = null;
		// 获取查询条件
		QueryWrapper queryWrapperNow = financeAdminVO.queryWrapperNow();
		QueryWrapper queryWrapperStart = financeAdminVO.queryWrapperStart();
		QueryWrapper queryWrapperEnd = financeAdminVO.queryWrapperEnd();

		queryWrapperNow.orderByDesc("m.create_time");
		queryWrapperStart.orderByDesc("m.create_time");
		queryWrapperEnd.orderByDesc("m.create_time");

		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 查询信息
		// 有时间区间
		if (financeAdminVO.getStartTime() != null && financeAdminVO.getEndTime() != null) {
			// 查询结束时间数据
			All1 = walletLogDetailService.getTSMemberAll(queryWrapperEnd);
			mapIPage1 = walletLogDetailService.getTSMember(page, queryWrapperEnd);
			// 根据结束时间查到的memberID 查询开始时间数据;
			All2 = walletLogDetailService.getTSMemberAll(queryWrapperStart);
			List<String> memberIdList = new ArrayList<>();
			if (mapIPage1 != null && mapIPage1.getRecords().size() > 0) {
				for (Map m : mapIPage1.getRecords()) {
					memberIdList.add(m.get("memberId").toString());
				}
				QueryWrapper queryWrapper = financeAdminVO.queryWrapperStart();
				queryWrapper.in("member_id", memberIdList);
				mapIPage2 = walletLogDetailService.getTSMember(page, queryWrapper);
			}
			// 以结束时间数据为基准
			mapIPageRetern = mapIPage1;
		} else {
			// 没有开始时间结束时间查询截止时间
			mapIPage3 = walletLogDetailService.getTSMember(page, queryWrapperNow);
			// 统计合计数据
			All = walletLogDetailService.getTSMemberAll(queryWrapperNow);
			mapIPageRetern = mapIPage3;
		}
		// 整理开始结束时间数据和合并数据
		// 区间数据为开始数据有结束时间的差值
		if (mapIPage1 != null && mapIPage2 != null) {
			List<Map<String, Object>> l1 = mapIPage1.getRecords();
			List<Map<String, Object>> l2 = mapIPage2.getRecords();
			List<Map<String, Object>> records = new ArrayList<>();
			for (Map m : l1) {
				Map member = new HashMap();
				member.putAll(m);
				for (Map m2 : l2) {
					if (StringUtils.equals(m.get("memberId").toString(), m2.get("memberId").toString())) {
						member.put("memberNum", (Double.parseDouble(m.get("memberNum").toString())
								- Double.parseDouble(m2.get("memberNum").toString())));
						member.put("dkfHb", (Double.parseDouble(m.get("dkfHb").toString())
								- Double.parseDouble(m2.get("dkfHb").toString())));
						member.put("dkfXj", (Double.parseDouble(m.get("dkfXj").toString())
								- Double.parseDouble(m2.get("dkfXj").toString())));
						member.put("czSrXj", (Double.parseDouble(m.get("czSrXj").toString())
								- Double.parseDouble(m2.get("czSrXj").toString())));
						member.put("czSrHb", (Double.parseDouble(m.get("czSrHb").toString())
								- Double.parseDouble(m2.get("czSrHb").toString())));
						member.put("czSrZr", (Double.parseDouble(m.get("czSrZr").toString())
								- Double.parseDouble(m2.get("czSrZr").toString())));
						member.put("czZcXf", (Double.parseDouble(m.get("czZcXf").toString())
								- Double.parseDouble(m2.get("czZcXf").toString())));
						member.put("czYe", (Double.parseDouble(m.get("czYe").toString())
								- Double.parseDouble(m2.get("czYe").toString())));
						member.put("tgSrXffy", (Double.parseDouble(m.get("tgSrXffy").toString())
								- Double.parseDouble(m2.get("tgSrXffy").toString())));
						member.put("tgSrHyfy", (Double.parseDouble(m.get("tgSrHyfy").toString())
								- Double.parseDouble(m2.get("tgSrHyfy").toString())));
						member.put("tgZcXf", (Double.parseDouble(m.get("tgZcXf").toString())
								- Double.parseDouble(m2.get("tgZcXf").toString())));
						member.put("tgZcFwf", (Double.parseDouble(m.get("tgZcFwf").toString())
								- Double.parseDouble(m2.get("tgZcFwf").toString())));
						member.put("tgYe", (Double.parseDouble(m.get("tgYe").toString())
								- Double.parseDouble(m2.get("tgYe").toString())));
						// member.put("xsYe",(Double.parseDouble(m.get("xsYe").toString()) -
						// Double.parseDouble(m2.get("xsYe").toString())));
						// member.put("xsSr",(Double.parseDouble(m.get("xsSr").toString()) -
						// Double.parseDouble(m2.get("xsSr").toString())));
						// member.put("xsZcXf",(Double.parseDouble(m.get("xsZcXf").toString()) -
						// Double.parseDouble(m2.get("xsZcXf").toString())));
						// member.put("xsZcFwf",(Double.parseDouble(m.get("xsZcFwf").toString()) -
						// Double.parseDouble(m2.get("xsZcFwf").toString())));
						// member.put("xsZcZc",(Double.parseDouble(m.get("xsZcZc").toString()) -
						// Double.parseDouble(m2.get("xsZcZc").toString())));
						// member.put("xsZcJf",(Double.parseDouble(m.get("xsZcJf").toString()) -
						// Double.parseDouble(m2.get("xsZcJf").toString())));
						member.put("tgTxXj", (Double.parseDouble(m.get("tgTxXj").toString())
								- Double.parseDouble(m2.get("tgTxXj").toString())));
						member.put("tgTxHb", (Double.parseDouble(m.get("tgTxHb").toString())
								- Double.parseDouble(m2.get("tgTxHb").toString())));
						// member.put("xsTxXj",(Double.parseDouble(m.get("xsTxXj").toString()) -
						// Double.parseDouble(m2.get("xsTxXj").toString())));
						// member.put("xsTxHb",(Double.parseDouble(m.get("xsTxHb").toString()) -
						// Double.parseDouble(m2.get("xsTxHb").toString())));
						member.put("yeHj", (Double.parseDouble(m.get("yeHj").toString())
								- Double.parseDouble(m2.get("yeHj").toString())));
					}
				}
				records.add(member);
			}
			mapIPageRetern.setRecords(records);
			if (All1 != null && All2 != null) {
				All.put("memberNum", (Double.parseDouble(All1.get("memberNum").toString())
						- Double.parseDouble(All2.get("memberNum").toString())));
				All.put("dkfHb", (Double.parseDouble(All1.get("dkfHb").toString())
						- Double.parseDouble(All2.get("dkfHb").toString())));
				All.put("dkfXj", (Double.parseDouble(All1.get("dkfXj").toString())
						- Double.parseDouble(All2.get("dkfXj").toString())));
				All.put("czSrXj", (Double.parseDouble(All1.get("czSrXj").toString())
						- Double.parseDouble(All2.get("czSrXj").toString())));
				All.put("czSrHb", (Double.parseDouble(All1.get("czSrHb").toString())
						- Double.parseDouble(All2.get("czSrHb").toString())));
				All.put("czSrZr", (Double.parseDouble(All1.get("czSrZr").toString())
						- Double.parseDouble(All2.get("czSrZr").toString())));
				All.put("czZcXf", (Double.parseDouble(All1.get("czZcXf").toString())
						- Double.parseDouble(All2.get("czZcXf").toString())));
				All.put("czYe", (Double.parseDouble(All1.get("czYe").toString())
						- Double.parseDouble(All2.get("czYe").toString())));
				All.put("tgSrXffy", (Double.parseDouble(All1.get("tgSrXffy").toString())
						- Double.parseDouble(All2.get("tgSrXffy").toString())));
				All.put("tgSrHyfy", (Double.parseDouble(All1.get("tgSrHyfy").toString())
						- Double.parseDouble(All2.get("tgSrHyfy").toString())));
				All.put("tgZcXf", (Double.parseDouble(All1.get("tgZcXf").toString())
						- Double.parseDouble(All2.get("tgZcXf").toString())));
				All.put("tgZcFwf", (Double.parseDouble(All1.get("tgZcFwf").toString())
						- Double.parseDouble(All2.get("tgZcFwf").toString())));
				All.put("tgYe", (Double.parseDouble(All1.get("tgYe").toString())
						- Double.parseDouble(All2.get("tgYe").toString())));
				// All.put("xsYe",(Double.parseDouble(All1.get("xsYe").toString()) -
				// Double.parseDouble(All2.get("xsYe").toString())));
				// All.put("xsSr",(Double.parseDouble(All1.get("xsSr").toString()) -
				// Double.parseDouble(All2.get("xsSr").toString())));
				// All.put("xsZcXf",(Double.parseDouble(All1.get("xsZcXf").toString()) -
				// Double.parseDouble(All2.get("xsZcXf").toString())));
				// All.put("xsZcFwf",(Double.parseDouble(All1.get("xsZcFwf").toString()) -
				// Double.parseDouble(All2.get("xsZcFwf").toString())));
				// All.put("xsZcZc",(Double.parseDouble(All1.get("xsZcZc").toString()) -
				// Double.parseDouble(All2.get("xsZcZc").toString())));
				// All.put("xsZcJf",(Double.parseDouble(All1.get("xsZcJf").toString()) -
				// Double.parseDouble(All2.get("xsZcJf").toString())));
				All.put("tgTxXj", (Double.parseDouble(All1.get("tgTxXj").toString())
						- Double.parseDouble(All2.get("tgTxXj").toString())));
				All.put("tgTxHb", (Double.parseDouble(All1.get("tgTxHb").toString())
						- Double.parseDouble(All2.get("tgTxHb").toString())));
				// All.put("xsTxXj",(Double.parseDouble(All1.get("xsTxXj").toString()) -
				// Double.parseDouble(All2.get("xsTxXj").toString())));
				// All.put("xsTxHb",(Double.parseDouble(All1.get("xsTxHb").toString()) -
				// Double.parseDouble(All2.get("xsTxHb").toString())));
				All.put("yeHj", (Double.parseDouble(All1.get("yeHj").toString())
						- Double.parseDouble(All2.get("yeHj").toString())));
				All.put("endTime", (All1.get("endTime")));
				All.put("startTime", (All2.get("startTime")));

			} else {
				if (All1 != null) {
					All.putAll(All1);
				} else {
					All.putAll(All2);
				}
			}
		}
		if (mapIPageRetern.getRecords() == null || mapIPageRetern.getRecords().size() == 0 || All == null
				|| All.size() == 0) {
			mapIPageRetern.setRecords(new ArrayList<>());
		} else {
			for (Map m : mapIPageRetern.getRecords()) {
				if (!StringUtils.equals("", m.get("location").toString()))
					m.put("region", regionService.getItemAdCodOrName(StringUtils.toString(m.get("location"))));
			}
		}
		map.put("page", mapIPageRetern);
		map.put("All", All);
		return ResultUtil.data(map);

	}

	@ApiOperation(value = "事业端会员统计")
	@RequestMapping(value = "/getSYMember")
	public ResultMessage<Map<String, Object>> getSYMember(FinanceAdminVO financeAdminVO, PageVO page) {

		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}

		// 返回数据
		Map<String, Object> map = new HashMap<>();
		// 返回统计数据
		Map<String, Object> All = new HashMap<>();
		// 结束时间统计数据
		Map<String, Object> All1 = new HashMap<>();
		// 开始时间统计数据
		Map<String, Object> All2 = new HashMap<>();
		// 结束时间统计数据
		IPage<Map<String, Object>> mapIPage1 = null;
		// 开始时间统计数据
		IPage<Map<String, Object>> mapIPage2 = null;
		// 无时间统计数据
		IPage<Map<String, Object>> mapIPage3 = null;
		// 返回统计数据统计数据
		IPage<Map<String, Object>> mapIPageRetern = null;
		// 获取查询条件
		QueryWrapper queryWrapperNow = financeAdminVO.queryWrapperNow();
		QueryWrapper queryWrapperStart = financeAdminVO.queryWrapperStart();
		QueryWrapper queryWrapperEnd = financeAdminVO.queryWrapperEnd();

		queryWrapperNow.orderByDesc("m.create_time");
		queryWrapperStart.orderByDesc("m.create_time");
		queryWrapperEnd.orderByDesc("m.create_time");

		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 查询信息
		// 有时间区间
		if (financeAdminVO.getStartTime() != null && financeAdminVO.getEndTime() != null) {
			// 查询结束时间数据
			All1 = walletLogDetailService.getSYMemberAll(queryWrapperEnd);
			mapIPage1 = walletLogDetailService.getSYMember(page, queryWrapperEnd);
			// 根据结束时间查到的memberID 查询开始时间数据;
			All2 = walletLogDetailService.getSYMemberAll(queryWrapperStart);
			List<String> memberIdList = new ArrayList<>();
			if (mapIPage1 != null && mapIPage1.getRecords().size() > 0) {
				for (Map m : mapIPage1.getRecords()) {
					memberIdList.add(m.get("memberId").toString());
				}
				QueryWrapper queryWrapper = financeAdminVO.queryWrapperStart();
				queryWrapper.in("member_id", memberIdList);
				mapIPage2 = walletLogDetailService.getSYMember(page, queryWrapper);
			}
			// 以结束时间数据为基准
			mapIPageRetern = mapIPage1;
		} else {
			// 没有开始时间结束时间查询截止时间
			mapIPage3 = walletLogDetailService.getSYMember(page, queryWrapperNow);
			// 统计合计数据
			All = walletLogDetailService.getSYMemberAll(queryWrapperNow);
			mapIPageRetern = mapIPage3;
		}
		// 整理开始结束时间数据和合并数据
		// 区间数据为开始数据有结束时间的差值
		if (mapIPage1 != null && mapIPage2 != null) {
			List<Map<String, Object>> l1 = mapIPage1.getRecords();
			List<Map<String, Object>> l2 = mapIPage2.getRecords();
			List<Map<String, Object>> records = new ArrayList<>();
			for (Map m : l1) {
				Map member = new HashMap();
				member.putAll(m);
				for (Map m2 : l2) {
					if (StringUtils.equals(m.get("memberId").toString(), m2.get("memberId").toString())) {
						member.put("memberNum", (Double.parseDouble(m.get("memberNum").toString())
								- Double.parseDouble(m2.get("memberNum").toString())));
						member.put("dkfHb", (Double.parseDouble(m.get("dkfHb").toString())
								- Double.parseDouble(m2.get("dkfHb").toString())));
						member.put("dkfXj", (Double.parseDouble(m.get("dkfXj").toString())
								- Double.parseDouble(m2.get("dkfXj").toString())));
						member.put("czSrXj", (Double.parseDouble(m.get("czSrXj").toString())
								- Double.parseDouble(m2.get("czSrXj").toString())));
						member.put("czSrHb", (Double.parseDouble(m.get("czSrHb").toString())
								- Double.parseDouble(m2.get("czSrHb").toString())));
						member.put("czSrZr", (Double.parseDouble(m.get("czSrZr").toString())
								- Double.parseDouble(m2.get("czSrZr").toString())));
						member.put("czZcXf", (Double.parseDouble(m.get("czZcXf").toString())
								- Double.parseDouble(m2.get("czZcXf").toString())));
						member.put("czYe", (Double.parseDouble(m.get("czYe").toString())
								- Double.parseDouble(m2.get("czYe").toString())));
						member.put("tgSrXffy", (Double.parseDouble(m.get("tgSrXffy").toString())
								- Double.parseDouble(m2.get("tgSrXffy").toString())));
						member.put("tgSrHyfy", (Double.parseDouble(m.get("tgSrHyfy").toString())
								- Double.parseDouble(m2.get("tgSrHyfy").toString())));
						member.put("tgZcXf", (Double.parseDouble(m.get("tgZcXf").toString())
								- Double.parseDouble(m2.get("tgZcXf").toString())));
						member.put("tgZcFwf", (Double.parseDouble(m.get("tgZcFwf").toString())
								- Double.parseDouble(m2.get("tgZcFwf").toString())));
						member.put("tgYe", (Double.parseDouble(m.get("tgYe").toString())
								- Double.parseDouble(m2.get("tgYe").toString())));
						member.put("xsYe", (Double.parseDouble(m.get("xsYe").toString())
								- Double.parseDouble(m2.get("xsYe").toString())));
						member.put("xsSr", (Double.parseDouble(m.get("xsSr").toString())
								- Double.parseDouble(m2.get("xsSr").toString())));
						member.put("xsZcXf", (Double.parseDouble(m.get("xsZcXf").toString())
								- Double.parseDouble(m2.get("xsZcXf").toString())));
						member.put("xsZcFwf", (Double.parseDouble(m.get("xsZcFwf").toString())
								- Double.parseDouble(m2.get("xsZcFwf").toString())));
						member.put("xsZcZc", (Double.parseDouble(m.get("xsZcZc").toString())
								- Double.parseDouble(m2.get("xsZcZc").toString())));
						member.put("xsZcJf", (Double.parseDouble(m.get("xsZcJf").toString())
								- Double.parseDouble(m2.get("xsZcJf").toString())));
						member.put("tgTxXj", (Double.parseDouble(m.get("tgTxXj").toString())
								- Double.parseDouble(m2.get("tgTxXj").toString())));
						member.put("tgTxHb", (Double.parseDouble(m.get("tgTxHb").toString())
								- Double.parseDouble(m2.get("tgTxHb").toString())));
						member.put("xsTxXj", (Double.parseDouble(m.get("xsTxXj").toString())
								- Double.parseDouble(m2.get("xsTxXj").toString())));
						member.put("xsTxHb", (Double.parseDouble(m.get("xsTxHb").toString())
								- Double.parseDouble(m2.get("xsTxHb").toString())));
						member.put("yeHj", (Double.parseDouble(m.get("yeHj").toString())
								- Double.parseDouble(m2.get("yeHj").toString())));
					}
				}
				records.add(member);
			}
			mapIPageRetern.setRecords(records);
			if (All1 != null && All2 != null) {
				All.put("memberNum", (Double.parseDouble(All1.get("memberNum").toString())
						- Double.parseDouble(All2.get("memberNum").toString())));
				All.put("dkfHb", (Double.parseDouble(All1.get("dkfHb").toString())
						- Double.parseDouble(All2.get("dkfHb").toString())));
				All.put("dkfXj", (Double.parseDouble(All1.get("dkfXj").toString())
						- Double.parseDouble(All2.get("dkfXj").toString())));
				All.put("czSrXj", (Double.parseDouble(All1.get("czSrXj").toString())
						- Double.parseDouble(All2.get("czSrXj").toString())));
				All.put("czSrHb", (Double.parseDouble(All1.get("czSrHb").toString())
						- Double.parseDouble(All2.get("czSrHb").toString())));
				All.put("czSrZr", (Double.parseDouble(All1.get("czSrZr").toString())
						- Double.parseDouble(All2.get("czSrZr").toString())));
				All.put("czZcXf", (Double.parseDouble(All1.get("czZcXf").toString())
						- Double.parseDouble(All2.get("czZcXf").toString())));
				All.put("czYe", (Double.parseDouble(All1.get("czYe").toString())
						- Double.parseDouble(All2.get("czYe").toString())));
				All.put("tgSrXffy", (Double.parseDouble(All1.get("tgSrXffy").toString())
						- Double.parseDouble(All2.get("tgSrXffy").toString())));
				All.put("tgSrHyfy", (Double.parseDouble(All1.get("tgSrHyfy").toString())
						- Double.parseDouble(All2.get("tgSrHyfy").toString())));
				All.put("tgZcXf", (Double.parseDouble(All1.get("tgZcXf").toString())
						- Double.parseDouble(All2.get("tgZcXf").toString())));
				All.put("tgZcFwf", (Double.parseDouble(All1.get("tgZcFwf").toString())
						- Double.parseDouble(All2.get("tgZcFwf").toString())));
				All.put("tgYe", (Double.parseDouble(All1.get("tgYe").toString())
						- Double.parseDouble(All2.get("tgYe").toString())));
				All.put("xsYe", (Double.parseDouble(All1.get("xsYe").toString())
						- Double.parseDouble(All2.get("xsYe").toString())));
				All.put("xsSr", (Double.parseDouble(All1.get("xsSr").toString())
						- Double.parseDouble(All2.get("xsSr").toString())));
				All.put("xsZcXf", (Double.parseDouble(All1.get("xsZcXf").toString())
						- Double.parseDouble(All2.get("xsZcXf").toString())));
				All.put("xsZcFwf", (Double.parseDouble(All1.get("xsZcFwf").toString())
						- Double.parseDouble(All2.get("xsZcFwf").toString())));
				All.put("xsZcZc", (Double.parseDouble(All1.get("xsZcZc").toString())
						- Double.parseDouble(All2.get("xsZcZc").toString())));
				All.put("xsZcJf", (Double.parseDouble(All1.get("xsZcJf").toString())
						- Double.parseDouble(All2.get("xsZcJf").toString())));
				All.put("tgTxXj", (Double.parseDouble(All1.get("tgTxXj").toString())
						- Double.parseDouble(All2.get("tgTxXj").toString())));
				All.put("tgTxHb", (Double.parseDouble(All1.get("tgTxHb").toString())
						- Double.parseDouble(All2.get("tgTxHb").toString())));
				All.put("xsTxXj", (Double.parseDouble(All1.get("xsTxXj").toString())
						- Double.parseDouble(All2.get("xsTxXj").toString())));
				All.put("xsTxHb", (Double.parseDouble(All1.get("xsTxHb").toString())
						- Double.parseDouble(All2.get("xsTxHb").toString())));
				All.put("yeHj", (Double.parseDouble(All1.get("yeHj").toString())
						- Double.parseDouble(All2.get("yeHj").toString())));
				All.put("endTime", (All1.get("endTime")));
				All.put("startTime", (All2.get("startTime")));

			} else {
				if (All1 != null) {
					All.putAll(All1);
				} else {
					All.putAll(All2);
				}
			}
		}
		if (mapIPageRetern.getRecords() == null || mapIPageRetern.getRecords().size() == 0 || All == null
				|| All.size() == 0) {
			mapIPageRetern.setRecords(new ArrayList<>());
		} else {
			for (Map m : mapIPageRetern.getRecords()) {
				if (!StringUtils.equals("", m.get("location").toString()))
					m.put("region", regionService.getItemAdCodOrName(StringUtils.toString(m.get("location"))));
			}
		}
		map.put("page", mapIPageRetern);
		map.put("All", All);
		return ResultUtil.data(map);

	}

	@ApiOperation(value = "区域端会员统计")
	@RequestMapping(value = "/getQYMember")

	public ResultMessage<Map<String, Object>> getQYMember(FinanceAdminVO financeAdminVO, PageVO page) {

		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}

		// 返回数据
		Map<String, Object> map = new HashMap<>();
		// 返回统计数据
		Map<String, Object> All = new HashMap<>();
		// 结束时间统计数据
		Map<String, Object> All1 = new HashMap<>();
		// 开始时间统计数据
		Map<String, Object> All2 = new HashMap<>();
		// 结束时间统计数据
		IPage<Map<String, Object>> mapIPage1 = null;
		// 开始时间统计数据
		IPage<Map<String, Object>> mapIPage2 = null;
		// 无时间统计数据
		IPage<Map<String, Object>> mapIPage3 = null;
		// 返回统计数据统计数据
		IPage<Map<String, Object>> mapIPageRetern = null;
		// 获取查询条件
		QueryWrapper queryWrapperNow = financeAdminVO.queryWrapperNow();
		QueryWrapper queryWrapperStart = financeAdminVO.queryWrapperStart();
		QueryWrapper queryWrapperEnd = financeAdminVO.queryWrapperEnd();

		queryWrapperNow.orderByDesc("m.create_time");
		queryWrapperStart.orderByDesc("m.create_time");
		queryWrapperEnd.orderByDesc("m.create_time");

		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapperNow.in(list.size() > 0, "a.location", list);
			queryWrapperStart.in(list.size() > 0, "a.location", list);
			queryWrapperEnd.in(list.size() > 0, "a.location", list);
		}
		// 查询信息
		// 有时间区间
		if (financeAdminVO.getStartTime() != null && financeAdminVO.getEndTime() != null) {
			// 查询结束时间数据
			All1 = walletLogDetailService.getQYMemberAll(queryWrapperEnd);
			mapIPage1 = walletLogDetailService.getQYMember(page, queryWrapperEnd);
			// 根据结束时间查到的memberID 查询开始时间数据;
			All2 = walletLogDetailService.getQYMemberAll(queryWrapperStart);
			List<String> memberIdList = new ArrayList<>();
			if (mapIPage1 != null && mapIPage1.getRecords().size() > 0) {
				for (Map m : mapIPage1.getRecords()) {
					memberIdList.add(m.get("memberId").toString());
				}
				QueryWrapper queryWrapper = financeAdminVO.queryWrapperStart();
				queryWrapper.in("member_id", memberIdList);
				mapIPage2 = walletLogDetailService.getQYMember(page, queryWrapper);
			}
			// 以结束时间数据为基准
			mapIPageRetern = mapIPage1;
		} else {
			// 没有开始时间结束时间查询截止时间
			mapIPage3 = walletLogDetailService.getQYMember(page, queryWrapperNow);
			// 统计合计数据
			All = walletLogDetailService.getQYMemberAll(queryWrapperNow);
			mapIPageRetern = mapIPage3;
		}
		// 整理开始结束时间数据和合并数据
		// 区间数据为开始数据有结束时间的差值
		if (mapIPage1 != null && mapIPage2 != null) {
			List<Map<String, Object>> l1 = mapIPage1.getRecords();
			List<Map<String, Object>> l2 = mapIPage2.getRecords();
			List<Map<String, Object>> records = new ArrayList<>();
			for (Map m : l1) {
				Map member = new HashMap();
				member.putAll(m);
				for (Map m2 : l2) {
					if (StringUtils.equals(m.get("memberId").toString(), m2.get("memberId").toString())) {
						member.put("memberNum", (Double.parseDouble(m.get("memberNum").toString())
								- Double.parseDouble(m2.get("memberNum").toString())));
						member.put("dkfHb", (Double.parseDouble(m.get("dkfHb").toString())
								- Double.parseDouble(m2.get("dkfHb").toString())));
						member.put("dkfXj", (Double.parseDouble(m.get("dkfXj").toString())
								- Double.parseDouble(m2.get("dkfXj").toString())));
						member.put("czSrXj", (Double.parseDouble(m.get("czSrXj").toString())
								- Double.parseDouble(m2.get("czSrXj").toString())));
						member.put("czSrHb", (Double.parseDouble(m.get("czSrHb").toString())
								- Double.parseDouble(m2.get("czSrHb").toString())));
						member.put("czSrZr", (Double.parseDouble(m.get("czSrZr").toString())
								- Double.parseDouble(m2.get("czSrZr").toString())));
						member.put("czZcXf", (Double.parseDouble(m.get("czZcXf").toString())
								- Double.parseDouble(m2.get("czZcXf").toString())));
						member.put("czYe", (Double.parseDouble(m.get("czYe").toString())
								- Double.parseDouble(m2.get("czYe").toString())));
						member.put("tgSrXffy", (Double.parseDouble(m.get("tgSrXffy").toString())
								- Double.parseDouble(m2.get("tgSrXffy").toString())));
						member.put("tgSrHyfy", (Double.parseDouble(m.get("tgSrHyfy").toString())
								- Double.parseDouble(m2.get("tgSrHyfy").toString())));
						member.put("tgHyZcXf", (Double.parseDouble(m.get("tgHyZcXf").toString())
								- Double.parseDouble(m2.get("tgHyZcXf").toString())));
						member.put("tgHyZcFwf", (Double.parseDouble(m.get("tgHyZcFwf").toString())
								- Double.parseDouble(m2.get("tgHyZcFwf").toString())));
						member.put("tgFwZcXf", (Double.parseDouble(m.get("tgFwZcXf").toString())
								- Double.parseDouble(m2.get("tgFwZcXf").toString())));
						member.put("tgFwZcFwf", (Double.parseDouble(m.get("tgFwZcFwf").toString())
								- Double.parseDouble(m2.get("tgFwZcFwf").toString())));
						member.put("tgHyYe", (Double.parseDouble(m.get("tgHyYe").toString())
								- Double.parseDouble(m2.get("tgHyYe").toString())));
						member.put("tgFwYe", (Double.parseDouble(m.get("tgFwYe").toString())
								- Double.parseDouble(m2.get("tgFwYe").toString())));
						member.put("xsYe", (Double.parseDouble(m.get("xsYe").toString())
								- Double.parseDouble(m2.get("xsYe").toString())));
						member.put("xsSr", (Double.parseDouble(m.get("xsSr").toString())
								- Double.parseDouble(m2.get("xsSr").toString())));
						member.put("xsZcXf", (Double.parseDouble(m.get("xsZcXf").toString())
								- Double.parseDouble(m2.get("xsZcXf").toString())));
						member.put("xsZcFwf", (Double.parseDouble(m.get("xsZcFwf").toString())
								- Double.parseDouble(m2.get("xsZcFwf").toString())));
						member.put("xsZcZc", (Double.parseDouble(m.get("xsZcZc").toString())
								- Double.parseDouble(m2.get("xsZcZc").toString())));
						member.put("xsZcJf", (Double.parseDouble(m.get("xsZcJf").toString())
								- Double.parseDouble(m2.get("xsZcJf").toString())));
						member.put("tgTxXj", (Double.parseDouble(m.get("tgTxXj").toString())
								- Double.parseDouble(m2.get("tgTxXj").toString())));
						member.put("tgTxHb", (Double.parseDouble(m.get("tgTxHb").toString())
								- Double.parseDouble(m2.get("tgTxHb").toString())));
						member.put("xsTxXj", (Double.parseDouble(m.get("xsTxXj").toString())
								- Double.parseDouble(m2.get("xsTxXj").toString())));
						member.put("xsTxHb", (Double.parseDouble(m.get("xsTxHb").toString())
								- Double.parseDouble(m2.get("xsTxHb").toString())));
						member.put("yeHj", (Double.parseDouble(m.get("yeHj").toString())
								- Double.parseDouble(m2.get("yeHj").toString())));
					}
				}
				records.add(member);
			}
			mapIPageRetern.setRecords(records);
			if (All1 != null && All2 != null) {
				All.put("memberNum", (Double.parseDouble(All1.get("memberNum").toString())
						- Double.parseDouble(All2.get("memberNum").toString())));
				All.put("dkfHb", (Double.parseDouble(All1.get("dkfHb").toString())
						- Double.parseDouble(All2.get("dkfHb").toString())));
				All.put("dkfXj", (Double.parseDouble(All1.get("dkfXj").toString())
						- Double.parseDouble(All2.get("dkfXj").toString())));
				All.put("czSrXj", (Double.parseDouble(All1.get("czSrXj").toString())
						- Double.parseDouble(All2.get("czSrXj").toString())));
				All.put("czSrHb", (Double.parseDouble(All1.get("czSrHb").toString())
						- Double.parseDouble(All2.get("czSrHb").toString())));
				All.put("czSrZr", (Double.parseDouble(All1.get("czSrZr").toString())
						- Double.parseDouble(All2.get("czSrZr").toString())));
				All.put("czZcXf", (Double.parseDouble(All1.get("czZcXf").toString())
						- Double.parseDouble(All2.get("czZcXf").toString())));
				All.put("czYe", (Double.parseDouble(All1.get("czYe").toString())
						- Double.parseDouble(All2.get("czYe").toString())));
				All.put("tgSrXffy", (Double.parseDouble(All1.get("tgSrXffy").toString())
						- Double.parseDouble(All2.get("tgSrXffy").toString())));
				All.put("tgSrHyfy", (Double.parseDouble(All1.get("tgSrHyfy").toString())
						- Double.parseDouble(All2.get("tgSrHyfy").toString())));
				All.put("tgHyZcXf", (Double.parseDouble(All1.get("tgHyZcXf").toString())
						- Double.parseDouble(All2.get("tgHyZcXf").toString())));
				All.put("tgHyZcFwf", (Double.parseDouble(All1.get("tgHyZcFwf").toString())
						- Double.parseDouble(All2.get("tgHyZcFwf").toString())));
				All.put("tgFwZcXf", (Double.parseDouble(All1.get("tgFwZcXf").toString())
						- Double.parseDouble(All2.get("tgFwZcXf").toString())));
				All.put("tgFwZcFwf", (Double.parseDouble(All1.get("tgFwZcFwf").toString())
						- Double.parseDouble(All2.get("tgFwZcFwf").toString())));
				All.put("tgHyYe", (Double.parseDouble(All1.get("tgHyYe").toString())
						- Double.parseDouble(All2.get("tgHyYe").toString())));
				All.put("tgFwYe", (Double.parseDouble(All1.get("tgFwYe").toString())
						- Double.parseDouble(All2.get("tgFwYe").toString())));
				All.put("xsYe", (Double.parseDouble(All1.get("xsYe").toString())
						- Double.parseDouble(All2.get("xsYe").toString())));
				All.put("xsSr", (Double.parseDouble(All1.get("xsSr").toString())
						- Double.parseDouble(All2.get("xsSr").toString())));
				All.put("xsZcXf", (Double.parseDouble(All1.get("xsZcXf").toString())
						- Double.parseDouble(All2.get("xsZcXf").toString())));
				All.put("xsZcFwf", (Double.parseDouble(All1.get("xsZcFwf").toString())
						- Double.parseDouble(All2.get("xsZcFwf").toString())));
				All.put("xsZcZc", (Double.parseDouble(All1.get("xsZcZc").toString())
						- Double.parseDouble(All2.get("xsZcZc").toString())));
				All.put("xsZcJf", (Double.parseDouble(All1.get("xsZcJf").toString())
						- Double.parseDouble(All2.get("xsZcJf").toString())));
				All.put("tgTxXj", (Double.parseDouble(All1.get("tgTxXj").toString())
						- Double.parseDouble(All2.get("tgTxXj").toString())));
				All.put("tgTxHb", (Double.parseDouble(All1.get("tgTxHb").toString())
						- Double.parseDouble(All2.get("tgTxHb").toString())));
				All.put("xsTxXj", (Double.parseDouble(All1.get("xsTxXj").toString())
						- Double.parseDouble(All2.get("xsTxXj").toString())));
				All.put("xsTxHb", (Double.parseDouble(All1.get("xsTxHb").toString())
						- Double.parseDouble(All2.get("xsTxHb").toString())));
				All.put("yeHj", (Double.parseDouble(All1.get("yeHj").toString())
						- Double.parseDouble(All2.get("yeHj").toString())));
				All.put("endTime", (All1.get("endTime")));
				All.put("startTime", (All2.get("startTime")));
			} else {
				if (All1 != null) {
					All.putAll(All1);
				} else {
					All.putAll(All2);
				}
			}
		}
		if (mapIPageRetern.getRecords() == null || mapIPageRetern.getRecords().size() == 0 || All == null
				|| All.size() == 0) {
			mapIPageRetern.setRecords(new ArrayList<>());
		} else {
			for (Map m : mapIPageRetern.getRecords()) {
				if (!StringUtils.equals("", m.get("location").toString()))
					m.put("region", regionService.getItemAdCodOrName(StringUtils.toString(m.get("location"))));
			}
		}
		map.put("page", mapIPageRetern);
		map.put("All", All);
		return ResultUtil.data(map);

	}

	@ApiOperation(value = "平台端查看数据统计信息 焕贝")
	@RequestMapping(value = "/getPTMemberAlltoHB")
	public ResultMessage<List<Map<String, Object>>> getPTMemberAlltoHBXJ(FinanceAdminVO financeAdminVO) {
		Map<String, Object> map = new HashMap();
		// 获取查询条件
		QueryWrapper queryWrapperRegionId = financeAdminVO.queryWrapperRegionId();
//		AuthUser currentUser = UserContext.getCurrentUser();
//		// 如果当前会员不为空，且为代理商角色
//		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
//			// 通过用户名获取会员id
//			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
//			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
//			Partner partner = partnerService.getOne(lambdaQueryWrapper);
//			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
//				financeAdminVO.setAdcode(partner.getRegionId());
//				List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
//				queryWrapperRegionId.in(list.size() > 0, "location", list);
//			}
//		}
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperRegionId.in(list.size() > 0, "location", list);
		}
		// 区域ID
		// 平台焕贝数据焕贝现金/现金数据
		map = walletLogDetailService.getPTMemberAlltoHB(queryWrapperRegionId);
		// 平台剩余数据(用户ID)
		String memberId = "admin";
		Map m = walletLogDetailService.getPTMemberAlltoHBYE(memberId);
		if (m != null && m.size() > 0) {
			map.putAll(m);
		}
		if (map != null && map.size() > 0) {
			List l = new ArrayList<>();
			l.add(map);
			return ResultUtil.data(l);
		} else {
			return ResultUtil.error(ResultCode.FINANCE_ADMIN_MONEY);
		}
	}

	@ApiOperation(value = "平台端查看数据统计信息现金")
	@RequestMapping(value = "/getPTMemberAlltoXJ")
	public ResultMessage<List<Map<String, Object>>> getPTMemberAlltoXJ(FinanceAdminVO financeAdminVO) {
		QueryWrapper queryWrapperRegionId = financeAdminVO.queryWrapperRegionId();
//		AuthUser currentUser = UserContext.getCurrentUser();
//		// 如果当前会员不为空，且为代理商角色
//		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
//			// 通过用户名获取会员id
//			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
//			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
//			Partner partner = partnerService.getOne(lambdaQueryWrapper);
//			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
//				financeAdminVO.setAdcode(partner.getRegionId());
//				List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
//				queryWrapperRegionId.in(list.size() > 0, "location", list);
//			}
//		}
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperRegionId.in(list.size() > 0, "location", list);
		}
		Map<String, Object> xjmap = new HashMap();
		// 平台焕贝数据焕贝现金 提现支出现金
		xjmap.putAll(walletLogDetailService.getPTMemberAlltoXJ(queryWrapperRegionId));

		// 平台提现支出
		Map<String, Object> adminMap = new HashMap();
		String memberId = "admin";
		adminMap.putAll(walletLogDetailService.getPTMemberAlltoTXXJ(memberId));

		Map<String, Object> hbmap = new HashMap();
		hbmap = walletLogDetailService.getPTMemberAlltoHB(queryWrapperRegionId);
		Map<String, Object> ktxxjmap = new HashMap();
		ktxxjmap = walletLogDetailService.getPTMemberAlltoKTxXj(queryWrapperRegionId);
		// 区域价差收益
//		Double czXjZ = Double.parseDouble(xjmap.get("srZe").toString());
//		Double czHbZ = Double.parseDouble(hbmap.get("czHbZe").toString());
//		Double qykTxXjZ = Double.parseDouble(ktxxjmap.get("qyTgFwYeXj").toString())
//				+ Double.parseDouble(ktxxjmap.get("qyTgHyYeXj").toString());
//		Double qykTxHbZ = Double.parseDouble(ktxxjmap.get("tgFwYeHbZ").toString())
//				+ Double.parseDouble(ktxxjmap.get("tgHyYeHbZ").toString());
		Double wxCzXj = Double.parseDouble(ktxxjmap.get("wxCzXj").toString());
		Double wxCzHb = Double.parseDouble(ktxxjmap.get("wxCzHb").toString());
		Double wxTxHb = Double.parseDouble(ktxxjmap.get("wxTxHb").toString());
		Double wxTxXj = Double.parseDouble(ktxxjmap.get("wxTxXj").toString());
		Double jcSyQy = 0.0;
		Double jcSyPt = 0.0;
//		if (wxTxXj != 0 && wxCzXj != 0) {
//			jcSyQy = wxTxXj * (wxCzXj / wxCzHb - wxTxXj / wxTxHb)
//				* Double.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_QY.dictCode()));
//			jcSyPt = wxTxXj * (wxCzXj / wxCzHb - wxTxXj / wxTxHb)
//				* Double.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_PT.dictCode()));
//		}
		xjmap.put("tgTxXjZ",
				CurrencyUtil.roundDown(CurrencyUtil.add(Double.parseDouble(xjmap.get("tgTxXjZ").toString()), jcSyQy), 2));
		xjmap.put("zcZe",
				CurrencyUtil.roundDown(CurrencyUtil.add(Double.parseDouble(xjmap.get("zcZe").toString()), jcSyQy), 2));
		adminMap.put("ptTgTxXjZ", CurrencyUtil
				.roundDown(CurrencyUtil.add(Double.parseDouble(adminMap.get("ptTgTxXjZ").toString()), jcSyPt), 2));
		adminMap.put("ptZcZe", Double.parseDouble(adminMap.get("ptTgTxXjZ").toString()));
		Map map = new HashMap();
		map.putAll(adminMap);
		map.putAll(xjmap);
		map.put("shengYu", CurrencyUtil.sub(Double.parseDouble(xjmap.get("srZe").toString()),
				Double.parseDouble(adminMap.get("ptZcZe").toString()),Double.parseDouble(xjmap.get("zcZe").toString())));
		map.put("jcSyQy", CurrencyUtil.roundDown(jcSyQy, 2));
		map.put("jcSyPt", CurrencyUtil.roundDown(jcSyPt, 2));
		List l = new ArrayList<>();
		l.add(map);
		return ResultUtil.data(l);
	}

	@ApiOperation(value = "平台端查看数据统计信息提现盈余")
	@RequestMapping(value = "/getPTMemberAlltoXJYU")
	public ResultMessage<List<Map<String, Object>>> getPTMemberAlltoXJYU(FinanceAdminVO financeAdminVO) {
		Map<String, Object> map = new HashMap();

		QueryWrapper queryWrapperRegionId = financeAdminVO.queryWrapperRegionId();
//		AuthUser currentUser = UserContext.getCurrentUser();
//		// 如果当前会员不为空，且为代理商角色
//		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
//			// 通过用户名获取会员id
//			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
//			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
//			Partner partner = partnerService.getOne(lambdaQueryWrapper);
//			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
//				financeAdminVO.setAdcode(partner.getRegionId());
//				List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
//				queryWrapperRegionId.in(list.size() > 0, "location", list);
//			}
//		}
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperRegionId.in(list.size() > 0, "location", list);
		}
		// 现金余额
		Map<String, Object> xjmap = new HashMap();
		xjmap.putAll(walletLogDetailService.getPTMemberAlltoXJ(queryWrapperRegionId));
		Map<String, Object> hbmap = new HashMap();
		hbmap = walletLogDetailService.getPTMemberAlltoHB(queryWrapperRegionId);

		// 平台提现支出
		Map<String, Object> adminmap = new HashMap();
		String memberId = "admin";
		adminmap.putAll(walletLogDetailService.getPTMemberAlltoTXXJ(memberId));

		// 应体现金额
		Map<String, Object> ktxxjmapadmin = new HashMap();
		ktxxjmapadmin = walletLogDetailService.getPTMemberAlltoHBYE(memberId);
		Map<String, Object> ktxxjmap = new HashMap();
		ktxxjmap = walletLogDetailService.getPTMemberAlltoKTxXj(queryWrapperRegionId);

		// 区域价差收益
		Double czXjZ = Double.parseDouble(xjmap.get("srZe").toString());
		Double czHbZ = Double.parseDouble(hbmap.get("czHbZe").toString());
//		Double shengYu = CurrencyUtil.sub(Double.parseDouble(xjmap.get("shengYu").toString()),
//				Double.parseDouble(adminmap.get("ptZcZe").toString()));
		Double pTKTxXjZ = Double.parseDouble(ktxxjmapadmin.get("tgFwXj").toString())
				+ Double.parseDouble(ktxxjmapadmin.get("tgHyXj").toString());
		Double qykTxXjZ = Double.parseDouble(ktxxjmap.get("qyTgFwYeXj").toString())
				+ Double.parseDouble(ktxxjmap.get("qyTgHyYeXj").toString());
		Double qykTxHbZ = Double.parseDouble(ktxxjmap.get("tgFwYeHbZ").toString())
				+ Double.parseDouble(ktxxjmap.get("tgHyYeHbZ").toString());
//		Double wxCzXj = Double.parseDouble(ktxxjmap.get("wxCzXj").toString());
//		Double wxCzHb = Double.parseDouble(ktxxjmap.get("wxCzHb").toString());
//		Double wxTxHb = Double.parseDouble(ktxxjmap.get("wxTxHb").toString());
//		Double wxTxXj = Double.parseDouble(ktxxjmap.get("wxTxXj").toString());
		Double jcSyQy = 0.0;
		Double jcSyPt = 0.0;
//		if (wxTxXj != 0 && wxCzXj != 0) {
//			jcSyQy = wxTxXj * (wxCzXj / wxCzHb - wxTxXj / wxTxHb)
//				* Double.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_QY.dictCode()));
//			jcSyPt = wxTxXj * (wxCzXj / wxCzHb - wxTxXj / wxTxHb)
//				* Double.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_PT.dictCode()));
//		}
		QueryWrapper query = new QueryWrapper<>();
		PageVO page = new PageVO();
		page.setPageSize(999999999);
		IPage<Spreadlncome> SpreadIncomeVOPage = spreadlncomeService.page(PageUtil.initPage(page), query);
		Spreadlncome spreadlncome = new Spreadlncome();
		List<Spreadlncome> spreadlncomeList = SpreadIncomeVOPage.getRecords();
		for (Spreadlncome s : spreadlncomeList) {
			spreadlncome.setAllocateAdminMoney(
				CurrencyUtil.add(s.getAllocateAdminMoney(), spreadlncome.getAllocateAdminMoney() == null ? 0 : spreadlncome.getAllocateAdminMoney()));
			spreadlncome.setAllocateMemberMoney(
				CurrencyUtil.add(s.getAllocateMemberMoney(), spreadlncome.getAllocateMemberMoney() == null ? 0 : spreadlncome.getAllocateMemberMoney()));
		}
		jcSyPt = spreadlncome.getAllocateAdminMoney() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getAllocateAdminMoney(), 2);
		jcSyQy = spreadlncome.getAllocateMemberMoney() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getAllocateMemberMoney(), 2);



		Double shengYu = CurrencyUtil.sub(Double.parseDouble(xjmap.get("srZe").toString()),
			Double.parseDouble(adminmap.get("ptZcZe").toString()),Double.parseDouble(xjmap.get("zcZe").toString()));
		map.put("czYeXjZ", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("czYeXjZ").toString()), 2));
		map.put("cCzYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("cCzYeXj").toString()), 2));
		map.put("bCzYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("bCzYeXj").toString()), 2));
		map.put("tsCzYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("tsCzYeXj").toString()), 2));
		map.put("syCzYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("syCzYeXj").toString()), 2));
		map.put("qyCzYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("qyCzYeXj").toString()), 2));
		map.put("tgYeXjZ", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("tgYeXj").toString()), 2));
		map.put("xsTxXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("xsTxXj").toString()), 2));
		map.put("jcSyQy", CurrencyUtil.roundDown(jcSyQy, 2));
		map.put("jcSyPt", CurrencyUtil.roundDown(jcSyPt, 2));
		map.put("shengYu", CurrencyUtil.roundDown(shengYu, 2));
		map.put("qyYTxZ", CurrencyUtil.roundDown(CurrencyUtil.add(qykTxXjZ, jcSyQy), 2));
		map.put("qyTgFwYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("qyTgFwYeXj").toString()), 2));
		map.put("qyTgHyYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("qyTgHyYeXj").toString()), 2));
		map.put("ptYTxZ", CurrencyUtil.roundDown(CurrencyUtil.add(pTKTxXjZ, jcSyPt), 2));
		map.put("ptTgFwYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmapadmin.get("tgFwXj").toString()), 2));
		map.put("ptTgHyYeXj", CurrencyUtil.roundDown(Double.parseDouble(ktxxjmapadmin.get("tgHyXj").toString()), 2));
		map.put("yTiXjZ", CurrencyUtil.roundDown(CurrencyUtil.add(Double.parseDouble(map.get("ptYTxZ").toString()),
				Double.parseDouble(map.get("qyYTxZ").toString()), Double.parseDouble(ktxxjmap.get("xsTxXj").toString()),
				Double.parseDouble(ktxxjmap.get("tgYeXj").toString())), 2));
		map.put("yingYu", CurrencyUtil.roundDown(shengYu - Double.parseDouble(map.get("yTiXjZ").toString())
				- Double.parseDouble(map.get("czYeXjZ").toString()), 2));

		List l = new ArrayList<>();
		l.add(map);
		return ResultUtil.data(l);
	}

	@ApiOperation(value = "平台端查看数据统计信息盈余分配")
	@RequestMapping(value = "/getPTMemberAlltoYUFP")
	public ResultMessage<List<Map<String, Object>>> getPTMemberAlltoYUFP(FinanceAdminVO financeAdminVO) {

		QueryWrapper queryWrapperRegionId = financeAdminVO.queryWrapperRegionId();
//		AuthUser currentUser = UserContext.getCurrentUser();
//		// 如果当前会员不为空，且为代理商角色
//		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
//			// 通过用户名获取会员id
//			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
//			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
//			Partner partner = partnerService.getOne(lambdaQueryWrapper);
//			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
//				financeAdminVO.setAdcode(partner.getRegionId());
//				List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
//				queryWrapperRegionId.in(list.size() > 0, "location", list);
//			}
//		}
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapperRegionId.in(list.size() > 0, "location", list);
		}
		// 现金数据
		Map<String, Object> xjmap = new HashMap();
		xjmap.putAll(walletLogDetailService.getPTMemberAlltoXJ(queryWrapperRegionId));
		Map<String, Object> ktxxjmap = new HashMap();
		// 平台提现数据
		ktxxjmap = walletLogDetailService.getPTMemberAlltoKTxXj(queryWrapperRegionId);
		Map<String, Object> hbmap = new HashMap();
		hbmap = walletLogDetailService.getPTMemberAlltoHB(queryWrapperRegionId);
		// 应体现金额
		Map<String, Object> ktxxjmapadmin = new HashMap();
		String memberId = "admin";
		ktxxjmapadmin = walletLogDetailService.getPTMemberAlltoHBYE(memberId);
		Map<String, Object> adminmap = new HashMap();
		adminmap.putAll(walletLogDetailService.getPTMemberAlltoTXXJ(memberId));
//		ktxxjmap = walletLogDetailService.getPTMemberAlltoKTxXj(queryWrapperRegionId);

		Map<String, Object> map = new HashMap();
		Map<String, Object> map1 = new HashMap();

		// 区域价差收益
		Double czXjZ = Double.parseDouble(xjmap.get("srZe").toString());
		Double czHbZ = Double.parseDouble(hbmap.get("czHbZe").toString());
//		Double shengYu = CurrencyUtil.sub(Double.parseDouble(xjmap.get("shengYu").toString()),
//				Double.parseDouble(adminmap.get("ptZcZe").toString()));
//		Double shengYu = Double.parseDouble(xjmap.get("shengYu").toString());
		Double pTKTxXjZ = Double.parseDouble(ktxxjmapadmin.get("tgFwXj").toString())
				+ Double.parseDouble(ktxxjmapadmin.get("tgHyXj").toString());
		Double qykTxXjZ = Double.parseDouble(ktxxjmap.get("qyTgFwYeXj").toString())
				+ Double.parseDouble(ktxxjmap.get("qyTgHyYeXj").toString());
		Double qykTxHbZ = Double.parseDouble(ktxxjmap.get("tgFwYeHbZ").toString())
				+ Double.parseDouble(ktxxjmap.get("tgHyYeHbZ").toString());
		Double wxCzXj = Double.parseDouble(ktxxjmap.get("wxCzXj").toString());
		Double wxCzHb = Double.parseDouble(ktxxjmap.get("wxCzHb").toString());
		Double wxTxHb = Double.parseDouble(ktxxjmap.get("wxTxHb").toString());
		Double wxTxXj = Double.parseDouble(ktxxjmap.get("wxTxXj").toString());
		Double jcSyQy = 0.0;
		Double jcSyPt = 0.0;
//		if (wxTxXj != 0 && wxCzXj != 0) {
//			jcSyQy = wxTxXj * (wxCzXj / wxCzHb - wxTxXj / wxTxHb)
//				* Double.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_QY.dictCode()));
//			jcSyPt = wxTxXj * (wxCzXj / wxCzHb - wxTxXj / wxTxHb)
//				* Double.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_PT.dictCode()));
//		}
		QueryWrapper query = new QueryWrapper<>();
		PageVO page = new PageVO();
		page.setPageSize(999999999);
		IPage<Spreadlncome> SpreadIncomeVOPage = spreadlncomeService.page(PageUtil.initPage(page), query);
		Spreadlncome spreadlncome = new Spreadlncome();
		List<Spreadlncome> spreadlncomeList = SpreadIncomeVOPage.getRecords();
		for (Spreadlncome s : spreadlncomeList) {
			spreadlncome.setAllocateAdminMoney(
				CurrencyUtil.add(s.getAllocateAdminMoney(), spreadlncome.getAllocateAdminMoney() == null ? 0 : spreadlncome.getAllocateAdminMoney()));
			spreadlncome.setAllocateMemberMoney(
				CurrencyUtil.add(s.getAllocateMemberMoney(), spreadlncome.getAllocateMemberMoney() == null ? 0 : spreadlncome.getAllocateMemberMoney()));
		}
		jcSyPt = spreadlncome.getAllocateAdminMoney() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getAllocateAdminMoney(), 2);
		jcSyQy = spreadlncome.getAllocateMemberMoney() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getAllocateMemberMoney(), 2);


		Double shengYu = CurrencyUtil.sub(Double.parseDouble(xjmap.get("srZe").toString()),
			Double.parseDouble(adminmap.get("ptZcZe").toString()),Double.parseDouble(xjmap.get("zcZe").toString()));
		map.put("ptYTxZ", CurrencyUtil.roundDown(CurrencyUtil.add(pTKTxXjZ, jcSyPt), 2));
		map.put("yTiXjZ", CurrencyUtil.roundDown(CurrencyUtil.add(pTKTxXjZ, jcSyPt, qykTxXjZ, jcSyQy), 2));

		Double pingTaiKeTiXian = pTKTxXjZ;
		Double pingTaiYiTiXian = Double.parseDouble(adminmap.get("ptTgTxXjZ").toString());
		Double quYuKeTiXian = Double.parseDouble(ktxxjmap.get("qyTgFwYeXj").toString())
				+ Double.parseDouble(ktxxjmap.get("qyTgHyYeXj").toString());
		Double quYuYiTiXian = Double.parseDouble(xjmap.get("tgTxXjZ").toString());
		Double pingTaiZ = pingTaiKeTiXian + pingTaiYiTiXian;
		Double quYuZ = quYuYiTiXian + quYuKeTiXian;
		Double tgYeXj = Double.parseDouble(ktxxjmap.get("tgYeXj").toString());
		Double xsTxXj = CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("xsTxXj").toString()), 2);
		Double czYeXjZ = CurrencyUtil.roundDown(Double.parseDouble(ktxxjmap.get("czYeXjZ").toString()), 2);
		Double yingYu = CurrencyUtil.roundDown(shengYu - pTKTxXjZ - quYuKeTiXian - tgYeXj - xsTxXj - jcSyQy - jcSyPt - czYeXjZ, 2);
		map.put("yingYu", yingYu);
		map.put("yingfenpei", CurrencyUtil.roundDown(pingTaiKeTiXian + jcSyPt, 2));
		map.put("yifenpei", CurrencyUtil.roundDown(pingTaiYiTiXian, 2));
		map.put("hj", CurrencyUtil.roundDown(pingTaiKeTiXian + pingTaiYiTiXian + yingYu + jcSyPt, 2));
		map.put("name", "平台");
		map1.put("yingfenpei", CurrencyUtil.roundDown(qykTxXjZ + jcSyQy, 2));
		map1.put("yifenpei", CurrencyUtil.roundDown(quYuYiTiXian, 2));
		map1.put("hj", CurrencyUtil.roundDown(qykTxXjZ + quYuYiTiXian + jcSyQy, 2));
		map1.put("name", "代理商");

		List l = new ArrayList();
		l.add(map);
		l.add(map1);
		return ResultUtil.data(l);
	}

	@ApiOperation(value = "价差收益")
	@RequestMapping(value = "/getSpreadIncomeAll")
	public ResultMessage<IPage<Spreadlncome>> getSpreadIncomeAll(FinanceAdminVO financeAdminVO, PageVO page) {
		QueryWrapper query = new QueryWrapper<>();
//		AuthUser currentUser = UserContext.getCurrentUser();
//		// 如果当前会员不为空，且为代理商角色
//		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
//			// 通过用户名获取会员id
//			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
//			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
//			Partner partner = partnerService.getOne(lambdaQueryWrapper);
//			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
//				financeAdminVO.setAdcode(partner.getRegionId());
//				List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
//				query.in(list.size() > 0, "location", list);
//			}
//		}
		if (financeAdminVO.getSearchTime() != null) {
			Date d = DateUtil.getN_MONTH_Time(financeAdminVO.getSearchTime(), 1);
			Date d2 = DateUtil.getN_MONTH_Time(financeAdminVO.getSearchTime(), 2);
			query.between("create_time", DateUtil.toString(d), DateUtil.toString(d2));

		}
		query.eq(StringUtils.isNotEmpty(financeAdminVO.getMobile()), "mobile", financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			query.in("location", list);
		}
		query.orderByDesc("create_time");
		IPage<Spreadlncome> SpreadIncomeVOPage = spreadlncomeService.page(PageUtil.initPage(page), query);
		return ResultUtil.data(SpreadIncomeVOPage);
	}

	@ApiOperation(value = "价差收益合计")
	@RequestMapping(value = "/getSpreadIncomeAll2")
	public ResultMessage<Spreadlncome> getSpreadIncomeAll2(FinanceAdminVO financeAdminVO, PageVO page) {
		QueryWrapper query = new QueryWrapper<>();
		page.setPageSize(999999999);
//		AuthUser currentUser = UserContext.getCurrentUser();
//		// 如果当前会员不为空，且为代理商角色
//		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
//			// 通过用户名获取会员id
//			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
//			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
//			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
//			Partner partner = partnerService.getOne(lambdaQueryWrapper);
//			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
//				financeAdminVO.setAdcode(partner.getRegionId());
//				List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
//				query.in(list.size() > 0, "location", list);
//			}
//		}
		if (financeAdminVO.getSearchTime() != null) {
			Date d = DateUtil.getN_MONTH_Time(financeAdminVO.getSearchTime(), 1);
			Date d2 = DateUtil.getN_MONTH_Time(financeAdminVO.getSearchTime(), 2);
			query.between("create_time", DateUtil.toString(d), DateUtil.toString(d2));
		}
		query.eq(StringUtils.isNotEmpty(financeAdminVO.getMobile()), "mobile", financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			query.in("location", list);
		}
		query.orderByDesc("create_time");
		IPage<Spreadlncome> SpreadIncomeVOPage = spreadlncomeService.page(PageUtil.initPage(page), query);
		Spreadlncome spreadlncome = new Spreadlncome();
		List<Spreadlncome> l = SpreadIncomeVOPage.getRecords();
		for (Spreadlncome s : l) {
			spreadlncome.setCzHb(
					CurrencyUtil.add(s.getCzHb(), spreadlncome.getCzHb() == null ? 0 : spreadlncome.getCzHb()));
			spreadlncome.setCzXj(
					CurrencyUtil.add(s.getCzXj(), spreadlncome.getCzXj() == null ? 0 : spreadlncome.getCzXj()));
			spreadlncome.setXsHb(
					CurrencyUtil.add(s.getXsHb(), spreadlncome.getXsHb() == null ? 0 : spreadlncome.getXsHb()));
			spreadlncome.setXsXj(
					CurrencyUtil.add(s.getXsXj(), spreadlncome.getXsXj() == null ? 0 : spreadlncome.getXsXj()));
			spreadlncome.setAllocateAllMoney(
				CurrencyUtil.add(s.getAllocateAllMoney(), spreadlncome.getAllocateAllMoney() == null ? 0 : spreadlncome.getAllocateAllMoney()));
			spreadlncome.setAllocateAdminMoney(
				CurrencyUtil.add(s.getAllocateAdminMoney(), spreadlncome.getAllocateAdminMoney() == null ? 0 : spreadlncome.getAllocateAdminMoney()));
			spreadlncome.setAllocateMemberMoney(
				CurrencyUtil.add(s.getAllocateMemberMoney(), spreadlncome.getAllocateMemberMoney() == null ? 0 : spreadlncome.getAllocateMemberMoney()));
		}
		spreadlncome.setCzHb(spreadlncome.getCzHb() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getCzHb(), 2));
		spreadlncome.setCzXj(spreadlncome.getCzXj() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getCzXj(), 2));
		spreadlncome.setXsHb(spreadlncome.getXsHb() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getXsHb(), 2));
		spreadlncome.setXsXj(spreadlncome.getXsXj() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getXsXj(), 2));
		spreadlncome.setAllocateAllMoney(spreadlncome.getAllocateAllMoney() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getAllocateAllMoney(), 2));
		spreadlncome.setAllocateAdminMoney(spreadlncome.getAllocateAdminMoney() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getAllocateAdminMoney(), 2));
		spreadlncome.setAllocateMemberMoney(spreadlncome.getAllocateMemberMoney() == null ? 0 : CurrencyUtil.roundDown(spreadlncome.getAllocateMemberMoney(), 2));

		spreadlncome.setCzZk(CurrencyUtil.roundDown(CurrencyUtil.div(spreadlncome.getCzXj(),spreadlncome.getCzHb()), 2));
		spreadlncome.setXsZk(CurrencyUtil.roundDown(CurrencyUtil.div(spreadlncome.getXsXj(),spreadlncome.getXsHb()), 2));
//		Double allocateAllMoney = CurrencyUtil.sub(spreadlncome.getCzXj(), spreadlncome.getXsXj());

//		Double allocateAllMoney =CurrencyUtil.roundDown(CurrencyUtil.mul(spreadlncome.getXsXj(),
//			CurrencyUtil.sub(spreadlncome.getCzZk(),spreadlncome.getXsZk())), 2);
//		spreadlncome.setAllocateAllMoney(allocateAllMoney);
		Double allocateMemberDiscount = CurrencyUtil.roundDown(Double
				.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_QY.dictCode())), 2);
//		Double allocateMemberMoney = CurrencyUtil.roundDown(CurrencyUtil.mul(allocateAllMoney, allocateMemberDiscount), 2);
		Double allocateAdminDiscount = CurrencyUtil.roundDown(Double
				.parseDouble(SysDictUtils.getValueString(DictCodeEnum.JIACHASHOUYI_PT.dictCode())), 2);
//		Double allocateAdminMoney = CurrencyUtil.roundDown(CurrencyUtil.mul(allocateAllMoney, allocateAdminDiscount), 2);
		spreadlncome.setAllocateMemberDiscount(allocateMemberDiscount);
//		spreadlncome.setAllocateMemberMoney(allocateMemberMoney);
		spreadlncome.setAllocateAdminDiscount(allocateAdminDiscount);
//		spreadlncome.setAllocateAdminMoney(allocateAdminMoney);
		return ResultUtil.data(spreadlncome);
	}

	@ApiOperation(value = "沉淀价差收益")
	@RequestMapping(value = "/setSpreadIncomeAll")
	@Scheduled(cron = "0 0 4 1 * ?")
	public void setSpreadIncomeAll() {
		FinanceAdminVO financeAdminVO = new FinanceAdminVO();
		PageVO page = new PageVO();
		page.setPageSize(999999999);
		financeAdminVO.setSearchTime(new Date());
		// 获取城市合伙人
		MemberSearchVO memberSearchVO = new MemberSearchVO();
		memberSearchVO.setRoleType("AGENT");
		memberSearchVO.setAuditStates("PASS");
		IPage<MemberVO> memberVOIPage = memberService.getPartnerList(memberSearchVO, page);
		List<MemberVO> records = memberVOIPage.getRecords();
		for (int i = 0; i < records.size(); i++) {
			MemberVO memberVO = records.get(i);
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				cal.setTime(DateUtil.getN_MONTH_Time(financeAdminVO.getSearchTime(), -1));
				cal.set(Calendar.DAY_OF_MONTH, 1);
				memberVO.setBeginTime(simpleDateFormat.parse(simpleDateFormat.format(cal.getTime())));
				cal.roll(Calendar.DAY_OF_MONTH, -1);
				memberVO.setEndTime(simpleDateFormat.parse(simpleDateFormat.format(cal.getTime())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			SpreadIncomeVO spreadIncomeVO = new SpreadIncomeVO();
			QueryWrapper queryWrapper = new QueryWrapper<>();
			if (StringUtils.isNotEmpty(memberVO.getPartnerRegionId())) {
//				List list = regionService.getItemAdCod(memberVO.getPartnerRegionId());
				List list = regionService.getPartnerAdCode(memberVO.getPartnerRegionId(), "4");
				queryWrapper.in(list != null && list.size() > 0, "a.location", list);
				// 全部下级充值钱包
				// 全部下级销售和推广钱包
				// 总收益差值（元）
				// 区域所得
				// 平台所得
				// 获取 区域下的 充值折率和销售推广折率 区域所得 和 平台所得
				spreadIncomeVO = walletLogDetailService.getSpreadIncomeAll(queryWrapper,
						financeAdminVO.getSearchTime());
				BeanUtil.copyProperties(memberVO, spreadIncomeVO);
				Spreadlncome spreadlncome = new Spreadlncome();
				BeanUtil.copyProperties(spreadIncomeVO, spreadlncome);
				spreadlncome.setMemberId(spreadlncome.getId());
				spreadlncome.setId(UuidUtils.getUUID());
				spreadlncomeService.save(spreadlncome);

			}
		}
	}

	@ApiOperation(value = "分佣流向明细-会员费")
	@RequestMapping(value = "/GetCommissionDistributionMember")
	public ResultMessage<IPage<CommissionDistributionSale>> GetCommissionDistributionMember(
			FinanceAdminVO financeAdminVO, PageVO page) {
		QueryWrapper queryWrapper = new QueryWrapper();

		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}

		queryWrapper.eq(StringUtils.isNotEmpty(financeAdminVO.getMobile()), "a.mobile", financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapper.in(list.size() > 0, "location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapper.in(list.size() > 0, "location", list);
		}
		if (financeAdminVO.getSearchTime() != null) {
			financeAdminVO.setStartTime(financeAdminVO.getSearchTime());
			// 获取下个月一号
			Calendar c = Calendar.getInstance();
			// 过去一月
			c.setTime(financeAdminVO.getSearchTime());
			c.add(Calendar.MONTH, 1);
			financeAdminVO.setEndTime(c.getTime());
			queryWrapper.between("a.createTime", financeAdminVO.getStartTime(), financeAdminVO.getEndTime());
		}
		queryWrapper.orderByDesc("a.createTime");
		queryWrapper.groupBy("a.sn");
		IPage<CommissionDistributionSale> data = regionService.GetCommissionDistributionMember(queryWrapper, page);
		if (CollectionUtils.isNotEmpty(data.getRecords())) {
			for (CommissionDistributionSale commissionDistributionSale : data.getRecords()) {
				if (StringUtils.isNotEmpty(commissionDistributionSale.getLocation())) {
					commissionDistributionSale.setLocationName(regionService
							.getItemAdCodOrName(StringUtils.toString(commissionDistributionSale.getLocation())));
				}
			}
		}
		return ResultUtil.data(data);

	}

	@ApiOperation(value = "分佣流向明细-会员服务费合计")
	@RequestMapping(value = "/GetCommissionDistributionMemberAll")
	public ResultMessage<CommissionDistributionSale> GetCommissionDistributionMemberAll(FinanceAdminVO financeAdminVO,
			PageVO page) {
		QueryWrapper queryWrapper = new QueryWrapper();
		page.setPageSize(999999999);
		page.setPageNumber(1);
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}

		queryWrapper.eq(StringUtils.isNotEmpty(financeAdminVO.getMobile()), "a.mobile", financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapper.in(list.size() > 0, "location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapper.in(list.size() > 0, "location", list);
		}
		if (financeAdminVO.getSearchTime() != null) {
			financeAdminVO.setStartTime(financeAdminVO.getSearchTime());
			// 获取下个月一号
			Calendar c = Calendar.getInstance();
			// 过去一月
			c.setTime(financeAdminVO.getSearchTime());
			c.add(Calendar.MONTH, 1);
			financeAdminVO.setEndTime(c.getTime());
			queryWrapper.between("a.createTime", financeAdminVO.getStartTime(), financeAdminVO.getEndTime());
		}
		queryWrapper.orderByDesc("a.createTime");
		queryWrapper.groupBy("a.sn");
		IPage<CommissionDistributionSale> data = regionService.GetCommissionDistributionMember(queryWrapper, page);
		List<CommissionDistributionSale> list = data.getRecords();
		CommissionDistributionSale sale = new CommissionDistributionSale();
		sale.setTsMoney(0);
		sale.setQyMoney(0);
		sale.setSyMoney(0);
		sale.setAdminMoney(0);
		for (CommissionDistributionSale s : list) {
			sale.setTsMoney(CurrencyUtil.add(s.getTsMoney(), sale.getTsMoney()));
			sale.setQyMoney(CurrencyUtil.add(s.getQyMoney(), sale.getQyMoney()));
			sale.setSyMoney(CurrencyUtil.add(s.getSyMoney(), sale.getSyMoney()));
			sale.setAdminMoney(CurrencyUtil.add(s.getAdminMoney(), sale.getAdminMoney()));
		}
		return ResultUtil.data(sale);
	}

	@ApiOperation(value = "分佣流向明细-销售服务费")
	@RequestMapping(value = "/GetCommissionDistributionSale")
	public ResultMessage<IPage<CommissionDistributionSale>> GetCommissionDistributionSale(FinanceAdminVO financeAdminVO,
			PageVO page) {
		QueryWrapper queryWrapper = new QueryWrapper();

		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}
		queryWrapper.eq(StringUtils.isNotEmpty(financeAdminVO.getMobile()), "a.mobile", financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapper.in(list.size() > 0, "location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapper.in(list.size() > 0, "location", list);
		}
		if (financeAdminVO.getSearchTime() != null) {
			financeAdminVO.setStartTime(financeAdminVO.getSearchTime());
			// 获取下个月一号
			Calendar c = Calendar.getInstance();
			// 过去一月
			c.setTime(financeAdminVO.getSearchTime());
			c.add(Calendar.MONTH, 1);
			financeAdminVO.setEndTime(c.getTime());
			queryWrapper.between("a.createTime", financeAdminVO.getStartTime(), financeAdminVO.getEndTime());
		}

		queryWrapper.orderByDesc("a.createTime");
		queryWrapper.groupBy("a.sn");
		IPage<CommissionDistributionSale> data = regionService.GetCommissionDistributionSale(queryWrapper, page);
		if (CollectionUtils.isNotEmpty(data.getRecords())) {
			for (CommissionDistributionSale commissionDistributionSale : data.getRecords()) {
				if (StringUtils.isNotEmpty(commissionDistributionSale.getLocation())) {
					commissionDistributionSale.setLocationName(regionService
							.getItemAdCodOrName(StringUtils.toString(commissionDistributionSale.getLocation())));
				}
			}
		}
		return ResultUtil.data(data);
	}

	@ApiOperation(value = "分佣流向明细-销售服务费合计")
	@RequestMapping(value = "/GetCommissionDistributionSaleAll")
	public ResultMessage<CommissionDistributionSale> GetCommissionDistributionSaleAll(FinanceAdminVO financeAdminVO,
			PageVO page) {
		QueryWrapper queryWrapper = new QueryWrapper();
		page.setPageSize(999999999);
		page.setPageNumber(1);
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (StringUtils.isNotEmpty(currentUser.getMemberId()))) {
			// AdminUser adminUser = adminUserService.getById(currentUser.getId());
			// 通过用户名获取会员id
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				// memberSearchVO.setExtension(adminUser.getPromotionCode());
				financeAdminVO.setAdcode(partner.getRegionId());
			}
		}
		queryWrapper.eq(StringUtils.isNotEmpty(financeAdminVO.getMobile()), "a.mobile", financeAdminVO.getMobile());
		String regionId = financeAdminVO.getRegionId();
		if (StringUtils.isNotEmpty(regionId)) {
			List list = regionService.getItemAdCod(regionId);
			queryWrapper.in(list.size() > 0, "location", list);
		}
		// 根据code查询合伙人管理的区域Code
		if (StringUtils.isNotEmpty(financeAdminVO.getAdcode())) {
			List list = regionService.getPartnerAdCode(financeAdminVO.getAdcode(), "4");
			queryWrapper.in(list.size() > 0, "location", list);
		}
		if (financeAdminVO.getSearchTime() != null) {
			financeAdminVO.setStartTime(financeAdminVO.getSearchTime());
			// 获取下个月一号
			Calendar c = Calendar.getInstance();
			// 过去一月
			c.setTime(financeAdminVO.getSearchTime());
			c.add(Calendar.MONTH, 1);
			financeAdminVO.setEndTime(c.getTime());
			queryWrapper.between("a.createTime", financeAdminVO.getStartTime(), financeAdminVO.getEndTime());
		}

		queryWrapper.orderByDesc("a.createTime");
		queryWrapper.groupBy("a.sn");
		IPage<CommissionDistributionSale> data = regionService.GetCommissionDistributionSale(queryWrapper, page);
		List<CommissionDistributionSale> list = data.getRecords();
		CommissionDistributionSale sale = new CommissionDistributionSale();
		sale.setCommission(0);
		sale.setFlowPrice(0);
		sale.setTsMoney(0);
		sale.setQyMoney(0);
		sale.setSyMoney(0);
		sale.setAdminMoney(0);
		for (CommissionDistributionSale s : list) {
			sale.setTsMoney(CurrencyUtil.add(s.getTsMoney(), sale.getTsMoney()));
			sale.setQyMoney(CurrencyUtil.add(s.getQyMoney(), sale.getQyMoney()));
			sale.setSyMoney(CurrencyUtil.add(s.getSyMoney(), sale.getSyMoney()));
			sale.setAdminMoney(CurrencyUtil.add(s.getAdminMoney(), sale.getAdminMoney()));
			sale.setCommission(CurrencyUtil.add(s.getCommission(), sale.getCommission()));
			sale.setFlowPrice(CurrencyUtil.add(s.getFlowPrice(), sale.getFlowPrice()));
		}
		return ResultUtil.data(sale);
	}

}