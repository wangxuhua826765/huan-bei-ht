package cn.lili.controller.member;

import cn.hutool.core.lang.Assert;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.Extension;
import cn.lili.modules.member.entity.vo.ExtensionVO;
import cn.lili.modules.member.service.EextensionService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

/**
 * 等级管理
 */
@RestController
@Api(tags = "管理端,推广管理")
@RequestMapping("/manager/Extension")
public class ExtensionController {
	@Autowired
	private EextensionService eextensionService;

	@ApiOperation(value = "推广管理列表")
	@GetMapping
	public ResultMessage<IPage<ExtensionVO>> getByPage(ExtensionVO extension, PageVO page) {
		return ResultUtil.data(eextensionService.getExtensionPage(extension, page));
	}

	@ApiOperation(value = "编辑推广信息")
	@PostMapping
	public ResultMessage<Extension> updateExtension(@Valid Extension extension) {
		if (StringUtils.isNotBlank(extension.getId())) {
			extension.setUpdateTime(new Date());
			return ResultUtil.data(eextensionService.updateExtension(extension));
		}
		extension.setCreateTime(new Date());
		return ResultUtil.data(eextensionService.addExtension(extension));
	}

	@ApiOperation(value = "通过ID获取推广信息")
	@ApiImplicitParam(name = "id", value = "推广Id", required = true, dataType = "String", paramType = "path")
	@RequestMapping(value = "/{id}")
	public ResultMessage<Extension> get(@PathVariable String id) {
		return ResultUtil.data(eextensionService.getById(id));
	}

	@ApiOperation(value = "生成推广码")
	@RequestMapping("getExtension")
	public ResultMessage<String> queryBusinessOrder() {
		return ResultUtil.data(eextensionService.getExtension());
	}

	@ApiOperation(value = "删除推广")
	@ApiImplicitParam(name = "id", value = "会员地址ID", dataType = "String", paramType = "path")
	@DeleteMapping(value = "/delById/{id}")
	public ResultMessage<Object> delShippingAddressById(@PathVariable String id) {
		eextensionService.removeExtension(id);
		return ResultUtil.success();
	}

	// @ApiOperation(value = "编辑或添加会员等级")
	// @PostMapping
	// public ResultMessage<Extension> save(@Valid Extension extension) {
	// if (StringUtils.isNotBlank(extension.getId())) {
	// extension.setUpdateTime(new Date());
	// return ResultUtil.data(gradeService.updateGrade(extension));
	// }
	// extension.setCreateTime(new Date());
	// return ResultUtil.data(gradeService.addGrade(extension));
	// }
	//
	// @ApiOperation(value = "通过ID获取会员等级信息")
	// @ApiImplicitParam(name = "id", value = "会员ID", required = true, dataType =
	// "String", paramType = "path")
	// @RequestMapping(value = "/{id}")
	// public ResultMessage<Extension> get(@PathVariable String id) {
	//
	// return ResultUtil.data(gradeService.getById(id));
	// }
	//
	// @ApiOperation(value = "删除会员等级")
	// @ApiImplicitParam(name = "id", value = "会员地址ID", dataType = "String",
	// paramType = "path")
	// @DeleteMapping(value = "/delById/{id}")
	// public ResultMessage<Object> delShippingAddressById(@PathVariable String id)
	// {
	// gradeService.removeGrade(id);
	// return ResultUtil.success();
	// }
}
