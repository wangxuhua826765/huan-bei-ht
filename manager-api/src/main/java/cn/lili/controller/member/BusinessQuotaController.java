package cn.lili.controller.member;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.BusinessQuota;
import cn.lili.modules.member.service.BusinessQuotaService;
import cn.lili.modules.order.order.entity.dos.Order;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 等级管理
 */
@RestController
@Api(tags = "管理端,风控管理")
@RequestMapping("/manager/Business")
public class BusinessQuotaController {
	@Autowired
	private BusinessQuotaService businessQuotaService;

	@ApiOperation(value = "风控管理")
	@GetMapping
	public ResultMessage<IPage<BusinessQuota>> getByPage(BusinessQuota businessQuota, PageVO page) {
		return ResultUtil.data(businessQuotaService.getBusinessQuotaPage(businessQuota, page));
	}

	@ApiOperation(value = "获取风控交易记录详情")
	@RequestMapping("queryBusinessOrder")
	public ResultMessage<IPage<Order>> queryBusinessOrder(@RequestParam("memberId") String memberId,
			@RequestParam("storeId") String storeId, @RequestParam("testingStartTime") String testingStartTime,
			@RequestParam("testingEndTime") String testingEndTime, PageVO page) {
		return ResultUtil.data(
				businessQuotaService.queryBusinessOrder(memberId, storeId, testingStartTime, testingEndTime, page));
	}

	@ApiOperation(value = "是否限额")
	@RequestMapping("limitOrNot")
	public ResultMessage<BusinessQuota> limitOrNot(BusinessQuota businessQuota) {
		return ResultUtil.data(businessQuotaService.limitOrNot(businessQuota));
	}
}
