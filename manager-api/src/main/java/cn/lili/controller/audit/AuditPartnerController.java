package cn.lili.controller.audit;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.audit.entity.dos.AuditPartner;
import cn.lili.modules.audit.entity.dto.AuditDto;
import cn.lili.modules.audit.entity.dto.AuditPartnerDto;
import cn.lili.modules.audit.entity.dto.LowListDto;
import cn.lili.modules.audit.entity.vo.AuditPartnerVo;
import cn.lili.modules.audit.service.AuditPartnerService;
import cn.lili.modules.goods.entity.dos.Brand;
import cn.lili.modules.goods.entity.dto.BrandPageDTO;
import cn.lili.modules.system.aspect.annotation.SystemLogPoint;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "管理端,撤销合伙人审核")
@RequestMapping("/manager/revoke/partner")
public class AuditPartnerController {

	@Autowired
	private AuditPartnerService auditPartnerService;

	@ApiOperation(value = "分页获取")
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<AuditPartnerVo>> getByPage(AuditPartnerDto partnerDto) {
		return ResultUtil.data(auditPartnerService.getByPage(partnerDto));
	}

	@ApiOperation(value = "审核")
	@RequestMapping(value = "/audit")
	@SystemLogPoint(description = "审核注销合伙人", customerLog = "'审核注销合伙人请求", type = "2")
	public ResultMessage<Boolean> audit(AuditDto auditDto) {
		return ResultUtil.data(auditPartnerService.audit(auditDto));
	}

	@ApiOperation(value = "查看下级名单")
	@RequestMapping(value = "/getLowList")
	public ResultMessage<IPage<LowListDto>> getLowList(@RequestParam("memberId") String memberId) {
		return ResultUtil.data(auditPartnerService.getLowList(memberId));
	}

	@ApiOperation(value = "退回金额")
	@RequestMapping(value = "/getBackMsg")
	@SystemLogPoint(description = "注销合伙人退回金额", customerLog = "'注销合伙人退回金额请求", type = "2")
	public ResultMessage<Boolean> getBackMsg(AuditDto auditDto) {
		return ResultUtil.data(auditPartnerService.getBackMsg(auditDto));
	}
}
