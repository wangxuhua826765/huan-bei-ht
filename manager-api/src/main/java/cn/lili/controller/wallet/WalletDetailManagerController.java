package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.validation.EnumValue;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.order.order.entity.enums.PayStatusEnum;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.transfer.entity.vo.TransferVO;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyQueryVO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyVO;
import cn.lili.modules.wallet.entity.vo.WalletDetailVO;
import cn.lili.modules.wallet.service.WalletDetailService;
import cn.lili.mybatis.util.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by yudan on 2022/7/26
 */
@RestController
@Api(tags = "管理端,钱包明细接口")
@RequestMapping("/manager/wallet/detail")
public class WalletDetailManagerController {

	@Autowired
	private RegionService regionService;
	@Autowired
	private WalletDetailService walletDetailService;

	@ApiOperation(value = "分页获取提现记录-收入")
	@RequestMapping("/in")
	public ResultMessage<IPage<WalletDetailVO>> getInByPage(PageVO page, WalletDetailVO walletDetailVO) {
		QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
		List listS = new ArrayList();
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY_REFUND.name());
		queryWrapper.in("d.transaction_type", listS);
		queryWrapper.eq(StringUtils.isNotEmpty(walletDetailVO.getTransferType()), "d.transaction_type",
				walletDetailVO.getTransferType());
		queryWrapper.likeLeft(StringUtils.isNotEmpty(walletDetailVO.getPayerOwner()), "d.payer_owner",
				walletDetailVO.getPayerOwner());
		queryWrapper.eq("d.payee", "admin");
		// 时间查询
		if (StringUtils.isNotEmpty(walletDetailVO.getStartTime())
				&& StringUtils.isNotEmpty(walletDetailVO.getEndTime())) {
			queryWrapper.le("DATE_FORMAT(d.create_time,'%Y-%m-%d')", walletDetailVO.getEndTime());
			queryWrapper.ge("DATE_FORMAT(d.create_time,'%Y-%m-%d')", walletDetailVO.getStartTime());
		}
		// 区域
		if (StringUtils.isNotEmpty(walletDetailVO.getRegionId())) {
			List list = regionService.getItemAdCod(walletDetailVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "m.location", list);
		}
		queryWrapper.isNotNull("m.id");
		queryWrapper.orderByDesc("d.create_time");
		// 过滤端口费
		queryWrapper.and(Wrapper -> Wrapper.isNotNull("o.id").or().isNotNull("lr.id").or().isNotNull("t.id"));
		// 构建查询 返回数据
		IPage<WalletDetailVO> walletDetailVOIPage = walletDetailService.getListAdminIn(page, queryWrapper);
		if (CollectionUtils.isNotEmpty(walletDetailVOIPage.getRecords())) {
			for (WalletDetailVO walletDetail : walletDetailVOIPage.getRecords()) {
				if (StringUtils.isNotEmpty(walletDetail.getRegionId())) {
					walletDetail.setRegion(
							regionService.getItemAdCodOrName(StringUtils.toString(walletDetail.getRegionId())));
				}
				walletDetail.setMoney(
						BigDecimal.valueOf(walletDetail.getMoney()).setScale(2, RoundingMode.DOWN).doubleValue());
			}
		}
		return ResultUtil.data(walletDetailVOIPage);
	}

	@ApiOperation(value = "平台分佣总额-收入")
	@RequestMapping("/allMoneyAdminIn")
	public ResultMessage<Map> rechargeMoneyAdmin() {
		Map<String, Object> map = new HashMap<>();
		QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
		List listS = new ArrayList();
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY_REFUND.name());
		queryWrapper.in("d.transaction_type", listS);
		queryWrapper.eq("d.payee", "admin");
		queryWrapper.isNotNull("m.id");
		// 过滤端口费
		queryWrapper.and(Wrapper -> Wrapper.isNotNull("o.id").or().isNotNull("lr.id").or().isNotNull("t.id"));

		Double allMoneyAdmin = 0D;
		List<WalletDetailVO> list = walletDetailService.getListAdminInList(queryWrapper);
		if (CollectionUtils.isNotEmpty(list)) {
			for (WalletDetailVO walletDetailVO : list) {
				allMoneyAdmin = CurrencyUtil.add(allMoneyAdmin,
						walletDetailVO.getMoney() != null ? walletDetailVO.getMoney() : 0D);
			}
		}
		map.put("allMoneyAdmin", BigDecimal.valueOf(allMoneyAdmin).setScale(2, RoundingMode.DOWN));
		return ResultUtil.data(map);
	}

	@ApiOperation(value = "分页获取提现记录-支出")
	@RequestMapping("/out")
	public ResultMessage<IPage<WalletDetailVO>> getByPage(PageVO page, WalletDetailVO walletDetailVO) {
		QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
		List listS = new ArrayList();
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY_REFUND.name());
		queryWrapper.in("d.transaction_type", listS);
		queryWrapper.eq(StringUtils.isNotEmpty(walletDetailVO.getTransferType()), "d.transaction_type",
				walletDetailVO.getTransferType());
		queryWrapper.likeLeft(StringUtils.isNotEmpty(walletDetailVO.getPayeeOwner()), "d.payee_owner",
				walletDetailVO.getPayeeOwner());
		queryWrapper.eq("d.payer", "admin");
		// 时间查询
		if (StringUtils.isNotEmpty(walletDetailVO.getStartTime())
				&& StringUtils.isNotEmpty(walletDetailVO.getEndTime())) {
			queryWrapper.le("DATE_FORMAT(d.create_time,'%Y-%m-%d')", walletDetailVO.getEndTime());
			queryWrapper.ge("DATE_FORMAT(d.create_time,'%Y-%m-%d')", walletDetailVO.getStartTime());
		}
		// 区域
		if (StringUtils.isNotEmpty(walletDetailVO.getRegionId())) {
			List list = regionService.getItemAdCod(walletDetailVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "m.location", list);
		}
		queryWrapper.isNotNull("m.id");
		queryWrapper.orderByDesc("d.create_time");
		// 构建查询 返回数据
		IPage<WalletDetailVO> walletDetailVOIPage = walletDetailService.getListAdminOut(page, queryWrapper);
		if (CollectionUtils.isNotEmpty(walletDetailVOIPage.getRecords())) {
			for (WalletDetailVO walletDetail : walletDetailVOIPage.getRecords()) {
				if (StringUtils.isNotEmpty(walletDetail.getRegionId())) {
					walletDetail.setRegion(
							regionService.getItemAdCodOrName(StringUtils.toString(walletDetail.getRegionId())));
				}
				walletDetail.setMoney(
						BigDecimal.valueOf(walletDetail.getMoney()).setScale(2, RoundingMode.DOWN).doubleValue());
			}
		}
		return ResultUtil.data(walletDetailVOIPage);
	}

	@ApiOperation(value = "平台分佣总额-支出")
	@RequestMapping("/allMoneyAdminOut")
	public ResultMessage<Map> rechargeMoneyAdminOut(String type) {
		Map<String, Object> map = new HashMap<>();
		QueryWrapper<WalletDetailVO> queryWrapper = new QueryWrapper<>();
		List listS = new ArrayList();
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_HY_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_FW_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_TS_REFUND.name());
		listS.add(DepositServiceTypeEnum.WALLET_COMMISSION_SY_REFUND.name());
		queryWrapper.in("d.transaction_type", listS);
		queryWrapper.eq("d.payer", "admin");
		queryWrapper.isNotNull("m.id");
		Double allMoneyAdmin = 0D;
		List<WalletDetailVO> list = walletDetailService.getListAdminOutList(queryWrapper);
		if (CollectionUtils.isNotEmpty(list)) {
			for (WalletDetailVO walletDetailVO : list) {
				allMoneyAdmin = CurrencyUtil.add(allMoneyAdmin,
						walletDetailVO.getMoney() != null ? walletDetailVO.getMoney() : 0D);
			}
		}
		map.put("allMoneyAdmin", BigDecimal.valueOf(allMoneyAdmin).setScale(2, RoundingMode.DOWN));
		return ResultUtil.data(map);
	}
}
