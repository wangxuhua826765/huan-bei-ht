package cn.lili.controller.wallet;

import cn.hutool.core.text.CharSequenceUtil;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.AuthUser;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.security.enums.UserEnums;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.utils.SnowFlake;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.controller.pay.pufa.PuFaPayController;
import cn.lili.modules.bank.entity.dos.DockingBankVO;
import cn.lili.modules.bank.entity.dos.MemberBank;
import cn.lili.modules.bank.service.MemberBankService;
import cn.lili.modules.member.entity.dos.Partner;
import cn.lili.modules.member.service.PartnerService;
import cn.lili.modules.order.order.entity.vo.OrderItemVO;
import cn.lili.modules.permission.entity.dos.AdminUser;
import cn.lili.modules.permission.service.AdminUserService;
import cn.lili.modules.system.service.RegionService;
import cn.lili.modules.wallet.entity.dos.MemberWallet;
import cn.lili.modules.wallet.entity.dos.MemberWithdrawApply;
import cn.lili.modules.wallet.entity.dos.WalletDetail;
import cn.lili.modules.wallet.entity.enums.DepositServiceTypeEnum;
import cn.lili.modules.wallet.entity.enums.WalletOwnerEnum;
import cn.lili.modules.wallet.entity.enums.WithdrawStatusEnum;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyQueryVO;
import cn.lili.modules.wallet.entity.vo.MemberWithdrawApplyVO;
import cn.lili.modules.wallet.service.MemberWalletService;
import cn.lili.modules.wallet.service.MemberWithdrawApplyService;
import cn.lili.modules.wallet.service.MemberWithdrawItemService;
import cn.lili.modules.wallet.service.WalletDetailService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 管理端,余额提现记录接口
 *
 * @author pikachu
 * @since 2020/11/16 10:07 下午
 */
@RestController
@Api(tags = "管理端,余额提现记录接口")
@RequestMapping("/manager/members/withdraw-apply")
@Transactional(rollbackFor = Exception.class)
public class MemberWithdrawApplyManagerController {
	@Autowired
	private MemberWithdrawApplyService memberWithdrawApplyService;

	@Autowired
	private MemberWalletService memberWalletService;

	@Autowired
	private WalletDetailService walletDetailService;

	@Autowired
	private MemberWithdrawItemService memberWithdrawItemService;

	@Autowired
	private RegionService regionService;
	@Autowired
	private MemberBankService memberBankService;

	@Autowired
	private PartnerService partnerService;
	@Autowired
	private AdminUserService adminUserService;

	@ApiOperation(value = "分页获取提现记录")
	@GetMapping
	public ResultMessage<IPage<MemberWithdrawApplyVO>> getByPage(PageVO page,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO) {
		// 构建查询 返回数据
		IPage<MemberWithdrawApplyVO> memberWithdrawApplyPage = memberWithdrawApplyService.getMemberWithdrawPage(page,
				memberWithdrawApplyQueryVO);
		return ResultUtil.data(memberWithdrawApplyPage);
	}

	@ApiOperation(value = "分页获取提现记录")
	@RequestMapping("/getList")
	public ResultMessage<IPage<MemberWithdrawApplyVO>> getList(PageVO page,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO) {
		// 构建查询 返回数据
		IPage<MemberWithdrawApplyVO> memberWithdrawApplyPage = memberWithdrawApplyService.getList(page,
				memberWithdrawApplyQueryVO);
		return ResultUtil.data(memberWithdrawApplyPage);
	}

	@ApiOperation(value = "提现明细-销售钱包")
	@RequestMapping("/getSaleList")
	public ResultMessage<Map<String, Object>> getSaleList(PageVO page,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO) {
		Map<String, Object> map = new HashMap<>();
		Double all = 0D;
		// 构建查询 返回数据
		QueryWrapper<MemberWithdrawApply> queryWrapper = new QueryWrapper<>();
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				queryWrapper.in("s.ad_code", regionService.getPartnerAdCode(partner.getRegionId(), "4"));
			}
		}
		// 手机号
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getMobile()), "member.mobile",
				memberWithdrawApplyQueryVO.getMobile());
		// 店铺名称
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getStoreName()), "s.store_name",
				memberWithdrawApplyQueryVO.getStoreName());
		queryWrapper.eq("s.delete_flag",0);
		// 提现状态
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getApplyStatus()), "apply.apply_status",
				memberWithdrawApplyQueryVO.getApplyStatus());
		// 创建时间
		if (!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getCreateDate())) {
			queryWrapper.eq("DATE_FORMAT(apply.create_time,'%Y-%m')", memberWithdrawApplyQueryVO.getCreateDate());
		}
		// 区域
		if (StringUtils.isNotEmpty(memberWithdrawApplyQueryVO.getRegionId())) {
			List list = regionService.getItemAdCod(memberWithdrawApplyQueryVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "member.location", list);
		}
		// 订单来源
		queryWrapper.likeLeft("apply.owner", WalletOwnerEnum.SALE.name());
		queryWrapper.eq("apply.delete_flag", 0);
//		queryWrapper.isNotNull("member.mobile");
		queryWrapper.orderByDesc("apply.create_time");

		IPage<MemberWithdrawApplyVO> memberWithdrawApplyPage = memberWithdrawApplyService.getListPage(page,
				queryWrapper);
		List<MemberWithdrawApplyVO> memberWithdrawApplyList = memberWithdrawApplyService.getListAll(queryWrapper);
		if (CollectionUtils.isNotEmpty(memberWithdrawApplyList)) {
			for (MemberWithdrawApplyVO memberWithdrawApplyVO : memberWithdrawApplyList) {
				if (memberWithdrawApplyVO.getApplyStatus().equals("FINISH"))
					all = CurrencyUtil.add(all, memberWithdrawApplyVO.getApplyMoney());
			}
		}
		map.put("all", BigDecimal.valueOf(all).setScale(2, RoundingMode.DOWN).doubleValue());
		map.put("data", memberWithdrawApplyPage);
		return ResultUtil.data(map);
	}

	@ApiOperation(value = "提现明细-销售钱包")
	@RequestMapping("/getPromoteList")
	public ResultMessage<Map<String, Object>> getPromoteList(PageVO page,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO) {
		Map<String, Object> map = new HashMap<>();
		Double all = 0D;
		// 构建查询 返回数据
		QueryWrapper<MemberWithdrawApply> queryWrapper = new QueryWrapper<>();
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			// 查询合伙人区域
			LambdaQueryWrapper<Partner> lambdaQueryWrapper = new LambdaQueryWrapper<>();
			lambdaQueryWrapper.eq(Partner::getMemberId, currentUser.getMemberId());
			lambdaQueryWrapper.eq(Partner::getDeleteFlag, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerState, 0);
			lambdaQueryWrapper.eq(Partner::getPartnerType, 4);
			Partner partner = partnerService.getOne(lambdaQueryWrapper);
			if (null != partner && StringUtils.isNotEmpty(partner.getRegionId())) {
				queryWrapper.in("member.location", regionService.getPartnerAdCode(partner.getRegionId(), "4"));
			}
		}
		// 手机号
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getMobile()), "member.mobile",
				memberWithdrawApplyQueryVO.getMobile());
		// 提现状态
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getApplyStatus()), "apply.apply_status",
				memberWithdrawApplyQueryVO.getApplyStatus());
		// 创建时间
		if (!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getCreateDate())) {
			queryWrapper.eq("DATE_FORMAT(apply.create_time,'%Y-%m')", memberWithdrawApplyQueryVO.getCreateDate());
		}
		// 有无店铺
		if ("0".equals(memberWithdrawApplyQueryVO.getHaveStore())) {
			// 没有
			queryWrapper.eq("member.have_store", false);
		} else if ("1".equals(memberWithdrawApplyQueryVO.getHaveStore())) {
			// 有
			queryWrapper.eq("member.have_store", true);
		}

		// 身份类型
		queryWrapper.likeLeft(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getPartnerName()), "role.role_name",
				memberWithdrawApplyQueryVO.getPartnerName());

		// 区域
		if (StringUtils.isNotEmpty(memberWithdrawApplyQueryVO.getRegionId())) {
			List list = regionService.getItemAdCod(memberWithdrawApplyQueryVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "member.location", list);
		}
		// 订单来源
		queryWrapper.likeLeft("apply.owner", WalletOwnerEnum.PROMOTE.name());
		queryWrapper.eq("apply.delete_flag", 0);
//		queryWrapper.isNotNull("member.mobile");
		queryWrapper.orderByDesc("apply.create_time");

		IPage<MemberWithdrawApplyVO> memberWithdrawApplyPage = memberWithdrawApplyService.getListPage(page,
				queryWrapper);
		List<MemberWithdrawApplyVO> memberWithdrawApplyList = memberWithdrawApplyService.getListAll(queryWrapper);
		if (CollectionUtils.isNotEmpty(memberWithdrawApplyList)) {
			for (MemberWithdrawApplyVO memberWithdrawApplyVO : memberWithdrawApplyList) {
				if (memberWithdrawApplyVO.getApplyStatus().equals("FINISH"))
					all = CurrencyUtil.add(all, memberWithdrawApplyVO.getApplyMoney());
			}
		}
		map.put("all", BigDecimal.valueOf(all).setScale(2, RoundingMode.DOWN).doubleValue());
		map.put("data", memberWithdrawApplyPage);
		return ResultUtil.data(map);
	}

	@ApiOperation(value = "提现明细-平台和城市")
	@RequestMapping("/getAdminList")
	public ResultMessage<Map<String, Object>> getAdminList(PageVO page,
			MemberWithdrawApplyQueryVO memberWithdrawApplyQueryVO) {
		Map<String, Object> map = new HashMap<>();
		Double all = 0D;
		// 构建查询 返回数据
		QueryWrapper<MemberWithdrawApply> queryWrapper = new QueryWrapper<>();
		AuthUser currentUser = UserContext.getCurrentUser();
		// 如果当前会员不为空，且为代理商角色
		if (currentUser != null && (currentUser.getRole().equals(UserEnums.AGENT))) {
			AdminUser adminUser = adminUserService.getById(currentUser.getId());
			if (null != adminUser && StringUtils.isNotEmpty(adminUser.getRegionId())) {
				queryWrapper.in("member.location", regionService.getItemAdCod(adminUser.getRegionId()));
				// queryWrapper.in("a.location",
				// regionService.getPartnerAdCode(memberSearchVO.getAdcode(), "4"));
			}
		}
		// 手机号
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getMobile()), "member.mobile",
				memberWithdrawApplyQueryVO.getMobile());
		// 提现状态
		queryWrapper.eq(!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getApplyStatus()), "apply.apply_status",
				memberWithdrawApplyQueryVO.getApplyStatus());
		// 创建时间
		if (!StringUtils.isEmpty(memberWithdrawApplyQueryVO.getCreateDate())) {
			queryWrapper.eq("DATE_FORMAT(apply.create_time,'%Y-%m')", memberWithdrawApplyQueryVO.getCreateDate());
		}
		// 身份类型
		if ("平台".equals(memberWithdrawApplyQueryVO.getPartnerName())) {
			queryWrapper.isNull("role.role_name");
		} else if (StringUtils.isNotEmpty(memberWithdrawApplyQueryVO.getPartnerName())) {
			queryWrapper.like("role.role_name", memberWithdrawApplyQueryVO.getPartnerName());
		}
		// 区域
		if (StringUtils.isNotEmpty(memberWithdrawApplyQueryVO.getRegionId())) {
			List list = regionService.getItemAdCod(memberWithdrawApplyQueryVO.getRegionId());
			queryWrapper.in(CollectionUtils.isNotEmpty(list), "member.location", list);
		}
		// 订单来源
		if (StringUtils.isEmpty(memberWithdrawApplyQueryVO.getOwner())) {
			queryWrapper.in("apply.owner", "BUYER_" + WalletOwnerEnum.PROMOTE_HY.name(),
					WalletOwnerEnum.PROMOTE_HY.name(), "BUYER_" + WalletOwnerEnum.PROMOTE_FW.name(),
					WalletOwnerEnum.PROMOTE_FW.name());
		} else {
			queryWrapper.likeLeft("apply.owner", memberWithdrawApplyQueryVO.getOwner());
		}
		queryWrapper.eq("apply.delete_flag", 0);
//		queryWrapper.isNotNull("member.mobile");
		queryWrapper.orderByDesc("apply.create_time");

		IPage<MemberWithdrawApplyVO> memberWithdrawApplyPage = memberWithdrawApplyService.getListPage(page,
				queryWrapper);
		List<MemberWithdrawApplyVO> memberWithdrawApplyList = memberWithdrawApplyService.getListAll(queryWrapper);
		if (CollectionUtils.isNotEmpty(memberWithdrawApplyList)) {
			for (MemberWithdrawApplyVO memberWithdrawApplyVO : memberWithdrawApplyList) {
				if (memberWithdrawApplyVO.getApplyStatus().equals("FINISH"))
					all = CurrencyUtil.add(all, memberWithdrawApplyVO.getApplyMoney());
			}
		}
		map.put("all", BigDecimal.valueOf(all).setScale(2, RoundingMode.DOWN).doubleValue());
		map.put("data", memberWithdrawApplyPage);
		return ResultUtil.data(map);
	}

	@ApiOperation(value = "扣率信息详情")
	@RequestMapping("/getFeeList")
	public ResultMessage<IPage<OrderItemVO>> getFeeList(PageVO page, String sn) {
		// 构建查询 返回数据
		IPage<OrderItemVO> orderItemVOIPage = memberWithdrawItemService.findItemBySn(page, sn);
		return ResultUtil.data(orderItemVOIPage);
	}

	@ApiOperation(value = "提现已完成(打款对接银行打款)")
	@RequestMapping("/finish/{id}")
	public ResultMessage finish(@PathVariable("id") String id) throws IOException {
		// 构建查询 返回数据
		MemberWithdrawApply memberWithdrawApply = memberWithdrawApplyService.getById(id);
		// MemberWithdrawApply memberWithdrawApply = new MemberWithdrawApply();
		// memberWithdrawApply.setId(id);
		// 打款中
		// memberWithdrawApply.setApplyStatus(WithdrawStatusEnum.INPAYMENT.name());
		// 对接银行接口状态;0:成功 ; 1:失败
		int banktype = 1;
		// 打款对接银行
		// 获取银行信息
		List<MemberBank> banks = memberBankService.getBank(memberWithdrawApply.getMemberId());
		MemberBank bank = new MemberBank();
		if (banks.size() > 0) {
			bank = banks.get(0);
		}
		// 判断信息是正常数据
		if (memberWithdrawApply != null && bank != null
				&& StringUtils.equals(memberWithdrawApply.getApplyStatus(), WithdrawStatusEnum.VIA_AUDITING.name())) {
			// 实体
			DockingBankVO dockingBankVO = new DockingBankVO();
			dockingBankVO.setPayeeAcctNo(bank.getCardNo()); // 收款人账号
			dockingBankVO.setPayeeName(bank.getAccountName());// 收款人名称
			Double applyMoney = memberWithdrawApply.getApplyMoney() == null ? 0D : memberWithdrawApply.getApplyMoney();
			Double withholdingAdvance = memberWithdrawApply.getWithholdingAdvance() == null
					? 0D
					: memberWithdrawApply.getWithholdingAdvance();
			dockingBankVO.setAmount(BigDecimal.valueOf(CurrencyUtil.sub(applyMoney, withholdingAdvance)));// 支付金额
			dockingBankVO.setPayeeBankName(bank.getBankName());// 收款行名称
			dockingBankVO.setAdCodeName(bank.getAdCodeName());
			dockingBankVO.setBankCode(bank.getBankCode());
			String rem = "";
			switch (memberWithdrawApply.getOwner()) {
				case "STORE_PROMOTE" : {
					rem = "推广钱包提现";
					break;
				}
				case "BUYER_PROMOTE" : {
					rem = "推广钱包提现";
					break;
				}
				case "BUYER_PROMOTE_HY" : {
					rem = "推广钱包";
					break;
				}
				case "BUYER_PROMOTE_FW" : {
					rem = "推广钱包";
					break;
				}
				case "STORE_SALE" : {
					rem = "销售钱包";
					break;
				}
				case "PROMOTE" : {
					rem = "推广钱包";
					break;
				}
				case "PROMOTE_HY" : {
					rem = "推广钱包";
					break;
				}
				case "PROMOTE_FW" : {
					rem = "推广钱包";
					break;
				}
				case "SALE" : {
					rem = "销售钱包";
					break;
				}
				default :
					break;
			}
			dockingBankVO.setWithdrawalId(memberWithdrawApply.getId());// 提现数据ID
			dockingBankVO.setWithdrawalSn(memberWithdrawApply.getSn());// 提现数据SN
			dockingBankVO.setNote(rem); // 备注信息
			// 判断信息没有空 并且提现金额大于0;
			if (dockingBankVO.isNotNull()) {
				// 调用打款工具类
				banktype = PuFaPayController.puFaWithdrawal(dockingBankVO);
				// banktype = puFaWithdrawal.
				// banktype = 0;
			} else {
				return ResultUtil.error(ResultCode.BANK_ERROR);
			}
		} else {
			return ResultUtil.error(ResultCode.BANK_ERROR);
		}
		// 对接返回状态成功
		if (banktype == 0) {
			// 修改订单状态
			memberWithdrawApply.setCardId(bank.getId());
			Double applyMoney = memberWithdrawApply.getApplyMoney() == null ? 0D : memberWithdrawApply.getApplyMoney();
			Double withholdingAdvance = memberWithdrawApply.getWithholdingAdvance() == null
					? 0D
					: memberWithdrawApply.getWithholdingAdvance();
			memberWithdrawApply.setRealMoney(CurrencyUtil.sub(applyMoney, withholdingAdvance));
			memberWithdrawApply.setFinishTime(new Date());
			memberWithdrawApply.setApplyStatus(WithdrawStatusEnum.FINISH.name());
			return ResultUtil.data(memberWithdrawApplyService.updateById(memberWithdrawApply));
		} else {
			return ResultUtil.error(ResultCode.TO_BANK_ERROR);
		}
	}

	@ApiOperation(value = "提现申请审核")
	@PostMapping
	@ApiImplicitParams({@ApiImplicitParam(name = "applyId", value = "审核记录id", required = true, paramType = "query"),
			@ApiImplicitParam(name = "result", value = "审核结果", required = true, paramType = "query"),
			@ApiImplicitParam(name = "remark", value = "审核备注", paramType = "query")})
	public Boolean audit(String applyId, String result, String remark) {
		return memberWithdrawApplyService.audit(applyId, result, remark);
	}

	// 提现列表查询
	@RequestMapping("/queryList")
	public ResultMessage<IPage<MemberWithdrawApply>> queryList(PageVO page, MemberWithdrawApply memberWithdrawApply,
			String owner) {

		QueryWrapper<MemberWithdrawApply> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("delete_flag", "0");
		queryWrapper.eq(CharSequenceUtil.isNotEmpty(owner), "owner", owner);
		queryWrapper.apply(" DATE_FORMAT( create_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )");
		queryWrapper.orderByDesc("create_time");
		IPage<MemberWithdrawApply> memberWithdrawApplyIPage = memberWithdrawApplyService.queryWithdrawApplyList(page,
				queryWrapper);
		return ResultUtil.data(memberWithdrawApplyIPage);
	}

	@RequestMapping(value = "/tradeDetail")
	@ApiOperation(value = "平台提现")
	public ResultMessage payment(Double price, String owner) {
		// 构建审核参数
		MemberWithdrawApply memberWithdrawApply = new MemberWithdrawApply();
		memberWithdrawApply.setMemberId("admin");
		memberWithdrawApply.setMemberName("平台");
		memberWithdrawApply.setApplyPrice(price);
		memberWithdrawApply.setApplyMoney(price);
		memberWithdrawApply.setRealMoney(price);
		memberWithdrawApply.setOwner("BUYER_" + owner);

		memberWithdrawApply.setApplyStatus(WithdrawStatusEnum.FINISH.name());
		memberWithdrawApply.setInspectTime(new Date());
		String sn = "W" + SnowFlake.getId();
		memberWithdrawApply.setSn(sn);
		MemberWallet admin = memberWalletService.getMemberWalletInfo("admin", owner, "平台");

		memberWithdrawApply.setBeforeMoney(admin.getMemberWallet());

		memberWithdrawApplyService.save(memberWithdrawApply);

		// 修改钱包余额
		WalletDetail detail = new WalletDetail();
		detail.setSn(sn);
		detail.setTransactionType(DepositServiceTypeEnum.WALLET_WITHDRAWAL.name());
		detail.setPayer(admin.getMemberId());
		detail.setPayerName(admin.getMemberName());
		detail.setPayerOwner(admin.getOwner());
		detail.setPayerBeforeMoney(admin.getMemberWallet());
		detail.setMoney(price);

		admin.setMemberWallet(CurrencyUtil.sub(admin.getMemberWallet(), price));
		memberWalletService.update(admin,
				new QueryWrapper<MemberWallet>().eq("id", admin.getId()).eq("owner", admin.getOwner()));
		detail.setPayerAfterMoney(admin.getMemberWallet());

		walletDetailService.save(detail);
		return ResultUtil.data(memberWithdrawApply);
	}
}
