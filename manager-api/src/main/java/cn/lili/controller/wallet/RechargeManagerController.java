package cn.lili.controller.wallet;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.utils.CurrencyUtil;
import cn.lili.common.vo.PageVO;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.member.entity.dos.RechargeFow;
import cn.lili.modules.order.order.entity.enums.PayStatusEnum;
import cn.lili.modules.order.trade.entity.vo.RechargeQueryVO;
import cn.lili.modules.wallet.entity.dos.Recharge;
import cn.lili.modules.wallet.entity.vo.RechargeVO;
import cn.lili.modules.wallet.service.RechargeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 管理端,预存款充值记录接口
 *
 * @author pikachu
 * @since 2020/11/16 10:07 下午
 */
@RestController
@Api(tags = "管理端,预存款充值记录接口")
@RequestMapping("/manager/recharge")
@Transactional(rollbackFor = Exception.class)
public class RechargeManagerController {
	@Autowired
	private RechargeService rechargeService;

	@ApiOperation(value = "分页获取预存款充值记录")
	@GetMapping
	public ResultMessage<IPage<RechargeVO>> getByPage(PageVO page, RechargeQueryVO rechargeQueryVO) {
		// 构建查询 返回数据
		IPage<RechargeVO> rechargePage = rechargeService.rechargePage(page, rechargeQueryVO);
		if (CollectionUtils.isNotEmpty(rechargePage.getRecords())) {
			for (RechargeVO rechargeVO : rechargePage.getRecords()) {
				rechargeVO.setRechargeMoney(
						BigDecimal.valueOf(rechargeVO.getRechargeMoney()).setScale(2, RoundingMode.DOWN).doubleValue());
				rechargeVO.setPayeeAfterMoney(BigDecimal.valueOf(rechargeVO.getPayeeAfterMoney())
						.setScale(2, RoundingMode.DOWN).doubleValue());
			}
		}
		return ResultUtil.data(rechargePage);
	}

	@ApiOperation(value = "流水分页列表")
	@RequestMapping("/gitLuys")
	public ResultMessage<IPage<Map>> getByPage(RechargeQueryVO recharge, PageVO page) {
		return ResultUtil.data(rechargeService.getRechargeFowPage(recharge, page));
	}

	@ApiOperation(value = "流水总额")
	@RequestMapping("/getAll")
	public ResultMessage<Map> getAll(RechargeQueryVO recharge, PageVO page) {
		Map<String, Object> map = new HashMap<>();
		page.setPageSize(999999999);
		Double huanbei = 0D;
		Double chongzhi = 0D;
		IPage<Map> rechargeFowPage = rechargeService.getRechargeFowPage(recharge, page);
		if (null != rechargeFowPage.getRecords()) {
			for (Map rechargeVO : rechargeFowPage.getRecords()) {
				huanbei = CurrencyUtil.add(huanbei,
						null != rechargeVO.get("huanbeiMoney")
								? Double.parseDouble(rechargeVO.get("huanbeiMoney").toString())
								: 0D);
				chongzhi = CurrencyUtil.add(chongzhi,
						null != rechargeVO.get("rechargeMoney")
								? Double.parseDouble(rechargeVO.get("rechargeMoney").toString())
								: 0D);
			}
		}
		map.put("huanbei", huanbei);
		map.put("chongzhi", chongzhi);
		return ResultUtil.data(map);
	}

	@ApiOperation(value = "平台充值总额")
	@RequestMapping("/rechargeMoneyAdmin")
	public ResultMessage<Map> rechargeMoneyAdmin() {
		Map<String, Object> map = new HashMap<>();
		QueryWrapper<Recharge> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("member_id", "admin");
		queryWrapper.eq("pay_status", PayStatusEnum.PAID.name());
		Double rechargeMoneyAdmin = 0D;
		List<Recharge> list = rechargeService.list(queryWrapper);
		if (CollectionUtils.isNotEmpty(list)) {
			for (Recharge recharge : list) {
				rechargeMoneyAdmin = CurrencyUtil.add(rechargeMoneyAdmin,
						recharge.getRechargeMoney() != null ? recharge.getRechargeMoney() : 0D);
			}
		}
		map.put("rechargeMoneyAdmin",
				BigDecimal.valueOf(rechargeMoneyAdmin).setScale(2, RoundingMode.DOWN).doubleValue());
		return ResultUtil.data(map);
	}

}
