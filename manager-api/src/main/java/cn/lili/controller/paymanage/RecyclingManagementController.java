package cn.lili.controller.paymanage;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.payment.entity.vo.RecyclingManagementSearchParams;
import cn.lili.modules.payment.entity.vo.RecyclingManagementVO;
import cn.lili.modules.payment.service.RecyclingManagementService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 回收（提现）管理
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Slf4j
@RestController
@Api(tags = "回收（提现）管理")
@RequestMapping("/manager/paymanage/recycling")
public class RecyclingManagementController {

	/**
	 * 回收（提现）管理
	 */
	@Autowired
	private RecyclingManagementService recyclingManagementService;

	@ApiOperation(value = "分页查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "withdrawalStatus", value = "提现状态", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<RecyclingManagementVO>> getByPage(
			RecyclingManagementSearchParams recyclingManagementSearchParams) {

		return ResultUtil.data(recyclingManagementService.recyclingManagementPage(recyclingManagementSearchParams));
	}

	@PutMapping(value = "/updateRecycling")
	@ApiOperation(value = "修改")
	public ResultMessage<Object> update(RecyclingManagementVO recyclingManagementVO) {
		Boolean bo = recyclingManagementService.updateRecyclingManagement(recyclingManagementVO);
		System.out.println(bo);
		return ResultUtil.success();
	}

}
