package cn.lili.controller.paymanage;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.entity.vos.DictionaryVO;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.payment.entity.vo.FundManagementSearchParams;
import cn.lili.modules.payment.entity.vo.FundManagementVO;
import cn.lili.modules.payment.service.FundManagementService;
import cn.lili.modules.wallet.entity.dto.WalletLogSearchParams;
import cn.lili.modules.wallet.entity.vo.WalletLogVO;
import cn.lili.modules.wallet.service.WalletLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 支付管理
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "资金管理")
@RequestMapping("/manager/paymanage/fundmanagement")
public class FundManagementController {

	/**
	 * 资金管理
	 */
	@Autowired
	private FundManagementService fundManagementService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private WalletLogService walletLogService;

	@ApiOperation(value = "根据商家分组")
	@ApiImplicitParams({@ApiImplicitParam(name = "bName", value = "商家名称", paramType = "query"),
			@ApiImplicitParam(name = "bContact", value = "联系方式", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<FundManagementVO>> getByPage(FundManagementSearchParams fundManagementSearchParams) {
		List<DictionaryVO> lsitdict = this.dictionaryService.allChildren();

		return ResultUtil.data(fundManagementService.fundManagementPage(fundManagementSearchParams));
	}

	@ApiOperation(value = "商家资金管理")
	@RequestMapping(value = "/findStoreLog")
	public ResultMessage<IPage<WalletLogVO>> findStoreLog(WalletLogSearchParams walletLogSearchParams) {
		return ResultUtil.data(walletLogService.findStoreLog(walletLogSearchParams));
	}

}
