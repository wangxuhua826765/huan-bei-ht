package cn.lili.controller.paymanage;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.payment.entity.vo.PaymentsCodeSearchParams;
import cn.lili.modules.payment.entity.vo.PaymentsCodeVO;
import cn.lili.modules.payment.service.PaymentsCodeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 收款码管理
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Slf4j
@RestController
@Api(tags = "收款码管理")
@RequestMapping("/manager/paymanage/paymentscode")
public class PaymentsCodeController {

	/**
	 * 收款码管理
	 */
	@Autowired
	private PaymentsCodeService paymentsCodeService;

	@ApiOperation(value = "分页查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "name", value = "商户名称", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<PaymentsCodeVO>> getByPage(PaymentsCodeSearchParams paymentsCodeSearchParams) {
		return ResultUtil.data(paymentsCodeService.getPaymentsCodeList(paymentsCodeSearchParams));
	}
}
