package cn.lili.controller.paymanage;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.dict.service.DictionaryService;
import cn.lili.modules.payment.entity.vo.QuotaManagementSearchParams;
import cn.lili.modules.payment.entity.vo.QuotaManagementVO;
import cn.lili.modules.payment.service.QuotaManagementService;
import cn.lili.modules.promotion.entity.vos.PointsGoodsCategoryVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 限额管理
 *
 * @author 贾送兵
 * @since 2021-02-11 11:32
 */
@Slf4j
@RestController
@Api(tags = "限额管理")
@RequestMapping("/manager/paymanage/quota")
public class QuotaManagementController {

	/**
	 * 资金管理
	 */
	@Autowired
	private QuotaManagementService quotaManagementService;
	@Autowired
	private DictionaryService dictionaryService;

	@ApiOperation(value = "分页查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "userType", value = "用户类型", paramType = "query"),
			@ApiImplicitParam(name = "busType", value = "业务类型", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<QuotaManagementVO>> getByPage(QuotaManagementSearchParams quotaManagementSearchParams) {
		// List<DictionaryVO> lsitdict = this.dictionaryService.allChildren();

		return ResultUtil.data(quotaManagementService.getQuotaManagementList(quotaManagementSearchParams));
	}

	@PutMapping(value = "/updateQuota")
	@ApiOperation(value = "修改")
	public ResultMessage<Object> update(QuotaManagementVO quotaManagementVO) {
		Boolean bo = quotaManagementService.updateQuotaManagement(quotaManagementVO);
		System.out.println(bo);
		return ResultUtil.success();
	}

}
