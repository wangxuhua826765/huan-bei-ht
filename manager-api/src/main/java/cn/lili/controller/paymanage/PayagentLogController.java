package cn.lili.controller.paymanage;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.payment.entity.PayagentLogVO;
import cn.lili.modules.payment.entity.PayagentSearchParams;
import cn.lili.modules.payment.service.PayagentLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付管理
 *
 * @author pikachu
 * @since 2020-05-5 15:10:16
 */
@Slf4j
@RestController
@Api(tags = "支付管理")
@RequestMapping("/manager/paymanage/agent")
public class PayagentLogController {

	/**
	 * 支付管理
	 */
	@Autowired
	private PayagentLogService payagentLogService;

	@ApiOperation(value = "根据商家分组")
	@ApiImplicitParams({@ApiImplicitParam(name = "bName", value = "商家名称", paramType = "query"),
			@ApiImplicitParam(name = "bContact", value = "联系方式", paramType = "query")})
	@RequestMapping(value = "/getByPage")
	public ResultMessage<IPage<PayagentLogVO>> getByPage(PayagentSearchParams payagentSearchParams) {
		return ResultUtil.data(payagentLogService.getPaybusinessListGroupBid(payagentSearchParams));
	}

	@ApiOperation(value = "分页获取")
	@ApiImplicitParams({@ApiImplicitParam(name = "bId", value = "商家Id", paramType = "query"),})
	@RequestMapping(value = "/getAgentByPage")
	public ResultMessage<IPage<PayagentLogVO>> getBusinessByPage(PayagentSearchParams payagentSearchParams) {
		return ResultUtil.data(payagentLogService.paybusinessLogPage(payagentSearchParams));
	}

}
