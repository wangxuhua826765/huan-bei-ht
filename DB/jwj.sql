ALTER TABLE `sys_dict`
    ADD COLUMN `sort` int(255) DEFAULT '0' COMMENT '排序',


ALTER TABLE `sys_dict_item`
    ADD COLUMN `del_flag` int(1) DEFAULT '0' COMMENT '删除状态（0未删除）',


ALTER TABLE `li_store`
    ADD COLUMN `lon` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '高德坐标系经度',
    ADD COLUMN`lat` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '高德坐标系纬度',
   ADD COLUMN`discount` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '店铺折扣',
   ADD COLUMN `store_phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '店铺联系方式',
    ADD COLUMN `store_image` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '店铺图片'

ALTER TABLE `li_goods`
    ADD COLUMN `lon` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '高德坐标系经度',
    ADD COLUMN`lat` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '高德坐标系纬度',
   ADD COLUMN address_path varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '地址名称',
   ADD COLUMN adress_code varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '行政区划代码',
    ADD COLUMN `sales_area` varchar(1) DEFAULT NULL COMMENT '0是全国销售  1是区域销售'

ALTER TABLE `li_partner`
    ADD COLUMN `code` int(255) DEFAULT '0' COMMENT '编码',
    ADD COLUMN`prefix` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '编码前缀'



ALTER TABLE `li_transfer`
    ADD COLUMN `type`varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '转账类型 端口费：PORT_FEE'


CREATE TABLE `li_activity` (
       `id` bigint(20) NOT NULL COMMENT 'ID',
       `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动名称',
       `content` longtext COLLATE utf8mb4_bin NOT NULL COMMENT '活动说明',
       `activity_start_time` datetime DEFAULT NULL COMMENT '活动开始时间',
       `activity_end_time` datetime DEFAULT NULL COMMENT '活动结束时间',
       `registration_start_time` datetime DEFAULT NULL COMMENT '报名开始时间',
       `registration_end_time` datetime DEFAULT NULL COMMENT '报名结束时间',
       `audit_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '审核状态',
       `sales_area` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '投放区域（0 全国投放  1 区域投放）',
       `region_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '投放区域地址ID',
       `launch_area` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '投放区域',
       `ad_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'adcode',
       `poster_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '海报',
       `submit_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '提交人类型（0 平台端  1城市合伙人 2商家端 ）',
       `publisher` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发布人',
       `activity_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动类型(0商品活动 1宣传活动)',
       `status` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上架状态（0未上架 1 已上架）',
       `remarks` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
       `is_quantity` bit(1) DEFAULT NULL COMMENT '是否限购（0不限购  1限购）',
       `quantity` tinyint(255) DEFAULT NULL COMMENT '限购数量',
       `store_id` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '店铺数量',
       `create_by` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '创建者',
       `create_time` datetime DEFAULT NULL COMMENT '创建时间',
       `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
       `update_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
       `update_time` datetime DEFAULT NULL COMMENT '更新时间',
       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPACT COMMENT='活动表';


CREATE TABLE `li_activity_examine` (
       `id` bigint(20) NOT NULL COMMENT 'ID',
       `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
       `reason` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '原因',
       `status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '审核状态（0审核通过 1 驳回）',
       `create_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
       `create_time` datetime DEFAULT NULL COMMENT '创建时间',
       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPACT COMMENT='活动审核详情';

CREATE TABLE `li_activity_goods` (
     `id` bigint(20) NOT NULL COMMENT 'id',
     `activity_id` bigint(20) DEFAULT NULL COMMENT '活动id',
     `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
     `store_id` bigint(20) DEFAULT NULL COMMENT '店铺id',
     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPACT COMMENT='活动商品关系';

-- 修改 li_activity 表中的 content 字段类型
alter table li_activity modify column `content` longtext COLLATE utf8mb4_bin COMMENT '活动说明'

-- 在 li_store 表中新增 is_have_contract 字段
ALTER TABLE `li_store`
    ADD COLUMN `is_have_contract` varchar(255) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '是否有合同(0 没有  1 有合同)'


-- 修改 li_activity 表中的 是否限购 字段名
alter table li_activity CHANGE `is_quantity` `quantity_flag` bit(1) DEFAULT NULL COMMENT '是否限购（0不限购  1限购）'
