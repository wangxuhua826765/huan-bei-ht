ALTER TABLE `li_member_bank`
ADD COLUMN `bank_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行行号' AFTER `update_time`,
ADD COLUMN `ad_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址ID' AFTER `bank_code`,
ADD COLUMN `ad_code_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址名称' AFTER `ad_code`;


ALTER TABLE `lilishop`.`li_after_sale`
MODIFY COLUMN `after_sale_image` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '评价图片' AFTER `actual_refund_price`;
