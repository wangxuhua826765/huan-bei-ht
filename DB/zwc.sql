CREATE TABLE `li_article_record_summary`  (
  `id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '已经查看过的用户id',
  `article_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '已经读取的文章類型',
  `view_status` tinyint(1) NULL DEFAULT 0 COMMENT '查看状态默认0是已阅读-1是需要重新阅读',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


CREATE TABLE `li_store_evaluation`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) NULL DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) NULL DEFAULT NULL COMMENT '更新时间',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '内容',
  `member_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '会员ID',
  `member_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '会员名称',
  `member_profile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '会员头像',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单号',
  `store_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '店铺ID',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态',
  `store_score` int(11) NULL DEFAULT NULL COMMENT '店铺评分',
  `store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '店铺名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

CREATE TABLE `li_member_withdraw_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `apply_money` double(20, 5) NULL DEFAULT NULL COMMENT '提现焕呗额',
  `real_money` double(20, 5) NULL DEFAULT NULL COMMENT '提现人民币',
  `order_item_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单明细表id',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流水号',
  `member_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '钱包主键',
  `fee` double(20, 5) NULL DEFAULT NULL COMMENT '商品折扣率',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型  11 销售-提现 12销售-订单额 13销售-运费  14销售-服务费   21 推广会员-提现 22推广会员-订单额 23推广会员-运费  24推广会员-服务费',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = ' 提现明细表' ROW_FORMAT = Dynamic;

ALTER TABLE `li_member_evaluation`
    ADD COLUMN `grade` bigint(1) NULL DEFAULT NULL COMMENT '星级评分';

ALTER TABLE `li_order_item`
    ADD COLUMN `deposit` double(10, 2) NULL DEFAULT NULL COMMENT '可提现金额',
    ADD COLUMN  `fee` double(20, 2) NULL DEFAULT NULL COMMENT '提现折扣率',
    ADD COLUMN `realization` tinyint(1) NULL DEFAULT 0 COMMENT '可提现金额是否提完,-1已提完  0未提现完',
    ADD COLUMN `surplus` double(10, 2) NULL DEFAULT NULL COMMENT '用户提现完剩余可提现余额',
    ADD COLUMN `merchandise_status` tinyint(1) NULL DEFAULT 0 COMMENT '0商品 1邮费';

ALTER TABLE `li_member_withdraw_apply`
ADD COLUMN `card_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '银行卡表主键' AFTER `owner`,
ADD COLUMN `withholding_advance` double(20, 5) DEFAULT NULL COMMENT '预扣预缴税额' AFTER `card_id`,
ADD COLUMN `payee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收款人id' AFTER `withholding_advance`,
ADD COLUMN `payee_owner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收款人钱包' AFTER `payee`,
ADD COLUMN `fee` double(20, 5) DEFAULT NULL COMMENT '折扣率' AFTER `payee_owner`,
ADD COLUMN `before_money` double(20, 5) DEFAULT NULL COMMENT '提现前余额' AFTER `fee`;
