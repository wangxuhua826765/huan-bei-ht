-- 推广下级关联表
CREATE TABLE `li_promotion` (
  `id` varchar(32) NOT NULL,
  `promotion_code` varchar(32) DEFAULT NULL COMMENT '推广码',
  `user_id` varchar(32) DEFAULT NULL COMMENT '被推广人id/店铺id',
  `user_role` int(1) DEFAULT NULL COMMENT '被推广人类型  0 用户   1 商家',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推广下级关联表';


-- 审核表
CREATE TABLE `li_audit` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) DEFAULT b'0' COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `user_id` varchar(32) DEFAULT NULL COMMENT '申请人',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '申请角色  0 商家 1 代理商   2 合伙人',
  `audit_by` varchar(32) DEFAULT NULL COMMENT '审核人',
  `state` int(1) NOT NULL DEFAULT '0' COMMENT '审核状态  0 待审核  1 审核通过  2 驳回',
  `reason` varchar(255) DEFAULT NULL COMMENT '驳回理由',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='审核表';


ALTER TABLE `huanbei`.`li_admin_user`
ADD COLUMN `region_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '代理区域id' AFTER `role_ids`,
ADD COLUMN `region` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '代理区域' AFTER `region_id`,
ADD COLUMN `promotion_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '推广码' AFTER `region`;



ALTER TABLE `lilishop`.`li_member`
ADD COLUMN `promotion_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '推广码' AFTER `type`;


CREATE TABLE `lilishop`.`li_partner`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `member_id` varchar(0) COMMENT '会员id',
  `partner_type` int(1) DEFAULT NULL COMMENT '合伙人类型   0 非合伙人  1 事业合伙人  2_ 天使合伙人  3 推广员',
  `partner_state` int(1) DEFAULT NULL COMMENT '合伙人状态  0 启用   1  禁用',
  `begin_time` datetime(6) COMMENT '开始时间',
  `end_time` datetime(6) COMMENT '结束时间',
  PRIMARY KEY (`id`)
) COMMENT = '合伙人表';

ALTER TABLE `lilishop`.`li_recycling_management`
MODIFY COLUMN `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注' AFTER `application_date`,
ADD COLUMN `withdrawal_remark` varchar(255) COMMENT '提现说明' AFTER `withdrawal_status`,
MODIFY COLUMN `audit_time` datetime(0) DEFAULT NULL COMMENT '审核时间' AFTER `withdrawal_remark`;


ALTER TABLE `lilishop`.`li_partner`
ADD COLUMN `role_id` varchar(32) COMMENT '角色id' AFTER `member_id`;


-- 2022.03.02
ALTER TABLE `lilishop`.`li_membership_card`
DROP COLUMN `grade_explain`,
DROP COLUMN `grade_interest`,
DROP COLUMN `money`,
DROP COLUMN `integral`,
CHANGE COLUMN `level` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员等级' AFTER `update_by`,
ADD COLUMN `name` varchar(255) COMMENT '等级名称' AFTER `level`,
ADD COLUMN `rebate` decimal(3, 2) COMMENT '享受折扣' AFTER `name`,
ADD COLUMN `money` decimal(5, 0) COMMENT '会员金额' AFTER `rebate`,
COMMENT = '会员等级表';

ALTER TABLE `lilishop`.`li_member`
ADD COLUMN `grade_id` varchar(32) COMMENT '会员等级id' AFTER `member_flag`;



CREATE TABLE `lilishop`.`li_grade_level`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `member_id` varchar(32) COMMENT '会员id',
  `grade_id` varchar(32) COMMENT '会员等级id',
  `grade_state` int(1) DEFAULT NULL COMMENT '会员状态  0 启用   1  禁用',
  `begin_time` datetime(6) COMMENT '开始时间',
  `end_time` datetime(6) COMMENT '结束时间',
  PRIMARY KEY (`id`)
) COMMENT = '会员等级表';


ALTER TABLE `lilishop`.`li_rate_setting`
ADD COLUMN `partner_rate` double COMMENT '合伙人费率' AFTER `enable_state`;

-- 2022.03.08
ALTER TABLE `lilishop`.`li_membership_card`
MODIFY COLUMN `level` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员等级' AFTER `update_by`;

-- 2022.03.09
ALTER TABLE `lilishop`.`li_order`
ADD COLUMN `owner` varchar(255) COMMENT '订单来源(用户、商家)' AFTER `verification_code`;


-- 2022.03.11
ALTER TABLE `lilishop`.`li_credit_management`
ADD COLUMN `expiry_date` datetime(6) COMMENT '到期日期' AFTER `name_type_text`,
ADD COLUMN `overdue` varchar(2) COMMENT '到期未还时间' AFTER `expiry_date`,
ADD COLUMN `freeze_reason` varchar(255) COMMENT '冻结原因' AFTER `overdue`;

-- 2022.03.13
ALTER TABLE `lilishop`.`li_store`
ADD COLUMN `recharge_rule` double DEFAULT NULL COMMENT '充值折扣' AFTER `promotion_code`,
ADD COLUMN `withdrawal_rate` double DEFAULT NULL COMMENT '提现折扣' AFTER `recharge_rule`,
ADD COLUMN `jiaoyifei` double DEFAULT NULL COMMENT 'BtoB交易费率' AFTER `withdrawal_rate`;


-- 2022.03.14
ALTER TABLE `lilishop`.`li_store_detail`
ADD COLUMN `qualification_license_photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '资质许可证电子版' AFTER `settlement_day`;

ALTER TABLE `lilishop`.`li_order`
ADD COLUMN `commission` double(10, 2)  COMMENT 'B2B订单佣金' AFTER `owner`;

ALTER TABLE `lilishop`.`li_trade`
ADD COLUMN `commission` double(10, 2)  COMMENT 'B2B订单佣金';

-- 2022.03.15
ALTER TABLE `lilishop`.`li_member_wallet`
ADD COLUMN `owner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '焕呗来源(用户、商家)' AFTER `wallet_password`;

ALTER TABLE `li_member_withdraw_apply`
ADD COLUMN `owner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单来源  STORE 商家端   BUYER 用户端';

ALTER TABLE `li_recharge`
ADD COLUMN `owner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单来源  STORE 商家端   BUYER 用户端';

ALTER TABLE `li_wallet_log`
ADD COLUMN `owner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单来源  STORE 商家端   BUYER 用户端';


-- 2022.03.19
ALTER TABLE `lilishop`.`li_member`
ADD COLUMN `commission` double(6, 2) COMMENT '会员交会费时暂存的佣金数' AFTER `promotion_img`;

ALTER TABLE `lilishop`.`li_rate_setting`
MODIFY COLUMN `partner_rate` double(10, 2) DEFAULT NULL COMMENT '合伙人费率' AFTER `enable_state`,
ADD COLUMN `partner_price` double(10, 2) COMMENT '合伙人会费' AFTER `partner_rate`;


-- 2022.03.25
ALTER TABLE `lilishop`.`li_membership_card`
ADD COLUMN `recharge_rule` double DEFAULT NULL COMMENT '充值折扣' AFTER `money`;

ALTER TABLE `lilishop`.`li_after_sale`
ADD COLUMN `goods_price` double(10, 2) DEFAULT NULL COMMENT '商品价格' AFTER `order_item_sn`;


-- 2022.03.26
ALTER TABLE `lilishop`.`li_member_withdraw_apply`
ADD COLUMN `real_money` double(20, 2) COMMENT '实际提现金额' AFTER `apply_money`,
ADD COLUMN `handling_money` double(20, 2) COMMENT '手续费' AFTER `real_money`;

alter TABLE li_order ADD frozend bit(1) DEFAULT 1 COMMENT "资金是否解冻，1-否 2-是" ;

-- 2022.03.28
ALTER TABLE `lilishop`.`li_wallet_log`
MODIFY COLUMN `money` double(9, 2) DEFAULT NULL COMMENT '金额' AFTER `member_name`,
ADD COLUMN `before_money` double(9, 2) DEFAULT NULL COMMENT '变动前金额' AFTER `money`,
ADD COLUMN `after_money` double(9, 2) DEFAULT NULL COMMENT '变动后金额' AFTER `before_money`;

ALTER TABLE `lilishop`.`li_grade_level`
ADD COLUMN `owner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源(用户、商家)' AFTER `end_time`;



ALTER TABLE `lilishop`.`li_partner`
MODIFY COLUMN `partner_type` int(1) DEFAULT NULL COMMENT '合伙人类型   0 非合伙人  1 事业合伙人  2 天使合伙人  3 普通合伙人  4 城市代理商' AFTER `role_id`,
ADD COLUMN `region` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区域' AFTER `end_time`,
ADD COLUMN `region_id` varchar(32) COMMENT '区域id' AFTER `region`,
ADD COLUMN `audit_state` varchar(255) COMMENT '审核状态' AFTER `region_id`;


CREATE TABLE `li_wallet_log_detail` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `store_address_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区域名称',
  `store_address_id_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区域id',
  `transaction_type` varchar(255) DEFAULT NULL COMMENT '交易类型（ 消费  佣金  销售）',
  `sn` varchar(255) DEFAULT NULL COMMENT '订单号',
  `payer` varchar(255) DEFAULT NULL COMMENT '付款人id',
  `payer_name` varchar(255) DEFAULT NULL COMMENT '付款人姓名',
  `payee` varchar(255) DEFAULT NULL COMMENT '收款人',
  `payee_name` varchar(255) DEFAULT NULL COMMENT '收款人姓名',
  `grade_level` varchar(255) DEFAULT NULL COMMENT '充值会员等级（1 普通会员  2  银卡会员   3  金卡会员  4 商家会员）',
  `second_activation` varchar(255) DEFAULT NULL COMMENT '是否二次激活',
  `percentage` varchar(255) DEFAULT NULL COMMENT '金额占比',
  `money` varchar(255) DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='钱包日志关联表';

-- 2022.04.01
ALTER TABLE `lilishop`.`li_wallet_log_detail`
ADD COLUMN `log_id` varchar(32) COMMENT '日志表id' AFTER `create_time`;

-- 2022.04.02
ALTER TABLE `lilishop`.`li_store`
ADD COLUMN `recommend` bit(1) COMMENT '是否推荐（0否 1 是）',
ADD COLUMN `recommend_ranking` int(3) COMMENT '推荐排名';

ALTER TABLE `lilishop`.`li_goods`
ADD COLUMN `recommend_ranking` int(3) COMMENT '推荐排名';



ALTER TABLE `li_goods`
ADD COLUMN `store_address_id_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '地址id' AFTER `recommend_ranking`;
ALTER TABLE `li_goods`
ADD COLUMN `store_address_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '地址名称' AFTER `store_address_id_path`;


ALTER TABLE `li_goods`
ADD COLUMN `sort` bigint(20) DEFAULT 0 COMMENT '排序 数字越大越前';

ALTER TABLE `li_goods_sku`
ADD COLUMN `sort` bigint(20) DEFAULT 0 COMMENT '排序 数字越大越前';


ALTER TABLE `li_recharge`
MODIFY COLUMN `recharge_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '1提现2充值年费会员3充值会员4普通充值 5 充值合伙人42 充值并交年费  43 充值并交银卡会员 44 充值并交金卡会员 45 充值并成为合伙人';

-- 2022.04.19
ALTER TABLE `li_partner`
MODIFY COLUMN `region_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区域id' AFTER `region`;

ALTER TABLE `li_wallet_log_detail`
MODIFY COLUMN `transaction_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易类型（ 0 余额充值 1消费  2佣金  3销售  4年费充值 5合伙人充值 6 提现 7 退款 8 退佣金）' AFTER `store_address_id_path`;


ALTER TABLE `li_wallet_log_detail`
MODIFY COLUMN `transaction_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易类型（ 0 余额充值 1消费  2佣金  3销售  4年费充值 5合伙人充值 6 提现  7 退款  8 退佣金  9 商家提现）' AFTER `store_address_id_path`;

ALTER TABLE `li_after_sale`
ADD COLUMN `commission_price` double(10, 2) DEFAULT NULL COMMENT '退还服务费' AFTER `goods_price`;


-- 2022.05.20
ALTER TABLE `li_credit_management`
DROP COLUMN `name_type`,
DROP COLUMN `contact_information`,
DROP COLUMN `user_address`,
DROP COLUMN `credit_period`,
DROP COLUMN `operator`,
DROP COLUMN `credit_status`,
DROP COLUMN `used_limit`,
DROP COLUMN `remaining_quota`,
DROP COLUMN `credit_status_text`,
DROP COLUMN `name_type_text`,
DROP COLUMN `overdue`,
DROP COLUMN `freeze_reason`,
ADD COLUMN `operator_id` varchar(255) COMMENT '操作人id',
ADD COLUMN `operator_name` varchar(255) COMMENT '操作人姓名',
ADD COLUMN `audit_status` varchar(255) COMMENT '审核状态  0 待审核  1 审核通过  2 驳回',
ADD COLUMN `repaymeent_date` datetime(6) COMMENT '还款时间',
ADD COLUMN `repayment_status` varchar(255) COMMENT '还款状态  0未还  1  已还' ,
MODIFY COLUMN `audit_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 0 COMMENT '审核状态  0 待审核  1 审核通过  2 驳回',
MODIFY COLUMN `repayment_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 0 COMMENT '还款状态  0未还  1  已还' ;

-- 2022.05.24
ALTER TABLE `huanbei`.`li_member`
ADD COLUMN `location` varchar(255) COMMENT '定位' AFTER `payment_password`;

-- 2022.06.15
ALTER TABLE `li_goods`
ADD COLUMN `shelf_life` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '保质期' AFTER `sort`,
ADD COLUMN `production_date` datetime(6) COMMENT '生产日期' AFTER `shelf_life`;

-- 2022.06.21
ALTER TABLE `li_goods_sku`
ADD COLUMN `fee` double(20, 2) DEFAULT NULL COMMENT '提现折扣率' AFTER `sort`;


-- 2022.06.21
ALTER TABLE `li_goods`
ADD COLUMN `num` int(20) DEFAULT NULL COMMENT '店铺内排序' ;

-- 2022.06.22
CREATE TABLE `lilishop`.`li_foot_store`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `store_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '店铺ID',
  `member_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '会员ID',
  PRIMARY KEY (`id`)
);

-- 2022.06.28
ALTER TABLE `lilishop`.`li_recharge`
ADD COLUMN `order_sn` varchar(255) COMMENT '订单号（只有消费时才会有）' AFTER `price`;

-- 2022.06.29
CREATE TABLE `lilishop`.`li_combined_payment`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `member_id` varchar(32) COMMENT '会员id',
  `sn` varchar(255) COMMENT '订单号',
  `money` double(20, 5) DEFAULT NULL COMMENT '订单金额',
  `commmission_money` double(20, 5) DEFAULT NULL COMMENT '手续费',
  `wallet_type` varchar(255) COMMENT '钱包类型',
  `wallet_money` double(20, 5) DEFAULT NULL COMMENT '扣款金额',
  PRIMARY KEY (`id`)
);

CREATE TABLE `lilishop`.`li_transfer`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `payee` varchar(255) COMMENT '收款人id',
  `payee_phone` varchar(255) COMMENT '收款人手机号',
  `payee_name` varchar(255) COMMENT '收款人姓名',
  `payee_card` varchar(255) COMMENT '收款人会员身份',
  `payee_img` varchar(255) COMMENT '收款人头像',
  `transfer_money` double(10, 5) COMMENT '转账金额',
  `commission` double(10, 5) COMMENT '手续费',
  `payment_wallet` varchar(255) COMMENT '付款钱包',
  `payer` varchar(255) COMMENT '付款人id',
  `payer_phone` varchar(255) COMMENT '付款人手机号',
  `sn` varchar(255) COMMENT '流水号',
  `trade_sn` varchar(255) COMMENT '交易编号 关联Trade' ,
  `payment_time` datetime(6) COMMENT '转账时间',
  `state` varchar(255) COMMENT '状态  success 转账成功  fail 转账失败'
  PRIMARY KEY (`id`)
);

-- 2022.07.03
ALTER TABLE `lilishop`.`li_partner`
ADD COLUMN `sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '充值订单号' AFTER `remark`;

-- 2022.07.04
ALTER TABLE `lilishop`.`li_recharge_flow`
CHANGE COLUMN `serial_number` `sn` varchar(32) DEFAULT NULL COMMENT '编号' AFTER `member_id`,
CHANGE COLUMN `recharge_amount` `money` decimal(20, 5)  DEFAULT NULL COMMENT '现金额' AFTER `sn`,
CHANGE COLUMN `yi_bei` `huan_bei` decimal(20, 5)  COMMENT '焕呗额' AFTER `money`;

-- 2022.07.05
ALTER TABLE `lilishop`.`li_recharge_flow`
ADD COLUMN `fee` decimal(20, 5) COMMENT '费率' AFTER `recharge_type`,
ADD COLUMN `can_money` decimal(20, 5) COMMENT '可提现现金额' AFTER `fee`,
ADD COLUMN `can_huan_bei` decimal(20, 5) COMMENT '可提现焕呗额' AFTER `can_money`,
ADD COLUMN `realization` varchar(255) COMMENT '可提现金额是否提完,-1已提完  0未提现完' AFTER `can_huan_bei`;


ALTER TABLE `lilishop`.`li_wallet_detail`
MODIFY COLUMN `payer_before_money` double(20, 5) DEFAULT NULL AFTER `payer_owner`,
MODIFY COLUMN `payer_after_money` double(20, 5) DEFAULT NULL AFTER `payer_before_money`,
MODIFY COLUMN `payee_before_money` double(20, 5) DEFAULT NULL AFTER `payee_owner`,
MODIFY COLUMN `payee_after_money` double(20, 5) DEFAULT NULL AFTER `payee_before_money`,
MODIFY COLUMN `money` double(20, 5) DEFAULT NULL AFTER `payee_after_money`;

ALTER TABLE `lilishop`.`li_wallet_log`
MODIFY COLUMN `money` double(20, 5) DEFAULT NULL COMMENT '金额' AFTER `member_name`,
MODIFY COLUMN `before_money` double(20, 5) DEFAULT NULL COMMENT '变动前金额' AFTER `money`,
MODIFY COLUMN `after_money` double(20, 5) DEFAULT NULL COMMENT '变动后金额' AFTER `before_money`;

-- 2022.07.06
ALTER TABLE `lilishop`.`li_member_withdraw_apply`
ADD COLUMN `before_money` double(20, 5) DEFAULT NULL COMMENT '提现前余额' AFTER `fee`;

ALTER TABLE `lilishop`.`li_recharge_flow`
MODIFY COLUMN `recharge_type` varchar(10) DEFAULT NULL COMMENT '1提现2充值年费会员3充值会员4普通充值 5 充值合伙人' AFTER `add_time`;


-- 2022.07.07
ALTER TABLE `lilishop`.`li_member_withdraw_apply`
ADD COLUMN `apply_price` double(20, 5) DEFAULT NULL COMMENT '申请提现焕呗金额' AFTER `update_time`,
MODIFY COLUMN `apply_money` double(20, 5) DEFAULT NULL COMMENT '申请提现现金额' AFTER `apply_price`,
MODIFY COLUMN `real_money` double(20, 5) DEFAULT NULL COMMENT '实际提现现金额' AFTER `apply_money`;

-- 2022.07.12
ALTER TABLE `lilishop`.`li_member`
MODIFY COLUMN `no_secret` bit(1) NOT NULL DEFAULT 1 COMMENT '是否免密支付（FALSE否 使用密码，TRUE 是 免密支付）' AFTER `location`;

-- 2022.07.18
ALTER TABLE `lilishop`.`li_goods`
ADD COLUMN `origin` varchar(255) COMMENT '来源（pc   app）';

-- 2022.07.19
ALTER TABLE `lilishop`.`li_partner`
ADD COLUMN `application_time` datetime(0) COMMENT '申请时间' AFTER `prefix`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `application_time`;

-- 2022.07.20
ALTER TABLE `lilishop`.`li_member_wallet`
MODIFY COLUMN `member_wallet` double(20, 5) DEFAULT NULL COMMENT '会员预存款' AFTER `member_name`;

-- 2022.07.23
ALTER TABLE `huanbeitest`.`li_member` 
ADD COLUMN `disable_time` datetime(0) DEFAULT NULL COMMENT '禁用时间' AFTER `no_secret`,
ADD COLUMN `home_town` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '户籍地' AFTER `disable_time`;

ALTER TABLE `li_goods` ADD COLUMN `sort2` bigint(20) DEFAULT 0 COMMENT '手动排序 数字越大越前' AFTER `origin`;

ALTER TABLE `sys_dict_item`
ADD COLUMN `sort` int(10) DEFAULT NULL COMMENT '排序' AFTER `status`;

ALTER TABLE `li_article`
ADD COLUMN `release_status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发布新文章是否用提醒用户（true提醒/false不提醒）' AFTER `open_status`;


ALTER TABLE `huanbeitest`.`li_article_record_summary`
DROP COLUMN `view_status`,
ADD COLUMN `view_status` tinyint(1) DEFAULT 0 COMMENT '查看状态默认1是已阅读0是需要重新阅读' AFTER `article_type`,
ADD COLUMN `article_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文章类型' AFTER `view_status`;

ALTER TABLE `huanbeitest`.`li_article_record_summary`
MODIFY COLUMN `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键' FIRST;

ALTER TABLE `huanbeitest`.`li_receipt`
ADD COLUMN `receipt_type` bit(1) DEFAULT NULL COMMENT '发票类型0普票1专票' AFTER `taxpayer_id`,
ADD COLUMN `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号' AFTER `receipt_type`,
ADD COLUMN `receipt_title_type` bit(1) DEFAULT NULL COMMENT '抬头类型0个人1企业' AFTER `mobile`;

ALTER TABLE `huanbeitest`.`li_order`
ADD COLUMN `receiving_time` datetime(0) DEFAULT NULL COMMENT '收货时间' AFTER `frozend`;


-- 2022.07.28
ALTER TABLE `lilishop`.`li_member_withdraw_apply`
ADD COLUMN `finish_time` datetime(6) COMMENT '完成时间' AFTER `before_money`;

-- 2022.07.29
ALTER TABLE `lilishop`.`li_member`
ADD COLUMN `extension_flag` bit(1) COMMENT '是否可以被绑定' AFTER `home_town`;


-- 2022.08.23
ALTER TABLE `lilishop0805`.`li_order_item`
ADD COLUMN `surplus_price` double(10, 2) DEFAULT NULL COMMENT '剩余可提现焕呗金额' AFTER `merchandise_status`;


CREATE TABLE `li_wallet_frozen_detail` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `transaction_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '交易类型（ 0 余额充值 1消费  2佣金  3销售  4年费充值 5合伙人充值 6 提现  7 退款  8 退佣金  9 商家提现）',
  `sn` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '订单号',
  `payer` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '付款人id',
  `payer_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '付款人姓名',
  `payer_owner` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `payer_before_money` double(20,5) DEFAULT NULL,
  `payer_after_money` double(20,5) DEFAULT NULL,
  `payee` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '收款人',
  `payee_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '收款人姓名',
  `payee_owner` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `payee_before_money` double(20,5) DEFAULT NULL,
  `payee_after_money` double(20,5) DEFAULT NULL,
  `money` double(20,5) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='钱包日志关联表';

ALTER TABLE `lilishop0805`.`li_wallet_frozen_detail`
ADD COLUMN `commission_flag` varchar(1) DEFAULT 0 COMMENT '是否已分佣  0否1是' AFTER `remark`;

-- 2022-08-31
ALTER TABLE `lilishop0805`.`li_membership_card`
ADD COLUMN `description` varchar(2550) COMMENT '会员描述' AFTER `recharge_rule`;

ALTER TABLE `lilishop0805`.`sys_dict`
MODIFY COLUMN `description` varchar(2550) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述' AFTER `dict_code`;

-- 会员卡描述
INSERT INTO `li_article`(`id`, `create_by`, `create_time`, `delete_flag`, `update_by`, `update_time`, `category_id`, `content`, `sort`, `title`, `type`, `open_status`, `release_status`) VALUES (1564876388269465602, 'm13804518006', '2022-08-31 15:23:00', b'0', 'admin', '2022-08-31 16:59:12', '1421035246573596674', '<p>11</p>', 1, '会员卡描述', 'CARD_DESCRIPTION', b'0', '0');


ALTER TABLE `lilishop0805`.`li_member`
MODIFY COLUMN `no_secret` int(1) NOT NULL DEFAULT 0  COMMENT '是否免密支付（0否 使用密码，1 是 免密支付）' AFTER `location`;

CREATE TABLE `lilishop0805`.`li_region_detail`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(6) DEFAULT NULL COMMENT '更新时间',
  `member_id` varchar(255) COMMENT '用户id',
  `before_ad_code` varchar(255) COMMENT '修改前区域ad_code',
  `before_name` varchar(255) COMMENT '修改前区域',
  `after_ad_code` varchar(255) COMMENT '修改后区域ad_code',
  `after_name` varchar(255) COMMENT '修改后区域',
  PRIMARY KEY (`id`)
);

INSERT INTO `lilishop0805`.`sys_dict`(`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('6729df2d6c190cfb75b6c3481554b238', '自动分佣   天', 'auto_commission', '自动分佣   天', 0, 0, 'admin', '2022-09-06 17:18:31', 'admin', '2022-09-06 17:20:30', 0);
INSERT INTO `lilishop0805`.`sys_dict`(`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('5a5e68114490ae9c97767252ba18158b', '自动收货 天', 'auto_receive', '自动收货 天', 0, 1, 'admin', '2022-09-06 17:17:18', NULL, NULL, 0);

INSERT INTO `lilishop0805`.`sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`) VALUES ('580e96f247ed980307d23818218aa1cf', '6729df2d6c190cfb75b6c3481554b238', '自动分佣   天', '7', '自动分佣   天', NULL, 1, 'admin', '2022-09-06 17:20:44', NULL, NULL, 0);
INSERT INTO `lilishop0805`.`sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`) VALUES ('26fa7e3a9df9db7621f745ec560b8825', '5a5e68114490ae9c97767252ba18158b', '自动收货 天', '7', '自动收货 天', 1, 1, 'admin', '2022-09-06 17:17:36', 'admin', '2022-09-06 17:17:48', 0);

-- 2022-09-14

ALTER TABLE `lilishop0805`.`li_store`
ADD COLUMN `store_audit_time` datetime(6) DEFAULT NULL COMMENT '店铺审核时间' AFTER `is_have_contract`;

-- 2022-09-23
CREATE TABLE `li_token`  (
  `id` varchar(32) NOT NULL,
  `create_time` datetime(0),
  `access_token` varchar(2550),
  PRIMARY KEY (`id`)
) COMMENT = 'token表';

-- ===================================已执行==================================
-- 2022-10-21
ALTER TABLE `lilishop0805`.`finance_admin_all_money`
ADD COLUMN `tg_fw_ye_hb_z` double(20, 5) COMMENT '推广服务余额焕呗' AFTER `cz_xj_z`,
ADD COLUMN `tg_hy_ye_hb_z` double(20, 5) COMMENT '推广会员余额焕呗' AFTER `tg_fw_ye_hb_z`;


ALTER TABLE `lilishop0805`.`li_transfer`
MODIFY COLUMN `payee_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收款人姓名' AFTER `payee_phone`;
ALTER TABLE `lilishop0805`.`li_wallet_detail`
MODIFY COLUMN `payer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '付款人姓名' AFTER `payer`,
MODIFY COLUMN `payee_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收款人姓名' AFTER `payee`;

ALTER TABLE `lilishop0805`.`finance_admin_all_member`
ADD COLUMN `tghy_ye` double(20, 5) AFTER `xs_tx_hb`,
ADD COLUMN `tgfw_ye` double(20, 5) AFTER `tghy_ye`,
ADD COLUMN `tg_hy_zc_zc` double(20, 5) AFTER `tgfw_ye`,
ADD COLUMN `tg_fw_zc_zc` double(20, 5) AFTER `tg_hy_zc_zc`,
ADD COLUMN `tg_zc_tsfy` double(20, 5) AFTER `tg_fw_zc_zc`,
ADD COLUMN `tg_zc_syfy` double(20, 5) AFTER `tg_zc_tsfy`;

ALTER TABLE `lilishop0805`.`finance_admin_qy`
ADD COLUMN `tghy_zc_xf_money` double(20, 5) AFTER `res_4`,
ADD COLUMN `tghy_zc_fwf_money` double(20, 5) AFTER `tghy_zc_xf_money`,
ADD COLUMN `tgfw_zc_xf_money` double(20, 5) AFTER `tghy_zc_fwf_money`,
ADD COLUMN `tgfw_zc_fwf_money` double(20, 5) AFTER `tgfw_zc_xf_money`,
ADD COLUMN `tghy_ye_money` double(20, 5) AFTER `tgfw_zc_fwf_money`,
ADD COLUMN `tgfw_ye_money` double(20, 5) AFTER `tghy_ye_money`;

ALTER TABLE `lilishop0805`.`finance_admin_c`
MODIFY COLUMN `tj_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '推荐用户ID' AFTER `tj_mobile`;

ALTER TABLE `lilishop0805`.`finance_admin_all_money`
ADD COLUMN `wx_tx_xj` double(20, 5) COMMENT '微信订单提现现金' AFTER `tg_hy_ye_hb_z`,
ADD COLUMN `wx_tx_hb` double(20, 5) COMMENT '微信订单提现焕呗' AFTER `wx_tx_xj`,
ADD COLUMN `wx_cz_xj` double(20, 5) COMMENT '微信订单充值现金' AFTER `wx_tx_hb`,
ADD COLUMN `wx_cz_hb` double(20, 5) COMMENT '微信订单充值焕呗' AFTER `wx_cz_xj`;


-- 2022-11-04
CREATE TABLE `lilishop0805`.`sys_dict_picture`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编码',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `content` varchar(2550) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
);

ALTER TABLE `lilishop0805`.`li_partner`
ADD COLUMN `recommend_phone` varchar(255) COMMENT '推荐人手机号' AFTER `audit_time`;
l
INSERT INTO `sys_dict_picture`(`id`, `create_by`, `create_time`, `update_by`, `update_time`, `code`, `name`, `content`) VALUES ('123', 'admin', '2022-11-04 17:55:31', 'develop', '2022-11-05 13:44:19', 'my_card', '我的-会员卡', 'https://rsyl.oss-cn-beijing.aliyuncs.com/21b10f8e400a40dca926b494a86e0da7.png');
INSERT INTO `sys_dict_picture`(`id`, `create_by`, `create_time`, `update_by`, `update_time`, `code`, `name`, `content`) VALUES ('124', 'admin', '2022-11-04 18:02:35', 'develop', '2022-11-05 14:57:27', 'guide', '引导页', 'https://rsyl.oss-cn-beijing.aliyuncs.com/9c6cc3b7518b40c28488f442f50da830.png,https://rsyl.oss-cn-beijing.aliyuncs.com/10c92dc5f89949e2b272cfd5c7334ddd.png,https://rsyl.oss-cn-beijing.aliyuncs.com/a8c5a11e64e743eba280a9029ef7f1ac.png');
INSERT INTO `sys_dict_picture`(`id`, `create_by`, `create_time`, `update_by`, `update_time`, `code`, `name`, `content`) VALUES ('125', 'admin', '2022-11-04 18:02:35', 'develop', '2022-11-05 13:40:18', 'vip_card1', '首页-会员-银卡', 'https://rsyl.oss-cn-beijing.aliyuncs.com/6d6553f6f0b04d31a55fb5e6f80892a9.png');
INSERT INTO `sys_dict_picture`(`id`, `create_by`, `create_time`, `update_by`, `update_time`, `code`, `name`, `content`) VALUES ('126', 'admin', '2022-11-04 18:02:35', 'develop', '2022-11-05 13:40:25', 'vip_card2', '首页-会员-金卡', 'https://rsyl.oss-cn-beijing.aliyuncs.com/437e1453d2d64dd88ff980df1bc6f033.png');
