ALTER TABLE li_store ADD `closing_return_amount` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '闭店退回金额';
ALTER TABLE li_store ADD `total_recovered` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回收焕贝总额';
ALTER TABLE li_store ADD `total_recovered_promote` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回收推广金额';
ALTER TABLE li_store ADD `total_recovered_sale` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回收销售金额';


 2022-08-01
ALTER TABLE `huanbeitest`.`finance_admin_c`
    MODIFY COLUMN `tj_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '推荐用户ID' AFTER `tj_mobile`;


ALTER TABLE `lilishop`.`finance_admin_all_wallet`
    MODIFY COLUMN `ptTgFwHb` double(20, 5) NULL DEFAULT NULL AFTER `create_time`,
    MODIFY COLUMN `ptTgHyHb` double(20, 5) NULL DEFAULT NULL AFTER `ptTgFwHb`,
    MODIFY COLUMN `ptXsHb` double(20, 5) NULL DEFAULT NULL AFTER `ptTgHyHb`,
    MODIFY COLUMN `ptCzHb` double(20, 5) NULL DEFAULT NULL AFTER `ptXsHb`,
    MODIFY COLUMN `ptTgHb` double(20, 5) NULL DEFAULT NULL AFTER `ptCzHb`,
    MODIFY COLUMN `ptTgHbZ` double(20, 5) NULL DEFAULT NULL AFTER `ptTgHb`,
    MODIFY COLUMN `ptHbYeZe` double(20, 5) NULL DEFAULT NULL AFTER `ptTgHbZ`,
    MODIFY COLUMN `tgFwXj` double(20, 5) NULL DEFAULT NULL AFTER `ptHbYeZe`,
    MODIFY COLUMN `tgHyXj` double(20, 5) NULL DEFAULT NULL AFTER `tgFwXj`,
    MODIFY COLUMN `tgXj` double(20, 5) NULL DEFAULT NULL AFTER `tgHyXj`,
    ADD COLUMN `ptHsHb` double(20, 5) NULL AFTER `tgXj`;

CREATE TABLE `finance_admin_spreadlncome` (
    `id` varchar(36) COLLATE utf8_bin NOT NULL COMMENT 'ID',
    `member_id` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '用户ID',
    `nick_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '用户名',
    `region_id` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '管理区域',
    `region` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '管理区域',
    `location` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '区域ID',
    `location_name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '区域名称',
    `mobile` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '用户电话',
    `level` varchar(5) COLLATE utf8_bin DEFAULT NULL COMMENT '等级',
    `level_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '等级名称',
    `face` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '头像',
    `cz_xj` double(20,5) DEFAULT NULL COMMENT '充值收入现金额',
    `cz_hb` double(20,5) DEFAULT NULL COMMENT '充值收入焕贝额',
    `cz_zk` double(20,5) DEFAULT NULL COMMENT '充值折扣',
    `xs_xj` double(20,5) DEFAULT NULL COMMENT '销售分佣收入现金额',
    `xs_hb` double(20,5) DEFAULT NULL COMMENT '销售分佣收入焕贝额',
    `xs_zk` double(20,5) DEFAULT NULL COMMENT '销售提现折扣',
    `allocate_all_money` double(20,5) DEFAULT NULL COMMENT '总收益差值',
    `allocate_member_discount` double(20,5) DEFAULT NULL COMMENT '区域所得分配折扣',
    `allocate_member_money` double(20,5) DEFAULT NULL COMMENT '区域所得差价收益额',
    `allocate_admin_discount` double(20,5) DEFAULT NULL COMMENT '平台所得分配折扣',
    `allocate_admin_money` double(20,5) DEFAULT NULL COMMENT '平台所得差价收益额',
    `begin_time` date DEFAULT NULL COMMENT '开始时间',
    `end_time` date DEFAULT NULL COMMENT '结束时间',
    `delete_flag` bit(1) DEFAULT NULL COMMENT '删除标志 true/false 删除/未删除',
    `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建者',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `role_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '会员类型',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='区域平台价差收益汇总';

CREATE DEFINER=`root`@`%` FUNCTION `RoundDown`(`InputValueNum` DOUBLE ,Num int) RETURNS decimal(20,2)
BEGIN
	if ISNULL(InputValueNum) then set InputValueNum := 0.00; end if;

	set InputValueNum :=  CEILING(InputValueNum*POWER(10,num+3) )/POWER(10,num+3);
	set InputValueNum := InputValueNum*POWER(10,num);
	set InputValueNum := CEILING(InputValueNum*POWER(10,2))/POWER(10,2);
	set InputValueNum := FLOOR(InputValueNum)/POWER(10,num);
RETURN(InputValueNum);

END

-- 8/5
ALTER TABLE finance_admin_c
    MODIFY COLUMN `tj_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '推荐用户ID' AFTER `tj_mobile`;


-- 10.12 区域报表
ALTER TABLE ``finance_admin_qy`
    CHANGE COLUMN `tg_zc_xf_money` `tghy_zc_xf_money` double(20, 5) NULL DEFAULT NULL COMMENT '推广会员支出消费' AFTER `tg_sr_hyfy_money`,
    CHANGE COLUMN `tg_zc_fwf_money` `tghy_zc_fwf_money` double(20, 5) NULL DEFAULT NULL COMMENT '推广会员支出服务费' AFTER `tghy_zc_xf_money`,
    ADD COLUMN `tgfw_zc_xf_money` double(20, 5) NULL DEFAULT NULL COMMENT '推广服务支出消费' AFTER `tghy_zc_fwf_money`,
    ADD COLUMN `tgfw_zc_fwf_money` double(20, 5) NULL DEFAULT NULL COMMENT '推广服务支出服务费' AFTER `tgfw_zc_xf_money`;
