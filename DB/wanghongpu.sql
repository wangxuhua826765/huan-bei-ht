ALTER TABLE `lilishop`.`li_member`
    ADD COLUMN `id_card_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证号' AFTER `link_mobile`;

ALTER TABLE `lilishop`.`li_member`
    ADD COLUMN `payment_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '会员支付密码' AFTER `link_mobile`;