package cn.lili;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 基础API
 *
 * @author Chopper
 * @since 2020/11/17 3:38 下午
 */
@EnableCaching
@SpringBootApplication
public class CommonApiApplication {

	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
	}
	public static void main(String[] args) {
		SpringApplication.run(CommonApiApplication.class, args);
	}

}
