package cn.lili.controller.common;

import cn.lili.cache.limit.annotation.LimitPoint;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.context.UserContext;
import cn.lili.common.utils.CommonUtil;
import cn.lili.common.utils.StringUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.sms.SmsUtil;
import cn.lili.modules.system.entity.dos.Setting;
import cn.lili.modules.system.entity.dto.SmsSetting;
import cn.lili.modules.system.entity.enums.SettingEnum;
import cn.lili.modules.system.service.SettingService;
import cn.lili.modules.verification.entity.enums.VerificationEnums;
import cn.lili.modules.verification.service.VerificationService;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 短信验证码接口
 *
 * @author Chopper
 * @since 2020/11/26 15:41
 */
@RestController
@Api(tags = "短信验证码接口")
@RequestMapping("/common/sms")
public class SmsController {

	@Autowired
	private SmsUtil smsUtil;
	@Autowired
	private VerificationService verificationService;

	@Autowired
	private SettingService settingService;

	// @LimitPoint(name = "sms_send", key = "sms")
	// @ApiImplicitParams({
	// @ApiImplicitParam(paramType = "path", dataType = "String", name = "mobile",
	// value = "手机号"),
	// @ApiImplicitParam(paramType = "header", dataType = "String", name = "uuid",
	// value = "uuid"),
	// })
	// @RequestMapping("/{verificationEnums}/{mobile}")
	// @ApiOperation(value = "发送短信验证码,一分钟同一个ip请求1次")
	// public ResultMessage getSmsCode(
	// @RequestHeader String uuid,
	// @PathVariable String mobile,
	// @PathVariable VerificationEnums verificationEnums) {
	// smsUtil.sendSmsCode(mobile, verificationEnums, uuid);
	// return ResultUtil.success(ResultCode.VERIFICATION_SEND_SUCCESS);
	// }

	// @LimitPoint(name = "sms_send2", key = "sms")
	@RequestMapping("/smsSend")
	@ApiOperation(value = "发送短信验证码,一分钟同一个ip请求1次")
	public ResultMessage getSmsCode2(@RequestParam("verificationEnums") VerificationEnums verificationEnums,
			@RequestParam("mobile") String mobile) {
		String memberId = UserContext.getCurrentUser().getId();
		if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(memberId) || verificationEnums == null) {
			return ResultUtil.error(ResultCode.PARAMS_ERROR);
		}
		// 准备发送短信参数
		Map<String, String> params = new HashMap<>(2);
		// 验证码
		String code = CommonUtil.getRandomNum();
		params.put("code", code);
		smsUtil.sendSmsCode(memberId, verificationEnums, mobile, params, "SMS_236870471");
		return ResultUtil.success(ResultCode.VERIFICATION_SEND_SUCCESS);
	}

}
